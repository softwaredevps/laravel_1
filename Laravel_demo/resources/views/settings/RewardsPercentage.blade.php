@extends('layout.member_master')
@section('title', 'Rewards Percentage')

@section('content')
<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center mb-3">
            <ul class="nav nav-tabs page-header-tab">

                <li class="nav-item"><a class="{{ (Request::segment(1) === 'MemberShipPmtType' && Request::segment(1) != 'ActivityPoints' && Request::segment(1) != 'RewardsPercentage'  && Request::segment(1) != 'MeetingType'  && Request::segment(1) != 'MeetingStructure') ? 'nav-link active' : 'nav-link'  }}" id="Employee-tab" href="/MemberShipPmtType">Membership Term/Investment </a></li>
                <li class="nav-item"><a class="{{ (Request::segment(1) === 'ActivityPoints' ) ? 'nav-link active' : 'nav-link'  }}" id="Employee-tab" href="/ActivityPoints">Activity Points</a></li>
                <li class="nav-item"><a class="{{ (Request::segment(1) === 'RewardsPercentage') ? 'nav-link active' : 'nav-link'  }}" id="Employee-tab" href="/RewardsPercentage"> Rewards Percentage </a></li>
                <li class="nav-item"><a class="{{ (Request::segment(1) === 'MeetingType') ? 'nav-link active' : 'nav-link'  }}" id="Employee-tab" href="/MeetingType"> Meeting Type </a></li>

                <li class="nav-item"><a class="{{ (Request::segment(1) === 'MeetingStructure') ? 'nav-link active' : 'nav-link'  }}" id="Employee-tab" href="/MeetingStructure"> Meeting Structure </a></li>

            
            </ul>
        </div>
    </div>
</div>
<div class="section-body">
    <div class="container-fluid">
        <div class="tab-content">
            <div class="tab-pane fade show active" id="Employee-list" role="tabpanel">                        
                <div class="card">
                    <div class="card-header">
                        <!--h3 class="card-title">Affiliate List</h3-->
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                                <table class="table table-hover table-striped table-vcenter text-nowrap mb-0 trtd" id="">
                                    <thead>
                                        <tr>
                                            <th class="text-left"><strong>#</strong></th>
                                            <th class="text-left"><strong>Rewards Percentage</strong></th>
                                            <th class="text-left"><strong>Rewards Details</strong></th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                      
                                    <?php $i=1; ?>
                                    @foreach($res as $val)  
                                    
                                        <tr class="row-{{$val->id}}">
                                            <td class="text-left"><span><?php echo $i;?></span></td>
                                            <td class="text-left">{{$val->RewardPercentage}}</td>
                                            <td class="text-left">{{$val->RewardDetail}}</td>
                                            
                                        </tr>  
                                    <?php $i++;?>
                                    @endforeach       
                                </tbody>
                                </table>
                            </form>                            
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </div>            
</div>

@stop

@section('page-styles')
<link rel="stylesheet" href="{{ asset('assets/plugins/fullcalendar/fullcalendar.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/sweetalert/sweetalert.css') }}">
<link href='http://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css' rel='stylesheet' type='text/css'>

@stop

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

@section('page-script')


<script src="{{ asset('assets/bundles/fullcalendar.bundle.js') }}"></script>


<script src="{{ asset('assets/js/page/calendar.js') }}"></script>
<script src="{{ asset('assets/plugins/sweetalert/sweetalert.min.js') }}"></script>

<script src="{{ asset('assets/js/core.js') }}"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.8/js/jquery.dataTables.min.js"></script>

@stop
