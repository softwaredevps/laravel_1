@extends('layout.member_master')
@section('title', 'Settings')
@section('content')
<div class="col-lg-12 col-md-12">
   <div class="col-lg-12 col-md-12">
      <div class="section-body">
         <div class="container-fluid">
            <div class="d-flex justify-content-between align-items-center">
               <ul class="nav nav-tabs page-header-tab">
                  <li class="nav-item"><a class="nav-link active" id="li-ref-tab" data-toggle="tab" href="#ref-tab">Dashboard</a></li>
                  <li class="nav-item"><a class="nav-link" id="li-send-a-ref" data-toggle="tab" href="#send-a-ref">Send A Referral</a></li>
                  <li class="nav-item"><a class="nav-link" id="li-send-esc-ref" data-toggle="tab" href="#send-esc-ref">Send An Escrow Referral</a></li>
                  <li class="nav-item"><a class="nav-link" id="li-rec-ref" data-toggle="tab" href="#rec-ref">Received Referral</a></li>
                  <li class="nav-item"><a class="nav-link" id="li-sent-ref" data-toggle="tab" href="#sent-ref">Sent Referral</a></li>
               </ul>
            </div>
         </div>
      </div>
      <div class="section-body mt-3">
         <div class="container-fluid">
            <div class="tab-content mt-3">
               <div class="tab-pane fade show active" id="ref-tab" role="tabpanel">
                  <div class="card">
                     <div class="card-header">
                        <div class="card-options">
                           <form>
                              <div class="input-group">
                                 <select class="form-control">
                                    <option value="past_12_months">Past 12 Months</option>
                                    <option value="this_year">This Year</option>
                                    <option value="last_year">Last Year</option>
                                 </select>
                              </div>
                           </form>
                        </div>
                     </div>
                     <div class="card-body">
                        <div id="apex-basic-column" style="min-height: 365px;">
                           
                        </div>
                     </div>
                  </div>
               </div>
               <div class="tab-pane fade" id="send-a-ref" role="tabpanel">
                  <div class="row clearfix">
                     <div class="col-12">
                        <div class="card">
                           <div class="card-body">
                              <div class="row">
                                 <div class="col-lg-4 col-md-4 col-sm-6">
                                    <label>Search By Name or Company</label>
                                    <div class="input-group">
                                       <input type="text" class="form-control" placeholder="Search...">
                                    </div>
                                 </div>
                                 <div class="col-lg-3 col-md-4 col-sm-6">
                                    <label>Search By Affiliate</label>
                                    <div class="multiselect_div">
                                       <select id="multiselect3-all" name="multiselect3[]" class="multiselect multiselect-custom" multiple="multiple" style="display: none;">
                                          <option value="multiselect-all">STATUS</option>
                                          <option value="All Statuses">All Statuses</option>
                                          <option value="New">New</option>
                                          <option value="Contacted">Contacted</option>
                                          <option value="Interviewed">Interviewed</option>
                                          <option value="Hired">Hired</option>
                                       </select>
                                       <div class="btn-group">
                                          <button type="button" class="multiselect dropdown-toggle btn btn-default" data-toggle="dropdown" title="None selected"><span class="multiselect-selected-text">None selected</span> <b class="caret"></b></button>
                                          <ul class="multiselect-container dropdown-menu">
                                             <li class="multiselect-item multiselect-all"><a tabindex="0" class="multiselect-all"><label class="checkbox"><input type="checkbox" value="multiselect-all">  Select all</label></a></li>
                                             <li><a tabindex="0"><label class="checkbox"><input type="checkbox" value="All Statuses"> All Statuses</label></a></li>
                                             <li><a tabindex="0"><label class="checkbox"><input type="checkbox" value="New"> New</label></a></li>
                                             <li><a tabindex="0"><label class="checkbox"><input type="checkbox" value="Contacted"> Contacted</label></a></li>
                                             <li><a tabindex="0"><label class="checkbox"><input type="checkbox" value="Interviewed"> Interviewed</label></a></li>
                                             <li><a tabindex="0"><label class="checkbox"><input type="checkbox" value="Hired"> Hired</label></a></li>
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-lg-3 col-md-4 col-sm-6">
                                    <label>Search By Category</label>
                                    <div class="form-group">
                                       <select class="custom-select">
                                          <option selected="">Newest first</option>
                                          <option value="1">Oldest first</option>
                                          <option value="2">Low salary first</option>
                                          <option value="3">High salary first</option>
                                          <option value="3">Sort by name</option>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="col-lg-2 col-md-4 col-sm-6">
                                    <label>&nbsp;</label>
                                    <a href="javascript:void(0);" class="btn btn-sm btn-primary btn-block" title="">Filter</a>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="table-responsive">
                           <table class="table table-hover table-vcenter table_custom text-nowrap spacing5 border-style mb-0">
                              <tbody>
                                 <tr>
                                    <td class="w60">
                                       <div class="avatar avatar-pink" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name">
                                          <span>GH</span>
                                       </div>
                                    </td>
                                    <td><span>Alice Agyemang-Badu</span></td>
                                    <td>
                                       <div class="font-15">New York Life</div>
                                       <span class="text-muted">Business Advisor</span>
                                    </td>
                                    <td><span>123 6th St. Melbourne, FL 32904</span></td>
                                    <td>
                                       
                                       <button type="button" class="btn btn-success btn-block"><i class="fa fa-send"></i>&nbsp;&nbsp;&nbsp;Send Referral</button>


                                    </td>
                                 </tr>
                                 <tr>
                                    <td class="">
                                       <img class="" src="../assets/images/xs/avatar1.jpg" alt="">
                                    </td>
                                    <td><a href="javascript:void(0);" data-toggle="modal" data-target="#exampleModal"><span >Celestina Ando</span></a></td>
                                    <td>
                                       <div class="font-15">Celestina Ando Photography LLC.</div>
                                       <span class="text-muted">Business Consultant</span>
                                    </td>
                                    <td><span>44 Shirley Ave. IL 60185</span></td>
                                    <td>
                                        <button type="button" class="btn btn-success btn-block"><i class="fa fa-send"></i>&nbsp;&nbsp;&nbsp;Send Referral</button>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td class="">
                                       <img class="" src="../assets/images/xs/avatar2.jpg" alt="">
                                    </td>
                                    <td><span>Marc Bautis</span></td>
                                    <td>
                                       <div class="font-15">Bautis Financial.</div>
                                       <span class="text-muted">Financial Advisor</span>
                                    </td>
                                    <td><span>44 Shirley Ave. IL 60185</span></td>
                                    <td>
                                        <button type="button" class="btn btn-success btn-block"><i class="fa fa-send"></i>&nbsp;&nbsp;&nbsp;Send Referral</button>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td class="">
                                       <img class="" src="../assets/images/xs/avatar3.jpg" alt="">
                                    </td>
                                    <td><span>Joseph DeLuca</span></td>
                                    <td>
                                       <div class="font-15">Evident Title Agency, Inc.</div>
                                       <span class="text-muted">Business Advisor</span>
                                    </td>
                                    <td><span>514 S. Magnolia St. Orlando</span></td>
                                    <td>
                                        <button type="button" class="btn btn-success btn-block"><i class="fa fa-send"></i>&nbsp;&nbsp;&nbsp;Send Referral</button>
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                        <ul class="pagination mt-2">
                           <li class="page-item"><a class="page-link" href="javascript:void(0);">Previous</a></li>
                           <li class="page-item active"><a class="page-link" href="javascript:void(0);">1</a></li>
                           <li class="page-item"><a class="page-link" href="javascript:void(0);">2</a></li>
                           <li class="page-item"><a class="page-link" href="javascript:void(0);">3</a></li>
                           <li class="page-item"><a class="page-link" href="javascript:void(0);">Next</a></li>
                        </ul>
                     </div>
                  </div>
               </div>

              <!-- Send An Escrow Referral start here -->

               <div class="tab-pane fade" id="send-esc-ref" role="tabpanel">
                     <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">123 6th St. Melbourne, FL 32904</h3>
                            </div>
                            <div class="card-body">
                                <form method="post" id="">
                                    <div class="row clearfix">
                                       <div class="col-lg-12 col-lg-12">
                                          <label>Referral For<span class="redstar">*<span> <span <i class="fa fa-question-circle" style="font-size: 22px;color: black;position: absolute;margin: 0 0 0 12px;"></i></span></label>

                                          <div class="form-group">
                                             <select class="custom-select">
                                                <option value="0">Select Chapter</option>
                                                <option value="1">Business Consultant</option>
                                                <option value="2">Financial Advisor</option>
                                                <option value="3">Business Advisor</option>
                                             </select>
                                          </div>

                                          <p class="text-danger">Escrow Referral are not given out until the guest joins the chapter.</p>
                                       </div>
                                       <div class="col-lg-6 col-md-6">
                                          <label>Date<span class="redstar">*<span></label>
                                          <div class="form-group">
                                             <input name="" data-provide="datepicker" data-date-autoclose="true" class="form-control" placeholder="Select Appointment">
                                          </div>
                                       </div>
                                       <div class="col-lg-6 col-md-6">
                                          <label>Referral Type</label>
                                          <div class="form-group">
                                             <select class="custom-select">
                                                <option value="0">Referral Type</option>
                                                <option value="1">Inside</option>
                                                <option value="2">OutSide</option>
                                             </select>
                                          </div>
                                       </div>
                                       <div class="col-lg-6 col-md-6">
                                          <label>Person To Contact</label>
                                          <div class="form-group">
                                             <input type="text" class="form-control" placeholder="Person To Contact">
                                          </div>
                                       </div>
                                       <div class="col-lg-6 col-md-6">
                                          <label>Company Name</label>
                                          <div class="form-group">
                                             <input type="text" class="form-control" placeholder="Company Name">
                                          </div>
                                       </div>
                                      <div class="col-lg-6 col-md-6">
                                          <label>Phone Number</label>
                                          <div class="form-group">
                                             <input type="text" class="form-control" placeholder="Phone Number">
                                          </div>
                                       </div>
                                       <div class="col-lg-6 col-md-6">
                                          <label>Email Address</label>
                                          <div class="form-group">
                                             <input type="text" class="form-control" placeholder="Email Address">
                                          </div>
                                       </div>
                                       <div class="col-lg-12 col-md-12">
                                          <label>Referral Details</label>
                                          <div class="form-group">
                                             <textarea rows="8" cols="100" type="text" class="form-control" placeholder="Details" name="terminationRemarks"></textarea>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1">
                                        <span class="custom-control-label">Specify the Referral value</span>
                                    </label>
                                     
                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      
                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1">
                                        <span class="custom-control-label">This is a private Referral</span>
                                        <span style="position: absolute; margin: 0 0 0 12px;"><i class="fa fa-question-circle" style="font-size: 22px;"></i></span>
                                    </label>

                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      
                                  </div>
                              </div>


                                    <button type="button" class="btn btn-round btn-primary">Submit Referral </button>
                                    <div class="col-lg-10 col-md-10 star-indigate"><span class="redstar">*<span> Indicates the required fields</span></span></div>
                                </form>
                           </div>
                     </div>
               </div>
               <!-- Send An Escrow Referral end here -->


               <!-- Received Referral start here -->

               <div class="tab-pane fade" id="rec-ref" role="tabpanel">
                  <div class="row clearfix">
                     <div class="col-12">
                        <div class="card">
                           <div class="card-body">
                              <div class="row">
                                 <div class="col-lg-4 col-md-4 col-sm-6">
                                    <label>Start Date</label>
                                    <div class="input-group">
                                       <input type="text" data-provide="datepicker" data-date-autoclose="true" class="form-control" placeholder="Start date *">
                                    </div>
                                 </div>
                                 <div class="col-lg-4 col-md-4 col-sm-6">
                                    <label>End Date</label>
                                    <div class="input-group">
                                       <input type="text" data-provide="datepicker" data-date-autoclose="true" class="form-control" placeholder="End date *">
                                    </div>
                                 </div>
                                 <div class="col-lg-3 col-md-4 col-sm-6">
                                    <label>Status</label>
                                    <div class="form-group">
                                       <select class="custom-select">
                                          <option selected="">Select..</option>
                                          <option value="1">Bogus/Invalid</option>
                                          <option value="2">Closed</option>
                                          <option value="3">Working</option>
                                          <option value="3">Sort by name</option>
                                       </select>
                                    </div>
                                 </div>
                                 
                              </div>
                        <div class="table-responsive">
                           <table class="table table-hover table-vcenter table_custom text-nowrap spacing5 border-style mb-0">

                              <thead>
                                  <tr>
                                      <th></th>
                                      <th></th>
                                      <th></th>
                                      <th></th>
                                      <th></th>
                                  </tr>
                              </thead>
                              <tbody>
                                 <tr>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral To :</span><br>
                                       </div>
                                       <div class="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name">
                                          <img class="" src="../assets/images/xs/avatar1.jpg" alt="">
                                       </div>
                                       <div class="font-12">
                                          <span>Alice Agyemang-Badu</span><br>
                                          <span>Business Advisor</span>
                                       </div>
                                    </td>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral From :</span><br>
                                       </div>
                                        <div class="avatar avatar-pink" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name">
                                          <span>GH</span>
                                       </div>
                                       <div class="font-12">
                                          <span>Celestina Ando</span><br>
                                          <span>Financial Advisor</span>
                                       </div>                                       
                                    </td>
                                    <td>
                                        <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Person To Contact : <span class="text-muted">Faith Bratlie</span></span><br>
                                          <span>Phone : <span class="text-muted">+1 470-385-2707</span></span><br>
                                          <span>Email : <span class="text-muted">fbratlie@myhst.com</span></span><br>
                                          <span>Company : <span class="text-muted">.....</span></span><br>
                                       </div>
                                    </td>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral Type : <span class="text-muted">Inter-Chapter</span></span><br>
                                          <span>Value : <span class="text-muted">....</span></span><br>
                                          <span>Status : <span class="text-muted">New</span></span>
                                       </div>
                                    </td>
                                    <td>
                                       <button type="button" class="btn btn-success btn-block">Edit</button>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral To :</span><br>
                                       </div>
                                       <div class="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name">
                                          <img class="" src="../assets/images/xs/avatar1.jpg" alt="">
                                       </div>
                                       <div class="font-12">
                                          <span>Alice Agyemang-Badu</span><br>
                                          <span>Business Advisor</span>
                                       </div>
                                    </td>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral From :</span><br>
                                       </div>
                                        <div class="avatar avatar-pink" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name">
                                          <span>GH</span>
                                       </div>
                                       <div class="font-12">
                                          <span>Celestina Ando</span><br>
                                          <span>Financial Advisor</span>
                                       </div>                                       
                                    </td>
                                    <td>
                                        <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Person To Contact : <span class="text-muted">Faith Bratlie</span></span><br>
                                          <span>Phone : <span class="text-muted">+1 470-385-2707</span></span><br>
                                          <span>Email : <span class="text-muted">fbratlie@myhst.com</span></span><br>
                                          <span>Company : <span class="text-muted">.....</span></span><br>
                                       </div>
                                    </td>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          
                                          <span>Referral Type : <span class="text-muted">Inter-Chapter</span></span><br>
                                          <span>Value : <span class="text-muted">....</span></span><br>
                                          <span>Status : <span class="text-muted">New</span></span>
                                          
                                       </div>
                                    </td>
                                    <td>
                                       <button type="button" class="btn btn-success btn-block">Edit</button>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral To :</span><br>
                                       </div>
                                       <div class="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name">
                                          <img class="" src="../assets/images/xs/avatar1.jpg" alt="">
                                       </div>
                                       <div class="font-12">
                                          <span>Alice Agyemang-Badu</span><br>
                                          <span>Business Advisor</span>
                                       </div>
                                    </td>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral From :</span><br>
                                       </div>
                                        <div class="avatar avatar-pink" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name">
                                          <span>GH</span>
                                       </div>
                                       <div class="font-12">
                                          <span>Celestina Ando</span><br>
                                          <span>Financial Advisor</span>
                                       </div>                                       
                                    </td>
                                    <td>
                                        <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Person To Contact : <span class="text-muted">Faith Bratlie</span></span><br>
                                          <span>Phone : <span class="text-muted">+1 470-385-2707</span></span><br>
                                          <span>Email : <span class="text-muted">fbratlie@myhst.com</span></span><br>
                                          <span>Company : <span class="text-muted">.....</span></span><br>
                                       </div>
                                    </td>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                         
                                          <span>Referral Type : <span class="text-muted">Inter-Chapter</span></span><br>
                                          <span>Value : <span class="text-muted">....</span></span><br>
                                          <span>Status : <span class="text-muted">New</span></span>
                                          
                                       </div>
                                    </td>
                                    <td>
                                       <button type="button" class="btn btn-success btn-block">Edit</button>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral To :</span><br>
                                       </div>
                                       <div class="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name">
                                          <img class="" src="../assets/images/xs/avatar1.jpg" alt="">
                                       </div>
                                       <div class="font-12">
                                          <span>Alice Agyemang-Badu</span><br>
                                          <span>Business Advisor</span>
                                       </div>

                                    </td>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral From :</span><br>
                                       </div>
                                        <div class="avatar avatar-pink" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name">
                                          <span>GH</span>
                                       </div>
                                       <div class="font-12">
                                          <span>Celestina Ando</span><br>
                                          <span>Financial Advisor</span>
                                       </div>                                       
                                    </td>
                                    <td>
                                        <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Person To Contact : <span class="text-muted">Faith Bratlie</span></span><br>
                                          <span>Phone : <span class="text-muted">+1 470-385-2707</span></span><br>
                                          <span>Email : <span class="text-muted">fbratlie@myhst.com</span></span><br>
                                          <span>Company : <span class="text-muted">.....</span></span><br>
                                       </div>
                                    </td>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral Type : <span class="text-muted">Inter-Chapter</span></span><br>
                                          <span>Value : <span class="text-muted">....</span></span><br>
                                          <span>Status : <span class="text-muted">New</span></span>
                                       </div>
                                    </td>
                                    
                                    <td>
                                       <button type="button" class="btn btn-success btn-block">Edit</button>
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                        <ul class="pagination mt-2">
                           <li class="page-item"><a class="page-link" href="javascript:void(0);">Previous</a></li>
                           <li class="page-item active"><a class="page-link" href="javascript:void(0);">1</a></li>
                           <li class="page-item"><a class="page-link" href="javascript:void(0);">2</a></li>
                           <li class="page-item"><a class="page-link" href="javascript:void(0);">3</a></li>
                           <li class="page-item"><a class="page-link" href="javascript:void(0);">Next</a></li>
                        </ul>

                        </div>
                        </div>

                     </div>
                  </div>
               </div>
               <!-- Received Referral end here -->

               <!-- Sent Referral start here -->

               <div class="tab-pane fade" id="sent-ref" role="tabpanel">
                  <div class="row clearfix">
                     <div class="col-12">
                        <div class="card">
                           <div class="card-body">
                              <div class="row">
                                 <div class="col-lg-4 col-md-4 col-sm-6">
                                    <label>Start Date</label>
                                    <div class="input-group">
                                       <input type="text" data-provide="datepicker" data-date-autoclose="true" class="form-control" placeholder="Start date *">
                                    </div>
                                 </div>
                                 <div class="col-lg-4 col-md-4 col-sm-6">
                                    <label>End Date</label>
                                    <div class="input-group">
                                       <input type="text" data-provide="datepicker" data-date-autoclose="true" class="form-control" placeholder="End date *">
                                    </div>
                                 </div>
                                 <div class="col-lg-3 col-md-4 col-sm-6">
                                    <label>Status</label>
                                    <div class="form-group">
                                       <select class="custom-select">
                                          <option selected="">Select..</option>
                                          <option value="1">Bogus/Invalid</option>
                                          <option value="2">Closed</option>
                                          <option value="3">Working</option>
                                          <option value="3">Sort by name</option>
                                       </select>
                                    </div>
                                 </div>
                                 
                              </div>
                        <div class="table-responsive">
                           <table class="table table-hover table-vcenter table_custom text-nowrap spacing5 border-style mb-0">

                              <thead>
                                  <tr>
                                      <th></th>
                                      <th></th>
                                      <th></th>
                                      <th></th>
                                      <th></th>
                                  </tr>
                              </thead>
                              <tbody>
                                 <tr>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral To :</span><br>
                                       </div>
                                       <div class="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name">
                                          <img class="" src="../assets/images/xs/avatar1.jpg" alt="">
                                       </div>
                                       <div class="font-12">
                                          <span>Alice Agyemang-Badu</span><br>
                                          <span>Business Advisor</span>
                                       </div>
                                    </td>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral From :</span><br>
                                       </div>
                                        <div class="avatar avatar-pink" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name">
                                          <span>GH</span>
                                       </div>
                                       <div class="font-12">
                                          <span>Celestina Ando</span><br>
                                          <span>Financial Advisor</span>
                                       </div>                                       
                                    </td>
                                    <td>
                                        <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Person To Contact : <span class="text-muted">Faith Bratlie</span></span><br>
                                          <span>Phone : <span class="text-muted">+1 470-385-2707</span></span><br>
                                          <span>Email : <span class="text-muted">fbratlie@myhst.com</span></span><br>
                                          <span>Company : <span class="text-muted">.....</span></span><br>
                                       </div>
                                    </td>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          
                                          <span>Referral Type : <span class="text-muted">Inter-Chapter</span></span><br>
                                          <span>Value : <span class="text-muted">....</span></span><br>
                                          <span>Status : <span class="text-muted">New</span></span>
                                       </div>
                                    </td>
                                    <td>
                                       <button type="button" class="btn btn-success btn-block">Edit</button>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral To :</span><br>
                                       </div>
                                       <div class="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name">
                                          <img class="" src="../assets/images/xs/avatar1.jpg" alt="">
                                       </div>
                                       <div class="font-12">
                                          <span>Alice Agyemang-Badu</span><br>
                                          <span>Business Advisor</span>
                                       </div>
                                    </td>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral From :</span><br>
                                       </div>
                                        <div class="avatar avatar-pink" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name">
                                          <span>GH</span>
                                       </div>
                                       <div class="font-12">
                                          <span>Celestina Ando</span><br>
                                          <span>Financial Advisor</span>
                                       </div>                                       
                                    </td>
                                    <td>
                                        <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Person To Contact : <span class="text-muted">Faith Bratlie</span></span><br>
                                          <span>Phone : <span class="text-muted">+1 470-385-2707</span></span><br>
                                          <span>Email : <span class="text-muted">fbratlie@myhst.com</span></span><br>
                                          <span>Company : <span class="text-muted">.....</span></span><br>
                                       </div>
                                    </td>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          
                                          <span>Referral Type : <span class="text-muted">Inter-Chapter</span></span><br>
                                          <span>Value : <span class="text-muted">....</span></span><br>
                                          <span>Status : <span class="text-muted">New</span></span>
                                          
                                       </div>
                                    </td>
                                    <td>
                                       <button type="button" class="btn btn-success btn-block">Edit</button>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral To :</span><br>
                                       </div>
                                       <div class="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name">
                                          <img class="" src="../assets/images/xs/avatar1.jpg" alt="">
                                       </div>
                                       <div class="font-12">
                                          <span>Alice Agyemang-Badu</span><br>
                                          <span>Business Advisor</span>
                                       </div>
                                    </td>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral From :</span><br>
                                       </div>
                                        <div class="avatar avatar-pink" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name">
                                          <span>GH</span>
                                       </div>
                                       <div class="font-12">
                                          <span>Celestina Ando</span><br>
                                          <span>Financial Advisor</span>
                                       </div>                                       
                                    </td>
                                    <td>
                                        <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Person To Contact : <span class="text-muted">Faith Bratlie</span></span><br>
                                          <span>Phone : <span class="text-muted">+1 470-385-2707</span></span><br>
                                          <span>Email : <span class="text-muted">fbratlie@myhst.com</span></span><br>
                                          <span>Company : <span class="text-muted">.....</span></span><br>
                                       </div>
                                    </td>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          
                                          <span>Referral Type : <span class="text-muted">Inter-Chapter</span></span><br>
                                          <span>Value : <span class="text-muted">....</span></span><br>
                                          <span>Status : <span class="text-muted">New</span></span>
                                          
                                       </div>
                                    </td>
                                    <td>
                                       <button type="button" class="btn btn-success btn-block">Edit</button>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral To :</span><br>
                                       </div>
                                       <div class="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name">
                                          <img class="" src="../assets/images/xs/avatar1.jpg" alt="">
                                       </div>
                                       <div class="font-12">
                                          <span>Alice Agyemang-Badu</span><br>
                                          <span>Business Advisor</span>
                                       </div>

                                    </td>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral From :</span><br>
                                       </div>
                                        <div class="avatar avatar-pink" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name">
                                          <span>GH</span>
                                       </div>
                                       <div class="font-12">
                                          <span>Celestina Ando</span><br>
                                          <span>Financial Advisor</span>
                                       </div>                                       
                                    </td>
                                    <td>
                                        <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Person To Contact : <span class="text-muted">Faith Bratlie</span></span><br>
                                          <span>Phone : <span class="text-muted">+1 470-385-2707</span></span><br>
                                          <span>Email : <span class="text-muted">fbratlie@myhst.com</span></span><br>
                                          <span>Company : <span class="text-muted">.....</span></span><br>
                                       </div>
                                    </td>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral Type : <span class="text-muted">Inter-Chapter</span></span><br>
                                          <span>Value : <span class="text-muted">....</span></span><br>
                                          <span>Status : <span class="text-muted">New</span></span>
                                       </div>
                                    </td>
                                    
                                    <td>
                                       <button type="button" class="btn btn-success btn-block">Edit</button>
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                        <ul class="pagination mt-2">
                           <li class="page-item"><a class="page-link" href="javascript:void(0);">Previous</a></li>
                           <li class="page-item active"><a class="page-link" href="javascript:void(0);">1</a></li>
                           <li class="page-item"><a class="page-link" href="javascript:void(0);">2</a></li>
                           <li class="page-item"><a class="page-link" href="javascript:void(0);">3</a></li>
                           <li class="page-item"><a class="page-link" href="javascript:void(0);">Next</a></li>
                        </ul>

                        </div>
                        </div>

                     </div>
                  </div>
               </div>
               <!-- Sent Referral end here -->


            </div>
         </div>
      </div>
   </div>
</div>

@stop



@section('popup')

<div class="modal fade " id="exampleModal" tabindex="-1" role="dialog">
   
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Referral Form</h5>
                    <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>-->
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
                <div class="modal-body">

                        <form>
                          <div class="row clearfix">

                           <style type="text/css">
                                 .tip-form-header{box-sizing: border-box; background-color: #e0a258; color: #fff; padding: 1rem;width: 100%;}
                                 .tip-form-header .tip-form-member{display: -webkit-box;display: flex; -webkit-box-flex: 1; flex: auto; align-items: center;
                                 }
                              .tip-form-header .profile-image{width: 120px; height: 120px; object-fit: cover; object-position: top center; margin-right: 20px; background-color: #ccc;}

                           </style>

                              <div class="tip-form-header">
                                 <div class="tip-form-member">
                                    <img class="profile-image" src="../assets/images/xs/avatar1.jpg" alt="">
                                    <div class="member-info">
                                       <div class="member-name">Alice Agyemang-Badu</div>
                                       <div class="business-name">New York Life</div>
                                       <div class="text-muted category-name">Recruiter</div>
                                       <div class="chapter-name">Besure of Upper Montclair, NJ</div>
                                    </div>
                                 </div>
                              </div>

                              <div class="text-center" style="padding: 10px; font-size: 90%; font-weight: 500;"><p>My qualified Business Referral is for a company or person who is interested in a specific service or product and is expecting a call from a Besure member.</p></div>



                              <div class="col-lg-6 col-md-6">
                                 <div class="form-group">
                                    <input type="text" data-provide="datepicker" data-date-autoclose="true" class="form-control" placeholder="Start date *">
                                 </div>
                              </div>


                              <div class="col-lg-6 col-md-6">
                                 <div class="form-group">
                                       <select class="custom-select">
                                          <option selected="">Referral Type</option>
                                          <option value="1">Inside</option>
                                          <option value="2">OutSide</option>
                                       </select>
                                 </div>
                              </div>


                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <input type="text" class="form-control" placeholder="Person To Contact">
                                  </div>
                              </div>


                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">                                   
                                      <input type="text" class="form-control" placeholder="Company Name">
                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <input type="text" class="form-control" placeholder="Phone Number">
                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <input type="text" class="form-control" placeholder="Email Address">
                                  </div>
                              </div>

                              <div class="col-lg-12 col-md-12">
                                  <div class="form-group">
                                     <textarea class="form-control" name="" id="" placeholder="Referral Details"></textarea>
                                  </div>
                              </div>


                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1">
                                        <span class="custom-control-label">Specify the Referral value</span>
                                    </label>
                                     
                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      
                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1">
                                        <span class="custom-control-label">This is a private Referral</span>
                                        <span style="position: absolute; margin: 0 0 0 12px;"><i class="fa fa-question-circle" style="font-size: 22px;"></i></span>
                                    </label>

                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      
                                  </div>
                              </div>



                          </div>
                        </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary">Submit Referral</button>
                </div>
            </div>
        </div>
    
</div>
@stop


@section('page-styles')

<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }} ">

@stop
@section('page-script')
<script src="{{ asset('assets/bundles/apexcharts.bundle.js') }}"></script>
<script src="{{ asset('assets/bundles/counterup.bundle.js') }}"></script>
<script src="{{ asset('assets/bundles/knobjs.bundle.js') }}"></script>
<script src="{{ asset('assets/js/core.js') }}"></script>
<script src="{{ asset('assets/js/page/index.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
@stop
