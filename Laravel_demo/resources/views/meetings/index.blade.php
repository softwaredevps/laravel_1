@extends('layout.master')
@section('title', 'Event/Meeting')
@section('content')
<div class="col-lg-12 col-md-12">
  <div class="col-lg-12 col-md-12">
    <div class="section-body">
      <div class="container-fluid">
          <div class="d-flex justify-content-between align-items-center">
              <ul class="nav nav-tabs page-header-tab">
                  <li class="nav-item"><a class="nav-link active" id="user-tab" data-toggle="tab" href="#add-event">Add Event/Meeting</a></li>
                  <li class="nav-item"><a class="nav-link" id="user-tab" data-toggle="tab" href="#affiliate-event-mapping">Meeting Attendance</a></li>
              </ul>
          </div>
      </div>
    </div>

    <div class="section-body mt-3">
      <div class="container-fluid">
        <div class="tab-content mt-3"-->
          <div class="tab-pane fade show active" id="add-event" role="tabpanel"-->
          <div class="card">
            <div class="card-header">
              <h2 class="card-title"><b>Add Event/Meeting</b></h2>
            </div>

            <div class="card-body">
              <form method="post" id="">
                <!--div class="row clearfix">
                  <div class="col-lg-12 col-lg-12">
                    <label>Type <span class="redstar">*<span></label>
                    <div class="form-group">
                      <input type="radio" id="event_type" name="event_type" value="event" checked> Event
                      <input type="radio" id="event_type" name="event_type" value="meeting"> Meeting
                    </div>
                  </div>
                </div-->

                <div class="row clearfix meeting_type_div">
                  <div class="col-lg-12 col-md-12">              
                    <label>Select Meeting Type <span class="redstar">*<span></label>
                    <div class="form-group">
                      <select class="custom-select" name="meeting_type" id="meeting_type" required>
                        <option value="0">Select Meeting Type</option>
                        @foreach($res as $val) 
                        <option value="{{ $val->id }}">{{ $val->MeetingType }}</option> 
                        @endforeach
                      </select>
                    </div>
                  </div>

                  <div class="col-lg-12 col-md-12">
                    <label>Meeting/Event Name <span class="redstar">*<span></label>
                    <div class="form-group">
                    <input type="text" class="form-control" placeholder="Meeting/Event Name" required>
                    </div>
                  </div> 

                  <div class="col-lg-12 col-md-12 zoom_url_div" style="display:none;">
                    <label>Zoom URL <span class="redstar">*<span></label>
                    <div class="form-group">
                    <input type="text" class="form-control" placeholder="Zoom URL" required>
                    </div>
                  </div>                  

                  <div class="col-lg-12 col-md-12" style="margin-bottom:15px;margin-top:20px;">
                      <h6><b>Meeting Date/Duration</b></h6>
                  </div>                  

                  <div class="col-lg-12 col-md-12">              
                    <label>Select Meeting Schedule <span class="redstar">*<span></label>
                    <div class="form-group">
                      <select class="custom-select" name="meeting_type" id="meeting_type" required>
                        <option value="">Select Meeting Schedule</option>
                        <option value="One_time_Meeting">One Time Meeting</option>
                        <option value="Weekly">Weekly</option>
                        <option value="Bi-Weekly">Bi-Weekly</option>
                        <option value="Monthly">Monthly</option>
                      </select>
                    </div>
                  </div>              
                  
                  <div class="col-lg-3 col-md-3" style="display:inline-block;">
                    <div class="meeting_time_div">
                      <label>Meeting Start Date <span class="redstar">*<span></label>
                      <div class="form-group">
                        <input type="text" data-provide="datepicker" data-date-autoclose="true" class="form-control" placeholder="Meeting Start Date *">
                      </div>
                    </div>
                  </div>

                  <div class="col-lg-3 col-md-3" style="display:inline-block;">
                    <div class="meeting_time_div">
                      <label>Meeting End Date <span class="redstar">*<span></label>
                      <div class="form-group">
                        <input type="text" data-provide="datepicker" data-date-autoclose="true" class="form-control" placeholder="Meeting End Date *">
                      </div>
                    </div>
                  </div>                  

                  <div class="col-lg-3 col-md-3" style="display:inline-block;">
                    <div class="meeting_time_div">
                    <label>Meeting Start Time (hh:mm) <span class="redstar">*<span></label>
                    <div class="form-group">
                      <input type="text" class="form-control" placeholder="Meeting Start Time *">
                    </div>
                    </div>
                  </div>

                  <div class="col-lg-3 col-md-3" style="display:inline-block;">
                    <div class="meeting_time_div">
                    <label>Meeting End Time (hh:mm) <span class="redstar">*<span></label>
                    <div class="form-group">
                      <input type="text" class="form-control" placeholder="Meeting End Time *">
                    </div>
                    </div>
                  </div> 
                </div>

                <div class="row clearfix">
                    <div class="col-lg-3 col-md-3" style="display:inline-block;">
                      <label>Event/Meeting Duration <span class="redstar">*<span></label>
                      <div class="form-group">
                        <label class="custom-control custom-checkbox">
                          <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1">
                          <span class="custom-control-label">All Day Event/Meeting</span>
                        </label>
                      </div>                      
                    </div>
                    <!--div class="col-lg-1 col-md-1" style="display:inline-block;">
                        <label></label>
                        <div class="meeting_duration_interval">
                          <h6 style="margin-top:10px;margin-bottom:10px;display:inline-block"><b>OR</b></h6>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-8" style="display:inline-block;">  
                      <div class="meeting_duration_interval">                      
                        <label>Select Event/Meeting Duration <span class="redstar">*<span></label>
                        <div class="form-group">
                          <select class="custom-select" name="meeting_type">
                            <option value="">Select Event/Meeting Duration (In Mins)</option>
                            <option value="30">30</option>
                            <option value="60">60</option>
                            <option value="90">90</option>
                            <option value="120">120</option>
                          </select>
                        </div>
                      </div>
                    </div-->
                </div>

                <div class="row clearfix">
                  <div class="col-lg-12 col-md-12" style="margin-bottom:15px;margin-top:20px;">
                    <div class="affilates_dropdown">
                      <h6><b>Affiliate & Other Information</b></h6>
                    </div>
                  </div>  
                </div>  

                <div class="row clearfix">
                  <div class="col-lg-12 col-md-12"> 
                    <div class="affilates_dropdown">             
                      <label>Select Affiliate <span class="redstar">*<span></label>
                      <div class="form-group">
                        <select class="custom-select" name="meeting_type" id="meeting_type">
                          <option value="">Select Affiliate</option>
                          <option value="1">BeSure in Glen Ridge</option>
                          <option value="2">BeSure of Montclair</option>
                          <option value="3">BeSure of New Jersey</option>
                          <option value="4">BeSure of San Diego</option>
                        </select>
                      </div>
                    </div>
                  </div>                   

                  <div class="col-lg-12 col-md-12">
                    <div class="business_category_dropdown" style="display: none">
                      <label>Select Business Category <span class="redstar">*<span></label>
                      <div class="form-group">
                        <select class="custom-select" name="meeting_type" id="meeting_type">
                          <option value="">Select Business Category</option>
                          <option value="1">3D Imaging</option>
                          <option value="2">Accountant</option>
                          <option value="2">Acupuncture</option>
                          <option value="3">Addictions Recovery</option>
                          <option value="4">Advertising</option>
                        </select>
                      </div>
                    </div>
                  </div>   

                  <div class="col-lg-6 col-md-6">
                    <div class="associate_dropdown" style="display:none;">
                      <label>Select Associate One <span class="redstar">*<span></label>
                      <div class="form-group">
                        <select class="custom-select" name="meeting_type" id="meeting_type">
                          <option value="">Select Associate One</option>
                          <option value="1">Marshall Nichols</option>
                          <option value="2">Marshall Nichols</option>
                          <option value="3">Marshall Nichols</option>
                        </select>
                      </div>
                    </div>
                  </div>

                 <div class="col-lg-6 col-md-6">
                    <div class="associate_dropdown" style="display:none;">
                      <label>Select Associate Two <span class="redstar">*<span></label>
                      <div class="form-group">
                        <select class="custom-select" name="meeting_type" id="meeting_type">
                          <option value="">Select Associate Two</option>
                          <option value="1">Marshall Nichols</option>
                          <option value="2">Marshall Nichols</option>
                          <option value="3">Marshall Nichols</option>
                        </select>
                      </div>
                    </div>
                  </div>                
                </div>                

                <div class="row clearfix" style="margin-top:20px;margin-bottom:15px;">
                  <div class="col-lg-12 col-md-12">
                    <div class="venue_div">
                      <h6><b>Meeting/Event Venue</b></h6>
                    </div>
                  </div>

                  <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="venue_div">
                      <label>Venue Name <span class="redstar">*<span></label>
                      <div class="form-group">
                      <input type="text" class="form-control" placeholder="Venue Name">
                      </div>
                    </div>
                  </div>

                  <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="venue_div">
                      <label>Street Address <span class="redstar">*<span></label>
                      <div class="form-group">
                      <input type="text" class="form-control" placeholder="Street Address">
                      </div>
                    </div>
                  </div>

                  <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="venue_div">
                      <label>Suite <span class="redstar">*<span></label>
                      <div class="form-group">
                      <input type="text" class="form-control" placeholder="Suite">
                      </div>
                    </div>
                  </div>

                  <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="venue_div">
                      <label>City <span class="redstar">*<span></label>
                      <div class="form-group">
                      <input type="text" class="form-control" placeholder="City">
                      </div>
                    </div>
                  </div>

                  <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="venue_div">
                      <label>State <span class="redstar">*<span></label>
                      <div class="form-group">
                        <select class="custom-select" name="state">
                          <option value="">Select State</option>                                           
                        </select>
                      </div>
                    </div>
                  </div> 

                  <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="venue_div">
                      <label>Zip <span class="redstar">*<span></label>
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="Zip">
                      </div>
                    </div>
                  </div>                   

                  <div class="col-lg-12 col-md-12">
                    <div class="venue_div">
                      <label>Meeting Location MAP (Please input URL) <span class="redstar">*<span></label>
                      <input type="text" class="form-control" placeholder="Meeting Location MAP">
                    </div>
                  </div>
                </div>

                <div class="row clearfix" style="margin-top:20px; ">
                  <div class="col-lg-12 col-md-12">
                      <button type="button" class="btn btn-round btn-primary">Submit Event/Meeting </button>
                      <div class="col-lg-10 col-md-10 star-indigate"><span class="redstar">*<span> Indicates the required fields</span></span></div>
                  </div>
                </div>
              </form>
            </div>
        </div>                       
      </div>
      <!-- Send An Escrow Referral end here -->  

      <div class="tab-pane fade" id="affiliate-event-mapping" role="tabpanel">
        <div class="card">
         <div class="card-body">
            <div class="row">
                <!--div class="col-lg-12 col-md-12">              
                  <label>Select Meeting Type</label>
                  <div class="form-group">
                    <select class="custom-select" name="meeting_type" id="meeting_type">
                      <option value="0">Select Meeting Type</option>
                      @foreach($res as $val) 
                      <option value="{{ $val->id }}">{{ $val->MeetingType }}</option> 
                      @endforeach
                    </select>
                  </div>
                </div--> 
                <div class="col-lg-5 col-md-5 col-sm-12">
                  <label>Select Affiliate</label>
                  <div class="form-group">
                    <select class="custom-select">
                      <option value="">Select Affiliate</option>
                      <option value="1">BeSure of New Jersey</option>
                      <option value="1">BeSure in Glen Ridge</option>
                      <option value="2">BeSure of Montclair</option>
                      <option value="3">BeSure of San Diego</option>
                    </select>
                  </div>
                </div>

                <div class="col-lg-5 col-md-5 col-sm-12">
                  <label>Active Meetings</label>
                  <div class="form-group">
                    <select class="custom-select">
                      <option value="">Active Meetings</option>
                      <option value="1">Meeting 1</option>
                      <option value="2">Meeting 2</option>
                      <option value="3">Meeting 3</option>
                    </select>
                  </div>
                </div>

               <div class="col-lg-2 col-md-2 col-sm-12">
                  <label>&nbsp;</label>
                  <a href="javascript:void(0);" class="btn btn-sm btn-primary btn-block" title="">Submit</a>
               </div>
            </div>
         </div>
      </div>

      <div class="card">
        <div class="card-header">
          <h4 class="card-title"><b>Please check the Checboxes for Present Affiliates</b></h4>
        </div>
        <div class="card-body">
          <div class="table-responsive">
             <table class="table table-hover table-vcenter table_custom text-nowrap spacing5 border-style mb-0">
                <tbody>
                   <tr>
                      <td class="w60">
                         <div class="avatar avatar-pink" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name">
                            <span>GH</span>
                         </div>
                      </td>
                      <td><span>Alice Agyemang-Badu</span></td>
                      <td><div class="font-15">New York Life</div></td>                                        
                      <td><span class="text-muted">Business Advisor</span></td>
                      <td>                         
                          <label class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1"><span class="custom-control-label"></span>Is Present ? 
                          </label>
                      </td>
                   </tr>

                   <tr>
                      <td class="">
                         <img class="" src="../assets/images/xs/avatar1.jpg" alt="">
                      </td>
                      <td><span >Celestina Ando</span></td>
                      <td><div class="font-15">Celestina Ando Photography LLC.</div></td>                     
                      <td><span class="text-muted">Business Consultant</span></td>
                      <td>
                          <label class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1"><span class="custom-control-label"></span>Is Present ? 
                          </label>
                      </td>
                   </tr>

                   <tr>
                      <td class="">
                         <img class="" src="../assets/images/xs/avatar2.jpg" alt="">
                      </td>
                      <td><span>Marc Bautis</span></td>
                      <td><div class="font-15">Bautis Financial.</div></td>                    
                      <td><span class="text-muted">Financial Advisor</span></td>
                      <td>
                          <label class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1"><span class="custom-control-label"></span>Is Present ? 
                          </label>
                      </td>
                   </tr>

                   <tr>
                      <td class="">
                         <img class="" src="../assets/images/xs/avatar3.jpg" alt="">
                      </td>
                      <td><span>Joseph DeLuca</span></td>
                      <td><div class="font-15">Evident Title Agency, Inc.</div></td>
                      <td><span class="text-muted">Business Advisor</span></td>
                      <td>
                          <label class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1"><span class="custom-control-label"></span>Is Present ? 
                          </label>
                      </td>
                   </tr>

                   <tr>
                      <td class="w60">
                         <div class="avatar avatar-pink" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name">
                            <span>GH</span>
                         </div>
                      </td>
                      <td><span>Alice Agyemang-Badu</span></td>
                      <td><div class="font-15">New York Life</div></td>                                        
                      <td><span class="text-muted">Business Advisor</span></td>
                      <td>                         
                          <label class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1"><span class="custom-control-label"></span>Is Present ? 
                          </label>
                      </td>
                   </tr>

                   <tr>
                      <td class="">
                         <img class="" src="../assets/images/xs/avatar1.jpg" alt="">
                      </td>
                      <td><span >Celestina Ando</span></td>
                      <td><div class="font-15">Celestina Ando Photography LLC.</div></td>                     
                      <td><span class="text-muted">Business Consultant</span></td>
                      <td>
                          <label class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1"><span class="custom-control-label"></span>Is Present ? 
                          </label>
                      </td>
                   </tr> 
                   
                    <tr>
                      <td class="">
                         <img class="" src="../assets/images/xs/avatar2.jpg" alt="">
                      </td>
                      <td><span>Marc Bautis</span></td>
                      <td><div class="font-15">Bautis Financial.</div></td>                    
                      <td><span class="text-muted">Financial Advisor</span></td>
                      <td>
                          <label class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1"><span class="custom-control-label"></span>Is Present ? 
                          </label>
                      </td>
                   </tr>

                   <tr>
                      <td class="">
                         <img class="" src="../assets/images/xs/avatar3.jpg" alt="">
                      </td>
                      <td><span>Joseph DeLuca</span></td>
                      <td><div class="font-15">Evident Title Agency, Inc.</div></td>
                      <td><span class="text-muted">Business Advisor</span></td>
                      <td>
                          <label class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1"><span class="custom-control-label"></span>Is Present ? 
                          </label>
                      </td>
                   </tr> 
                   
                    <tr>
                      <td class="">
                         <img class="" src="../assets/images/xs/avatar1.jpg" alt="">
                      </td>
                      <td><span >Celestina Ando</span></td>
                      <td><div class="font-15">Celestina Ando Photography LLC.</div></td>                     
                      <td><span class="text-muted">Business Consultant</span></td>
                      <td>
                          <label class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1"><span class="custom-control-label"></span>Is Present ? 
                          </label>
                      </td>
                   </tr>

                   <tr>
                      <td class="w60">
                         <div class="avatar avatar-pink" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name">
                            <span>GH</span>
                         </div>
                      </td>
                      <td><span>Alice Agyemang-Badu</span></td>
                      <td><div class="font-15">New York Life</div></td>                                        
                      <td><span class="text-muted">Business Advisor</span></td>
                      <td>                         
                          <label class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1"><span class="custom-control-label"></span>Is Present ? 
                          </label>
                      </td>
                   </tr>                 
                </tbody>
             </table>
          </div>
        </div>
      </div>

      <div class="card">
        <div class="card-body">
            <form id="submit_meeting_attendance" method="" action="">
              <div class="row clearfix">
                <div class="col-lg-12 col-md-12">
                  <label>Meeting Notes <span class="redstar">*<span></label>
                  <div class="form-group">
                    <textarea rows="6" class="form-control" name="" id="" placeholder="Meeting Notes" required></textarea>
                  </div>
                </div>
              </div>

              <div class="row clearfix">
                <div class="col-lg-12 col-md-12">
                  <label>Any Supporting Documents ?  Please upload here...</label>
                  <div class="form-group">
                    <input type="file" name="meeting_attendance_attachment" id="meeting_attendance_attachment">
                  </div>
                </div>
              </div>

              <div class="row clearfix" style="margin-top:20px; ">
                <div class="col-lg-12 col-md-12">
                    <button type="button" class="btn btn-round btn-primary">Submit Meeting Attendance</button>
                    <div class="col-lg-10 col-md-10 star-indigate"><span class="redstar">*<span> Indicates the required fields</span></span></div>
                </div>
              </div>              
            </form>
        </div>
      </div>

    </div>
    </div>
  </div>
</div>
</div>

@stop




@section('popup')
<div class="modal fade " id="editModal" tabindex="-1" role="dialog">
   
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Referral</h5>
                    <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>-->
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
                <div class="modal-body">

                        <form>
                          <div class="row clearfix">

                              <div class="col-lg-6 col-md-6">
                                 <div class="form-group">
                                    <input type="text" data-provide="datepicker" data-date-autoclose="true" class="form-control" placeholder="Start date *">
                                 </div>
                              </div>

                              <div class="col-lg-6 col-md-6">
                                 <div class="form-group">
                                       <select class="custom-select">
                                          <option selected="">Referral Type</option>
                                          <option value="1">Inside</option>
                                          <option value="2">OutSide</option>
                                       </select>
                                 </div>
                              </div>

                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <input type="text" class="form-control" placeholder="Person To Contact">
                                  </div>
                              </div>

                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">                                   
                                      <input type="text" class="form-control" placeholder="Company Name">
                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <input type="text" class="form-control" placeholder="Phone Number">
                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <input type="text" class="form-control" placeholder="Email Address">
                                  </div>
                              </div>

                              <div class="col-lg-12 col-md-12">
                                  <div class="form-group">
                                     <textarea class="form-control" name="" id="" placeholder="Referral Details"></textarea>
                                  </div>
                              </div>

                              
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <input type="text" class="form-control" placeholder="$">
                                  </div>
                              </div>

                             <div class="col-lg-6 col-md-6">
                                 <div class="form-group">
                                       <select class="custom-select">
                                          <option selected="">Status</option>
                                          <option value="1">New</option>
                                          <option value="2">Bogus/Invalid</option>
                                          <option value="2">Closed</option>
                                          <option value="2">Working</option>
                                       </select>
                                 </div>
                              </div>

                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1">
                                        <span class="custom-control-label">This is a private Referral</span>
                                        <span style="position: absolute; margin: 0 0 0 12px;"><i class="fa fa-question-circle" style="font-size: 22px;"></i></span>
                                    </label>

                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      
                                  </div>
                              </div>

                          </div>
                        </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary">Submit Referral</button>
                </div>
            </div>
        </div>
</div>

<div class="modal fade " id="exampleModal" tabindex="-1" role="dialog">
   
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Referral Form</h5>
                    <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>-->
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
                <div class="modal-body">

                        <form>
                          <div class="row clearfix">

                           <style type="text/css">
                                 .tip-form-header{box-sizing: border-box; background-color: #e0a258; color: #fff; padding: 1rem;width: 100%;}
                                 .tip-form-header .tip-form-member{display: -webkit-box;display: flex; -webkit-box-flex: 1; flex: auto; align-items: center;
                                 }
                              .tip-form-header .profile-image{width: 120px; height: 120px; object-fit: cover; object-position: top center; margin-right: 20px; background-color: #ccc;}

                           </style>

                              <div class="tip-form-header">
                                 <div class="tip-form-member">
                                    <img class="profile-image" src="../assets/images/xs/avatar1.jpg" alt="">
                                    <div class="member-info">
                                       <div class="member-name">Alice Agyemang-Badu</div>
                                       <div class="business-name">New York Life</div>
                                       <div class="text-muted category-name">Recruiter</div>
                                       <div class="chapter-name">Besure of Upper Montclair, NJ</div>
                                    </div>
                                 </div>
                              </div>

                              <div class="text-center" style="padding: 10px; font-size: 90%; font-weight: 500;"><p>My qualified Business Referral is for a company or person who is interested in a specific service or product and is expecting a call from a Besure member.</p></div>

                              <div class="col-lg-12 col-md-12">
                                 <div class="form-group">
                                    <!--input type="text" data-provide="datepicker" data-date-autoclose="true" class="form-control" placeholder="Start date *"-->
                                    <input type="text" class="form-control" placeholder="Referral date *" value="{{ date('d/m/Y') }}" readonly>
                                 </div>
                              </div>

                              <!--div class="col-lg-6 col-md-6">
                                 <div class="form-group">
                                       <select class="custom-select">
                                          <option selected="">Referral Type</option>
                                          <option value="1">Inside</option>
                                          <option value="2">OutSide</option>
                                       </select>
                                 </div>
                              </div-->

                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <input type="text" class="form-control" placeholder="Person To Contact *">
                                  </div>
                              </div>

                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">                                   
                                      <input type="text" class="form-control" placeholder="Company Name *">
                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <input type="text" class="form-control" placeholder="Phone Number #">
                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <input type="text" class="form-control" placeholder="Email Address #">
                                  </div>
                              </div>

                              <div class="col-lg-12 col-md-12">
                                  <div class="form-group">
                                     <textarea class="form-control" name="" id="" placeholder="Referral Details"></textarea>
                                  </div>
                              </div>

                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <p><span class="redstar">*<span> Indicates the required fields</p>
                                      <p><span class="redstar">#<span> any of phone or email is required in valid format</p>
                                  </div>
                              </div>

                              <!--div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1">
                                        <span class="custom-control-label">Specify the Referral value</span>
                                    </label>
                                     
                                  </div>
                              </div-->

                              <!--div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1">
                                        <span class="custom-control-label">This is a private Referral</span>
                                        <span style="position: absolute; margin: 0 0 0 12px;"><i class="fa fa-question-circle" style="font-size: 22px;"></i></span>
                                    </label>

                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      
                                  </div>
                              </div-->

                          </div>
                        </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary">Submit Referral</button>
                </div>
            </div>
        </div>
</div>
@stop


@section('page-styles')

<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }} ">

@stop
@section('page-script')
<script src="{{ asset('assets/bundles/apexcharts.bundle.js') }}"></script>
<script src="{{ asset('assets/bundles/counterup.bundle.js') }}"></script>
<script src="{{ asset('assets/bundles/knobjs.bundle.js') }}"></script>
<script src="{{ asset('assets/js/core.js') }}"></script>
<script src="{{ asset('assets/js/page/index.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<script>
   $(document).ready(function()
   {
      $('#meeting_type').change(function()
      {
        var meeting_type = $('#meeting_type').val();
        //alert(meeting_type);
        if(meeting_type == "6")
        {
          $('.affilates_dropdown').css('display','none');
          $('.venue_div').css('display','block');
          $('.zoom_url_div').css('display','none');
          $('.business_category_dropdown').css('display','none');
          $('.associate_dropdown').css('display','none');
        }
        else if(meeting_type == "2")
        {
          $('.venue_div').css('display','none');
          $('.zoom_url_div').css('display','block');
          $('.affilates_dropdown').css('display','block');
          $('.business_category_dropdown').css('display','none');
          $('.associate_dropdown').css('display','none');
        }
        else if(meeting_type == "3")
        {
          $('.business_category_dropdown').css('display','block');
          $('.affilates_dropdown').css('display','block');
          $('.zoom_url_div').css('display','none');
          $('.venue_div').css('display','block');
          $('.associate_dropdown').css('display','none');
        }
        else if(meeting_type == "4")
        {
          $('.associate_dropdown').css('display','block');
          $('.affilates_dropdown').css('display','block');
          $('.zoom_url_div').css('display','none');
          $('.venue_div').css('display','block');
          $('.business_category_dropdown').css('display','none');
        }
        else
        {
          $('.zoom_url_div').css('display','none');
          $('.venue_div').css('display','block');
          $('.affilates_dropdown').css('display','block');
          $('.business_category_dropdown').css('display','none');
          $('.associate_dropdown').css('display','none');
        }
      });

      $('input[type="checkbox"]'). click(function()
      {
        if($(this). is(":checked"))
        {
          $('.meeting_duration_interval').css('display','none');
          $('.meeting_time_div').css('display','none');
        }
        else if($(this). is(":not(:checked)"))
        {
          $('.meeting_duration_interval').css('display','block');
          $('.meeting_time_div').css('display','block');
        }
      });
   });
   
</script>
@stop
