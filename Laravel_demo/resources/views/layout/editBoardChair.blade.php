@extends('layout.master')
@section('title', 'Leadership')

@section('content')
<?php 
    $data = $res[0];
?>
<div class="col-lg-12 col-md-12">

<div class="section-body mt-3">
        <div class="container-fluid">
            <div class="tab-content mt-3">              
                <div class="tab-pane fade active show" id="user-add" role="tabpanel">
                        <div class="col-lg-12 col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <form method="post" id="BardChairForm">
                                    {{ csrf_field() }}                                      
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12">
                                                <div class="form-group">
                                                    <div>Role<span class="redstar">*<span> </span></span></div>
                                                    <select class="custom-select form-control" name="ChairType">
                                                        <option value="1" <?php echo ($data->ChairType == 1)?'selected':'' ?>>Executive Team</option>
                                                        <option value="2" <?php echo ($data->ChairType == 2)?'selected':'' ?>>Chair Person</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-lg-12 col-md-12">
                                                <div class="form-group">
                                                    <div>Position<span class="redstar">*<span> </span></span></div>

                                                    <input type="text" value="<?php echo isset($data->DesignationName)?$data->DesignationName:''; ?>"  name="DesignationName" class="form-control" placeholder="Position" >
                                                    <input type="hidden" name="hiddenUpdateId" value="<?php echo ($data->id)?$data->id:'' ?>">
                                                </div>
                                            </div>

                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group">
                                                <div>Active<span class="redstar">*<span> </span></span></div>
                                                    <select size="1" class="form-control select-border-true  custom-select" name="isActive">
                                                        <option value="1" <?php echo ($data->isActive == 1)?'selected':'' ?>>YES</option>
                                                        <option value="2" <?php echo ($data->isActive == 2)?'selected':'' ?>>NO</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group">
                                                    <div>Position Level/ Display Order<span class="redstar">*<span> </span></span></div>
                                                    <input type="text" placeholder="Position Level/ Display Order" class="form-control" name="Hierarchy" value="<?php echo ($data->Hierarchy)?$data->Hierarchy:'1' ?>">
                                                </div>
                                            </div>

                                        </div>
                                        <button type="button" class="btn btn-round btn-primary" onclick="updateBardChair(this);">Update</button> &nbsp;&nbsp;

                                        <div class="col-lg-10 col-md-10 star-indigate"><span class="redstar">*<span> Indicates the required fields</span></span></div>

                                    </form>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>            
    </div>

</div>

@stop

@section('page-styles')
<link rel="stylesheet" href="{{ asset('assets/plugins/sweetalert/sweetalert.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/dropify/css/dropify.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/summernote/dist/summernote.css') }}">

@stop

@section('page-script')
<script src="{{ asset('assets/bundles/counterup.bundle.js') }}"></script>
<script src="{{ asset('assets/plugins/sweetalert/sweetalert.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/plugins/dropify/js/dropify.min.js') }}"></script>

<script src="{{ asset('assets/js/core.js') }}"></script>
<script src="{{ asset('assets/js/page/dialogs.js') }}"></script>
<script src="{{ asset('assets/js/form/dropify.js') }}"></script>


@stop



<script>

    function updateBardChair(e){ 
      //  $("input[name='hiddenbizcatdescription']" ).val(tinymce.activeEditor.getContent());
      var isValid = true;
        $('.form-control').each(function() {   
           
            if ( $(this).val().trim() == '' && $(this).val().trim() === ''){
                $(this).addClass('borderColor');
                isValid = false;
            }else{
                $(this).removeClass('borderColor');
                isValid = true;
            }
               
        });
        if(isValid){
         $.ajax({
            url:"/updateBardChair",
            method:"POST",
            data:new FormData(document.getElementById("BardChairForm")),
            dataType:'JSON',
            contentType: false,
            cache: false,
            processData: false,
                success:function(data)
                {
                    if(data.data){
                        toastr.success('Success.');
                        setTimeout(function(){
                            window.location.replace("/board_chair");
                        },1000);
                    }else{
                        toastr.error('Error.');
                    }
                },error: function (xhr, ajaxOptions, thrownError) {
                    if(xhr.status == 300){
                        setTimeout(function(){
                            window.location.replace("/board_chair");
                        },1000);
                    }
                }
            })
        }
    }


</script>