<?php 
    $data = getApplicationViewList(1);
    //echo "<pre>";print_r($data);die('sdf');
?>

@extends('layout.master')
@section('title', 'Associate View')

@section('content')

@include('hrms.countinfo')   


<div class="section-body">
    <div class="container-fluid">
        <div class="tab-content">
        <div class="row">
                        <div class="col-lg-4 col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="media mb-4">
                                        <img class="avatar avatar-xl mr-3" src="../assets/images/sm/avatar1.jpg" alt="avatar">
                                        <div class="media-body">
                                            <h5 class="m-0">Sara Hopkins</h5>
                                            <p class="text-muted mb-0">Webdeveloper</p>
                                            <ul class="social-links list-inline mb-0 mt-2">
                                                <li class="list-inline-item"><a href="javascript:void(0)" title="" data-toggle="tooltip" data-original-title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                                <li class="list-inline-item"><a href="javascript:void(0)" title="" data-toggle="tooltip" data-original-title="Twitter"><i class="fa fa-twitter"></i></a></li>
                                                <li class="list-inline-item"><a href="javascript:void(0)" title="" data-toggle="tooltip" data-original-title="1234567890" aria-describedby="tooltip302288"><i class="fa fa-phone"></i></a></li>
                                                <li class="list-inline-item"><a href="javascript:void(0)" title="" data-toggle="tooltip" data-original-title="@skypename"><i class="fa fa-skype"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <p class="mb-4">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.</p>
                                    <button class="btn btn-outline-primary btn-sm"><span class="fa fa-twitter"></span> Follow</button>                            
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">Statistics</h3>
                                    <div class="card-options">
                                        <a href="#" class="card-options-collapse" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a>
                                        <a href="#" class="card-options-remove" data-toggle="card-remove"><i class="fe fe-x"></i></a>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="text-center">
                                        <div class="row">
                                            <div class="col-6 pb-3">
                                                <label class="mb-0">Project</label>
                                                <h4 class="font-30 font-weight-bold">45</h4>
                                            </div>
                                            <div class="col-6 pb-3">
                                                <label class="mb-0">Growth</label>
                                                <h4 class="font-30 font-weight-bold">87%</h4>
                                            </div>
                                        </div>
                                    </div>                
                                    <div class="form-group">
                                        <label class="d-block">Laravel<span class="float-right">77%</span></label>
                                        <div class="progress progress-xs">
                                            <div class="progress-bar bg-blue" role="progressbar" aria-valuenow="77" aria-valuemin="0" aria-valuemax="100" style="width: 77%;"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="d-block">HTML<span class="float-right">50%</span></label>
                                        <div class="progress progress-xs">
                                            <div class="progress-bar bg-danger" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%;"></div>
                                        </div>
                                    </div>
                                    <div class="form-group mb-0">
                                        <label class="d-block">Photoshop <span class="float-right">23%</span></label>
                                        <div class="progress progress-xs">
                                            <div class="progress-bar bg-green" role="progressbar" aria-valuenow="23" aria-valuemin="0" aria-valuemax="100" style="width: 23%;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <ul class="new_timeline mt-3">
                                        <li>
                                            <div class="bullet pink"></div>
                                            <div class="time">11:00am</div>
                                            <div class="desc">
                                                <h3>Attendance</h3>
                                                <h4>Computer Class</h4>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="bullet pink"></div>
                                            <div class="time">11:30am</div>
                                            <div class="desc">
                                                <h3>Added an interest</h3>
                                                <h4>“Volunteer Activities”</h4>
                                                <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.</p>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="bullet green"></div>
                                            <div class="time">12:00pm</div>
                                            <div class="desc">
                                                <h3>Developer Team</h3>
                                                <h4>Hangouts</h4>
                                                <ul class="list-unstyled team-info margin-0 p-t-5">
                                                    <li><img src="../assets/images/xs/avatar1.jpg" alt="Avatar"></li>
                                                    <li><img src="../assets/images/xs/avatar2.jpg" alt="Avatar"></li>
                                                    <li><img src="../assets/images/xs/avatar3.jpg" alt="Avatar"></li>
                                                    <li><img src="../assets/images/xs/avatar4.jpg" alt="Avatar"></li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="bullet green"></div>
                                            <div class="time">2:00pm</div>
                                            <div class="desc">
                                                <h3>Responded to need</h3>
                                                <a href="javascript:void(0)">“In-Kind Opportunity”</a>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="bullet orange"></div>
                                            <div class="time">1:30pm</div>
                                            <div class="desc">
                                                <h3>Lunch Break</h3>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="bullet green"></div>
                                            <div class="time">2:38pm</div>
                                            <div class="desc">
                                                <h3>Finish</h3>
                                                <h4>Go to Home</h4>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
        </div>
    </div>    
    
    
</div>

@stop

@section('page-styles')

<link rel="stylesheet" href="{{ asset('assets/plugins/fullcalendar/fullcalendar.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/sweetalert/sweetalert.css') }}">
<link href='http://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css' rel='stylesheet' type='text/css'>
<link href='https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css' rel='stylesheet' type='text/css'>
<link href='https://cdn.datatables.net/colreorder/1.5.2/js/dataTables.colReorder.min.js' rel='stylesheet' type='text/css'>

@stop
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
@section('page-script')
<script src="{{ asset('assets/bundles/fullcalendar.bundle.js') }}"></script>

<script src="{{ asset('assets/plugins/sweetalert/sweetalert.min.js') }}"></script>

<script src="{{ asset('assets/js/core.js') }}"></script>
<script src="{{ asset('assets/js/page/calendar.js') }}"></script>

<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
@stop
 
<script>

function application_rejected(id){
    $.ajax({
        url:"/applicationRejected",
        method:"POST",
        data: {
                "_token": "{{ csrf_token() }}",
                "data": {'id':id},
            },
            success:function(data)
            {
                if(data.data){                  
                    toastr.success('Application Rejected Successfully');
                    window.location.replace("/associateApplication");
                }else{
                    toastr.error('Application Status Already Rejected.');
                }
            }
        })  
}

function approve(id){
    $.ajax({
        url:"/applicationApprove",
        method:"POST",
        data: {
                "_token": "{{ csrf_token() }}",
                "data": {'id':id},
            },
            success:function(data)
            {
                if(data.data){                  
                    toastr.success('Application Approved Successfully.');
                    window.location.replace("/associateApplication");
                }else{
                    toastr.error('Application Status Already Approved.');
                }
            }
        })  
}

$(function() {
     
    function statusChange($id,$action){
        $.ajax({
        url:"/application_status_change",
        method:"POST",
        data: {
                "_token": "{{ csrf_token() }}",
                "data": {'id':$id,'status':$action},
            },
            success:function(data)
            {
                if(data.data){
                    var text = $(".row-"+$id+" .btn-block.btn-primary").text();
                    if(text == 'NO'){
                        $(".row-"+$id+" .btn-block.btn-primary").text('YES');
                    }else{
                        $(".row-"+$id+" .btn-block.btn-primary").text('NO');
                    }
                    toastr.success('Success.');
                    swal.close()
                }else{
                    toastr.error('Error.');
                }
            }
        })          
    }

    function submitAjax(id){          
        $.ajax({
        url:"/actionapplicationdelete",
        method:"POST",
        data: {
                "_token": "{{ csrf_token() }}",
                "data": id,
            },
            success:function(data)
            {
                if(data.data){
                    $(".row-"+id).remove();
                    toastr.success('Success.');
                    swal.close()
                }else{
                    toastr.error('Error.');
                }
            }
        })          
    }

    function deleteApplication(id){
        var form = event.target.form; 
        swal({
            title: "Are you sure?",
            text: "But you will still be able to retrieve this file.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#dc3545",
            confirmButtonText: "Delete",
            cancelButtonText: "No, cancel please!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){
            if (isConfirm) {
                submitAjax(id); 
            } else {
                swal("Cancelled", "Your Request Is Cancelled.. :)", "error");
            }
        });
    }



});

</script>