@extends('layout.master')
@section('title', 'Leadership')

@section('content')

<div class="col-lg-12 col-md-12">

<div class="col-lg-12 col-md-12">
      
    <div class="section-body">
        <div class="container-fluid">
            <div class="d-flex justify-content-between align-items-center">
                <ul class="nav nav-tabs page-header-tab">
                    <li class="nav-item"><a class="nav-link active" id="user-tab" data-toggle="tab" href="#user-list">List</a></li>
                    <li class="nav-item"><a class="nav-link" id="user-tab" data-toggle="tab" href="#user-add">Add New</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="section-body mt-3">
        <div class="container-fluid">
            <div class="tab-content mt-3">
                <div class="tab-pane fade show active" id="user-list" role="tabpanel">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-options">
                                <form>
                                    <div class="input-group">
                                        <input type="text" class="form-control form-control-sm" placeholder="Search something..." name="s">
                                        <span class="input-group-btn ml-2"><button class="btn btn-sm btn-default" type="submit"><span class="fe fe-search"></span></button></span>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover table-vcenter text-nowrap mb-0">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th class="w60">Role</th>
                                            <th>Position</th>
                                            <!-- <th>Hirerachy</th> -->
                                            <th>Active</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=1; ?>
                                        <?php foreach($res as $key=>$val){ ?>
                                            <tr class="row-<?php echo  $val->id; ?>">
                                                <td><?php echo $i;?></td>
                                                <td><?php echo ($val->ChairType == 1)?'Executive Team':'Chair Person'; ?></td>
                                                <td><?php echo ($val->DesignationName)?$val->DesignationName:''; ?></td>
                                                <!-- <td><?php //echo ($val->Hierarchy)?$val->Hierarchy:''; ?></td> -->
                                                <td><?php echo ($val->isActive == 1)?'YES':'NO'; ?></td>
                                                <td>
                                                    <a href="/editBoardChair/<?php echo ($val->id)?$val->id:''; ?>">
                                                        <button type="button" class="btn btn-icon btn-sm" title="Edit"><i class="fa fa-edit"></i></button>
                                                    </a>
                                                    <button onclick="deleteBoardMember(<?php echo $val->id; ?>);" type="button" class="btn btn-icon btn-sm js-sweetalert" title="Delete" data-type="confirm"><i class="fa fa-trash-o text-danger"></i></button>
                                                </td>
                                            </tr>
                                            <?php $i++;?>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="user-add" role="tabpanel">
                    
                        <div class="col-lg-12 col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <form method="post" id="BardChairForm">
                                        {{ csrf_field() }}
                                      
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12">
                                                <div class="form-group">
                                                    <div>Role<span class="redstar">*<span> </div>
                                                    <select class="custom-select" name="ChairType">
                                                        <option value="1">Executive Team</option>
                                                        <option value="2">Chair Person</option>
                                                    </select> 
                                                </div>
                                            </div>

                                            <div class="col-lg-12 col-md-12">
                                                <div class="form-group">
                                                    <div>Position<span class="redstar">*<span> </div>

                                                    <input type="text" value="<?php echo isset($res->DesignationName)?$res->DesignationName:''; ?>"  name="DesignationName" class="form-control" placeholder="Position" >
 
                                                    <input type="hidden" name="hiddenChairType"  value="<?php echo isset($res->DesignationName)?strip_tags($res->DesignationName):''; ?>">
                                                </div>
                                            </div>

                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group">
                                                <div>Active<span class="redstar">*<span> </div>
                                                <select size="1" class="form-control select-border-true  custom-select" name="isActive">
                                                        <option value="1" 
                                                        <?php echo ((isset($res->isactive) &&  $res->isactive == 1))?'selected="selected"':'';?>>YES</option>
                                                        <option value="0" 
                                                        <?php echo ((isset($res->isactive) &&  $res->isactive == 0))?'selected="selected"':'';?>>NO</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group">
                                                    <div>Position Level/ Display Order<span class="redstar">*<span> </div>
                                                    <input type="text" placeholder="Position Level/ Display Order" class="form-control" name="Hierarchy"  value="<?php echo isset($res->Hierarchy)?$res->Hierarchy:''; ?>">
                                                </div>
                                            </div>

                                        </div>
                                        <?php if(isset($res->id)){ ?>
                                            <button type="button" class="btn btn-round btn-primary" onclick="updateBardChair(this);">Update</button> &nbsp;&nbsp;
                                        <?php } else { ?>
                                            <button type="button" class="btn btn-round btn-primary" onclick="savebBardChair(this);">Add</button> &nbsp;&nbsp;
                                        <?php } ?>
                                        <div class="col-lg-10 col-md-10 star-indigate"><span class="redstar">*<span> Indicates the required fields</div>

                                    </form>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>            
    </div>

</div>

</div>

@stop

@section('page-styles')
<link rel="stylesheet" href="{{ asset('assets/plugins/fullcalendar/fullcalendar.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/sweetalert/sweetalert.css') }}">
<link href='http://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css' rel='stylesheet' type='text/css'>
<link href='https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css' rel='stylesheet' type='text/css'>
<link href='https://cdn.datatables.net/colreorder/1.5.2/js/dataTables.colReorder.min.js' rel='stylesheet' type='text/css'>
@stop
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
@section('page-script')
<script src="{{ asset('assets/bundles/fullcalendar.bundle.js') }}"></script>

<script src="{{ asset('assets/plugins/sweetalert/sweetalert.min.js') }}"></script>

<script src="{{ asset('assets/js/core.js') }}"></script>
<script src="{{ asset('assets/js/page/calendar.js') }}"></script>

<script type="text/javascript" src="https://cdn.datatables.net/1.10.8/js/jquery.dataTables.min.js"></script>
@stop



<script>

    function savebBardChair(e){
       // $("input[name='hiddenbizcatdescription']" ).val(tinymce.activeEditor.getContent());

       var isValid = true;
        $('.form-control').each(function() {      
            if ( $(this).val() == '' && $(this).val() === ''){
                $(this).addClass('borderColor');
                isValid = false;
            }else{
                $(this).removeClass('borderColor');
                isValid = true;
            }
               
        });
        if(isValid){
            $.ajax({
            url:"/BardChair",
            method:"POST",
            data:new FormData(document.getElementById("BardChairForm")),
            dataType:'JSON',
            contentType: false,
            cache: false,
            processData: false,
                success:function(data)
                {
                    if(data.data){
                        $("#BardChairForm")[0].reset();
                        toastr.success('Success.');
                        // setTimeout(function(){
                        //     window.location.replace("/board_chair");
                        // },1000);
                    }else{
                        toastr.error('Error.');
                    }
                }
            })
        }
    }

    function updateBardChair(e){
      //  $("input[name='hiddenbizcatdescription']" ).val(tinymce.activeEditor.getContent());
        $.ajax({
        url:"/updateBardChair",
        method:"POST",
        data:new FormData(document.getElementById("BardChairForm")),
        dataType:'JSON',
        contentType: false,
        cache: false,
        processData: false,
            success:function(data)
            {
                if(data.data){
                    toastr.success('Success.');
                    setTimeout(function(){
                        window.location.replace("/board_chair");
                    },1000);
                }else{
                    toastr.error('Error.');
                }
            }
        })
    }


    function submitAjax(id){
        $.ajax({
        url:"/deleteBoardMember",
        method:"POST",
        data: {
                "_token": "{{ csrf_token() }}",
                "data": id,
            },
            success:function(data)
            {
                if(data.data){
                    $(".row-"+id).remove();
                    toastr.success('Success.');
                    swal.close()
                }else{
                    toastr.error('Error.');
                }
            }
        })       
    }


    function deleteBoardMember(id){
        var form = event.target.form; 
        swal({
            title: "Are you sure?",
            text: "But you will still be able to retrieve this file.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#dc3545",
            confirmButtonText: "Delete",
            cancelButtonText: "No, cancel please!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){
            if (isConfirm) {
                submitAjax(id); 
            } else {
                swal("Cancelled", "Your Request Is Cancelled.. :)", "error");
            }
        });
    }

</script>