
@extends('layout.member_master')
<?php 

$head_name = 'My Affiliate';
    if( (isset($res['get']) && isset($res['get'][0]) ) && $res['get'][0]->chapter_name){
        $head_name = 'Affiliate / '.$res['get'][0]->chapter_name;
    }
?>

@section('title', $head_name)
@section('content')

<?php 
//echo "<pre>";print_r($res);die;
//    if(!isset($res->chapter_name)){
//        die();
//    }else{
//        $val = getuserInfo($res->uuid);
       
//        $member_data = getmembers($res->uuid);
//    }

//$val = getuserInfo($res->uuid);
       
//$member_data = getmembers($res->uuid);
 
?>
<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="nav nav-tabs page-header-tab">
                <li class="nav-item"><a class="nav-link active" id="dashboard-tab" data-toggle="tab"
                        href="#Accounts-dashboard">Dashboard</a></li>
                <li class="nav-item"><a class="nav-link" id="Accounts-tab" data-toggle="tab"
                        href="#Accounts-Invoices">General</a></li>
                <li class="nav-item"><a class="nav-link" id="Accounts-tab" data-toggle="tab"
                        href="#Accounts-Payments">Associate List</a></li>
                <li class="nav-item"><a class="nav-link" id="Accounts-tab" data-toggle="tab"
                        href="#Accounts-Expenses">Leadership</a></li>
            </ul>

        </div>
    </div>
</div>

<div class="section-body mt-3">
    <div class="container-fluid">
        <div class="tab-content mt-3">

            <div class="tab-pane fade show active" id="Accounts-dashboard" role="tabpanel">
                <div class="section-body">
                    <div class="container-fluid">
                        <div class="tab-content">
                            <div class="table-view-wrapper">
                                <div class="card"> 
                                    <div class="card-header">
                                        <h3 class="card-title">Dashboard</h3>
                                    </div>
                                    <div class="card-header">
                                       
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="tab-content">
                                                    <div class="tab-pane active">
                                                        <div class="lazy-resource-panel">
                                                            <div class="row">
                                                                @include('hrms.countinfo') 
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="Accounts-Invoices" role="tabpanel">
                <?php 
                $cnt_val = 1;
                if(isset($res['get'])){
                foreach($res['get'] as $key=>$val){
                    //$val = $val[0];
                if($cnt_val == 1){
            ?>
                <div class="card">
                    <div class="card-body">
                        <div class="tab-content">
                            <div class="tab-pane active">
                                <div class="lazy-resource-panel">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h5 class="text-muted">Name</h5>
                                            <div>
                                                <?php echo $val->chapter_name; ?>
                                            </div>
                                            <h5 class="text-muted mt-4">Bio</h5>
                                            <div>
                                                <span>
                                                    <?php echo $val->chapter_description; ?>
                                                </span>
                                            </div>
                                            <h5 class="text-muted mt-4">Website</h5>
                                            <div>
                                                <span>
                                                    <?php echo ($val->chapter_website)?$val->chapter_website:''; ?>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <h5 class="text-muted">General Meeting Information</h5>
                                            <div class="row">
                                                <div class="col-md-4"><b>Day</b>
                                                    <br> <?php echo $val->chapter_general_meeting_day; ?></div>
                                                <div class="col-md-4"><b>Time</b>
                                                    <br> <?php echo $val->chapter_general_meeting_time_from; ?></div>
                                            </div>
                                            <br>
                                            <div><b> <?php echo $val->chapter_address; ?> </b>
                                                <div>
                                                    <?php 
                                                            $country_name = country_name($val->chapter_country);
                                                            echo (isset($country_name->name) && $country_name->name !='')?$country_name->name:'';
                                                        ?>
                                                    <?php 
                                         
                                                            $state_name = get_states($val->chapter_state);
                                                            echo (isset($state_name->name) && $state_name->name !='')?$state_name->name:' ...';
                                                        
                                                        ?>

                                                    <?php 
                                                        
                                                            $state_name = get_states($val->chapter_city);
                                                            echo (isset($state_name->name) && $state_name->name !='')?$state_name->name:' ...';
                                                    
                                                        ?>
                                                </div>
                                            </div>

                                            <div></br>
                                                <h6>Metting Time From / Metting Time To </h6>
                                                <?php 
                                                    echo ($val->chapter_general_meeting_time_from)?$val->chapter_general_meeting_time_from.' / ':'...';
                                                    echo ($val->chapter_general_meeting_time_on)?$val->chapter_general_meeting_time_on:'...';
                                                ?>
                                            </div>

                                            <div></br>
                                                <h6>Metting Day </h6>
                                                <?php 
                                                    echo ($val->chapter_general_meeting_day)?$val->chapter_general_meeting_day:'...';
                                                ?>
                                            </div>

                                            <div></br>
                                                <h6>City </h6>
                                                <?php 
                                                    echo ($val->chapter_city)?$val->chapter_city:'...';
                                                ?>
                                            </div>

                                            <div></br>
                                                <h6>Meeting Location </h6>
                                                <?php 
                                                    echo ($val->meeting_location)?$val->meeting_location:'...';
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php 
                } 
                $cnt_val++;
            }
        }
            ?>
            </div>
            <div class="tab-pane fade" id="Accounts-Payments" role="tabpanel">
                <div class="section-body" >
                    <div class="container-fluid">
                        <div class="tab-content">
                            <div class="table-view-wrapper">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Leadership</h3>
                                        <div class="card-options">
                                            <form>
                                                <div class="input-group">
                                                    <input type="text" class="form-control form-control-sm"
                                                        placeholder="Search something..." name="s">
                                                    <span class="input-group-btn ml-2"><button
                                                            class="btn btn-icon btn-sm" type="submit"><span
                                                                class="fe fe-search"></span></button></span>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table
                                                class="table table-hover table-striped table-vcenter text-nowrap mb-0">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Name</th>
                                                        <th>Email</th>
                                                        <th>Phone</th>
                                                        <th>Member Since</th>
                                                        <th>Company</th>
                                                        <th>Business Category</th>
                                                        <!-- <th>Action</th> -->
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <?php 
                                                    if(isset($res['assos'])){
                                                        $rowno = 1;
                                                        foreach($res['assos'] as $key=>$val){
                                                         
                                                         
                                                        ?>
                                                    <tr>
                                                        <td class="w40">
                                                           {{$rowno}}
                                                        </td>
                                                        <td class="d-flex">
                                                           
                                                                <img onerror="this.src='/images/user.png'"
                                                                    class="avatar avatar-blue" id="imguser"
                                                                    src="/images/{{ $val->your_image }}" alt="avatar" />
                                                          
                                                            <a href="/affiliacte_member_view_info/{{$val->uuid}}">
                                                                <div class="ml-3 text-left">
                                                                    <h6 class="mb-1"> {{$val->first_name}}
                                                                        {{$val->last_name}} </h6>
                                                                    
                                                                </div>
                                                            </a>
                                                           
                                                        </td> 
                                                        <td>
                                                            {{$val->your_email}}
                                                        </td>
                                                        <td>
                                                            {{$val->phone_no}}
                                                        </td>
                                                      
                                                        <td><?php echo date("d-M-Y", strtotime($val->created_at)); ?></td>
                                                        <td>
                                                            {{$val->company_address}}
                                                        </td>
                                                        <td>
                                                            <?php 
                                                                $business_cat_name =  business_categories_fetch_byid($val->business_category); 
                                                                echo isset($business_cat_name->bizcatname)?$business_cat_name->bizcatname:"...";
                                                            ?>
                                                        </td>                    

                                                        <!-- <td>
                                                            <a href="/application/{{$val->id}} ">
                                                                <button type="button" class="btn btn-icon btn-sm"
                                                                    title="View"><i class="fa fa-eye"></i></button>
                                                            </a>
                                                         </td> -->
                                                    </tr>
                                                    <?php $rowno++; } }?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          <div class="tab-pane fade" id="Accounts-Expenses" role="tabpanel">
                <div class="section-body">
                    <div class="container-fluid">
                        <div class="tab-content">
                            <div class="table-view-wrapper">
                                <div class="card">
                                    <div class="tab-pane active">
                                        <div class="lazy-resource-panel" style="padding:40px;">
                                            <div class="row">

                                                <div class="col-md-6">
                                                    <h5>Executive Team</h5>
                                                    <?php 
                                                        if(isset($res['member'])){
                                                        foreach($res['member'] as $key=>$val){
                                                            if($val->DesignationID == 1){   
                                                        ?>
                                                    <br>
                                                    <?php $nameChairType =  getNameChairType($val->ChairType);  
                                                          $nameMemberName =  get_members_name($val->MemberID);                                                  
                                                                echo $nameChairType->DesignationName.'</br>';
                                                                echo "<strong>".(isset($nameMemberName->first_name) && $nameMemberName->first_name != '')?@$nameMemberName->first_name:'...'."</strong>";
                                                    ?>
                                                    </p>
                                                    <?php } } }?>
                                                </div>

                                                <div class="col-md-6">
                                                    <h5>Chair Person</h5>
                                                    <?php 
                                                    if(isset($res['member'])){
                                                    foreach($res['member'] as $key=>$val){
                                                    if($val->DesignationID == 2){   
                                                ?>
                                                    <br>
                                                    <?php $nameChairType =  getNameChairType($val->ChairType); 
                                                          $nameMemberName =  get_members_name($val->MemberID); 
                                                                echo $nameChairType->DesignationName.'</br>';
                                                                echo "<strong>".($nameMemberName->first_name)?@$nameMemberName->first_name:'...'."</strong>";
                                                    ?>
                                                    </p>
                                                    <?php } } }?>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('page-styles')
<link rel="stylesheet" href="{{ asset('assets/plugins/sweetalert/sweetalert.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/dropify/css/dropify.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/summernote/dist/summernote.css') }}">

@stop

@section('page-script')
<script src="{{ asset('assets/bundles/counterup.bundle.js') }}"></script>
<script src="{{ asset('assets/plugins/sweetalert/sweetalert.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/plugins/dropify/js/dropify.min.js') }}"></script>

<script src="{{ asset('assets/js/core.js') }}"></script>
<script src="{{ asset('assets/js/page/dialogs.js') }}"></script>
<script src="{{ asset('assets/js/form/dropify.js') }}"></script>


@stop