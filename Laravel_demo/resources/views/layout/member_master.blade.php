<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="icon" href="{{ asset('assets/images/fav_f_ic.png') }}" type="image/x-icon"> <!-- Favicon-->
<title>@yield('title') - {{ config('app.name') }}</title>
<meta name="description" content="@yield('meta_description', config('app.name'))">
<meta name="author" content="@yield('meta_author', config('app.name'))">

@yield('meta')
@stack('before-styles')
<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}">    
<link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css" >

@stack('after-styles')
@if (trim($__env->yieldContent('page-styles')))    
@yield('page-styles')
@endif    

<!-- Custom Css -->
<link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/theme1.css') }}">
</head>

<body class="font-montserrat">
    
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
    </div>
</div>

<div id="main_content">
    
  {{--  @include('layout.headertop') --}} 
    @include('layout.rightbar')
    @include('layout.userdiv')    
    @include('layout.member_sidebar')

    <div class="page">
        @include('layout.page_header')

        @yield('content')

        @include('layout.footer')
    </div>        
</div>


@yield('popup')

<!-- Scripts -->
@stack('before-scripts')
<script src="{{ asset('assets/bundles/lib.vendor.bundle.js') }}"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
<script src="{{ asset('assets/js/custom.js') }}"></script>

@stack('after-scripts')

@if (trim($__env->yieldContent('page-script')))
@yield('page-script')
@endif

</body>
</html>
