<div id="left-sidebar" class="sidebar ">
    <h5 class="brand-name" style="text-align:center;">
        <img src="{{url('../assets/images/logo.png')}}" alt="Image" style="width:50%"; /><a href="javascript:void(0)" class="menu_option float-right"><i class="icon-grid font-16" data-toggle="tooltip" data-placement="left" title="Grid & List Toggle"></i></a>
    </h5>
   
    <nav id="left-sidebar-nav" class="sidebar-nav">
        <ul class="metismenu">
            <!-- <li class="g_heading">Hr</li> -->
            <li class="{{ Request::segment(1) === 'index' ? 'active' : null }}"><a href="/users"><i class="icon-speedometer"></i><span>Dashboard</span></a></li>
            <!-- <li class="{{ Request::segment(1) === 'profle' ? 'active' : null }}"><a href="{{route('hrms.employee')}}"><i class="icon-user"></i><span>My Profile</span></a></li> -->

            <li class="{{ (Request::segment(1) === 'profle' || 
                Request::segment(1) === 'my_attendance' || 
                Request::segment(1) === 'my_performance' || 
                Request::segment(1) === 'referral' || 
                Request::segment(1) === 'my_rewards' || 
                Request::segment(1) === 'services' || 
                Request::segment(1) === 'rewards' ) ? 'active' : null }}">
                <a href="javascript:void(0)" class="has-arrow arrow-c"><i class="fa fa-code-fork"></i><span>My Account</span></a>
                <ul>
                    <li class="{{ Request::segment(1) === 'profle' ? 'active' : null }}">
                        <a href="/member_profile">My Profile</a>
                    </li>
                    <li class="{{ Request::segment(1) === 'my_attendance' ? 'active' : null }}">
                        <a href="javascript:;">My Attendance</a>
                    </li>
                    <li class="{{ Request::segment(1) === 'my_performance' ? 'active' : null }}">
                        <a href="javascript:;">My Performance</a>
                    </li>
                    <li class="{{ Request::segment(1) === 'my_rewards' ? 'active' : null }}">
                        <a href="javascript:;">My Rewards Percentage</a>
                    </li>

                    <li class="{{ Request::segment(1) === 'my_rewards' ? 'active' : null }}">
                        <a href="javascript:;">	My Activity Points</a>
                    </li>
                    <li class="{{ Request::segment(1) === 'referral' ? 'active' : null }}">
                        <a href="/referral">Referral</a>
                    </li>
                    <li class="{{ Request::segment(1) === 'services' ? 'active' : null }}">
                        <a href="/services">Product/Service</a>
                    </li>
                    <li class="{{ Request::segment(1) === 'rewards' ? 'active' : null }}">
                        <a href="/rewards">Rewards Catalogue</a>
                    </li>
                    <li class="{{ Request::segment(1) === 'my_rewards' ? 'active' : null }}">
                        <a href="javascript:;">Billing</a>
                    </li>
                    <li class="{{ Request::segment(1) === 'my_rewards' ? 'active' : null }}">
                        <a href="javascript:;">Messages</a>
                    </li>
                    <li class="{{ Request::segment(1) === 'my_rewards' ? 'active' : null }}">
                        <a href="javascript:;">Settings & Notifications</a>
                    </li>
               </ul>
            </li>
         
         
            <li class="{{ (Request::segment(1) === 'myAffiliate' ||  Request::segment(1) === 'my_invitees' || 
                Request::segment(1) === 'invite_guest' || 
                Request::segment(1) === 'chapter_guest' || 
                Request::segment(1) === 'affiliate-referrals' || 
                Request::segment(1) === 'affiliate-calendar') ? 'active' : null }}">
                <a href="javascript:void(0)" class="has-arrow arrow-c"><i class="fa fa-gear" ></i></i></i><span>My Affiliate</span></a>
                <ul>
                    <li class="{{ Request::segment(1) === 'myAffiliate' ? 'active' : null }}"><a href="/myAffiliate">My Affiliate</a></li>

                    <li class="{{ Request::segment(1) === 'affiliate-referrals' ? 'active' : null }}"><a href="{{ route('affiliate-referrals') }}">Affiliate Referral</a></li>
                    
                    <li class="{{ Request::segment(1) === 'affiliate-calendar' ? 'active' : null }}"><a href="{{ route('affiliate-calendar') }}"> Affiliate Calendar</a></li>

                    <li class="{{ Request::segment(1) === 'chapter_guest' ? 'active' : null }}"><a href="/chapter_guest">Affiliate Guest</a></li>

               </ul>
            </li>

            <!-- <li class="{{ (Request::segment(1) === 'mychapter') ? 'active' : null }}">
                <a href="javascript:void(0)" class="has-arrow arrow-c"><i class="fa fa-book"></i><span>My Chapter</span></a>
                <ul>
                    <li class="{{ Request::segment(1) === 'mychapter' ? 'active' : null }}"><a href="javascript:;">My Chapter</a></li>
               </ul>
            </li> -->

           <!-- <li class="{{ ( Request::segment(1) === 'referral' || Request::segment(1) === 'send_Referral' || Request::segment(1) === 'send_escrow_Referral' || Request::segment(1) === 'received_Referral' || Request::segment(1) === 'my_send_Referral') ? 'active' : null }}">
                <a href="javascript:void(0)" class="has-arrow arrow-c"><i class="fa fa-user-plus"></i><span>Referral</span></a>
                <ul>
                    <li class="{{ Request::segment(1) === 'referral' ? 'active' : null }}"><a href="/referral">Referral</a></li>

                    <li class="{{ Request::segment(1) === 'send_Referral' ? 'active' : null }}"><a href="/send_Referral">Send a Referral</a></li>

                    <li class="{{ Request::segment(1) === 'send_escrow_Referral' ? 'active' : null }}"><a href="send_escrow_Referral">Send an Escrow Referral</a></li>

                    <li class="{{ Request::segment(1) === 'received_Referral' ? 'active' : null }}"><a href="/received_Referral">My Received Referral</a></li>
                    <li class="{{ Request::segment(1) === 'my_send_Referral' ? 'active' : null }}"><a href="/my_send_Referral">My Sent Referral</a></li>
               </ul>
            </li> -->

            <!-- <li class="{{ (Request::segment(1) === 'calendar' || Request::segment(1) === 'event') ? 'active' : null }}">
                <a href="javascript:void(0)" class="has-arrow arrow-c"><i class="icon-calendar"></i><span>Calendar</span></a>
                <ul>
                    <li class="{{ Request::segment(1) === 'calendar' ? 'active' : null }}"><a href="javascript:;">Calendar </a></li>
                    <li class="{{ Request::segment(1) === 'event' ? 'active' : null }}"><a href="javascript:;">Event</a></li>
               </ul>
            </li> -->

           

            <!-- <li class="{{ (Request::segment(1) === 'invite_guest' || Request::segment(1) === 'chapter_guest' || Request::segment(1) === 'my_invitees') ? 'active' : null }}">
                <a href="javascript:void(0)" class="has-arrow arrow-c"><i class="fa fa-user"></i><span>Guests</span></a>
                <ul>
                    <li class="{{ Request::segment(1) === 'invite_guest' ? 'active' : null }}"><a href="/invite_guest">Invite a Guest </a></li>
                    <li class="{{ Request::segment(1) === 'chapter_guest' ? 'active' : null }}"><a href="/chapter_guest">My Chapter Guest </a></li>
                    <li class="{{ Request::segment(1) === 'my_invitees' ? 'active' : null }}"><a href="/my_invitees"> My Invitees </a></li>
               </ul>
            </li> -->
            
            <!-- <div class="text-center admin-row-message"></div> -->
            <!-- <li class="{{ (Request::segment(1) === 'categories' || Request::segment(1) === 'categorieslist') ? 'active' : null }}">
                <a href="javascript:void(0)" class="has-arrow arrow-c"><i class="fa fa-list-alt"></i><span> Business Categories</span></a>
                <ul>                    
                    <li class="{{ Request::segment(1) === 'categorieslist' ? 'active' : null }}"><a href="/categorieslist">Categories List</a></li>

                     <li class="{{ Request::segment(1) === 'categories' ? 'active' : null }}"><a href="/categories">Add New Categories</a></li>
               </ul>
            </li>  -->
            

            <!-- <li class="{{ ( Request::segment(1) === 'editBoardChair' || Request::segment(1) === 'board_chair') ? 'active' : null }}">
                <a href="javascript:void(0)" class="has-arrow arrow-c"><i class="fa fa-building-o"></i><span>Designation Type</span></a>
                <ul>
                    <li class="{{ Request::segment(1) === 'board_chair' ? 'active' : null }}"><a href="/board_chair">Designation</a></li>
                 </ul>
            </li> -->

            <!-- <li class="{{ (Request::segment(1) === 'chapter_roster_list' || Request::segment(1) === 'board_chair_members' || Request::segment(1) === 'chapter' || Request::segment(1) === 'chapterlist' || Request::segment(1) === 'chapter_roster') ? 'active' : null }}">
                <a href="javascript:void(0)" class="has-arrow arrow-c"><i class="fa fa-book"></i><span> Affiliates</span></a>
                <ul>
                    <li class="{{ Request::segment(1) === 'chapterlist' ? 'active' : null }}"><a href="/chapterlist">Affiliates List</a></li>
                    <li class="{{ Request::segment(1) === 'chapter' ? 'active' : null }}"><a href="/chapter">Add New Affiliates</a></li>
                    <li class="{{ Request::segment(1) === 'board_chair_members' ? 'active' : null }}"><a href="/board_chair_members">Affiliates Designation</a></li>
                </ul>
            </li>
             -->

            <li class="{{ (Request::segment(1) === 'businessCatInfoMembers' || Request::segment(1) === 'associateslistMember' || Request::segment(1) === 'member_affiliates') ? 'active' : null }}">
                <a href="javascript:void(0)" class="has-arrow arrow-c"><i class="fa fa-code-fork"></i><span>BeSure Network Search</span></a>
                <ul> 
                    <li class="{{ Request::segment(1) === 'associateslistMember' ? 'active' : null }}"><a href="/associateslistMember">Associates</a></li>
                    <li class="{{ Request::segment(1) === 'member_affiliates' ? 'active' : null }}"><a href="/member_affiliates">Affiliates</a></li>

                    <li class="{{ Request::segment(1) === 'businessCatInfoMembers' ? 'active' : null }}"><a href="/businessCatInfoMembers"> Business Category</a></li>

                </ul>
            </li>


            <!-- <li class="{{ (Request::segment(1) === 'application' || Request::segment(1) === 'associateApplication' || Request::segment(1) === '2' || Request::segment(1) === '3') ? 'active' : null }}">
                <a href="javascript:void(0)" class="has-arrow arrow-c"><i class="fa fa-code-fork"></i><span> Associate/Applications </span></a>
                <ul> 
                    <li class="{{ Request::segment(1) === 'associateApplication' ? 'active' : null }}"><a href="/associateApplication">New Received</a></li>
                     <li class="{{ Request::segment(2) === '2' ? 'active' : null }}"><a href="/associateApplication/2">Approved Applications</a></li>
                    <li class="{{ Request::segment(2) === '3' ? 'active' : null }}"><a href="/associateApplication/3">Rejected Applications</a></li>
                    
               </ul>
            </li> -->

          

<!-- 
            <li class="{{ (Request::segment(1) === 'members' || Request::segment(1) === 'member_view') ? 'active' : null }}">
                <a href="javascript:void(0)" class="has-arrow arrow-c"><i class="fa fa-user"></i><span> Associate List</span></a>
                <ul>
                    <li class="{{ Request::segment(1) === 'members' ? 'active' : null }}"><a href="/members">Associate</a></li>   
               </ul>
            </li> -->

            <!-- <li class="{{ (Request::segment(1) === 'reports') ? 'active' : null }}">
                <a href="javascript:void(0)" ><i class="fa fa-file"></i><span>Reports</span></a>
            </li>  -->
            
            <!-- <li class="{{ (Request::segment(1) === 'MemberShipPmtType' || Request::segment(1) === 'ActivityPoints' || Request::segment(1) === 'RewardsPercentage' || Request::segment(1) === 'MeetingType' || Request::segment(1) === 'MeetingStructure') ? 'active' : null }}">
                <a href="javascript:void(0)" class="has-arrow arrow-c"><i class="fa fa-gear" ></i></i></i><span>Settings</span></a>
                <ul>
                    <li class="{{ Request::segment(1) === 'MemberShipPmtType' ? 'active' : null }}"><a href="/MemberShipPmtType">Membership Term/Cost</a></li>

                    <li class="{{ Request::segment(1) === 'ActivityPoints' ? 'active' : null }}"><a href="/ActivityPoints">Activity Points</a></li>
                    <li class="{{ Request::segment(1) === 'RewardsPercentage' ? 'active' : null }}"><a href="/RewardsPercentage">Rewards Percentage</a></li>
                    <li class="{{ Request::segment(1) === 'MeetingType' ? 'active' : null }}"><a href="/MeetingType">Meeting Type</a></li>

                    <li class="{{ Request::segment(1) === 'MeetingStructure' ? 'active' : null }}"><a href="/MeetingStructure">Meeting Structure</a></li>

               </ul>


            </li> -->
            
            <li class="">
                <a href="javascript:void(0)" ><i class="fa fa-support"></i><span>Support</span></a>
            </li>
            <li class="">
                <a href="javascript:void(0)" ><i class="fa fa-file"></i><span>Documentation</span></a>
            </li>
            <li class="">
                <a href="javascript:void(0)" ><i class="fa fa-question-circle"></i><span>FAQ</span></a>
            </li>
            <!-- <li class="">
                <a href="/logout" ><i class="fa fa-question-circle"></i><span>Logout</span></a>
            </li> -->
            <!--   <li class="{{ Request::segment(2) === 'users' ? 'active' : null }}"><a href="{{route('hrms.users')}}"><i class="icon-users"></i><span>Users</span></a></li>
            <li class="{{ Request::segment(2) === 'departments' ? 'active' : null }}"><a href="{{route('hrms.departments')}}"><i class="icon-control-pause"></i><span>Departments</span></a></li>
            <li class="{{ Request::segment(2) === 'employee' ? 'active' : null }}"><a href="{{route('hrms.employee')}}"><i class="icon-user"></i><span>Employee</span></a></li>
            <li class="{{ Request::segment(2) === 'activities' ? 'active' : null }}"><a href="{{route('hrms.activities')}}"><i class="icon-equalizer"></i><span>Activities</span></a></li>
            <li class="{{ Request::segment(2) === 'holidays' ? 'active' : null }}"><a href="{{route('hrms.holidays')}}"><i class="icon-like"></i><span>Holidays</span></a></li>
            <li class="{{ Request::segment(2) === 'events' ? 'active' : null }}"><a href="{{route('hrms.events')}}"><i class="icon-calendar"></i><span>Events</span></a></li>
            <li class="{{ Request::segment(2) === 'payroll' ? 'active' : null }}"><a href="{{route('hrms.payroll')}}"><i class="icon-briefcase"></i><span>Payroll</span></a></li>
            <li class="{{ Request::segment(2) === 'accounts' ? 'active' : null }}"><a href="{{route('hrms.accounts')}}"><i class="icon-credit-card"></i><span>Accounts</span></a></li>
            <li class="{{ Request::segment(2) === 'report' ? 'active' : null }}"><a href="{{route('hrms.report')}}"><i class="icon-bar-chart"></i><span>Report</span></a></li>
            <li class="g_heading">Project</li>
            <li class="{{ Request::segment(1) === 'project' ? 'active' : null }}">
                <a href="javascript:void(0)" class="has-arrow arrow-c"><i class="icon-cup"></i><span>Project</span></a>
                <ul>
                    <li class="{{ Request::segment(2) === 'index2' ? 'active' : null }}"><a href="{{route('project.index2')}}">Dashboard</a></li>
                    <li class="{{ Request::segment(2) === 'list' ? 'active' : null }}"><a href="{{route('project.list')}}">Project list</a></li>
                    <li class="{{ Request::segment(2) === 'taskboard' ? 'active' : null }}"><a href="{{route('project.taskboard')}}">Taskboard</a></li>
                    <li class="{{ Request::segment(2) === 'ticket' ? 'active' : null }}"><a href="{{route('project.ticket')}}">Ticket List</a></li>
                    <li class="{{ Request::segment(2) === 'ticketdetails' ? 'active' : null }}"><a href="{{route('project.ticketdetails')}}">Ticket Details</a></li>
                    <li class="{{ Request::segment(2) === 'clients' ? 'active' : null }}"><a href="{{route('project.clients')}}">Clients</a></li>
                    <li class="{{ Request::segment(2) === 'todo' ? 'active' : null }}"><a href="{{route('project.todo')}}">Todo List</a></li>
                </ul>
            </li>
            <li class="{{ Request::segment(1) === 'job' ? 'active' : null }}">
                <a href="javascript:void(0)" class="has-arrow arrow-c"><i class="icon-briefcase"></i><span>Job Portal</span></a>
                <ul>
                    <li class="{{ Request::segment(2) === 'index3' ? 'active' : null }}"><a href="{{route('job.index3')}}">Dashboard</a></li>
                    <li class="{{ Request::segment(2) === 'positions' ? 'active' : null }}"><a href="{{route('job.positions')}}">Positions</a></li>
                    <li class="{{ Request::segment(2) === 'applicants' ? 'active' : null }}"><a href="{{route('job.applicants')}}">Applicants</a></li>
                    <li class="{{ Request::segment(2) === 'resumes' ? 'active' : null }}"><a href="{{route('job.resumes')}}">Resumes</a></li>
                    <li class="{{ Request::segment(2) === 'jobsettings' ? 'active' : null }}"><a href="{{route('job.jobsettings')}}">Settings</a></li>
                </ul>
            </li>
            <li class="{{ Request::segment(1) === 'authentication' ? 'active' : null }}">
                <a href="javascript:void(0)" class="has-arrow arrow-c"><i class="icon-lock"></i><span>Authentication</span></a>
                <ul>
                    <li class="{{ Request::segment(2) === 'login' ? 'active' : null }}"><a href="{{route('authentication.login')}}">Login</a></li>
                    <li class="{{ Request::segment(2) === 'register' ? 'active' : null }}"><a href="{{route('authentication.register')}}">Register</a></li>
                    <li class="{{ Request::segment(2) === 'forgotpassword' ? 'active' : null }}"><a href="{{route('authentication.forgotpassword')}}">Forgot password</a></li>
                    <li class="{{ Request::segment(2) === 'error404' ? 'active' : null }}"><a href="{{route('authentication.error404')}}">Error 404</a></li>
                    <li class="{{ Request::segment(2) === 'error500' ? 'active' : null }}"><a href="{{route('authentication.error500')}}">Error 500</a></li>
                </ul>
            </li> -->
        </ul>
    </nav>        
</div>
