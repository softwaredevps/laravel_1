<div class="section-body">
    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    Copyright © <?php echo date('Y')?>  <a href="javascript:;">Besure</a>.
                </div>
                <div class="col-md-6 col-sm-12 text-md-right">
                    <ul class="list-inline mb-0">
                        <li class="list-inline-item"><a href="javascript:void(0)">Documentation</a></li>
                        <li class="list-inline-item"><a href="javascript:void(0)">FAQ</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
</div>
<script>
$(document).ready(function() {
    $('#associatelist_table').DataTable({
        "pageLength": 25,
    });
    $('#member_chapter_list_table').DataTable({
        "pageLength": 25,
    });
    $('#affiliatesInfo').DataTable({
        "pageLength": 25,
    });

    
    $('#businessCatInfo').DataTable({
        "pageLength": 25,
    });
   
    
    $('#chapter_associate_list').DataTable({
        "pageLength": 25,
        "lengthMenu": [[25, 50, 100, 200, 300, 500, -1], [25, 50, 100, 200, 300, 500, "All"]],
        'columns': [
        { data: 'id' },
        { data: 'bizcatname' }, 
        { data: 'bizcatdescription' }, 
        { data: 'isactive' },
        {data:'Action'}
     ],
     'columnDefs': [ {
        'targets': [4], 
        'orderable': false,
     }]
    });  

    $('#categories_listing_table').DataTable({
        "pageLength": 25,
        "lengthMenu": [[25, 50, 100, 200, 300, 500, -1], [25, 50, 100, 200, 300, 500, "All"]],
        'columns': [
        { data: 'id' },
        { data: 'bizcatname' }, 
        { data: 'bizcatdescription' }, 
        { data: 'isactive' },
        {data:'Action'}
     ],
     'columnDefs': [ {
        'targets': [4], 
        'orderable': false,
     }]
    });  

    $('#chapter_list_table').DataTable({
        "pageLength": 25,
        responsive: true,
        colReorder: true,

        "lengthMenu": [[25, 50, 100, 200, 300, 500, -1], [25, 50, 100, 200, 300, 500, "All"]],
        'columns': [
        { data: 'id' },
        { data: 'chapter_name' }, 
        { data: 'chapter_state' },
        { data: 'chapter_city' },
        { data: 'isActive' }, 
        {data:'Action'}
     ],
     'columnDefs': [ {
        'targets': [5], 
        'orderable': false, 
     }]
    }); 

    $('#associate_application_table').DataTable({
        "pageLength": 25,
        "lengthMenu": [[25, 50, 100, 200, 300, 500, -1], [25, 50, 100, 200, 300, 500, "All"]],
        'columns': [
        { data: 'id' },
        { data: 'first_name' },
        { data: 'created_at' },
        { data: 'chapter_name' },
        { data: 'bizcatname' }, 
        { data: 'city'},
        { data: 'state'},

        { data: 'country'}
        ],
        'columnDefs': [ {
            'targets': [7], 
            'orderable': false, 
        }]
    }); 
});
</script>