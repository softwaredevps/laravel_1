@extends('layout.master')
@section('title', 'Appointment/Termination')

@section('content')

<div class="col-lg-12 col-md-12">
    <div class="section-body">
            <div class="container-fluid">
                <div class="d-flex justify-content-between align-items-center">
                    <ul class="nav nav-tabs page-header-tab">
                        <li class="nav-item"><a class="nav-link active" id="user-tab" data-toggle="tab" href="#user-list">Appointment</a></li>
                        <li class="nav-item"><a class="nav-link" id="user-tab" data-toggle="tab" href="#user-add">Termination</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="section-body mt-3">
            <div class="container-fluid">
                <div class="tab-content mt-3">
                    <div class="tab-pane fade show active" id="user-list" role="tabpanel">
                        <div class="col-lg-12 col-md-12">
                            <div class="card">
                                <div class="alert alert-warning warning-message" style="display:none;">
                                    <strong>Warning!</strong> Block... " This User is already Assigned in this affilate. To appoint him to new position, please terminate his current role". 
                                </div>
                                <div class="card-header">
                                    <h3 class="card-title">Appointment</h3>
                                </div>
                                <div class="card-body">
                                    <form method="post" id="appointmentForm">
                                        {{ csrf_field() }}
                                        <div class="row clearfix">
                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group">
                                                    <div>Date Of Appointment<span class="redstar">*<span> </div>
                                                
                                                    <input name="AppointmentDt" data-provide="datepicker" data-date-autoclose="true" class="form-control" placeholder="Date Of Appointment">

                                                </div>
                                            </div>

                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group">
                                                    <div>Select Affiliate<span class="redstar">*<span> </div>
                                                    <!-- <input type="text" class="form-control" placeholder="Select Chapter" name="ChapterID"
                                                    > -->

                                                    <select class="form-control" name="ChapterID">
                                                        <option value="">Select Affiliate</option>
                                                        <?php foreach(chapter_tbl_fetch() as $key=>$val){?>                                                        
                                                            <option value="<?php echo isset($val->id)?$val->id:''; ?>"><?php echo isset($val->chapter_name)?$val->chapter_name:''; ?></option>
                                                        <?php } ?>
                                                    </select>


                                                </div>
                                            </div>

                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group">
                                                    <div>Select Associate<span class="redstar"><span> </div>                                             
                                                    <select class="form-control" name="member">
                                                    
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group">
                                                    <div>Designation Type<span class="redstar">*<span> </div>

                                                    <select class="form-control DesignationID" name="DesignationID">
                                                        <option value="">Select Designation</option>
                                                        <option value="1">Executive Team</option>
                                                        <option value="2">Chair Person</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="col-lg-12 col-md-12">
                                                <div class="form-group">
                                                    <div>Select Designation<span class="redstar"><span> </div>
                                                
                                                    <select class="form-control ChairType" name="ChairType">
                                                    
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-lg-12 col-md-12">
                                                <div class="form-group">
                                                    <div>Remark</div>
                                                    <textarea rows="8" cols="100" type="text" class="form-control" placeholder="Remark" name="AppointmentRemarks"></textarea>
                                                </div>
                                            </div>

                                                <div class="append-data">
                                            

                                                    <!-- <div class="col-md-6 executive-append">
                                                        <h5>Executive Team</h5>
                                                    </div>

                                                    <div class="col-md-6 chairerson-append">
                                                        <h5>Chair Person</h5>
                                                    </div> -->


                                                </div>
                                        </div>
                                        <button type="button" class="btn btn-round btn-primary" onclick="saveAppointment(this);">Appoint </button>
                                        <div class="col-lg-10 col-md-10 star-indigate"><span class="redstar">*<span> Indicates the required fields</div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="user-add" role="tabpanel">                        
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Terminations</h3>
                            </div>
                            <div class="card-body">
                                <form method="post" id="terminationForm">
                                    {{ csrf_field() }}
                                    <div class="row clearfix">
                                        <div class="col-lg-6 col-md-6">
                                            <div class="form-group">
                                                <div>Chapter<span class="redstar">*<span> </div>                                               
                                                <select class="form-control" name="Chapter_termination">
                                                    <option value="">Select Chapter</option>
                                                    <?php foreach(chapter_tbl_fetch() as $key=>$val){?>                                                        
                                                          <option value="<?php echo isset($val->id)?$val->id:''; ?>"><?php echo isset($val->chapter_name)?$val->chapter_name:''; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-6 col-md-6">
                                            <div class="form-group">
                                                <div>Date Of Termination<span class="redstar">*<span> </div>
                                               
                                                <input name="TerminationDt" data-provide="datepicker" data-date-autoclose="true" class="form-control" placeholder="Select Appointment">
                                            </div>
                                        </div>

                                        <div class="col-lg-6 col-md-6">
                                            <div class="form-group">
                                                <div>Designated Associate<span class="redstar">*<span> </div>
                                                <!-- <select class="form-control" name="Chapter_members">
                                                    <option value="">Select Designated Membe</option>
                                                    <?php //foreach(termination_designated_member() as $key=>$val){
                                                      //$name = isset($val->MemberID)?get_members_name($val->MemberID):'';
                                                        //if(isset($name) && $name && @$name->first_name != '')
                                                     
                                                    ?>                                                        
                                                          <option value="<?php //echo isset($val->MemberID)?$val->MemberID:''; ?>">
                                                            <?php //echo @$name->first_name.' '.@$name->last_name.'('.@$name->your_email .')';  ?>
                                                          </option>
                                                    <?php //}    ?>
                                                </select> -->

                                                <select class="form-control" name="Chapter_members">
                                                    
                                                </select>

                                            </div>
                                        </div>

                                        <div class="col-lg-6 col-md-6">
                                            <div class="form-group">
                                                <div>Terminations Reason<span class="redstar">*<span> </div>
                                                <select class="form-control" name="TerminationReason">
                                                    <option value="">Select Terminations </option>
                                                    <option value="1">Resignation </option>
                                                    <option value="2">Termination</option>
                                                    <option value="3">Don't Want to Disclose</option>
                                                    <option value="4">Assigning Different Role</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-12 col-md-12">
                                            <div class="form-group">
                                                <div>Remark </div>
                                                <textarea rows="8" cols="100" type="text" class="form-control" placeholder="Remark" name="terminationRemarks"></textarea>
                                            </div>
                                        </div>

                                    </div>

                                   

                                    <button type="button" class="btn btn-round btn-primary" onclick="saveTermination(this);">Terminate </button>
                                    <div class="col-lg-10 col-md-10 star-indigate"><span class="redstar">*<span> Indicates the required fields</div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('page-styles')

<link rel="stylesheet" href="{{ asset('assets/plugins/fullcalendar/fullcalendar.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/sweetalert/sweetalert.css') }}">
<link href='http://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css' rel='stylesheet' type='text/css'>
<link href='https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css' rel='stylesheet' type='text/css'>
<link href='https://cdn.datatables.net/colreorder/1.5.2/js/dataTables.colReorder.min.js' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}">

<script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>

@stop
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
@section('page-script')
<script src="{{ asset('assets/bundles/fullcalendar.bundle.js') }}"></script>

<script src="{{ asset('assets/plugins/sweetalert/sweetalert.min.js') }}"></script>

<script src="{{ asset('assets/js/core.js') }}"></script>
<script src="{{ asset('assets/js/page/calendar.js') }}"></script>

<script type="text/javascript" src="https://cdn.datatables.net/1.10.8/js/jquery.dataTables.min.js"></script>
@stop

<script>
    function saveAppointment(val){ 
        var isValid = true;
        $('#appointmentForm .form-control').each(function() {      
            if ($(this).attr('name') != 'AppointmentRemarks' && $(this).val() == '' ){
                $(this).addClass('borderColor');
                isValid = false;
            }else{
                $(this).removeClass('borderColor');
                isValid = true;
            }               
        });
        if(isValid){
         $.ajax({
            url:"/insertAppointment",
            method:"POST",
            data:new FormData(document.getElementById("appointmentForm")),
            dataType:'json',
            contentType: false,
            cache: false,
            processData: false,
                success:function(data)
                {
                    console.log(data,'123');
                    if(data.data){
                        $("#appointmentForm")[0].reset();
                        toastr.success('Success.');   
                        setTimeout(function(){
                            location.reload();   
                        },1000)
                                         
                    }else{
                        toastr.error('Error.');
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                if(jqXHR.status == 200){
                    $("#appointmentForm")[0].reset();
                    toastr.success('Success.');   
                    setTimeout(function(){
                        location.reload();   
                    },1000)  
                }
                  if(jqXHR.status == 350){
                      $('.warning-message').css('display','block');
                      setTimeout(function(){
                        $('.warning-message').css('display','none');
                      },5000);
                  }
                }
            })
        }
      
    }

    function saveTermination(val){   
        var isValid = true;
        $('#terminationForm .form-control').each(function() {      
            if ( $(this).attr('name') != 'terminationRemarks' && $(this).val() === ''){
                $(this).addClass('borderColor');
                isValid = false;
            }else{
                $(this).removeClass('borderColor');
                isValid = true;
            }               
        });
        if(isValid){ 
            $.ajax({
            url:"/terminationForm",
            method:"POST",
            data:new FormData(document.getElementById("terminationForm")),
            dataType:'JSON',
            contentType: false,
            cache: false,
            processData: false,
                success:function(data)
                {
                    if(data.data){
                        $("#terminationForm")[0].reset();
                        toastr.success('Success.');   
                        setTimeout(function(){
                            location.reload();   
                        },1000)                   
                    }else{
                        toastr.error('Error.');
                    }
                }
            }) 
        }
    }

     $(document).on('change', '.DesignationID', function() {
       var id = $(this).val();
       $.ajax({
        url:"/getDesignation",
        method:"POST",
        data: {
            "_token": "{{ csrf_token() }}",
            "data": id,
        },
        success:function(data)
        {
            $('select[name="ChairType"]').empty();
            $.each(data, function(e,v) {
                $(".ChairType").append("<option value='"+v.id+"'>"+v.DesignationName+"</option>");
            });
        } 
        })
    }); 
 
    $(document).on('change', 'select[name="ChapterID"]', function() { 
       var id = $(this).val();
       $.ajax({
        url:"/getMembers",
        method:"POST",
        data: {
            "_token": "{{ csrf_token() }}",
            "data": id
        },
        success:function(data)
        {
            $('select[name="member"]').empty();  
            $('select[name="member"]').append("<option value=''>Select Associate</option>");
            $.each(data, function(e,v) {
                $('select[name="member"]').append("<option value='"+v.id+"'>"+v.first_name+' '+v.last_name+' ('+v.your_email+')'+"</option>");
            });
        } 
        })
    });

    $(document).on('change', 'select[name="Chapter_termination"]', function() { 
       var id = $(this).val();
       $.ajax({
        url:"/getMembersTerminate",
        method:"POST",
        data: {
            "_token": "{{ csrf_token() }}",
            "data": id,
        },
        success:function(data)
        {
            $('select[name="Chapter_members"]').empty();  
            $('select[name="Chapter_members"]').append("<option value=''>Select Associate</option>");
            $.each(data, function(e,v) {
                $('select[name="Chapter_members"]').append("<option value='"+v.uuid+"'>"+v.first_name+' '+v.last_name+' ('+v.your_email+')'+"</option>");
            });
        } 
        })
    });

    $(document).on('change', 'select[name="member"]', function() { 
       var id = $(this).val();
       $.ajax({
        url:"/getMembersbyid",
        method:"POST",
        data: {
            "_token": "{{ csrf_token() }}",
            "data": id,
        },
        success:function(data)
        {
            // $('.append-data').empty();  
            // $.each(data, function(e,v) {
            //     if(v.DesignationID == 2){ 
            //         $('.append-data').append( v.DesignationName);
            //     }else{
            //         $('.append-data').append( v.DesignationName);
            //     }
            // });
        } 
        })
    });

</script>