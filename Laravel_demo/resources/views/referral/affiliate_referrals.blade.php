@extends('layout.member_master')
@section('title', 'Affiliate Referrals')
@section('content')
<div class="col-lg-12 col-md-12">
      <div class="section-body mt-3">
         <div class="container-fluid">
              <!-- Affiliate Referral start here -->
                  <div class="row clearfix">
                     <div class="col-12">
                        <div class="card">
                           <div class="card-body">
                              <div class="row">

                                 <div class="col-lg-2 col-md-2 col-sm-6">
                                    <label>Start Date</label>
                                    <div class="input-group">
                                       <input type="text" data-provide="datepicker" data-date-autoclose="true" class="form-control" placeholder="Start date *">
                                    </div>
                                 </div>

                                 <div class="col-lg-2 col-md-2 col-sm-6">
                                    <label>End Date</label>
                                    <div class="input-group">
                                       <input type="text" data-provide="datepicker" data-date-autoclose="true" class="form-control" placeholder="End date *">
                                    </div>
                                 </div>

                                 <div class="col-lg-3 col-md-3 col-sm-6">
                                    <label>Status</label>
                                    <div class="form-group">
                                       <select class="custom-select">
                                          <option selected="">Select Status</option>
                                          <option value="1">Bogus/Invalid</option>
                                          <option value="2">Closed</option>
                                          <option value="3">Working</option>
                                          <option value="3">Sort by name</option>
                                       </select>
                                    </div>
                                 </div>

                                 <div class="col-lg-3 col-md-3 col-sm-6">
                                    <label>Associate</label>
                                    <div class="form-group">
                                       <select class="custom-select">
                                          <option selected="">Select Associate</option>
                                          <option value="1">Associate 1</option>
                                          <option value="2">Associate 2</option>
                                          <option value="3">Associate 3</option>
                                          <option value="3">Associate 4</option>
                                       </select>
                                    </div>
                                 </div> 

                                 <div class="col-lg-2 col-md-2 col-sm-6">
                                    <label>&nbsp;</label>
                                    <a href="javascript:void(0);" class="btn btn-sm btn-primary btn-block" title="">Filter</a>
                                 </div>                                                                 
                                 
                              </div>
                        <div class="table-responsive">
                           <table class="table table-hover table-vcenter table_custom text-nowrap spacing5 border-style mb-0">
                              <thead>
                                  <tr>
                                      <th></th>
                                      <th></th>
                                      <th></th>
                                      <th></th>
                                      
                                  </tr>
                              </thead>
                              <tbody>
                                 <tr>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral To :</span><br>
                                       </div>
                                       <div class="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name">
                                          <img class="" src="../assets/images/xs/avatar1.jpg" alt="">
                                       </div>
                                       <div class="font-12">
                                          <span>Alice Agyemang-Badu</span><br>
                                          <span>Business Advisor</span>
                                       </div>
                                    </td>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral From :</span><br>
                                       </div>
                                        <div class="avatar avatar-pink" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name">
                                          <span>GH</span>
                                       </div>
                                       <div class="font-12">
                                          <span>Celestina Ando</span><br>
                                          <span>Financial Advisor</span>
                                       </div>                                       
                                    </td>
                                    <td>
                                        <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Person To Contact : <span class="text-muted">Faith Bratlie</span></span><br>
                                          <span>Phone : <span class="text-muted">+1 470-385-2707</span></span><br>
                                          <span>Email : <span class="text-muted">fbratlie@myhst.com</span></span><br>
                                          <span>Company : <span class="text-muted">.....</span></span><br>
                                       </div>
                                    </td>
                                    <!--td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral Details: </span><br>
                                       </div>
                                       <div class="font-12">
                                          <span>Lorem Ipsum is simply dummy text.</span>
                                          <br> 
                                          <span>Lorem Ipsum is simply dummy text.</span>
                                       </div>
                                    </td--> 
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          
                                          <span>Referral Type : <span class="text-muted">Affiliate</span></span><br>
                                          <span>Value : <span class="text-muted">....</span></span><br>
                                          <span>Status : <span class="text-muted">New</span></span>
                                       </div>
                                    </td>
                                    <td>
                                       <button style="margin-left:0px !important" type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#exampleModal">View</button>
                                       <!--button type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#editModal">View</button-->                                       
                                    </td>                                    
                                    
                                 </tr>
                                 <tr>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral To :</span><br>
                                       </div>
                                       <div class="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name">
                                          <img class="" src="../assets/images/xs/avatar1.jpg" alt="">
                                       </div>
                                       <div class="font-12">
                                          <span>Alice Agyemang-Badu</span><br>
                                          <span>Business Advisor</span>
                                       </div>
                                    </td>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral From :</span><br>
                                       </div>
                                        <div class="avatar avatar-pink" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name">
                                          <span>GH</span>
                                       </div>
                                       <div class="font-12">
                                          <span>Celestina Ando</span><br>
                                          <span>Financial Advisor</span>
                                       </div>                                       
                                    </td>
                                    <td>
                                        <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Person To Contact : <span class="text-muted">Faith Bratlie</span></span><br>
                                          <span>Phone : <span class="text-muted">+1 470-385-2707</span></span><br>
                                          <span>Email : <span class="text-muted">fbratlie@myhst.com</span></span><br>
                                          <span>Company : <span class="text-muted">.....</span></span><br>
                                       </div>
                                    </td>
                                    <!--td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral Details: </span><br>
                                       </div>
                                       <div class="font-12">
                                          <span>Lorem Ipsum is simply dummy text.</span>
                                          <br> 
                                          <span>Lorem Ipsum is simply dummy text.</span>
                                       </div>
                                    </td-->                                   
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          
                                          <span>Referral Type : <span class="text-muted">Affiliate</span></span><br>
                                          <span>Value : <span class="text-muted">....</span></span><br>
                                          <span>Status : <span class="text-muted">New</span></span>
                                          
                                       </div>
                                    </td>
                                    <td>
                                       <button style="margin-left:0px !important" type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#exampleModal">View</button>
                                       <!--button type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#editModal">View</button-->                                       
                                    </td>                                    
                                    
                                 </tr>
                                 <tr>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral To :</span><br>
                                       </div>
                                       <div class="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name">
                                          <img class="" src="../assets/images/xs/avatar1.jpg" alt="">
                                       </div>
                                       <div class="font-12">
                                          <span>Alice Agyemang-Badu</span><br>
                                          <span>Business Advisor</span>
                                       </div>
                                    </td>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral From :</span><br>
                                       </div>
                                        <div class="avatar avatar-pink" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name">
                                          <span>GH</span>
                                       </div>
                                       <div class="font-12">
                                          <span>Celestina Ando</span><br>
                                          <span>Financial Advisor</span>
                                       </div>                                       
                                    </td>
                                    <td>
                                        <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Person To Contact : <span class="text-muted">Faith Bratlie</span></span><br>
                                          <span>Phone : <span class="text-muted">+1 470-385-2707</span></span><br>
                                          <span>Email : <span class="text-muted">fbratlie@myhst.com</span></span><br>
                                          <span>Company : <span class="text-muted">.....</span></span><br>
                                       </div>
                                    </td>
                                    <!--td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral Details: </span><br>
                                       </div>
                                       <div class="font-12">
                                          <span>Lorem Ipsum is simply dummy text.</span>
                                          <br> 
                                          <span>Lorem Ipsum is simply dummy text.</span>
                                       </div>
                                    </td-->                                    
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">                                         
                                          <span>Referral Type : <span class="text-muted">Guest</span></span><br>
                                          <span>Value : <span class="text-muted">....</span></span><br>
                                          <span>Status : <span class="text-muted">New</span></span>
                                          
                                       </div>
                                    </td>
                                    <td>
                                       <button style="margin-left:0px !important" type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#exampleModal">View</button>
                                       <!--button type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#editModal">View</button-->                                       
                                    </td>                                    
                                    
                                 </tr>
                                 <tr>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral To :</span><br>
                                       </div>
                                       <div class="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name">
                                          <img class="" src="../assets/images/xs/avatar1.jpg" alt="">
                                       </div>
                                       <div class="font-12">
                                          <span>Alice Agyemang-Badu</span><br>
                                          <span>Business Advisor</span>
                                       </div>

                                    </td>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral From :</span><br>
                                       </div>
                                        <div class="avatar avatar-pink" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name">
                                          <span>GH</span>
                                       </div>
                                       <div class="font-12">
                                          <span>Celestina Ando</span><br>
                                          <span>Financial Advisor</span>
                                       </div>                                       
                                    </td>
                                    <td>
                                        <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Person To Contact : <span class="text-muted">Faith Bratlie</span></span><br>
                                          <span>Phone : <span class="text-muted">+1 470-385-2707</span></span><br>
                                          <span>Email : <span class="text-muted">fbratlie@myhst.com</span></span><br>
                                          <span>Company : <span class="text-muted">.....</span></span><br>
                                       </div>
                                    </td>
                                    <!--td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral Details: </span><br>
                                       </div>
                                       <div class="font-12">
                                          <span>Lorem Ipsum is simply dummy text.</span>
                                          <br> 
                                          <span>Lorem Ipsum is simply dummy text.</span>
                                       </div>
                                    </td-->                                    
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral Type : <span class="text-muted">Guest</span></span><br>
                                          <span>Value : <span class="text-muted">....</span></span><br>
                                          <span>Status : <span class="text-muted">New</span></span>
                                       </div>
                                    </td>
                                    <td class="text-center">
                                       <!--button type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#editModal">Edit</button-->
                                       <button style="margin-left:0px !important" type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#exampleModal">View</button>                                      
                                    </td>                                                                                                   
                                 </tr>

                                 <tr>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral To :</span><br>
                                       </div>
                                       <div class="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name">
                                          <img class="" src="../assets/images/xs/avatar1.jpg" alt="">
                                       </div>
                                       <div class="font-12">
                                          <span>Alice Agyemang-Badu</span><br>
                                          <span>Business Advisor</span>
                                       </div>
                                    </td>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral From :</span><br>
                                       </div>
                                        <div class="avatar avatar-pink" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name">
                                          <span>GH</span>
                                       </div>
                                       <div class="font-12">
                                          <span>Celestina Ando</span><br>
                                          <span>Financial Advisor</span>
                                       </div>                                       
                                    </td>
                                    <td>
                                        <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Person To Contact : <span class="text-muted">Faith Bratlie</span></span><br>
                                          <span>Phone : <span class="text-muted">+1 470-385-2707</span></span><br>
                                          <span>Email : <span class="text-muted">fbratlie@myhst.com</span></span><br>
                                          <span>Company : <span class="text-muted">.....</span></span><br>
                                       </div>
                                    </td>
                                    <!--td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral Details: </span><br>
                                       </div>
                                       <div class="font-12">
                                          <span>Lorem Ipsum is simply dummy text.</span>
                                          <br> 
                                          <span>Lorem Ipsum is simply dummy text.</span>
                                       </div>
                                    </td--> 
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          
                                          <span>Referral Type : <span class="text-muted">Affiliate</span></span><br>
                                          <span>Value : <span class="text-muted">....</span></span><br>
                                          <span>Status : <span class="text-muted">New</span></span>
                                       </div>
                                    </td>
                                    <td>
                                       <button style="margin-left:0px !important" type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#exampleModal">View</button>
                                       <!--button type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#editModal">View</button-->                                       
                                    </td>                                    
                                    
                                 </tr>

                                 <tr>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral To :</span><br>
                                       </div>
                                       <div class="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name">
                                          <img class="" src="../assets/images/xs/avatar1.jpg" alt="">
                                       </div>
                                       <div class="font-12">
                                          <span>Alice Agyemang-Badu</span><br>
                                          <span>Business Advisor</span>
                                       </div>
                                    </td>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral From :</span><br>
                                       </div>
                                        <div class="avatar avatar-pink" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name">
                                          <span>GH</span>
                                       </div>
                                       <div class="font-12">
                                          <span>Celestina Ando</span><br>
                                          <span>Financial Advisor</span>
                                       </div>                                       
                                    </td>
                                    <td>
                                        <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Person To Contact : <span class="text-muted">Faith Bratlie</span></span><br>
                                          <span>Phone : <span class="text-muted">+1 470-385-2707</span></span><br>
                                          <span>Email : <span class="text-muted">fbratlie@myhst.com</span></span><br>
                                          <span>Company : <span class="text-muted">.....</span></span><br>
                                       </div>
                                    </td>
                                    <!--td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral Details: </span><br>
                                       </div>
                                       <div class="font-12">
                                          <span>Lorem Ipsum is simply dummy text.</span>
                                          <br> 
                                          <span>Lorem Ipsum is simply dummy text.</span>
                                       </div>
                                    </td--> 
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          
                                          <span>Referral Type : <span class="text-muted">Affiliate</span></span><br>
                                          <span>Value : <span class="text-muted">....</span></span><br>
                                          <span>Status : <span class="text-muted">New</span></span>
                                       </div>
                                    </td>
                                    <td>
                                       <button style="margin-left:0px !important" type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#exampleModal">View</button>
                                       <!--button type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#editModal">View</button-->                                       
                                    </td>                                    
                                    
                                 </tr>

                                 <tr>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral To :</span><br>
                                       </div>
                                       <div class="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name">
                                          <img class="" src="../assets/images/xs/avatar1.jpg" alt="">
                                       </div>
                                       <div class="font-12">
                                          <span>Alice Agyemang-Badu</span><br>
                                          <span>Business Advisor</span>
                                       </div>
                                    </td>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral From :</span><br>
                                       </div>
                                        <div class="avatar avatar-pink" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name">
                                          <span>GH</span>
                                       </div>
                                       <div class="font-12">
                                          <span>Celestina Ando</span><br>
                                          <span>Financial Advisor</span>
                                       </div>                                       
                                    </td>
                                    <td>
                                        <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Person To Contact : <span class="text-muted">Faith Bratlie</span></span><br>
                                          <span>Phone : <span class="text-muted">+1 470-385-2707</span></span><br>
                                          <span>Email : <span class="text-muted">fbratlie@myhst.com</span></span><br>
                                          <span>Company : <span class="text-muted">.....</span></span><br>
                                       </div>
                                    </td>
                                    <!--td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral Details: </span><br>
                                       </div>
                                       <div class="font-12">
                                          <span>Lorem Ipsum is simply dummy text.</span>
                                          <br> 
                                          <span>Lorem Ipsum is simply dummy text.</span>
                                       </div>
                                    </td--> 
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          
                                          <span>Referral Type : <span class="text-muted">Affiliate</span></span><br>
                                          <span>Value : <span class="text-muted">....</span></span><br>
                                          <span>Status : <span class="text-muted">New</span></span>
                                       </div>
                                    </td>
                                    <td>
                                       <button style="margin-left:0px !important" type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#exampleModal">View</button>
                                       <!--button type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#editModal">View</button-->                                       
                                    </td>                                    
                                    
                                 </tr>

                                 <tr>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral To :</span><br>
                                       </div>
                                       <div class="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name">
                                          <img class="" src="../assets/images/xs/avatar1.jpg" alt="">
                                       </div>
                                       <div class="font-12">
                                          <span>Alice Agyemang-Badu</span><br>
                                          <span>Business Advisor</span>
                                       </div>
                                    </td>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral From :</span><br>
                                       </div>
                                        <div class="avatar avatar-pink" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name">
                                          <span>GH</span>
                                       </div>
                                       <div class="font-12">
                                          <span>Celestina Ando</span><br>
                                          <span>Financial Advisor</span>
                                       </div>                                       
                                    </td>
                                    <td>
                                        <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Person To Contact : <span class="text-muted">Faith Bratlie</span></span><br>
                                          <span>Phone : <span class="text-muted">+1 470-385-2707</span></span><br>
                                          <span>Email : <span class="text-muted">fbratlie@myhst.com</span></span><br>
                                          <span>Company : <span class="text-muted">.....</span></span><br>
                                       </div>
                                    </td>
                                    <!--td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral Details: </span><br>
                                       </div>
                                       <div class="font-12">
                                          <span>Lorem Ipsum is simply dummy text.</span>
                                          <br> 
                                          <span>Lorem Ipsum is simply dummy text.</span>
                                       </div>
                                    </td--> 
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          
                                          <span>Referral Type : <span class="text-muted">Affiliate</span></span><br>
                                          <span>Value : <span class="text-muted">....</span></span><br>
                                          <span>Status : <span class="text-muted">New</span></span>
                                       </div>
                                    </td>
                                    <td>
                                       <button style="margin-left:0px !important" type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#exampleModal">View</button>
                                       <!--button type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#editModal">View</button-->                                       
                                    </td>                                    
                                    
                                 </tr>

                                 <tr>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral To :</span><br>
                                       </div>
                                       <div class="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name">
                                          <img class="" src="../assets/images/xs/avatar1.jpg" alt="">
                                       </div>
                                       <div class="font-12">
                                          <span>Alice Agyemang-Badu</span><br>
                                          <span>Business Advisor</span>
                                       </div>
                                    </td>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral From :</span><br>
                                       </div>
                                        <div class="avatar avatar-pink" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name">
                                          <span>GH</span>
                                       </div>
                                       <div class="font-12">
                                          <span>Celestina Ando</span><br>
                                          <span>Financial Advisor</span>
                                       </div>                                       
                                    </td>
                                    <td>
                                        <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Person To Contact : <span class="text-muted">Faith Bratlie</span></span><br>
                                          <span>Phone : <span class="text-muted">+1 470-385-2707</span></span><br>
                                          <span>Email : <span class="text-muted">fbratlie@myhst.com</span></span><br>
                                          <span>Company : <span class="text-muted">.....</span></span><br>
                                       </div>
                                    </td>
                                    <!--td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral Details: </span><br>
                                       </div>
                                       <div class="font-12">
                                          <span>Lorem Ipsum is simply dummy text.</span>
                                          <br> 
                                          <span>Lorem Ipsum is simply dummy text.</span>
                                       </div>
                                    </td--> 
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          
                                          <span>Referral Type : <span class="text-muted">Guest</span></span><br>
                                          <span>Value : <span class="text-muted">....</span></span><br>
                                          <span>Status : <span class="text-muted">New</span></span>
                                       </div>
                                    </td>
                                    <td>
                                       <button style="margin-left:0px !important" type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#exampleModal">View</button>
                                       <!--button type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#editModal">View</button-->                                       
                                    </td>                                    
                                    
                                 </tr>

                                 <tr>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral To :</span><br>
                                       </div>
                                       <div class="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name">
                                          <img class="" src="../assets/images/xs/avatar1.jpg" alt="">
                                       </div>
                                       <div class="font-12">
                                          <span>Alice Agyemang-Badu</span><br>
                                          <span>Business Advisor</span>
                                       </div>
                                    </td>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral From :</span><br>
                                       </div>
                                        <div class="avatar avatar-pink" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name">
                                          <span>GH</span>
                                       </div>
                                       <div class="font-12">
                                          <span>Celestina Ando</span><br>
                                          <span>Financial Advisor</span>
                                       </div>                                       
                                    </td>
                                    <td>
                                        <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Person To Contact : <span class="text-muted">Faith Bratlie</span></span><br>
                                          <span>Phone : <span class="text-muted">+1 470-385-2707</span></span><br>
                                          <span>Email : <span class="text-muted">fbratlie@myhst.com</span></span><br>
                                          <span>Company : <span class="text-muted">.....</span></span><br>
                                       </div>
                                    </td>
                                    <!--td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral Details: </span><br>
                                       </div>
                                       <div class="font-12">
                                          <span>Lorem Ipsum is simply dummy text.</span>
                                          <br> 
                                          <span>Lorem Ipsum is simply dummy text.</span>
                                       </div>
                                    </td--> 
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          
                                          <span>Referral Type : <span class="text-muted">Affiliate</span></span><br>
                                          <span>Value : <span class="text-muted">....</span></span><br>
                                          <span>Status : <span class="text-muted">New</span></span>
                                       </div>
                                    </td>
                                    <td>
                                       <button style="margin-left:0px !important" type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#exampleModal">View</button>
                                       <!--button type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#editModal">View</button-->                                       
                                    </td>                                    
                                    
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                        <ul class="pagination mt-2">
                           <li class="page-item"><a class="page-link" href="javascript:void(0);">Previous</a></li>
                           <li class="page-item active"><a class="page-link" href="javascript:void(0);">1</a></li>
                           <li class="page-item"><a class="page-link" href="javascript:void(0);">2</a></li>
                           <li class="page-item"><a class="page-link" href="javascript:void(0);">3</a></li>
                           <li class="page-item"><a class="page-link" href="javascript:void(0);">Next</a></li>
                        </ul>

                        </div>
                        </div>

                     </div>
                  </div>
               <!--/div-->
               <!-- Sent Referral end here -->


            </div>
         </div>
      </div>
   </div>
</div>

@stop




@section('popup')
<div class="modal fade " id="editModal" tabindex="-1" role="dialog">
   
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Referral</h5>
                    <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>-->
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
                <div class="modal-body">

                        <form>
                          <div class="row clearfix">

                              <!--div class="col-lg-6 col-md-6">
                                 <div class="form-group">
                                    <input type="text" data-provide="datepicker" data-date-autoclose="true" class="form-control" placeholder="Start date *">
                                 </div>
                              </div-->

                              <!--div class="col-lg-6 col-md-6">
                                 <div class="form-group">
                                       <select class="custom-select">
                                          <option selected="">Referral Type</option>
                                          <option value="1">Inside</option>
                                          <option value="2">OutSide</option>
                                       </select>
                                 </div>
                              </div-->

                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <input type="text" class="form-control" placeholder="Person To Contact">
                                  </div>
                              </div>

                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">                                   
                                      <input type="text" class="form-control" placeholder="Company Name">
                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <input type="text" class="form-control" placeholder="Phone Number">
                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <input type="text" class="form-control" placeholder="Email Address">
                                  </div>
                              </div>

                              <div class="col-lg-12 col-md-12">
                                  <div class="form-group">
                                     <textarea rows="6" class="form-control" name="" id="" placeholder="Referral Details"></textarea>
                                  </div>
                              </div>

                              
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <input type="text" class="form-control" placeholder="$">
                                  </div>
                              </div>

                             <div class="col-lg-6 col-md-6">
                                 <div class="form-group">
                                       <select class="custom-select">
                                          <option selected="">Status</option>
                                          <option value="1">New</option>
                                          <option value="2">Bogus/Invalid</option>
                                          <option value="2">Closed</option>
                                          <option value="2">Working</option>
                                       </select>
                                 </div>
                              </div>

                              <!--div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1">
                                        <span class="custom-control-label">This is a private Referral</span>
                                        <span style="position: absolute; margin: 0 0 0 12px;"><i class="fa fa-question-circle" style="font-size: 22px;"></i></span>
                                    </label>

                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      
                                  </div>
                              </div-->

                          </div>
                        </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary">Submit Referral</button>
                </div>
            </div>
        </div>
</div>

<div class="modal fade " id="exampleModal" tabindex="-1" role="dialog">
   
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Referral Details</h5>
                    <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>-->
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
                <div class="modal-body">

                        <form>
                          <div class="row clearfix">

                           <style type="text/css">
                                 .tip-form-header{box-sizing: border-box; background-color: #e0a258; color: #fff; padding: 1rem;width: 100%;}
                                 .tip-form-header .tip-form-member{display: -webkit-box;display: flex; -webkit-box-flex: 1; flex: auto; align-items: center;
                                 }
                              .tip-form-header .profile-image{width: 120px; height: 120px; object-fit: cover; object-position: top center; margin-right: 20px; background-color: #ccc;}

                           </style>
                              <div class="col-lg-6 col-md-6">
                              <div class="tip-form-header">
                                 <h5>Referral From:</h5>
                                 <div class="tip-form-member">
                                    <img class="profile-image" src="../assets/images/xs/avatar1.jpg" alt="">
                                    <div class="member-info">
                                       <div class="member-name">Alice Agyemang-Badu</div>
                                       <div class="business-name">New York Life</div>
                                       <div class="text-muted category-name">Recruiter</div>
                                       <div class="chapter-name">Besure of Upper Montclair, NJ</div>
                                    </div>
                                 </div>
                              </div>
                              </div>

                              <div class="col-lg-6 col-md-6">
                              <div class="tip-form-header">
                                 <h5>Referral To:</h5>
                                 <div class="tip-form-member">
                                    <img class="profile-image" src="../assets/images/xs/avatar1.jpg" alt="">
                                    <div class="member-info">
                                       <div class="member-name">Alice Agyemang-Badu</div>
                                       <div class="business-name">New York Life</div>
                                       <div class="text-muted category-name">Recruiter</div>
                                       <div class="chapter-name">Besure of Upper Montclair, NJ</div>
                                    </div>
                                 </div>
                              </div>
                              </div>

                              <!--div class="text-center" style="padding: 10px; font-size: 90%; font-weight: 500;"><p>My qualified Business Referral is for a company or person who is interested in a specific service or product and is expecting a call from a Besure member.</p></div-->

                              <!--div class="col-lg-12 col-md-12">
                                 <div class="form-group">
                                    <!-input type="text" data-provide="datepicker" data-date-autoclose="true" class="form-control" placeholder="Start date *"->
                                    <input type="text" class="form-control" placeholder="Referral date *" value="{{ date('d/m/Y') }}" readonly>
                                 </div>
                              </div-->

                              <!--div class="col-lg-6 col-md-6">
                                 <div class="form-group">
                                       <select class="custom-select">
                                          <option selected="">Referral Type</option>
                                          <option value="1">Inside</option>
                                          <option value="2">OutSide</option>
                                       </select>
                                 </div>
                              </div-->

                              <div class="col-lg-6 col-md-6" style="margin-top:20px;">
                                 <label>Person To Contact</label>
                                  <div class="form-group">
                                      <input type="text" class="form-control" placeholder="Person To Contact *" value="Faith Bratlie" readonly>
                                  </div>
                              </div>

                              <div class="col-lg-6 col-md-6" style="margin-top:20px;">
                                 <label>Company Name</label>
                                  <div class="form-group">                                   
                                      <input type="text" class="form-control" placeholder="Company Name *"  value="....."readonly>
                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                                 <label>Phone Number</label>
                                  <div class="form-group">
                                      <input type="text" class="form-control" placeholder="Phone Number #" value="+1 470-385-2707" readonly>
                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                                 <label>Email Address</label>
                                  <div class="form-group">
                                      <input type="text" class="form-control" placeholder="Email Address #" value="fbratlie@myhst.com" readonly>
                                  </div>
                              </div>

                              <div class="col-lg-12 col-md-12">
                                 <label>Referral Details</label>
                                  <div class="form-group">
                                     <textarea rows="6" class="form-control" name="" id="" placeholder="Referral Details" readonly>Lorem Ipsum is simplay a dummy text.Lorem Ipsum is simplay a dummy text. Lorem Ipsum is simplay a dummy text. Lorem Ipsum is simplay a dummy text. Lorem Ipsum is simplay a dummy text.</textarea>
                                  </div>
                              </div>

                              <!--div class="col-lg-12 col-md-12">
                                  <div class="form-group">
                                      <span><span class="redstar">*<span> Indicates the required fields</span>
                                      <span style="float:right;"><span class="redstar">#<span> any of phone or email is required in valid format</span>
                                  </div>
                              </div-->

                              <!--div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1">
                                        <span class="custom-control-label">Specify the Referral value</span>
                                    </label>
                                     
                                  </div>
                              </div-->

                              <!--div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1">
                                        <span class="custom-control-label">This is a private Referral</span>
                                        <span style="position: absolute; margin: 0 0 0 12px;"><i class="fa fa-question-circle" style="font-size: 22px;"></i></span>
                                    </label>

                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      
                                  </div>
                              </div-->

                          </div>
                        </form>
                </div>
                <!--div class="modal-footer">
                    <button type="button" class="btn btn-secondary">Close</button>
                </div-->
            </div>
        </div>
</div>
@stop


@section('page-styles')

<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }} ">

@stop
@section('page-script')
<script src="{{ asset('assets/bundles/apexcharts.bundle.js') }}"></script>
<script src="{{ asset('assets/bundles/counterup.bundle.js') }}"></script>
<script src="{{ asset('assets/bundles/knobjs.bundle.js') }}"></script>
<script src="{{ asset('assets/js/core.js') }}"></script>
<script src="{{ asset('assets/js/page/index.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
@stop
