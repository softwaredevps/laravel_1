@extends('layout.member_master')
@section('title', 'Referral')
@section('content')
<div class="col-lg-12 col-md-12">
   <div class="col-lg-12 col-md-12">
      
      <div class="section-body mt-3">
         <div class="container-fluid">
            <div class="tab-content mt-3">
               
              
               <!-- Received Referral start here -->

               
                  <div class="row clearfix">
                     <div class="col-12">
                        <div class="card">
                           <div class="card-body">
                              <div class="row">
                                 <div class="col-lg-4 col-md-4 col-sm-6">
                                    <label>Start Date</label>
                                    <div class="input-group">
                                       <input type="text" data-provide="datepicker" data-date-autoclose="true" class="form-control" placeholder="Start date *">
                                    </div>
                                 </div>
                                 <div class="col-lg-4 col-md-4 col-sm-6">
                                    <label>End Date</label>
                                    <div class="input-group">
                                       <input type="text" data-provide="datepicker" data-date-autoclose="true" class="form-control" placeholder="End date *">
                                    </div>
                                 </div>
                                 <div class="col-lg-3 col-md-4 col-sm-6">
                                    <label>Status</label>
                                    <div class="form-group">
                                       <select class="custom-select">
                                          <option selected="">Select..</option>
                                          <option value="1">Bogus/Invalid</option>
                                          <option value="2">Closed</option>
                                          <option value="3">Working</option>
                                          <option value="3">Sort by name</option>
                                       </select>
                                    </div>
                                 </div>
                                 
                              </div>
                        <div class="table-responsive">
                           <table class="table table-hover table-vcenter table_custom text-nowrap spacing5 border-style mb-0">

                              <thead>
                                  <tr>
                                      <th></th>
                                      <th></th>
                                      <th></th>
                                      <th></th>
                                      <th></th>
                                  </tr>
                              </thead>
                              <tbody>
                                 <tr>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral To :</span><br>
                                       </div>
                                       <div class="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name">
                                          <img class="" src="../assets/images/xs/avatar1.jpg" alt="">
                                       </div>
                                       <div class="font-12">
                                          <span>Alice Agyemang-Badu</span><br>
                                          <span>Business Advisor</span>
                                       </div>
                                    </td>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral From :</span><br>
                                       </div>
                                        <div class="avatar avatar-pink" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name">
                                          <span>GH</span>
                                       </div>
                                       <div class="font-12">
                                          <span>Celestina Ando</span><br>
                                          <span>Financial Advisor</span>
                                       </div>                                       
                                    </td>
                                    <td>
                                        <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Person To Contact : <span class="text-muted">Faith Bratlie</span></span><br>
                                          <span>Phone : <span class="text-muted">+1 470-385-2707</span></span><br>
                                          <span>Email : <span class="text-muted">fbratlie@myhst.com</span></span><br>
                                          <span>Company : <span class="text-muted">.....</span></span><br>
                                       </div>
                                    </td>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral Type : <span class="text-muted">Inter-Chapter</span></span><br>
                                          <span>Value : <span class="text-muted">....</span></span><br>
                                          <span>Status : <span class="text-muted">New</span></span>
                                       </div>
                                    </td>
                                    <td>
                                       <button type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#editModal">Edit</button>

                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral To :</span><br>
                                       </div>
                                       <div class="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name">
                                          <img class="" src="../assets/images/xs/avatar1.jpg" alt="">
                                       </div>
                                       <div class="font-12">
                                          <span>Alice Agyemang-Badu</span><br>
                                          <span>Business Advisor</span>
                                       </div>
                                    </td>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral From :</span><br>
                                       </div>
                                        <div class="avatar avatar-pink" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name">
                                          <span>GH</span>
                                       </div>
                                       <div class="font-12">
                                          <span>Celestina Ando</span><br>
                                          <span>Financial Advisor</span>
                                       </div>                                       
                                    </td>
                                    <td>
                                        <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Person To Contact : <span class="text-muted">Faith Bratlie</span></span><br>
                                          <span>Phone : <span class="text-muted">+1 470-385-2707</span></span><br>
                                          <span>Email : <span class="text-muted">fbratlie@myhst.com</span></span><br>
                                          <span>Company : <span class="text-muted">.....</span></span><br>
                                       </div>
                                    </td>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          
                                          <span>Referral Type : <span class="text-muted">Inter-Chapter</span></span><br>
                                          <span>Value : <span class="text-muted">....</span></span><br>
                                          <span>Status : <span class="text-muted">New</span></span>
                                          
                                       </div>
                                    </td>
                                    <td>
                                       <button type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#editModal">Edit</button>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral To :</span><br>
                                       </div>
                                       <div class="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name">
                                          <img class="" src="../assets/images/xs/avatar1.jpg" alt="">
                                       </div>
                                       <div class="font-12">
                                          <span>Alice Agyemang-Badu</span><br>
                                          <span>Business Advisor</span>
                                       </div>
                                    </td>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral From :</span><br>
                                       </div>
                                        <div class="avatar avatar-pink" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name">
                                          <span>GH</span>
                                       </div>
                                       <div class="font-12">
                                          <span>Celestina Ando</span><br>
                                          <span>Financial Advisor</span>
                                       </div>                                       
                                    </td>
                                    <td>
                                        <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Person To Contact : <span class="text-muted">Faith Bratlie</span></span><br>
                                          <span>Phone : <span class="text-muted">+1 470-385-2707</span></span><br>
                                          <span>Email : <span class="text-muted">fbratlie@myhst.com</span></span><br>
                                          <span>Company : <span class="text-muted">.....</span></span><br>
                                       </div>
                                    </td>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                         
                                          <span>Referral Type : <span class="text-muted">Inter-Chapter</span></span><br>
                                          <span>Value : <span class="text-muted">....</span></span><br>
                                          <span>Status : <span class="text-muted">New</span></span>
                                          
                                       </div>
                                    </td>
                                    <td>
                                       <button type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#editModal">Edit</button>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral To :</span><br>
                                       </div>
                                       <div class="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name">
                                          <img class="" src="../assets/images/xs/avatar1.jpg" alt="">
                                       </div>
                                       <div class="font-12">
                                          <span>Alice Agyemang-Badu</span><br>
                                          <span>Business Advisor</span>
                                       </div>

                                    </td>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral From :</span><br>
                                       </div>
                                        <div class="avatar avatar-pink" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name">
                                          <span>GH</span>
                                       </div>
                                       <div class="font-12">
                                          <span>Celestina Ando</span><br>
                                          <span>Financial Advisor</span>
                                       </div>                                       
                                    </td>
                                    <td>
                                        <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Person To Contact : <span class="text-muted">Faith Bratlie</span></span><br>
                                          <span>Phone : <span class="text-muted">+1 470-385-2707</span></span><br>
                                          <span>Email : <span class="text-muted">fbratlie@myhst.com</span></span><br>
                                          <span>Company : <span class="text-muted">.....</span></span><br>
                                       </div>
                                    </td>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral Type : <span class="text-muted">Inter-Chapter</span></span><br>
                                          <span>Value : <span class="text-muted">....</span></span><br>
                                          <span>Status : <span class="text-muted">New</span></span>
                                       </div>
                                    </td>
                                    
                                    <td>
                                       <button type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#editModal">Edit</button>
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                        <ul class="pagination mt-2">
                           <li class="page-item"><a class="page-link" href="javascript:void(0);">Previous</a></li>
                           <li class="page-item active"><a class="page-link" href="javascript:void(0);">1</a></li>
                           <li class="page-item"><a class="page-link" href="javascript:void(0);">2</a></li>
                           <li class="page-item"><a class="page-link" href="javascript:void(0);">3</a></li>
                           <li class="page-item"><a class="page-link" href="javascript:void(0);">Next</a></li>
                        </ul>

                        </div>
                        </div>

                     </div>
                  </div>
               
               <!-- Received Referral end here -->

               


            </div>
         </div>
      </div>
   </div>
</div>

@stop




@section('popup')
<div class="modal fade " id="editModal" tabindex="-1" role="dialog">
   
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Referral</h5>
                    <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>-->
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
                <div class="modal-body">

                        <form>
                          <div class="row clearfix">

                              <div class="col-lg-6 col-md-6">
                                 <div class="form-group">
                                    <input type="text" data-provide="datepicker" data-date-autoclose="true" class="form-control" placeholder="Start date *">
                                 </div>
                              </div>

                              <div class="col-lg-6 col-md-6">
                                 <div class="form-group">
                                       <select class="custom-select">
                                          <option selected="">Referral Type</option>
                                          <option value="1">Inside</option>
                                          <option value="2">OutSide</option>
                                       </select>
                                 </div>
                              </div>

                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <input type="text" class="form-control" placeholder="Person To Contact">
                                  </div>
                              </div>

                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">                                   
                                      <input type="text" class="form-control" placeholder="Company Name">
                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <input type="text" class="form-control" placeholder="Phone Number">
                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <input type="text" class="form-control" placeholder="Email Address">
                                  </div>
                              </div>

                              <div class="col-lg-12 col-md-12">
                                  <div class="form-group">
                                     <textarea class="form-control" name="" id="" placeholder="Referral Details"></textarea>
                                  </div>
                              </div>

                              
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <input type="text" class="form-control" placeholder="$">
                                  </div>
                              </div>

                             <div class="col-lg-6 col-md-6">
                                 <div class="form-group">
                                       <select class="custom-select">
                                          <option selected="">Status</option>
                                          <option value="1">New</option>
                                          <option value="2">Bogus/Invalid</option>
                                          <option value="2">Closed</option>
                                          <option value="2">Working</option>
                                       </select>
                                 </div>
                              </div>

                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1">
                                        <span class="custom-control-label">This is a private Referral</span>
                                        <span style="position: absolute; margin: 0 0 0 12px;"><i class="fa fa-question-circle" style="font-size: 22px;"></i></span>
                                    </label>

                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      
                                  </div>
                              </div>

                          </div>
                        </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary">Submit Referral</button>
                </div>
            </div>
        </div>
</div>


@stop


@section('page-styles')

<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }} ">

@stop
@section('page-script')
<script src="{{ asset('assets/bundles/apexcharts.bundle.js') }}"></script>
<script src="{{ asset('assets/bundles/counterup.bundle.js') }}"></script>
<script src="{{ asset('assets/bundles/knobjs.bundle.js') }}"></script>
<script src="{{ asset('assets/js/core.js') }}"></script>
<script src="{{ asset('assets/js/page/index.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
@stop
