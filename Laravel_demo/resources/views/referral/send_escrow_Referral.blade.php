@extends('layout.member_master')
@section('title', 'Send  Escrow Referral')
@section('content')
<div class="col-lg-12 col-md-12">
   <div class="col-lg-12 col-md-12">
      
      <div class="section-body mt-3">
         <div class="container-fluid">
            <div class="tab-content mt-3">
               
               

              <!-- Send An Escrow Referral start here -->
               
                     <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">123 6th St. Melbourne, FL 32904</h3>
                            </div>
                            <div class="card-body">
                                <form method="post" id="">
                                    <div class="row clearfix">
                                       <div class="col-lg-12 col-lg-12">
                                          <label>Referral For<span class="redstar">*<span> <span <i class="fa fa-question-circle" style="font-size: 22px;color: black;position: absolute;margin: 0 0 0 12px;"></i></span></label>

                                          <div class="form-group">
                                             <select class="custom-select">
                                                <option value="0">Select Chapter</option>
                                                <option value="1">Business Consultant</option>
                                                <option value="2">Financial Advisor</option>
                                                <option value="3">Business Advisor</option>
                                             </select>
                                          </div>

                                          <p class="text-danger">Escrow Referral are not given out until the guest joins the chapter.</p>
                                       </div>
                                       <div class="col-lg-6 col-md-6">
                                          <label>Date<span class="redstar">*<span></label>
                                          <div class="form-group">
                                             <input name="" data-provide="datepicker" data-date-autoclose="true" class="form-control" placeholder="Select Appointment">
                                          </div>
                                       </div>
                                       <div class="col-lg-6 col-md-6">
                                          <label>Referral Type</label>
                                          <div class="form-group">
                                             <select class="custom-select">
                                                <option value="0">Referral Type</option>
                                                <option value="1">Inside</option>
                                                <option value="2">OutSide</option>
                                             </select>
                                          </div>
                                       </div>
                                       <div class="col-lg-6 col-md-6">
                                          <label>Person To Contact</label>
                                          <div class="form-group">
                                             <input type="text" class="form-control" placeholder="Person To Contact">
                                          </div>
                                       </div>
                                       <div class="col-lg-6 col-md-6">
                                          <label>Company Name</label>
                                          <div class="form-group">
                                             <input type="text" class="form-control" placeholder="Company Name">
                                          </div>
                                       </div>
                                      <div class="col-lg-6 col-md-6">
                                          <label>Phone Number</label>
                                          <div class="form-group">
                                             <input type="text" class="form-control" placeholder="Phone Number">
                                          </div>
                                       </div>
                                       <div class="col-lg-6 col-md-6">
                                          <label>Email Address</label>
                                          <div class="form-group">
                                             <input type="text" class="form-control" placeholder="Email Address">
                                          </div>
                                       </div>
                                       <div class="col-lg-12 col-md-12">
                                          <label>Referral Details</label>
                                          <div class="form-group">
                                             <textarea rows="8" cols="100" type="text" class="form-control" placeholder="Details" name="terminationRemarks"></textarea>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1">
                                        <span class="custom-control-label">Specify the Referral value</span>
                                    </label>
                                     
                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      
                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1">
                                        <span class="custom-control-label">This is a private Referral</span>
                                        <span style="position: absolute; margin: 0 0 0 12px;"><i class="fa fa-question-circle" style="font-size: 22px;"></i></span>
                                    </label>

                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      
                                  </div>
                              </div>


                                    <button type="button" class="btn btn-round btn-primary">Submit Referral </button>
                                    <div class="col-lg-10 col-md-10 star-indigate"><span class="redstar">*<span> Indicates the required fields</span></span></div>
                                </form>
                           </div>
                     </div>
              
               <!-- Send An Escrow Referral end here -->

             


            </div>
         </div>
      </div>
   </div>
</div>

@stop




@section('popup')

@stop


@section('page-styles')

<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }} ">

@stop
@section('page-script')
<script src="{{ asset('assets/bundles/apexcharts.bundle.js') }}"></script>
<script src="{{ asset('assets/bundles/counterup.bundle.js') }}"></script>
<script src="{{ asset('assets/bundles/knobjs.bundle.js') }}"></script>
<script src="{{ asset('assets/js/core.js') }}"></script>
<script src="{{ asset('assets/js/page/index.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
@stop
