@extends('layout.member_master')
@section('title', 'Referral')
@section('content')
<div class="col-lg-12 col-md-12">
   <div class="col-lg-12 col-md-12">
      <div class="section-body">
         <div class="container-fluid">
            <div class="d-flex justify-content-between align-items-center">
               <ul class="nav nav-tabs page-header-tab">
                  <li class="nav-item"><a class="nav-link active" id="li-ref-tab" data-toggle="tab" href="#ref-tab">Dashboard</a></li>
                  <li class="nav-item"><a class="nav-link" id="li-send-a-ref" data-toggle="tab" href="#send-a-ref">Send A Referral</a></li>
                  <li class="nav-item"><a class="nav-link" id="li-send-esc-ref" data-toggle="tab" href="#send-esc-ref">Send Guest A Referral</a></li>
                  <li class="nav-item"><a class="nav-link" id="li-rec-ref" data-toggle="tab" href="#rec-ref">Received Referral</a></li>
                  <li class="nav-item"><a class="nav-link" id="li-sent-ref" data-toggle="tab" href="#sent-ref">Sent Referral</a></li>
               </ul>
            </div>
         </div>
      </div>
      <div class="section-body mt-3">
         <div class="container-fluid">
            <div class="tab-content mt-3">
               <div class="tab-pane fade show active" id="ref-tab" role="tabpanel">
                  <div class="card">
                     <div class="card-header">
                        <div class="card-options">
                           <form>
                              <div class="input-group">
                                 <select class="form-control">
                                    <option value="past_12_months">Past 12 Months</option>
                                    <option value="this_year">This Year</option>
                                    <option value="last_year">Last Year</option>
                                 </select>
                              </div>
                           </form>
                        </div>
                     </div>
                     <div class="card-body">
                        <div id="apex-basic-column" style="min-height: 365px;">
                           
                        </div>
                     </div>
                  </div>
               </div>

               <div class="tab-pane fade" id="send-a-ref" role="tabpanel">
                  <div class="row clearfix">
                     <div class="col-12">
                        <!--div class="card">
                           <div class="card-body">
                              <div class="row">
                                 <div class="col-lg-4 col-md-4 col-sm-6">
                                    <label>Search By Name or Company</label>
                                    <div class="input-group">
                                       <input type="text" class="form-control" placeholder="Search...">
                                    </div>
                                 </div>
                                 <div class="col-lg-3 col-md-4 col-sm-6">
                                    <label>Search By Affiliate</label>
                                    <div class="multiselect_div">
                                       <select id="multiselect3-all" name="multiselect3[]" class="multiselect multiselect-custom" multiple="multiple" style="display: none;">
                                          <option value="multiselect-all">STATUS</option>
                                          <option value="All Statuses">All Statuses</option>
                                          <option value="New">New</option>
                                          <option value="Contacted">Contacted</option>
                                          <option value="Interviewed">Interviewed</option>
                                          <option value="Hired">Hired</option>
                                       </select>
                                       <div class="btn-group">
                                          <button type="button" class="multiselect dropdown-toggle btn btn-default" data-toggle="dropdown" title="None selected"><span class="multiselect-selected-text">None selected</span> <b class="caret"></b></button>
                                          <ul class="multiselect-container dropdown-menu">
                                             <li class="multiselect-item multiselect-all"><a tabindex="0" class="multiselect-all"><label class="checkbox"><input type="checkbox" value="multiselect-all">  Select all</label></a></li>
                                             <li><a tabindex="0"><label class="checkbox"><input type="checkbox" value="All Statuses"> All Statuses</label></a></li>
                                             <li><a tabindex="0"><label class="checkbox"><input type="checkbox" value="New"> New</label></a></li>
                                             <li><a tabindex="0"><label class="checkbox"><input type="checkbox" value="Contacted"> Contacted</label></a></li>
                                             <li><a tabindex="0"><label class="checkbox"><input type="checkbox" value="Interviewed"> Interviewed</label></a></li>
                                             <li><a tabindex="0"><label class="checkbox"><input type="checkbox" value="Hired"> Hired</label></a></li>
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-lg-3 col-md-4 col-sm-6">
                                    <label>Search By Category</label>
                                    <div class="form-group">
                                       <select class="custom-select">
                                          <option selected="">Newest first</option>
                                          <option value="1">Oldest first</option>
                                          <option value="2">Low salary first</option>
                                          <option value="3">High salary first</option>
                                          <option value="3">Sort by name</option>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="col-lg-2 col-md-4 col-sm-6">
                                    <label>&nbsp;</label>
                                    <a href="javascript:void(0);" class="btn btn-sm btn-primary btn-block" title="">Filter</a>
                                 </div>
                              </div>
                           </div>
                        </div-->
                        <div class="table-responsive">
                           <table id="send_referral_associates_list" class="table table-hover table-vcenter table_custom text-nowrap spacing5 border-style mb-0 send_referral">
                              <thead style="display:none;">
                                 <tr>
                                    <th class="text-left"><strong>Image</strong></th>
                                    <th class="text-left"><strong>Name</strong></th>
                                    <th class=><strong><p class="text-left mb-0">Business</strong></p></th>
                                    <th class="text-left"><strong>Address</strong></th>
                                    <th><strong>Action</strong></th>
                                 </tr>
                              </thead>
                              <tbody>
                                 @foreach($all_associates as $item)
                                 <tr>
                                    <td class="w90">
                                       <!--div class="avatar avatar-pink" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name"-->
                                          <span><a href="/affiliacte_member_info/{{ $item->id }}">
                                             <img class="imguser avatar avatar-xl mr-3" onerror="this.src='/images/user.png'" src="/images/{{ $item->your_image }}" alt="avatar">
                                          </a></span>
                                       <!--/div-->
                                    </td>
                                    <td>
                                       <span>
                                          <a href="/affiliacte_member_info/{{$item->id}}">{{ ($item->first_name) ? $item->first_name : '...' }} {{ ($item->last_name) ? $item->last_name : '...' }}
                                          </a>
                                       </span>
                                    </td>
                                    <td>
                                       <div class="font-15">{{ (isset($item->business_name) && $item->business_name !='') ? $item->business_name : ' ...' }}</div>
                                       @php
                                          $bizcatname =  getBusinessName(@$item->business_category);
                                       @endphp
                                       <span class="text-muted">{{ (isset($bizcatname->bizcatname)) ? $bizcatname->bizcatname : "..." }}</span>
                                    </td>
                                    <td>
                                       @php
                                          $state_name   = get_states($item->state);
                                          $chapter_name = getChapterName($item->affiliate_for);
                                       @endphp
                                       <span style="display: block">{{ (isset($chapter_name->chapter_name)) ? $chapter_name->chapter_name : "..." }}</span>
                                       <span>{{ (isset($state_name->name) && $state_name->name !='')?$state_name->name:' ...' }} {{ (isset($item->city))?', '.$item->city:' ...' }}</span>
                                    </td>
                                    <td>                                       
                                      <button type="button" style="margin-left: 0px !important;" class="btn btn-success btn-block referral_form_btn" data-associate-id="{{ $item->id }}"><i class="fa fa-send"></i>&nbsp;&nbsp;&nbsp;Send Referral</button>
                                    </td>
                                 </tr>
                                 @endforeach
                              </tbody>
                           </table>
                        </div>
                        <!--ul class="pagination mt-2">
                           <li class="page-item"><a class="page-link" href="javascript:void(0);">Previous</a></li>
                           <li class="page-item active"><a class="page-link" href="javascript:void(0);">1</a></li>
                           <li class="page-item"><a class="page-link" href="javascript:void(0);">2</a></li>
                           <li class="page-item"><a class="page-link" href="javascript:void(0);">3</a></li>
                           <li class="page-item"><a class="page-link" href="javascript:void(0);">Next</a></li>
                        </ul-->
                     </div>
                  </div>
               </div>

              <!-- Send An Escrow Referral start here -->

               <div class="tab-pane fade" id="send-esc-ref" role="tabpanel">
                  <div class="card">
                           <div class="card-body">
                              <div class="row">
                                 <div class="col-lg-4 col-md-4 col-sm-6">
                                    <label>Search By Name or Company</label>
                                    <div class="input-group">
                                       <input type="text" class="form-control" placeholder="Search...">
                                    </div>
                                 </div>
                                 <div class="col-lg-3 col-md-4 col-sm-6">
                                    <label>Search By Affiliate</label>
                                    <div class="multiselect_div">
                                       <select id="multiselect3-all" name="multiselect3[]" class="multiselect multiselect-custom" multiple="multiple" style="display: none;">
                                          <option value="multiselect-all">STATUS</option>
                                          <option value="All Statuses">All Statuses</option>
                                          <option value="New">New</option>
                                          <option value="Contacted">Contacted</option>
                                          <option value="Interviewed">Interviewed</option>
                                          <option value="Hired">Hired</option>
                                       </select>
                                       <div class="btn-group">
                                          <button type="button" class="multiselect dropdown-toggle btn btn-default" data-toggle="dropdown" title="None selected"><span class="multiselect-selected-text">None selected</span> <b class="caret"></b></button>
                                          <ul class="multiselect-container dropdown-menu">
                                             <li class="multiselect-item multiselect-all"><a tabindex="0" class="multiselect-all"><label class="checkbox"><input type="checkbox" value="multiselect-all">  Select all</label></a></li>
                                             <li><a tabindex="0"><label class="checkbox"><input type="checkbox" value="All Statuses"> All Statuses</label></a></li>
                                             <li><a tabindex="0"><label class="checkbox"><input type="checkbox" value="New"> New</label></a></li>
                                             <li><a tabindex="0"><label class="checkbox"><input type="checkbox" value="Contacted"> Contacted</label></a></li>
                                             <li><a tabindex="0"><label class="checkbox"><input type="checkbox" value="Interviewed"> Interviewed</label></a></li>
                                             <li><a tabindex="0"><label class="checkbox"><input type="checkbox" value="Hired"> Hired</label></a></li>
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-lg-3 col-md-4 col-sm-6">
                                    <label>Search By Category</label>
                                    <div class="form-group">
                                       <select class="custom-select">
                                          <option selected="">Newest first</option>
                                          <option value="1">Oldest first</option>
                                          <option value="2">Low salary first</option>
                                          <option value="3">High salary first</option>
                                          <option value="3">Sort by name</option>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="col-lg-2 col-md-4 col-sm-6">
                                    <label>&nbsp;</label>
                                    <a href="javascript:void(0);" class="btn btn-sm btn-primary btn-block" title="">Filter</a>
                                 </div>
                              </div>
                           </div>
                        </div>
                  <div class="card">
                  <div class="card-body">
                        <div class="table-responsive">
                            <form>
                               
                                <table class="table table-hover table-striped table-vcenter text-nowrap mb-0 trtd" id="chapter_list_table">
                                    <thead>
                                        <tr>
                                            <th class="text-left"><strong>#</strong></th>                                         
                                            <th class="text-left"><strong>Guest</strong></th>
                                            <th class="text-left"><strong>Affiliate</strong></th>
                                            <th class="text-left"><strong>Category</strong></th>
                                            <th class="text-left"><strong>My Invitee</strong></th>
                                            <th class="text-left"><strong>Action</strong></th>                                         
                                        </tr>
                                    </thead>
                                    <tbody>
                                      
                                    
                                        <tr class="">
                                            <td class="text-left"><span>1</span></td>
                                            <td class="text-left">
                                                <a href="javascript:;">Leslie Morrison</a><br>
                                                <span>Besure</span><br>
                                                <span>lesliemorrison2@gmail.com</span><br>
                                                <span>+1 917-912-6907</span>
                                            </td>
                                            <td class="text-left">Besure in San Diego, CA</td>
                                            <td class="text-left">3D Imaging</td>
                                            <td class="text-left">Yes</td>
                                            <td class="text-left"><button type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#exampleModal" ><i class="fa fa-send"></i>&nbsp;&nbsp;&nbsp;Send Referral</button></td>
                                        </tr>  

                                          <tr class="">
                                            <td class="text-left"><span>2</span></td>
                                            <td class="text-left">
                                                <a href="javascript:;">Carline Colquhoun</a><br>
                                                <span>Besure</span><br>
                                                <span>unknown@gmail.com</span><br>
                                                <span>+1 973-902-8114</span>
                                            </td>
                                            <td class="text-left">Besure of Jersey</td>
                                            <td class="text-left">Accountant</td>
                                            <td class="text-left"></td>
                                            <td class="text-left"><button type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#exampleModal" ><i class="fa fa-send"></i>&nbsp;&nbsp;&nbsp;Send Referral</button></td>
                                          </tr> 

                                          <tr class="">
                                            <td class="text-left"><span>3</span></td>
                                            <td class="text-left">
                                                <a href="javascript:;">Pauline Koni</a><br>
                                                <span>Besure</span><br>
                                                <span>misskoni@yahoo.com</span><br>
                                                <span>+1 973-902-8114</span>
                                            </td>
                                            <td class="text-left">Besure of New York</td>
                                            <td class="text-left">Acupuncture</td>
                                            <td class="text-left"></td>
                                            <td class="text-left"><button type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#exampleModal" ><i class="fa fa-send"></i>&nbsp;&nbsp;&nbsp;Send Referral</button></td>
                                          </tr>   

                                          <tr class="">
                                            <td class="text-left"><span>4</span></td>
                                            <td class="text-left">
                                                <a href="javascript:;">Carline Colquhoun</a><br>
                                                <span>Besure</span><br>
                                                <span>unknown@gmail.com</span><br>
                                                <span>+1 973-902-8114</span>
                                            </td>
                                            <td class="text-left">Besure of Jersey</td>
                                            <td class="text-left">Accountant</td>
                                            <td class="text-left">Yes</td>
                                            <td class="text-left"><button type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#exampleModal" ><i class="fa fa-send"></i>&nbsp;&nbsp;&nbsp;Send Referral</button></td>
                                          </tr>  

                                          <tr class="">
                                            <td class="text-left"><span>5</span></td>
                                            <td class="text-left">
                                                <a href="javascript:;">Pauline Koni</a><br>
                                                <span>Besure</span><br>
                                                <span>misskoni@yahoo.com</span><br>
                                                <span>+1 973-902-8114</span>
                                            </td>
                                            <td class="text-left">Besure of New York</td>
                                            <td class="text-left">Acupuncture</td>
                                            <td class="text-left">Yes</td>
                                            <td class="text-left"><button type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#exampleModal" ><i class="fa fa-send"></i>&nbsp;&nbsp;&nbsp;Send Referral</button></td>
                                          </tr>                                       
                                </tbody>
                                </table>
                            </form>                            
                        </div>
                    </div>
                    </div>
                     <!--div class="card">
                            <div class="card-header">
                                <h3 class="card-title">123 6th St. Melbourne, FL 32904</h3>
                            </div>
                            <div class="card-body">
                                <form method="post" id="">
                                    <div class="row clearfix">
                                       <div class="col-lg-12 col-lg-12">
                                          <label>Referral For<span class="redstar">*<span> <span <i class="fa fa-question-circle" style="font-size: 22px;color: black;position: absolute;margin: 0 0 0 12px;"></i></span></label>

                                          <div class="form-group">
                                             <select class="custom-select">
                                                <option value="0">Select Chapter</option>
                                                <option value="1">Business Consultant</option>
                                                <option value="2">Financial Advisor</option>
                                                <option value="3">Business Advisor</option>
                                             </select>
                                          </div>

                                          <p class="text-danger">Escrow Referral are not given out until the guest joins the chapter.</p>
                                       </div>
                                       <div class="col-lg-6 col-md-6">
                                          <label>Date<span class="redstar">*<span></label>
                                          <div class="form-group">
                                             <input name="" data-provide="datepicker" data-date-autoclose="true" class="form-control" placeholder="Select Appointment">
                                          </div>
                                       </div>
                                       <div class="col-lg-6 col-md-6">
                                          <label>Referral Type</label>
                                          <div class="form-group">
                                             <select class="custom-select">
                                                <option value="0">Referral Type</option>
                                                <option value="1">Inside</option>
                                                <option value="2">OutSide</option>
                                             </select>
                                          </div>
                                       </div>
                                       <div class="col-lg-6 col-md-6">
                                          <label>Person To Contact</label>
                                          <div class="form-group">
                                             <input type="text" class="form-control" placeholder="Person To Contact">
                                          </div>
                                       </div>
                                       <div class="col-lg-6 col-md-6">
                                          <label>Company Name</label>
                                          <div class="form-group">
                                             <input type="text" class="form-control" placeholder="Company Name">
                                          </div>
                                       </div>
                                      <div class="col-lg-6 col-md-6">
                                          <label>Phone Number</label>
                                          <div class="form-group">
                                             <input type="text" class="form-control" placeholder="Phone Number">
                                          </div>
                                       </div>
                                       <div class="col-lg-6 col-md-6">
                                          <label>Email Address</label>
                                          <div class="form-group">
                                             <input type="text" class="form-control" placeholder="Email Address">
                                          </div>
                                       </div>
                                       <div class="col-lg-12 col-md-12">
                                          <label>Referral Details</label>
                                          <div class="form-group">
                                             <textarea rows="8" cols="100" type="text" class="form-control" placeholder="Details" name="terminationRemarks"></textarea>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1">
                                        <span class="custom-control-label">Specify the Referral value</span>
                                    </label>
                                     
                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      
                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1">
                                        <span class="custom-control-label">This is a private Referral</span>
                                        <span style="position: absolute; margin: 0 0 0 12px;"><i class="fa fa-question-circle" style="font-size: 22px;"></i></span>
                                    </label>

                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      
                                  </div>
                              </div>


                                    <button type="button" class="btn btn-round btn-primary">Submit Referral </button>
                                    <div class="col-lg-10 col-md-10 star-indigate"><span class="redstar">*<span> Indicates the required fields</span></span></div>
                                </form>
                           </div>
                     </div-->
               </div>
               <!-- Send An Escrow Referral end here -->


               <!-- Received Referral start here -->

               <div class="tab-pane fade" id="rec-ref" role="tabpanel">
                  <div class="row clearfix">
                     <div class="col-12">
                        <div class="card">
                           <div class="card-body">
                              <div class="row">
                                 <div class="col-lg-4 col-md-4 col-sm-6">
                                    <label>Start Date</label>
                                    <div class="input-group">
                                       <input type="text" data-provide="datepicker" data-date-autoclose="true" class="form-control" placeholder="Start date *">
                                    </div>
                                 </div>
                                 <div class="col-lg-4 col-md-4 col-sm-6">
                                    <label>End Date</label>
                                    <div class="input-group">
                                       <input type="text" data-provide="datepicker" data-date-autoclose="true" class="form-control" placeholder="End date *">
                                    </div>
                                 </div>
                                 <div class="col-lg-3 col-md-4 col-sm-6">
                                    <label>Status</label>
                                    <div class="form-group">
                                       <select class="custom-select">
                                          <option selected="">Select..</option>
                                          <option value="1">Bogus/Invalid</option>
                                          <option value="2">Closed</option>
                                          <option value="3">Working</option>
                                          <option value="3">Sort by name</option>
                                       </select>
                                    </div>
                                 </div>
                                 
                              </div>
                        <div class="table-responsive">
                           <table id="received-referral-table" class="table table-hover table-vcenter table_custom text-nowrap spacing5 border-style mb-0 datatable_custom_bordered">
                              <thead style="display: none">
                                  <tr>
                                      <th></th>
                                      <th></th>
                                      <th></th>
                                      <th></th>
                                      <th></th>
                                      
                                  </tr>
                              </thead>
                              <tbody>
                                 @if($received_referral_details_array)
                                 @foreach($received_referral_details_array as $received_referral_detail)
                                 <tr>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral From :</span><br>
                                       </div>
                                        <div class="avatar avatar-pink" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name">
                                          @if($received_referral_detail['referral_to_your_image'] != "")
                                          <img class="" src="{{ '/images/'.$received_referral_detail['referral_to_your_image'] }}" alt="{{ $received_referral_detail['referral_to_your_image'] }}">
                                          @else
                                          <img class="" src="/images/users.png" alt="">
                                          @endif
                                       </div>
                                       <br>
                                       <div class="font-12">
                                          <span>{{ ucwords($received_referral_detail['referral_to_name']) }}</span><br>
                                          <span>{{ ucwords($received_referral_detail['referral_to_business_cat']) }}</span><br>
                                          <span>{{ ucwords($received_referral_detail['referral_to_affiliate']) }}</span>
                                       </div>                                       
                                    </td>
                                    <td>
                                        <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Person To Contact : <span class="text-muted">{{ ucwords($received_referral_detail['contact_person']) }}</span></span><br>
                                          <span>Phone : <span class="text-muted">{{ ($received_referral_detail['phone']=="") ? "-----" : $received_referral_detail['phone'] }}</span></span><br>
                                          <span>Email : <span class="text-muted">{{ ($received_referral_detail['email']=="") ? "-----" : $received_referral_detail['email'] }}</span></span><br>
                                          <span>Company : <span class="text-muted"></span>{{ $received_referral_detail['company_name'] }}</span><br>
                                       </div>
                                    </td>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral Details: </span><br>
                                       </div>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>{{ trim($received_referral_detail['referral_details']) }}</span>
                                       </div>
                                    </td>                                     
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Value : <span class="text-muted">....</span></span><br>
                                          <span>Status : <span class="text-muted">{{ $received_referral_detail['status'] }}</span></span>
                                       </div>
                                    </td>
                                    <td>
                                       <button style="margin-left: 0px !important;" type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#editModal">Edit</button>
                                       <!--button type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#editModal">View</button-->
                                    </td>
                                 </tr>
                                 @endforeach
                                 
                                
                                 @endif
                              </tbody>
                           </table>
                        </div>
                        </div>
                        </div>

                     </div>
                  </div>
               </div>
               <!-- Received Referral end here -->

               <!-- Sent Referral start here -->

               <div class="tab-pane fade" id="sent-ref" role="tabpanel">
                  <div class="row clearfix">
                     <div class="col-12">
                        <div class="card">
                           <div class="card-body">
                              <div class="row">
                                 <div class="col-lg-3 col-md-3 col-sm-6">
                                    <label>Start Date</label>
                                    <div class="input-group">
                                       <input type="text" data-provide="datepicker" data-date-autoclose="true" class="form-control" placeholder="Start date *">
                                    </div>
                                 </div>
                                 <div class="col-lg-3 col-md-3 col-sm-6">
                                    <label>End Date</label>
                                    <div class="input-group">
                                       <input type="text" data-provide="datepicker" data-date-autoclose="true" class="form-control" placeholder="End date *">
                                    </div>
                                 </div>
                                 <div class="col-lg-3 col-md-3 col-sm-6">
                                    <label>Status</label>
                                    <div class="form-group">
                                       <select class="custom-select">
                                          <option selected="">Select..</option>
                                          <option value="1">Bogus/Invalid</option>
                                          <option value="2">Closed</option>
                                          <option value="3">Working</option>
                                          <option value="3">Sort by name</option>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="col-lg-3 col-md-3 col-sm-6">
                                    <label>&nbsp;</label>
                                    <a href="javascript:void(0);" class="btn btn-sm btn-primary btn-block" title="">Filter</a>
                                 </div>
                                 
                              </div>
                        <div class="table-responsive">
                           <table id="sent_referrals_table" class="table table-hover table-vcenter table_custom text-nowrap spacing5 border-style mb-0 datatable_custom_bordered">
                              <thead style="display: none">
                                  <tr>
                                      <th></th>
                                      <th></th>
                                      <th></th>
                                      <th></th>
                                      <th></th>                                      
                                  </tr>
                              </thead>
                              <tbody>
                                 @foreach($sent_referral_details_array as $sent_referral_detail)
                                 <tr>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral To :</span><br>
                                       </div>
                                       <div class="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name">
                                          @if($sent_referral_detail['referral_to_your_image'] != "")
                                          <img class="" style="height:45px;width:45px;" src="{{ '/images/'.$sent_referral_detail['referral_to_your_image'] }}" alt="{{ $sent_referral_detail['referral_to_your_image'] }}">
                                          @else
                                          <img class="" style="height:45px;width:45px;" src="/images/users.png" alt="">
                                          @endif
                                       </div>
                                       <div class="font-12">
                                          <span>{{ ucwords($sent_referral_detail['referral_to_name']) }}</span><br>
                                          <span>{{ ucwords($sent_referral_detail['referral_to_business_cat']) }}</span><br>
                                          <span>{{ ucwords($sent_referral_detail['referral_to_affiliate']) }}</span>
                                       </div>
                                    </td>
                                    <td>
                                        <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Person To Contact : <span class="text-muted">{{ ucwords($sent_referral_detail['contact_person']) }}</span></span><br>
                                          <span>Phone : <span class="text-muted">{{ ($sent_referral_detail['phone']=="") ? "-----" : $sent_referral_detail['phone'] }}</span></span><br>
                                          <span>Email : <span class="text-muted">{{ ($sent_referral_detail['email']=="") ? "-----" : $sent_referral_detail['email'] }}</span></span><br>
                                          <span>Company : <span class="text-muted"></span>{{ $sent_referral_detail['company_name'] }}</span><br>
                                       </div>
                                    </td>
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Referral Details: </span><br>
                                       </div>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>{{ trim($sent_referral_detail['referral_details']) }}</span>
                                       </div>
                                    </td> 
                                    <td>
                                       <div class="font-12" style="text-align: left;padding: 0 0 8px 0;">
                                          <span>Value : <span class="text-muted">....</span></span><br>
                                          <span>Status : <span class="text-muted">{{ $sent_referral_detail['status'] }}</span></span>
                                       </div>
                                    </td>
                                    <td>
                                       <button type="button" style="margin-left: 0px !important;" class="btn btn-success btn-block" data-toggle="modal" data-target="#editModal">Edit</button>                                    
                                    </td>                                                                   
                                 </tr>
                                 @endforeach
                              </tbody>
                           </table>
                        </div>
                        </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- Sent Referral end here -->
            </div>
         </div>
      </div>
   </div>
</div>

@stop


@section('popup')
<div class="modal fade " id="editModal" tabindex="-1" role="dialog">
   
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Referral</h5>
                    <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>-->
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
                <div class="modal-body">

                        <form>
                          <div class="row clearfix">

                              <!--div class="col-lg-6 col-md-6">
                                 <div class="form-group">
                                    <input type="text" data-provide="datepicker" data-date-autoclose="true" class="form-control" placeholder="Start date *">
                                 </div>
                              </div-->

                              <!--div class="col-lg-6 col-md-6">
                                 <div class="form-group">
                                       <select class="custom-select">
                                          <option selected="">Referral Type</option>
                                          <option value="1">Inside</option>
                                          <option value="2">OutSide</option>
                                       </select>
                                 </div>
                              </div-->

                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <input type="text" class="form-control" placeholder="Person To Contact">
                                  </div>
                              </div>

                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">                                   
                                      <input type="text" class="form-control" placeholder="Company Name">
                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <input type="text" class="form-control" placeholder="Phone Number">
                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <input type="text" class="form-control" placeholder="Email Address">
                                  </div>
                              </div>

                              <div class="col-lg-12 col-md-12">
                                  <div class="form-group">
                                     <textarea rows="6" class="form-control" name="" id="" placeholder="Referral Details"></textarea>
                                  </div>
                              </div>

                              
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <input type="text" class="form-control" placeholder="$">
                                  </div>
                              </div>

                             <div class="col-lg-6 col-md-6">
                                 <div class="form-group">
                                       <select class="custom-select">
                                          <option selected="">Status</option>
                                          <option value="1">New</option>
                                          <option value="2">Bogus/Invalid</option>
                                          <option value="2">Closed</option>
                                          <option value="2">Working</option>
                                       </select>
                                 </div>
                              </div>

                              <!--div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1">
                                        <span class="custom-control-label">This is a private Referral</span>
                                        <span style="position: absolute; margin: 0 0 0 12px;"><i class="fa fa-question-circle" style="font-size: 22px;"></i></span>
                                    </label>

                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      
                                  </div>
                              </div-->

                          </div>
                        </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary">Submit Referral</button>
                </div>
            </div>
        </div>
</div>

<div class="modal fade " id="add_referral_form_popup" tabindex="-1" role="dialog">   
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Referral Form</h5>
                    <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>-->
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
                <div class="modal-body">
                        <form name="add_referral_form" id="add_referral_form" method="POST">
                        @csrf
                        <input type="hidden" name="referral_to" id="associate_to" value="">
                          <div class="row clearfix">
                           <style type="text/css">
                                 .tip-form-header{box-sizing: border-box; background-color: #e0a258; color: #fff; padding: 1rem;width: 100%;}
                                 .tip-form-header .tip-form-member{display: -webkit-box;display: flex; -webkit-box-flex: 1; flex: auto; align-items: center;
                                 }
                              .tip-form-header .profile-image{width: 120px; height: 120px; object-fit: cover; object-position: top center; margin-right: 20px; background-color: #ccc;}
                           </style>

                              <div class="tip-form-header">
                                 <div class="tip-form-member">
                                    <img class="profile-image" src="/images/user.png" alt="">
                                    <div class="member-info">
                                       <div class="member-name">Alice Agyemang-Badu</div>
                                       <div class="business-name">New York Life</div>
                                       <div class="category-name">Recruiter</div>
                                       <div class="chapter-name">Besure of Upper Montclair, NJ</div>
                                    </div>
                                 </div>
                              </div>

                              <div class="text-center" style="padding: 10px; font-size: 90%; font-weight: 500;"><p>My qualified Business Referral is for a company or person who is interested in a specific service or product and is expecting a call from a Besure member.</p></div>
                              <div class="col-lg-12 col-md-12">
                                 <div class="error_success_message">
                                 </div>
                              </div>

                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <input type="text" class="form-control" name="contact_person" value="" placeholder="Person To Contact *">
                                  </div>
                              </div>

                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">                                   
                                      <input type="text" class="form-control" name="company_name" placeholder="Company Name *">
                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <input type="number" id="phone" name="phone" placeholder="Phone Number #" class="form-control" pattern="/^-?\d+\.?\d*$/" onkeypress="if(this.value.length==10) return false;">
                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <input type="text" class="form-control" name="email" placeholder="Email Address">
                                  </div>
                              </div>

                              <div class="col-lg-12 col-md-12">
                                  <div class="form-group">
                                     <textarea rows="6" class="form-control" name="referral_details" id="referral_details" placeholder="Referral Details"></textarea>
                                  </div>
                              </div>

                              <input type="hidden" name="status" value="New">
                              <input type="hidden" name="created_by" value="{{ $logged_in_user_id }}">
                              <input type="hidden" name="referral_from" value="{{ $logged_in_user_id }}">
                              <input type="hidden" name="referral_type" value="member">


                              <div class="col-lg-12 col-md-12">
                                  <div class="form-group">
                                      <span><span class="redstar">*<span> indicates a required field</span>
                                      <span style="float:right;">either a valid phone # or email address is required</span>
                                  </div>
                              </div>
                          </div>
                          <hr>
                          <button style="float: right;" type="submit" class="btn btn-secondary add_referral_form_submit">Submit Referral</button>
                        </form>
                  </div>
                  <div class="data_insert_success col-lg-12 col-md-12" style="display: block">
                     
                  </div>
                <!--div class="modal-footer">
                    <button type="submit" class="btn btn-secondary add_referral_form_submit">Submit Referral</button>
                </div-->
                
            </div>
        </div>
</div>
@stop


@section('page-styles')

<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }} ">
<link href='http://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css' rel='stylesheet' type='text/css'>
<link href='https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css' rel='stylesheet' type='text/css'>
<link href='https://cdn.datatables.net/colreorder/1.5.2/js/dataTables.colReorder.min.js' rel='stylesheet' type='text/css'>

@stop

@section('page-script')
<script src="{{ asset('assets/bundles/apexcharts.bundle.js') }}"></script>
<script src="{{ asset('assets/bundles/counterup.bundle.js') }}"></script>
<script src="{{ asset('assets/bundles/knobjs.bundle.js') }}"></script>
<script src="{{ asset('assets/js/core.js') }}"></script>
<script src="{{ asset('assets/js/page/index.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.8/js/jquery.dataTables.min.js"></script>

<script>
   $(document).ready(function()
   {
      function IsEmail(email) 
      {
         var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
         if(!regex.test(email)) 
         {
            return false;
         }
         else
         {
            return true;
         }
      }

      $('#send_referral_associates_list').DataTable({
         "oLanguage": {
            "sSearch": "Search By Keyword:"
         }
      });

      
      $('#sent_referrals_table').DataTable({
         "oLanguage": {
            "sSearch": "Search By Keyword:"
         }
      });

      $('#received-referral-table').DataTable({
         "oLanguage": {
            "sSearch": "Search By Keyword:"
         }
      });      
      /****************************************************************************/
      $(document).on('click','.referral_form_btn',function(e)
      {
         e.preventDefault();
         $('#add_referral_form_popup .tip-form-member .profile-image').attr('src','/images/user.png');
         $('#add_referral_form_popup .tip-form-member .member-name').html('');
         $('#add_referral_form_popup .tip-form-member .business-name').html('');
         $('#add_referral_form_popup .tip-form-member .category-name').html('');
         $('#add_referral_form_popup .tip-form-member .chapter-name').html('');

         var associate_id = $(this).attr('data-associate-id');
         //alert(associate_id);
         $.ajax({
            url: "{{ route('get_associate_details_from_id') }}",
            type:"GET",
            data: {'associate_id':associate_id,"_token": "{{ csrf_token() }}"},
            success:function(response)
            {
               var result = JSON.parse(response);   
               console.log(result);
               if(result.message == 'success')
               {
                  if(result.image == "")
                  {
                     $('#add_referral_form_popup .tip-form-member .profile-image').attr('src','/images/user.png');
                  }
                  else
                  {
                     $('#add_referral_form_popup .tip-form-member .profile-image').attr('src','/images/'+result.image);
                  }

                  $('#add_referral_form_popup .tip-form-member .member-name').append(result.name);
                  $('#add_referral_form_popup .tip-form-member .business-name').append(result.payment_city);
                  $('#add_referral_form_popup .tip-form-member .category-name').append(result.bizcatname);
                  $('#add_referral_form_popup .tip-form-member .chapter-name').append(result.chapter_name);
                  $('input[name="referral_to"]').val(result.uuid);
                 
                  $('#add_referral_form_popup').modal('show');   
               }
               else
               {
                  alert('Error Fetching Data...');
               }
            }
         });
      });

      /*********************************************************************************************/
      $(document).on('click','.add_referral_form_submit',function(e)
      {
         e.preventDefault();
         $('.error_success_message').html('');
         $('.data_insert_success').html('');

         var referral_to = $('input[name="referral_to"]').val();
         var contact_person = $('input[name="contact_person"]').val();
         var company_name = $('input[name="company_name"]').val();
         var phone = $('input[name="phone"]').val();
         var email = $('input[name="email"]').val();
         var referral_details = $('#referral_details').val();
         var status = $('input[name="status"]').val();
         var created_by = $('input[name="created_by"]').val();
         var referral_from = $('input[name="referral_from"]').val();
         var referral_type = $('input[name="referral_type"]').val();

         if(contact_person == "" || company_name == "")
         {
            $('.error_success_message').append('<p style="color:#ff0000;font-weight:600;">* Kindly Fill in all the Required Fields.</p>');
            return false;
         }
         else if(email =="" && phone == "")
         {
            $('.error_success_message').append('<p style="color:#ff0000;font-weight:600;">* Kindly Fill in all the Required Fields.</p>');
            return false;
         }
         else if((email != "" && phone == "") || (email != "" && phone != ""))
         {
            if(IsEmail(email)==false)
            {
               $('.error_success_message').append('<p style="color:#ff0000;font-weight:600;"><i class="fa fa-exclamation-triangle"></i> Please enter Valid Email Address.</p>');
               return false;
            }
         } 

         $.ajax({
            url: "{{ route('add_referral_from_referral_form') }}",
            type:"POST",
            data: {'referral_to':referral_to,'contact_person':contact_person,'company_name':company_name,"phone":phone,"email":email,"referral_details":referral_details,"status":status,"created_by":created_by,"referral_from":referral_from,"referral_type":referral_type,"_token": "{{ csrf_token() }}"},
            success:function(response)
            {
               var result = JSON.parse(response);   
               console.log(result);
               if(result.message == 'success')
               {
                  $('#add_referral_form').trigger('reset');
                  $('.data_insert_success').append('<p style="font-weight:600;color:#008000;float:right;">Referral Submitted Successfully</p>');

                  setTimeout(function()
                  {  
                     $('#add_referral_form_popup').modal('hide');   
                  },1200);
               }
               else
               {
                  $('.data_insert_success').append('<p style="font-weight:600;color:#008000;float:right;">Error in Submitting Referral. Please Try Again Later.</p>');
               }
            }
         });
      });      
   });   
</script>
@stop
