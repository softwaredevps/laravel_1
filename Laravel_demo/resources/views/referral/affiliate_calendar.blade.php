@extends('layout.member_master')
@section('title', 'Affiliate Calendar')
@section('content')
<div class="col-lg-12 col-md-12">
   <div class="col-lg-12 col-md-12">
      <div class="section-body">
         <div class="container-fluid">
            <div class="d-flex justify-content-between align-items-center">
               <ul class="nav nav-tabs page-header-tab">
                  <li class="nav-item"><a class="nav-link active" id="li-calendar" data-toggle="tab" href="#calendar-view">Calendar View</a></li>
                  <li class="nav-item"><a class="nav-link" id="li-list-view" data-toggle="tab" href="#list-view">List View</a></li>
               </ul>
            </div>
         </div>
      </div>
      <div class="section-body mt-3">
         <div class="container-fluid">
            <div class="tab-content mt-3">

               <div class="tab-pane fade show active" id="calendar-view" role="tabpanel">
                  <div class="row clearfix">
                     <div class="col-12">
                        <div class="card">
                           <div class="card-header bline">
                              <h3 class="card-title"><b>Event/Meeting Calendar</b></h3>
                              <div class="card-options">
                                 <a href="#" class="card-options-collapse" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a>
                                 <a href="#" class="card-options-fullscreen" data-toggle="card-fullscreen"><i class="fe fe-maximize"></i></a>
                                 <a href="#" class="card-options-remove" data-toggle="card-remove"><i class="fe fe-x"></i></a>
                              </div>
                           </div>
                           <div class="card-body">
                              <div class="calendar_filter_section">
                                 <h4 class="card-title"><b>Event/Meeting Type</b></h4>
                                    <div class="form-group">
                                       <label class="custom-control custom-checkbox" style="display:inline-block; margin-right:10px;">
                                          <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1">
                                          <span class="custom-control-label">Formal Meetings</span>
                                       </label>

                                       <label class="custom-control custom-checkbox" style="display:inline-block; margin-right:10px;">
                                          <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1">
                                          <span class="custom-control-label">Zoom/Video Meetings</span>
                                       </label>

                                       <label class="custom-control custom-checkbox" style="display:inline-block; margin-right:10px;">
                                          <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1">
                                          <span class="custom-control-label">Department Meetings</span>
                                       </label>

                                       <label class="custom-control custom-checkbox" style="display:inline-block; margin-right:10px;">
                                          <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1">
                                          <span class="custom-control-label">Associate Meetings</span>
                                       </label>

                                       <label class="custom-control custom-checkbox" style="display:inline-block; margin-right:10px;">
                                          <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1">
                                          <span class="custom-control-label">Business Showcase</span>
                                       </label>

                                       <label class="custom-control custom-checkbox" style="display:inline-block; margin-right:10px;">
                                          <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1">
                                          <span class="custom-control-label">Social Event</span>
                                       </label>

                                    </div>
                              </div>
                           <div id="calendar"></div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>

               <div class="tab-pane fade" id="list-view" role="tabpanel">
                  <div class="card">
                           <div class="card-body">
                              <div class="row">
                                 <div class="col-lg-3 col-md-3 col-sm-6">
                                    <label>Search By Meeting Type</label>
                                    <div class="form-group">
                                       <select class="custom-select">
                                          <option value="">Select Meeting</option>
                                          <option value="1">Formal Meetings</option>
                                          <option value="2">Zoom/Video Meetings</option>
                                          <option value="3">Department Meetings</option>
                                          <option value="4">Associate Meetings</option>
                                          <option value="5">Business Showcase</option>
                                          <option value="6">Social Event</option>
                                       </select>
                                    </div>
                                 </div>

                                 <div class="col-lg-3 col-md-3 col-sm-6">
                                    <label>Search By State</label>
                                    <div class="form-group">
                                       <select class="custom-select">
                                          <option value="">Select State</option>
                                          <option value="1">Alabama</option>
                                          <option value="2">Alaska</option>
                                          <option value="3">Arizona</option>
                                          <option value="3">Arkansas</option>
                                       </select>
                                    </div>
                                 </div>

                                 <div class="col-lg-3 col-md-3 col-sm-6">
                                    <label>Past/Upcoming Meetings</label>
                                    <div class="form-group">
                                       <select class="custom-select">
                                          <option value="">Select</option>
                                          <option value="1">Past Meetings</option>
                                          <option value="2">Upcoming Meeings</option>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="col-lg-3 col-md-3 col-sm-6">
                                    <label>&nbsp;</label>
                                    <a href="javascript:void(0);" class="btn btn-sm btn-primary btn-block" title="">Filter</a>
                                 </div>
                              </div>
                           </div>
                        </div>
                  <div class="card">
                  <div class="card-body">
                        <div class="table-responsive">
                            <form>                               
                                <table class="table table-hover table-striped table-vcenter text-nowrap mb-0 trtd" id="meetings_list_table">
                                    <thead>
                                        <tr>
                                            <th class="text-left"><strong>#</strong></th>                                         
                                            <th class="text-left"><strong>Event/Meeting Type</strong></th>
                                            <th class="text-left"><strong>Event/Meeting Name</strong></th>
                                            <th class="text-left"><strong>Duration</strong></th>
                                            <th class="text-left"><strong>State</strong></th>
                                            <th class="text-left"><strong>Action</strong></th>                                         
                                        </tr>
                                    </thead>
                                    <tbody>                                                                     
                                       <tr class="">
                                          <td class="text-left"><span>1</span></td>
                                          <td class="text-left">Formal Meeting</td>
                                          <td class="text-left">Meeting 1</td>
                                          <td class="text-left">120 Mins</td>
                                          <td class="text-left">Alabama</td>
                                          <td class="text-left"><button type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#exampleModal" > View Details</button></td>
                                       </tr>

                                       <tr class="">
                                          <td class="text-left"><span>2</span></td>
                                          <td class="text-left">Zoom/Video Meeting</td>
                                          <td class="text-left">Meeting 2</td>
                                          <td class="text-left">60 Mins</td>
                                          <td class="text-left">Alaska</td>
                                          <td class="text-left"><button type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#exampleModal" > View Details</button></td>
                                       </tr> 

                                       <tr class="">
                                          <td class="text-left"><span>3</span></td>
                                          <td class="text-left">Social Event</td>
                                          <td class="text-left">Event 1</td>
                                          <td class="text-left">All Day</td>
                                          <td class="text-left">Arkansas</td>
                                          <td class="text-left"><button type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#exampleModal" > View Details</button></td>
                                       </tr> 

                                       <tr class="">
                                          <td class="text-left"><span>3</span></td>
                                          <td class="text-left">Formal Meeting</td>
                                          <td class="text-left">Meeting 3</td>
                                          <td class="text-left">200 Mins</td>
                                          <td class="text-left">Alaska</td>
                                          <td class="text-left"><button type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#exampleModal" > View Details</button></td>
                                       </tr>                                                                               
                                                                                                                     
                                </tbody>
                                </table>
                            </form>                            
                        </div>
                    </div>
                 </div>
               </div>
               <!-- Meetings in List Form -->
            </div>
         </div>
      </div>
   </div>
</div>

@stop




@section('popup')
<div class="modal fade " id="editModal" tabindex="-1" role="dialog">
   
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Referral</h5>
                    <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>-->
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
                <div class="modal-body">

                        <form>
                          <div class="row clearfix">

                              <!--div class="col-lg-6 col-md-6">
                                 <div class="form-group">
                                    <input type="text" data-provide="datepicker" data-date-autoclose="true" class="form-control" placeholder="Start date *">
                                 </div>
                              </div-->

                              <!--div class="col-lg-6 col-md-6">
                                 <div class="form-group">
                                       <select class="custom-select">
                                          <option selected="">Referral Type</option>
                                          <option value="1">Inside</option>
                                          <option value="2">OutSide</option>
                                       </select>
                                 </div>
                              </div-->

                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <input type="text" class="form-control" placeholder="Person To Contact">
                                  </div>
                              </div>

                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">                                   
                                      <input type="text" class="form-control" placeholder="Company Name">
                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <input type="text" class="form-control" placeholder="Phone Number">
                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <input type="text" class="form-control" placeholder="Email Address">
                                  </div>
                              </div>

                              <div class="col-lg-12 col-md-12">
                                  <div class="form-group">
                                     <textarea rows="6" class="form-control" name="" id="" placeholder="Referral Details"></textarea>
                                  </div>
                              </div>

                              
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <input type="text" class="form-control" placeholder="$">
                                  </div>
                              </div>

                             <div class="col-lg-6 col-md-6">
                                 <div class="form-group">
                                       <select class="custom-select">
                                          <option selected="">Status</option>
                                          <option value="1">New</option>
                                          <option value="2">Bogus/Invalid</option>
                                          <option value="2">Closed</option>
                                          <option value="2">Working</option>
                                       </select>
                                 </div>
                              </div>

                              <!--div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1">
                                        <span class="custom-control-label">This is a private Referral</span>
                                        <span style="position: absolute; margin: 0 0 0 12px;"><i class="fa fa-question-circle" style="font-size: 22px;"></i></span>
                                    </label>

                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      
                                  </div>
                              </div-->

                          </div>
                        </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary">Submit Referral</button>
                </div>
            </div>
        </div>
</div>

<div class="modal fade " id="exampleModal" tabindex="-1" role="dialog">
   
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Event/Meeting Details</h5>
                    <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>-->
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
                <div class="modal-body">

                           <style type="text/css">
                                 .tip-form-header{box-sizing: border-box; background-color: #e0a258; color: #fff; padding: 1rem;width: 100%;}
                                 .tip-form-header .tip-form-member{display: -webkit-box;display: flex; -webkit-box-flex: 1; flex: auto; align-items: center;
                                 }
                              .tip-form-header .profile-image{width: 120px; height: 120px; object-fit: cover; object-position: top center; margin-right: 20px; background-color: #ccc;}

                           </style>

                              <!--div class="tip-form-header">
                                 <div class="tip-form-member">
                                    <img class="profile-image" src="../assets/images/xs/avatar1.jpg" alt="">
                                    <div class="member-info">
                                       <div class="member-name">Alice Agyemang-Badu</div>
                                       <div class="business-name">New York Life</div>
                                       <div class="text-muted category-name">Recruiter</div>
                                       <div class="chapter-name">Besure of Upper Montclair, NJ</div>
                                    </div>
                                 </div>
                              </div>

                              <div class="text-center" style="padding: 10px; font-size: 90%; font-weight: 500;"><p>My qualified Business Referral is for a company or person who is interested in a specific service or product and is expecting a call from a Besure member.</p></div-->

                              <!--div class="col-lg-12 col-md-12">
                                 <div class="form-group">
                                    <!-input type="text" data-provide="datepicker" data-date-autoclose="true" class="form-control" placeholder="Start date *"->
                                    <input type="text" class="form-control" placeholder="Referral date *" value="{{ date('d/m/Y') }}" readonly>
                                 </div>
                              </div-->

                              <!--div class="col-lg-6 col-md-6">
                                 <div class="form-group">
                                       <select class="custom-select">
                                          <option selected="">Referral Type</option>
                                          <option value="1">Inside</option>
                                          <option value="2">OutSide</option>
                                       </select>
                                 </div>
                              </div-->

                              <div class="row">
                                <div class="col-lg-6 col-md-6">
                                    <label>Event/Meeting Type</label>
                                    <div class="form-group">
                                        <input type="text" class="form-control" value="Formal Meeting " readonly>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6">
                                    <label>Event/Meeting Name</label>
                                    <div class="form-group">
                                        <input type="text" class="form-control" value="Meeting 1" readonly>
                                    </div>
                                </div>    

                                <div class="col-lg-6 col-md-6">
                                    <label>Duration</label>
                                    <div class="form-group">
                                        <input type="text" class="form-control" value="120 Mins" readonly>
                                    </div>
                                </div>  
                              </div> 

                              <div class="row">
                                <h5><b>Event/Meeting Venue</b></h5>
                              </div>

                              <div class="row">
                                <div class="col-lg-6 col-md-6">
                                    <label>Venue Name</label>
                                    <div class="form-group">
                                        <input type="text" class="form-control" value="Venue" readonly>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6">
                                    <label>Street Address</label>
                                    <div class="form-group">
                                        <input type="text" class="form-control" value="123 street" readonly>
                                    </div>
                                </div>    

                                <div class="col-lg-6 col-md-6">
                                    <label>Suite</label>
                                    <div class="form-group">
                                        <input type="text" class="form-control" value="ABC Suite" readonly>
                                    </div>
                                </div>  

                                <div class="col-lg-6 col-md-6">
                                    <label>City</label>
                                    <div class="form-group">
                                        <input type="text" class="form-control" value="City" readonly>
                                    </div>
                                </div> 

                                <div class="col-lg-6 col-md-6">
                                    <label>State</label>
                                    <div class="form-group">
                                        <input type="text" class="form-control" value="Alabama" readonly>
                                    </div>
                                </div>   

                                <div class="col-lg-6 col-md-6">
                                    <label>Zip</label>
                                    <div class="form-group">
                                        <input type="text" class="form-control" value="123456" readonly>
                                    </div>
                                </div>     

                                <div class="col-lg-12 col-md-12">
                                    <label>Meeting Location MAP</label>
                                    <div class="form-group">
                                        <input type="text" class="form-control" value="https://maps.google.com" readonly>
                                    </div>
                                </div>                            

                              </div>    
                              </div>                           

                <!--div class="modal-footer">
                    <button type="button" class="btn btn-secondary">Submit Referral</button>
                </div-->
            </div>
        </div>
</div>
@stop


@section('page-styles')

<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }} ">
<link rel="stylesheet" href="{{ asset('assets/plugins/fullcalendar/fullcalendar.min.css') }} ">
<link href='http://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css' rel='stylesheet' type='text/css'>

@stop
@section('page-script')
<script src="{{ asset('assets/bundles/apexcharts.bundle.js') }}"></script>
<script src="{{ asset('assets/bundles/counterup.bundle.js') }}"></script>
<script src="{{ asset('assets/bundles/knobjs.bundle.js') }}"></script>

<script src="{{ asset('assets/bundles/lib.vendor.bundle.js') }}"></script>
<script src="{{ asset('assets/bundles/fullcalendar.bundle.js') }}"></script>
<script src="{{ asset('assets/js/core.js') }}"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.8/js/jquery.dataTables.min.js"></script>
<script src="{{ asset('assets/js/page/index.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/js/page/calendar.js') }}"></script>
<script>
   $(document).ready(function()
   {
      $('#meetings_list_table').DataTable();
   });
</script>
@stop
