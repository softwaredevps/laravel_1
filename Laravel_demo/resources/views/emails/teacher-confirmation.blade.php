@component('mail::message')

Hello <br/>

Dear <strong><?php echo $message['first_name']; ?>  <?php echo $message['last_name']; ?></strong>,<br/><br/>

<?php if(isset($message['resetpass']) && $message['resetpass'] ){ ?>

We Received Your Forgot Password Request Use This Below password for login now . 
<br/><br/>
User Name:  <strong><?php echo $message['your_email']; ?></strong><br/><br/>
Password: <strong><?php echo $message['uuid']; ?></strong><br/><br/>


<?php } else { ?>
    It is my pleasure to welcome you as the newest Partner in (affiliate)! Thank you
    for recognizing the opportunity to join us, grow your business and increase your
    presence in your local community.
<br/><br/>

    We have enclosed your credentials to access the Partner Portal below. Please
    log in before our next meeting and complete your Partner Profile. Should you
    have any questions, please do not hesitate to reach out to your Affiliate's
    Partner Liaison. (Partner Liaison) can be reached at (phone # & email address).
    We look forward to seeing you at our next meeting which is listed below;
    (Next Meeting type, day, time & venue)

<br/><br/>
User Name:  <strong><?php echo $message['your_email']; ?></strong><br/><br/>
Password: <strong><?php echo $message['uuid']; ?></strong><br/><br/>

Thank you again for choosing beSure! I am personally committed to making
your experience with us, the best it can be!

</br>
<br/><br/>

<?php } ?>
Sincerely,<br/>
David Faccone,
Founder