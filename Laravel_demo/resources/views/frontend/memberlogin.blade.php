@extends('layout.authentication')
@section('title', 'Login')
 

@section('content')
    <div class="card"> 
        <div class="text-center mb-2">
            <a class="header-brand" href="{{route('hrms.index')}}"><i class="fe fe-command brand-logo"></i></a>
        </div>
        <div class="card-body">
            <form method="post">
            @csrf
                <div class="card-title">BESURE ASSOCIATE PORTAL</div>
                <div class="form-group">
                    <label class="form-label">Email
                        <input type="email" name="useremail" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                    </label>
                </div>
                <div class="form-group">
                    <label class="form-label">Password
                       <a href="/member_forgot_pass" class="float-right small">I forgot password</a> 
                    </label>
                    <input type="password" name="password"  class="form-control" id="exampleInputPassword1" placeholder="Password">
                </div>
                <div class="form-group">
                    <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" />
                    <span class="custom-control-label">Remember me</span>
                    </label>
                </div>
                <div class="form-footer">
                    <!-- <a href="/memberlogin" class="btn btn-primary btn-block" title="">Sign in</a> -->
                    <input type="submit" class="btn btn-primary btn-block" value="Sign in">
                </div>
            </form>
        </div>
    </div>
@stop

