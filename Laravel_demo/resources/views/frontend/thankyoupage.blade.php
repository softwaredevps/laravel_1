@include('frontend.common.header')   
<?php 
    $decode_val = base64_decode(isset($_GET['name'])?$_GET['name']:'');
?>
<div class="container">
	<div class="row text-center">
        <div class="col-sm-6 col-sm-offset-3" style="text-align:left">
        <br><br> <h2 style="color:#0fad00">Success</h2>

            <p style="font-size:20px;color:#5C5C5C;">
                <div>Dear 
                <?php echo $decode_val; ?></div>

                <div>Thank you very much for showing interest in Besure. </div>

                <div>We have received your Application and we will get back to you soon with more information. </div>


                <div>Regards
                Team BeSure</div>
            </p>
     
        </div>
        
	</div>
</div>



@include('frontend.common.footer')