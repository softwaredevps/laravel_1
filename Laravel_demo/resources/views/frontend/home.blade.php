@include('frontend.common.header')
<section class="video_slider home-section-margin">
   <div class="container">
      <div class="row">
         <div class="col-12 text-center s-w-p">
            <h1>Lorem Ipsum</h1>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
         </div>
         <div class="col-12 mt-4 mt-md-5">
            <ul class="img-slider">
               <li class="slide">
                  <a class="thumb" href="javascript:void(0)"><img class="slide-img" src="images/video_img.png" alt="slide 1"></a>
               </li>
               <li class="slide">
                  <a class="thumb" href="javascript:void(0)"><img class="slide-img" src="images/video_img.png" alt="slide 1"></a>
               </li>
               <li class="slide">
                  <a class="thumb" href="javascript:void(0)"><img class="slide-img" src="images/video_img.png" alt="slide 1"></a>
               </li>
               <li class="slide">
                  <a class="thumb" href="javascript:void(0)"><img class="slide-img" src="images/video_img.png" alt="slide 1"></a>
               </li>
               <li class="slide">
                  <a class="thumb" href="javascript:void(0)"><img class="slide-img" src="images/video_img.png" alt="slide 1"></a>
               </li>
               <li class="slide">
                  <a class="thumb" href="javascript:void(0)"><img class="slide-img" src="images/video_img.png" alt="slide 1"></a>
               </li>
               <li class="slide">
                  <a class="thumb" href="javascript:void(0)"><img class="slide-img" src="images/video_img.png" alt="slide 1"></a>
               </li>
               <li class="slide">
                  <a class="thumb" href="javascript:void(0)"><img class="slide-img" src="images/video_img.png" alt="slide 1"></a>
               </li>
            </ul>
         </div>
      </div>
   </div>
</section>
<section class="whyBeSure home-section-margin">
   <div class="container">
      <div class="row">
         <div class="col-12 text-center s-w-p mb-4 mb-md-5">
            <h2>Why BeSure?</h2>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry'sLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry. since the 1500s,</p>
         </div>
      </div>
   </div>
   <div class="innnerWhyUsSec">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <h2 class="font-bold mb-4 mb-md-5">This is how we do it</h2>
               <ul class="howWeDoList">
                  <li>A local resident or business owner has an exceptional experience with a company and refers that company to beSure</li>
                  <li>BeSure contacts the company and begins the application process</li>
                  <li>A local beSure representative meets with the owner or manager of the business and completes an interview and background check</li>
                  <li>After completing the application process, the new company becomes a Associate of the beSure network of businesses. The company is added to our database and a local press release is issued both in print and on our Social Media platforms.</li>
                  <li>Local residents can now use this company's products or services knowing that they are a high-quality, trustworthy organization!</li>
                  <li>At beSure we intend to create an effortless referral process. For whatever product or service you need simply log onto our website and search for one of our Associate to help you, that's it! You can rest easy knowing that you are making an informed choice. So beSure to check us out today!!</li>
               </ul>
            </div>
         </div>
      </div>
   </div>
</section>
<section class="form_section_col home-section-margin">
   <div class="container">
      <div class="row">
         <div class="col-12 text-center">
            <h2>Recommend a Business to BeSure link</h2>
         </div>
      </div>
      <div class="mt-4 mt-md-5 inner_form_col">
         <form class="main-besure-form">
            <div class="row">
               <div class="form-group col-12 col-md-6">
                  <label class="form-control-label">Business Name</label>
                  <input type="text" class="form-control" placeholder="Lorem Ipsum">
               </div>
               <div class="form-group col-12 col-md-6">
                  <label class="form-control-label">Business Location</label>
                  <input type="text" class="form-control" placeholder="Lorem Ipsum">
               </div>
               <div class="form-group col-12 col-md-6">
                  <label class="form-control-label">Did You Work With a Particular Employee?</label>
                  <select class="form-control custom-select">
                     <option>Yes</option>
                     <option>No</option>
                  </select>
               </div>
               <div class="form-group col-12 col-md-6">
                  <label class="form-control-label">Contact Phone</label>
                  <input type="number" class="form-control" placeholder="Lorem Ipsum">
               </div>
               <div class="form-group col-12 col-md-6">
                  <label class="form-control-label">Contact Email Address</label>
                  <input type="email" class="form-control" placeholder="Lorem Ipsum">
               </div>
               <div class="form-group col-12 col-md-6">
                  <label class="form-control-label">What Type of Companyis This?</label>
                  <select class="form-control custom-select">
                     <option>Payroll</option>
                     <option>Payroll1</option>
                  </select>
               </div>
               <div class="form-group col-12 col-md-6">
                  <label class="form-control-label">State</label>
                  <input type="text" class="form-control" placeholder="Lorem Ipsum">
               </div>
               <div class="form-group col-12 col-md-6">
                  <label class="form-control-label">City</label>
                  <input type="text" class="form-control" placeholder="Lorem Ipsum">
               </div>
               <div class="form-group col-12 col-md-6">
                  <label class="form-control-label">Please explain in detail the type of work that was performe <span class="bd_padd" data-toggle="tooltip" data-placement="top" title="Lorem Ipsum">!</span> </label>
                  <textarea class="form-control"></textarea>
               </div>
               <div class="form-group col-12 col-md-6">
                  <label class="form-control-label">What did you like most about this company? <span class="bd_padd" data-toggle="tooltip" data-placement="top" title="Lorem Ipsum">!</span> </label>
                  <textarea class="form-control"></textarea>
               </div>
               <div class="bottom-new-box col-12">
                  <button class="button btn_col" type="submit">Submit</button>
               </div>
            </div>
         </form>
      </div>
   </div>
</section>
@include('frontend.common.footer')