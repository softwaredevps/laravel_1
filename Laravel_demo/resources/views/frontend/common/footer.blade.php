<footer class="section_margin">
   <div class="inner_footer">
      <div class="container">
         <div class="row">
            <div class="col-12 col-md-auto">
               <a href="index.html"><img class="footer-logo" src="images/logo.png"></a>
            </div>
            <div class="col-12 col-md">
               <ul class="footer-nav d-flex">
                  <li>
                     <a href="javascript:void(0)">Home</a>
                  </li>
                  <li>
                     <a href="javascript:void(0)">Lorem</a>
                  </li>
                  <li>
                     <a href="javascript:void(0)">Ipsum</a>
                  </li>
               </ul>
            </div>
            <div class="col-12 col-md-auto">
               <h5 class="mb-3">Follow Us</h5>
               <ul class="social-links d-flex">
                  <li>
                     <a href="javascript:void(0)"><i class="fab fa-linkedin"></i></a>
                  </li>
                  <li>
                     <a href="javascript:void(0)"><i class="fab fa-twitter"></i></a>
                  </li>
                  <li>
                     <a href="javascript:void(0)"><i class="fab fa-instagram"></i></a>
                  </li>
                  <li>
                     <a href="javascript:void(0)"><i class="fab fa-facebook"></i></a>
                  </li>
               </ul>
            </div>
         </div>
      </div>
   </div>
   <div class="copyrihgt_footer text-center">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <p>Copyright @ 2020 besure All Right Reserved</a></p>
            </div>
         </div>
      </div>
   </div>
</footer>

<script src="{{ asset('assets/frontend/js/slim.min.js') }}"></script>
<script src="{{ asset('assets/frontend/js/popper.min.js') }}"></script>
<script src="{{ asset('assets/frontend/js/bootstrap.min.js') }}"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<script src="{{ asset('assets/frontend/js/slick.min.js') }}"></script>
<script src="{{ asset('assets/frontend/js/custom.js') }}"></script>
</body>

</html>