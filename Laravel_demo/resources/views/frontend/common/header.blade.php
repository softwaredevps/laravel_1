<!doctype html>
<html lang="en">

<head>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
   <link rel="stylesheet" href="{{ asset('assets/frontend/css/bootstrap.min.css') }}">
   <link rel="stylesheet" href="{{ asset('assets/frontend/css/custom.css') }}">
   <link rel="stylesheet" href="{{ asset('assets/frontend/css/font-awesome.min.css') }}">
   <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700&display=swap">   
   <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}">
   <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
   <title>BeSure</title>
</head>

<body>
   <header class="top-header">
      <nav class="navbar navbar-expand-lg ">
         <a class="navbar-brand" href="javascript:;"><img src="images/logo.png" width="125px"></a>
         <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"><img src="images/responsive-menu.png"></span>
         </button>
         <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
               <li class="nav-item active">
                  <a class="nav-link" href="javascript:;">Home <span class="sr-only">(current)</span></a>
               </li>
               <li class="nav-item  position-relative">
                  <a class="nav-link" href="javascript:;">Discover a local Business</a>
                  <ul class="dropdown-menu custom_dropdown">
                     <li><a class="dropdown-item" href="javascript:;">Submit A Review</a></li>
                  </ul>
               </li>

               <li class="nav-item">
                  <a class="nav-link cursor-pointer" href="/memberlogin">Associate Login</a>
               </li>

               <li class="nav-item">
                  <a class="nav-link cursor-pointer" href="/login">Log In</a>
               </li>
               <a class="button_roll_link button btn_col ml-3" href="/registration">Join Now</a>
               <!-- log in Modal -->
               <div class="modal fade login_model" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                     <div class="modal-content">
                        <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                           </button>
                        </div>
                        <div class="modal-body">
                           <div class="logo text-center"><a class="navbar-brand" href="#"><img src="images/logo.png"></a></div>
                           <div class="mt-5 inner_form_col">
                              <h3 class="mb-5">Log In</h3>
                              <form class="form_col login">
                                 <div class="row">
                                    <div class="col-md-12 mb-4">
                                       <div class="form-group email-col">
                                          <input type="text" class="form-control box-shadow" placeholder="Lorem Ipsum">
                                          <span class="input-group-btn">
                                             <span class=" email"><i class="fas fa-envelope"></i></span>
                                          </span>
                                       </div>
                                    </div>
                                    <div class="col-md-12 mb-5">
                                       <div class="input-group">
                                          <input type="password" class="form-control pwd box-shadow" value="iamapassword">
                                          <span class="input-group-btn">
                                             <button class="btn btn-default reveal" type="button"><i class="fas fa-eye"></i></button>
                                          </span>
                                       </div>
                                    </div>
                                    <div class="col-md-12 mb-5">
                                       <a class="forgot_pass" href="#">Forgot Password?</a>
                                    </div>
                                    <div class="col-md-12 mb-5 text-center">
                                       <button class="button btn_col mt-4" type="submit">Log In</button>
                                    </div>
                                    <div class="col-md-12 text-center">
                                       <p>Don't have an account? <a class="active-color font-bold" href="#">Register</a> </p>
                                    </div>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </ul>
      </nav>
   </header>
   <section class="banner_sec">
      <div class="container">
         <div class="row justify-content-center">
            <div class="col-12 col-xl-auto">
               <form class="affiliates-form">
                  <div class="form-row">
                  <div class="col-12 col-sm-6 col-lg-2 col-xl-auto">
                     <label>Affiliate</label>
                     <input type="text" class="form-control" placeholder="Lorem Ipsum">
                  </div>
                  <div class="col-12 col-sm-6 col-lg-2 col-xl-auto">
                     <label>Business Category</label>
                     <input type="text" class="form-control" placeholder="Life">
                  </div>
                  <div class="col-12 col-sm-6 col-lg-2 col-xl-auto">
                     <label>State</label>
                     <select class="form-control chapter_state select-border-true  custom-select" name="chapter_state">
                        <option value="">Select State</option>
                        <?php
                           $city_data = get_states_america(231);
                           
                           foreach ($city_data as $key => $city_data) {

                           ?>
                              <option value="<?php echo $city_data->id; ?>">
                                 <?php echo $city_data->name; ?></option>
                           <?php
                           }
                        ?>

                     </select>
                  </div>
                  <div class="col-12 col-sm-6 col-lg-2 col-xl-auto">
                     <label>City</label>
                     <input type="text" class="form-control" placeholder="City">
                  </div>
                  <div class="bottom-button col-12 col-lg-auto">
                     <button class="button btn_col" type="submit">Search</button>
                  </div>
                  </div>
               </form>
               <div class="banner_text mt-2 mt-lg-5 text-center ">
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
               </div>
            </div>
         </div>
      </div>
   </section>

   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
   <script>
      $(document).on('change', '.chapter_country', function() {
         var id = $(this).val();
         $.ajax({
            url: "/getState",
            method: "POST",
            data: {
               "_token": "{{ csrf_token() }}",
               "data": id,
            },
            success: function(data) {
               $('select[name="chapter_city"]').empty();
               $('select[name="chapter_state"]').empty();
               $.each(data.data, function(e, v) {
                  $(".chapter_state").append("<option value='" + v.id + "'>" + v.name + "</option>");
               });
            }
         })
      });


      //  $(document).on('change', 'select.chapter_state', function() {
      //     var id = $(this).val();
      //     $.ajax({
      //      url:"/getCity",
      //      method:"POST",
      //      data: {
      //          "_token": "{{ csrf_token() }}",
      //          "data": id,
      //      },
      //      success:function(data)
      //      {
      //          //$(".chapter_city select").empty();
      //         // $('select[name="chapter_city"]').empty();
      //          $.each(data.data, function(e,v) {
      //              $(".chapter_city").append("<option value='"+v.state_id+"'>"+v.name+"</option>");
      //          });
      //      } 
      //      })
      //  });
   </script>