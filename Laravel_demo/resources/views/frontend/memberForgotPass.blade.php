@extends('layout.authentication')
@section('title', 'Login')
 

@section('content')
    <div class="card"> 
        <div class="text-center mb-2">
            <a class="header-brand" href="{{route('hrms.index')}}"><i class="fe fe-command brand-logo"></i></a>
        </div>
        <div class="card-body">
            <form method="post">
                @csrf
                <div class="card-title">Forgot Password</div>
                <div class="form-group">
                    <input type="email" name="useremail" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                </div>
                <div class="alert alert-info meesagebox" style="display:none">
                
                </div>
                <div class="form-footer">
                    <!-- <input onClick="MemberResetPas(this)" type="button" class="btn btn-primary btn-block" value="Submit"> -->
                    <button type="button" onClick="MemberResetPas(this)" class="btn btn-primary btn-block">
                        <span class="fa fa-spinner fa-spin" style="display:none;"></span> Submit
                    </button>
                    
                </div>
            </form>
        </div>
    </div>
@stop


<script>

      function MemberResetPas(res) {
        var email = $('input[name="useremail"]').val();
       if(email == ''){
        $('.meesagebox').css('display','block');
        $('.meesagebox').empty();
        $('.meesagebox').append('Please Enter Email.');
       }else{
           $('.fa-spinner.fa-spin').css('display','block');
            $.ajax({ 
                type: 'POST',
                url: '/memberResetPassword',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "data": email,
                },
                success: function(e, xhr, data) {
                    $('.meesagebox').css('display','block');
                    $('.meesagebox').empty();
                    if (data.status == 200) {
                        $('.meesagebox').append('Pleace Check you Email.');
                    } else {
                        $('.meesagebox').append('Please Try Again.');
                    }
                    $('input[name="useremail"]').val('');
                    $('.fa-spinner.fa-spin').css('display','none');
                },
                error: function(data) {
                    $('.meesagebox').css('display','block');
                    var errors = data.responseJSON;
                    if(errors.status == 'error'){
                        $('.meesagebox').empty();
                        $('.meesagebox').append('Please Try Again This Email Not Registered Or Verified.');
                    }
                    $('input[name="useremail"]').val('');
                    $('.fa-spinner.fa-spin').css('display','none');
                }
            });
       }
       
    }

</script>