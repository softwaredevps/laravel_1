@include('frontend.common.header')
<div class="container">
    <div class="row">
        <div class="col-12">
            <!--multisteps-form-->
            <div class="multisteps-form">
                <!--progress bar-->
                <form class="multisteps-form__form" id="registration_form" enctype="multipart/form-data">
                    <!--single form panel-->
                    {{ csrf_field() }}
                    <h2>Associate Application</h2>
                    <div class="multisteps-form__panel rounded js-active" data-animation="scaleIn">

                        <div class="multisteps-form__content">
                            <h4 class="multisteps-form__title">Affiliate Information<br> <span class="small-text">(<span class="redstar">*</span> Indicates the required fields)</span></h4>
                            <p>This information will be used to place you in the beSure Affiliate that suits
                                you best.</p>

                            <div class="row">
                                <div class="form-group col-12 col-md-6">
                                    <label class="form-control-label">Which business category will you be
                                        representing?</label>
                                    <select id="select2" name="business_category" class="form-control">
                                        <option value="">Select Business Category From List</option>
                                        <?php foreach (business_categories_fetch() as $key => $val) { ?>
                                            <option value="<?php echo isset($val->id) ? $val->id : ''; ?>">
                                                <?php echo isset($val->bizcatname) ? $val->bizcatname : ''; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>

                                <div class="form-group col-12 col-md-6">
                                    <label class="form-control-label">My Business Category Is Not Listed,
                                        Please help me in select?</label>
                                    <input type="text" class="form-control" placeholder="Please Mention Your Business Category Here" name="extra_business_category">

                                </div>

                                <div class="form-group col-12 col-md-6">
                                    <label class="form-control-label">Which Affiliate are you interested in
                                        joining?</label>
                                    <select name="affiliate_are" class="form-control custom-select">
                                        <option value="">Select Affiliate</option>
                                        <?php foreach (chapter_tbl_fetch() as $key => $val) { ?>
                                            <option value="<?php echo isset($val->id) ? $val->id : ''; ?>">
                                                <?php echo isset($val->chapter_name) ? $val->chapter_name : ''; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>

                                <div class="form-group col-12 col-md-6">
                                    <label class="form-control-label">Which beSure Associate introduced you
                                        to our organization?</label>
                                    <input type="text" name="besure_organization" class="form-control" placeholder="Which beSure Associate introduced you to our organization">
                                </div>
                            </div>
                        </div>

                        <div class="multisteps-form__content mt-5 register">
                            <h4>Personal Information</h4>
                            <div class="row">
                                <!-- <div class="col-md-2">
                                <div class="form-group"> 
                                <label class="form-control-label">Prefix<span class="redstar">*</span></label> 
                                <select class="form-control" id="sel1" name="prefix" onchange="SetDefault(this);">
                                    <option value="">(choose)</option>
                                    <option value="Mr.">Mr.</option>
                                    <option value="Mrs." selected="">Mrs.</option>
                                    <option value="Dr.">Dr.</option>
                                    <option value="M.">M.</option>
                                </select>
                                </div>
                            </div> -->
                                <div class="form-group col-12 col-md-6 col-lg-4">
                                    <label class="form-control-label">First Name<span class="redstar">*</span></label>
                                    <input type="text" id="name" name="name" placeholder="First Name" class="form-control">
                                </div>

                                <div class="form-group col-12 col-md-6 col-lg-4">
                                    <label class="form-control-label">Last Name<span class="redstar">*</span></label>
                                    <input type="text" id="name" name="last_name" placeholder="Last Name" class="form-control">
                                </div>

                                <div class="form-group col-12 col-md-6 col-lg-4 important-border">
                                    <label class="form-control-label">Preferred Name</label>
                                    <!-- <select class="form-control" id="sel1" name="suffix" onchange="SetDefault(this);">
                                    <option value="">(choose)</option>
                                    <option value="Jr." selected="">Jr.</option>
                                    <option value="Sr.">Sr.</option>
                                </select> -->
                                    <input type="text" id="name" name="suffix" placeholder="Preferred Name" class="form-control">
                                </div>

                                <!-- <div class="col-md-4">
                                <div class="form-group">
                                <label class="form-control-label">Gender<span class="redstar">*</span></label> 
                                <select class="form-control" id="sel1" name="gender" onchange="SetDefault(this);">
                                        <option value="">(choose)</option>
                                        <option value="Male" selected="">Male</option>
                                        <option value="Female">Female</option>
                                        <option value="Do Not Disclose">Do Not Disclose</option>
                                    </select>
                                </div>
                            </div> -->

                                <div class="form-group col-12 col-md-6 col-lg-4">
                                    <label class="form-control-label">Mobile Phone<span class="redstar">*</span></label>
                                    <input type="number" id="name" name="phone_no" placeholder="Mobile Phone" class="form-control" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==10) return false;" >
                                </div>

                                <div class="form-group col-12 col-md-6 col-lg-4">
                                    <label class="form-control-label">Date of Birth(MM:YYYY)<span class="redstar">*</span></label>
                                    <!-- <input type="date" id="date" name="dob" placeholder="Date of Birth" class="box-shadow">  -->

                                    <input name="dob" data-date-format="mm/yyyy" data-provide="datepicker" data-date-autoclose="true" class="form-control" placeholder="Date of Birth" autocomplete="off">


                                    <!-- <input type="date" name="dob" id="datepicker" data-date-autoclose="true" class="box-shadow" placeholder="Date of Birth" value="" autocomplete="off"> -->


                                </div>


                                <div class="form-group col-12 col-md-6 col-lg-4">
                                    <label class="form-control-label">Prefered Email Address<span class="redstar">*</span></label>
                                    <input onchange="checkMail(this)" type="email" id="email" name="your_email" placeholder="Prefered Email Address" class="form-control">

                                </div>
 

                                <!-- <div class="col-md-4">
                                    <div class="form-group">
                                    <label class="form-control-label">Webpage<span class="redstar">*</span></label> 
                                    <input type="text" id="webpage" name="webpage" placeholder="Webpage" class="box-shadow"> 
                                    </div>
                            </div> -->

                                <div class="form-group col-12 col-md-6 col-lg-4">
                                    <label class="form-control-label">Your Headshot</label>
                                    <div class="custom-file">
                                        <input type="file" name="your_image1" class="form-control custom-file-input">
                                        <label class="custom-file-label">No file selected.</label>
                                    </div>
                                </div>

                                <!-- <div class="col-md-6">
                                <div class="form-group">
                                <label class="form-control-label">Home Address<span class="redstar">*<span></label> 
                                <input type="text" id="address" name="home_address" placeholder="Home Address" class="box-shadow"> 
                                </div>
                            </div>
                                
                            <div class="col-md-6">
                                <div class="form-group">
                                    
                                <input type="text" id="date" name="shipping_address" placeholder="Home Address" class="box-shadow" style="margin-top:20px;"> 
                                </div>
                            </div> -->


                            </div>
                            <div class="row">
                                <!-- <div class="col-lg-3 col-md-3">
                                <div class="form-group ">
                                <div>Country <span class="redstar">*</span></div>
                            
                                <select name="country" class="chapter_country box-shadow">
                                    <option value="">Select</option>
                                        <?php foreach (country_fetch() as $key => $val) {
                                            if ($val->code == 'US' || $val->code == 'CA') {
                                        ?>
                                        <option value="<?php echo isset($val->id) ? $val->id : ''; ?>"><?php echo isset($val->id) ? $val->name : ''; ?></option>
                                        <?php }
                                        } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group ">
                                <div>State/Area <span class="redstar">*</span></div>
                                <select name="state" class="chapter_state  custom-select chapter_country box-shadow">

                                    <?php
                                    if (isset($res->chapter_country) && $res->chapter_country != '') {

                                        $city_data = isset($res->chapter_country) ? get_states($res->chapter_country) : '';

                                    ?>
                                        <option value="<?php echo $city_data->id; ?>"><?php echo $city_data->name; ?></option>
                                    <?php
                                    }
                                    ?>
                                    
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <div>City <span class="redstar">*</span></div>
                                        <input type="text" class="chapter_country box-shadow" name="city" value="" placeholder="Enter City">                          
                            </div>
                        </div> 

                        <div class="col-lg-3   col-md-3">
                            <div class="form-group">
                                <div>Zip Code<span class="redstar">*<span> </div>
                                <input type="number" class="box-shadow" placeholder="Zip Code" name="zip_code" 
                                
                                value="<?php echo isset($res->zip_code) ? $res->zip_code : ''; ?>"
                                >
                            </div>
                        </div>

                            <div class="col-md-12">
                            <div class="form-group"> 
                                <label class="form-control-label">Biography<span class="redstar">*</span></label> 
                                <textarea name="bio" class="box-shadow"></textarea>
                            </div>
                            </div> -->

                            </div>

                        </div>

                        <div class="multisteps-form__content bussiness-info-div">
                            <h4>Business Information</h4>
                            <div class="row">
                                <div class="form-group col-12 col-md-6 col-lg-4">
                                    <label class="form-control-label">Company Name <span class="redstar">*</span></label>
                                    <input type="text" id="name" name="business_name" placeholder="Company Name" class="form-control">

                                </div>

                                <div class="form-group col-12 col-md-6 col-lg-4">
                                    <label class="form-control-label">Business Phone Number <span class="redstar">*</span></label>
                                    <input type="number" id="name" name="business_phone_no" placeholder="Business Phone Number" class="form-control" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==10) return false;" >

                                </div>

                                <div class="form-group col-12 col-md-6 col-lg-4">
                                    <label class="form-control-label">Street Address <span class="redstar">*</span></label>
                                    <input type="text" id="company_address" name="company_address" placeholder="Street Address" class="form-control">

                                </div>

                                <div class="form-group col-12 col-md-6 col-lg-4 important-border">
                                    <label class="form-control-label">Suite</label>
                                    <input type="text" id="company_address" name="company_address_second" placeholder="Suite" class="form-control">

                                </div>

                                <div class="form-group col-12 col-md-6 col-lg-4 important-border">
                                    <label class="form-control-label">Company Website</label>
                                    <input type="text" id="bio" name="business_website" placeholder="Company Website" class="form-control">

                                </div>


                                <div class="form-group col-12 col-md-6 col-lg-4">
                                    <div class="registrationCity">
                                        <label class="form-control-label">City<span class="redstar">*</span></label>

                                        <input type="text" class="form-control" name="city" value="" placeholder="Enter City">
                                    </div>
                                </div>

                                <div class="form-group col-12 col-md-6 col-lg-4">
                                    <label class="form-control-label">State/Area <span class="redstar">*</span></label>
                                    <select name="state" class="chapter_state form-control custom-select">

                                        <?php
                                        $city_data = get_states_america(231);

                                        foreach ($city_data as $key => $city_data) {

                                        ?>
                                            <option value="<?php echo $city_data->id; ?>">
                                                <?php echo $city_data->name; ?></option>
                                        <?php
                                        }
                                        ?>

                                    </select>

                                </div>

                                <div class="form-group col-12 col-md-6 col-lg-4">
                                    <label class="form-control-label">Zip Code<span class="redstar">*<span> </label>
                                    <input type="number" class="form-control" placeholder="Zip Code" name="zip_code" value="<?php echo isset($res->zip_code) ? $res->zip_code : ''; ?>">

                                </div>

                                <div class="form-group col-12 col-md-6 col-lg-4">
                                    <label class="form-control-label">Please upload your business card here</label>
                                    <div class="custom-file">
                                        <input type="file" name="your_image2" class="form-control custom-file-input">
                                        <label class="custom-file-label">No file selected.</label>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="multisteps-form__content">
                            <h4>Experience</h4>

                            <div class="row">
                                <div class="form-group col-12 col-lg-6">
                                    <label class="form-control-label">What is your position in the
                                        company?<span class="bd_padd" data-toggle="tooltip" data-placement="top" title="What is your position in the company">!</span></label>
                                    <select name="position_in_comp" class="form-control custom-select">
                                        <option value="owner">Owner</option>
                                        <option value="partner">Partner</option>
                                        <option value="manager">Manager</option>
                                        <option value="sales_representative">Sales Representative</option>
                                        <option value="other">Other</option>
                                    </select>
                                </div>

                                <div class="form-group col-12 col-lg-6">
                                    <label class="form-control-label">How long have you owned/worked for
                                        this company?<span class="bd_padd" data-toggle="tooltip" data-placement="top" title="How long have you owned/worked for this company">!</span></label>
                                    <select name="position_working_year" class="form-control custom-select">
                                        <option value="1">Less than 1 year</option>
                                        <option value="1-3">1-3 Years</option>
                                        <option value="3-10">3-10 Years</option>
                                        <option value="10+">Over 10 years</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-12 col-lg-6">
                                    <label class="form-control-label">How long have you worked in this
                                        industry? <span class="bd_padd" data-toggle="tooltip" data-placement="top" title="How long have you worked in this industry? *">!</span></label>
                                    <select name="position_working_industry" class="form-control custom-select">
                                        <option value="1">Less than 1 year</option>
                                        <option value="1-3">1-3 Years</option>
                                        <option value="3-10">3-10 Years</option>
                                        <option value="10+">Over 10 years</option>
                                    </select>
                                </div>

                                <div class="form-group col-12 col-lg-6">
                                    <label class="form-control-label">Is this your full-time, primary
                                        occupation?<span class="bd_padd" data-toggle="tooltip" data-placement="top" title="Is this your full-time, primary occupation?">!</span></label>
                                    <select name="primary_occupation" class="form-control custom-select">
                                        <option value="1">YES</option>
                                        <option value="0">NO</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-12 col-lg-6">
                                    <label class="form-control-label">Are you currently licensed, bonded
                                        and/or insured to meet the requirements of your occupation? <span class="bd_padd" data-toggle="tooltip" data-placement="top" title="Are you currently licensed, bonded and/or insured to meet the requirements of your occupation">!</span></label>
                                    <select name="bonded" class="form-control custom-select">
                                        <option value="YES">YES</option>
                                        <option value="NO">NO</option>
                                        <option value="N/A">N/A</option>
                                    </select>
                                </div>

                                <div class="form-group col-12 col-lg-6">
                                    <label class="form-control-label">Please list any professional licenses,
                                        degrees, credentials or achievements you have that support your
                                        current occupation.
                                        <span class="bd_padd" data-toggle="tooltip" data-placement="top" title="Please list any professional licenses, degrees, credentials or achievements you have that support your current occupation.">!</span></label>
                                    <input type="text" name="any_professional_licenses" placeholder="Any professional Licenses" class="form-control">

                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-12 col-lg-6">
                                    <label class="form-control-label">Are you currently a Associate of any
                                        business network or organization? If so, please list which
                                        organization(s) such as BNI, LeTip, Chamber of Commerce, Meetup,
                                        etc.
                                        <span class="bd_padd" data-toggle="tooltip" data-placement="top" title="Are you currently a Associate of any business network or organization? If so, please list which organization(s) such as BNI, LeTip, Chamber of Commerce, Meetup, etc.">!</span></label>
                                    <select name="currently_member_of_network" class="form-control custom-select currently_member_of_network">
                                        <option value="">Your Answer</option>
                                        <option value="YES">YES</option>
                                        <option value="NO">NO</option>
                                    </select>
                                </div>


                                <div class="form-group col-12  currently_member_of_network_text" style="display:none;">
                                    <label class="form-control-label">Which network or organization do you
                                        currently belong to?</label>
                                    <input typ="text" name="currently_member_of_network_text" class="form-control" placeholder="Which network or organization do you currently belong to?">

                                </div>
                            </div>
                        </div>
                        <div class="multisteps-form__content">
                            <h4>Profile/Social Media Information</h4>
                            <p>
                                Please take a few minutes to provide this information so we can complete your
                                profile upon approval of your application. This information will allow us to tag
                                you in posts that will help your business grow!
                            </p>
                            <div class="profile">
                                <span><strong>(please fill in complete format http://www.abc.com)</strong></span></br></br>
                                <div class="row">
                                    <div class="form-group col-12 col-lg-6">
                                        <label class="form-control-label">Facebook URL(business page if
                                            available)</label>
                                        <input type="text" id="social_url" name="fb_social_url" placeholder="Facebook Url" class="form-control">

                                    </div>

                                    <div class="form-group col-12 col-lg-6">
                                        <label class="form-control-label">Instagram Account (business
                                            account if available)</label>
                                        <input type="text" id="instagram_social_url" name="instagram_social_url" placeholder="Instagram url" class="form-control">

                                    </div>
                                </div>
                                <div class="row">

                                    <div class="form-group col-12 col-lg-6">
                                        <label class="form-control-label">LinkedIn URL</label>
                                        <input type="text" id="linkedin_social_url" name="linkedin_social_url" placeholder="Linkedin social url" class="form-control">

                                    </div>

                                    <div class="form-group col-12 col-lg-6">
                                        <label class="form-control-label">YouTube Channel URL</label>
                                        <input type="text" id="youtube_social_url" name="youtube_social_url" placeholder="Youtube social url" class="form-control">

                                    </div>
                                </div>
                                <div class="row">

                                    <div class="form-group col-12 col-lg-6">
                                        <label class="form-control-label">TikTok Username</label>
                                        <input type="text" id="tiktok_user_name" name="tiktok_user_name" placeholder="TikTok social url" class="form-control">

                                    </div>

                                    <div class="form-group multi-select-box col-12 col-lg-6">
                                        <label class="form-control-label">Where do your clients submit
                                            reviews for your business?<br> You can do multiple select and type as well.</label>

                                        <select name="where_submit_review[]" multiple="multiple" class="form-control custom-select where_submit_review">
                                            <option value="yelp">Yelp</option>
                                            <option value="google">Google</option>
                                            <option value="bbb">BBB (Better Business Bureau)</option>
                                            <option value="houzz">Houzz</option>
                                            <option value="zillow">Zillow</option>
                                            <option value="tripadvisor">Tripadvisor</option>
                                        </select>
                                        <input type="hidden" class="form-control selectpickerhidden" name="selectpickerhidden">

                                        <!-- <input type="text" name="where_submit_review_text" placeholder="Where do your clients submit reviews" class="form-group col-12 form-control where_submit_review_text" style="margin:10px 0px;"> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="multisteps-form__content checkboxRadio">
                            <h4>Application Requirements</h4>
                            <p>Please check off each statement below to acknowledge
                                you have read, understand and agree to the beSure application requirements.
                            </p>
                            <div class="row">
                                <div class="form-group col-12">
                                    <label class="form-control-label">Please confirm you have received a
                                        copy of the beSure Guidelines, and have read, understand and agree
                                        to follow the Guidelines as an Associate of beSure.</label>
                                </div>

                                <div class="form-group col-12">
                                    <div class="form-check">
                                        <input class="form-check-input" checked type="checkbox" value="" id="male" name="guidelines_0">
                                        <label class="form-check-label" for="male">Yes, I have read, understand and agree to follow the beSure Guidelines.</label>
                                    </div>
                                </div>


                                <div class="form-group col-12">
                                    <label checkedclass="form-control-label">Please confirm that you have
                                        read and understand the beSure Scorecard and agree to achieve 1000
                                        points per month, through various activities and contributions, and
                                        should you fall short of your 1000 point requirement for three
                                        consecutive months your membership will be terminated.</label>
                                </div>

                                <div class="form-group col-12">
                                    <div class="form-check">
                                        <input class="form-check-input" checked type="checkbox" value="" id="guidelines_1" name="guidelines_1">
                                        <label class="form-check-label" for="guidelines_1">Yes, I have read, understand and agree to the guidelines surrounding the beSure Scorecard</label>
                                    </div>
                                </div>

                                <div class="form-group col-12">
                                    <label class="form-control-label">Please confirm that as a new Associate
                                        you will complete your online profile within seven days.</label>
                                </div>

                                <div class="form-group col-12">
                                    <div class="form-check">
                                        <input class="form-check-input" checked type="checkbox" value="" id="guidelines_2" name="guidelines_2">
                                        <label class="form-check-label" for="guidelines_2">Yes, I will complete my online profile within seven days</label>
                                    </div>
                                </div>

                                <div class="form-group col-12">
                                    <label class="form-control-label">Please confirm that as a new Associate
                                        you will select a role or responsibility within your
                                        Affiliate.</label>
                                </div>

                                <div class="form-group col-12">
                                    <div class="form-check">
                                        <input class="form-check-input" checked type="checkbox" value="" id="guidelines_3" name="guidelines_3">
                                        <label class="form-check-label" for="guidelines_3">Yes, I agree to take on a role or responsibility within my Affiliate</label>
                                    </div>
                                </div>


                                <div class="form-group col-12">
                                    <label class="form-control-label">The goal of beSure is to provide its
                                        Associates with the structure they need to become better business
                                        people, increase revenue and achieve a positive source of business
                                        referrals within their local community. To achieve this success, we
                                        ask that our Associates have the discipline to follow our system and
                                        participate when required to do so. Your engagement and
                                        participation will certainly provide you with the biggest benefit as
                                        an Associate of beSure.</label>
                                </div>

                                <div class="form-group col-12">
                                    <div class="form-check">
                                        <input class="form-check-input" checked type="checkbox" value="" id="guidelines_4" name="guidelines_4">
                                        <label class="form-check-label" for="guidelines_4">I agree!.</label>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="multisteps-form__content checkboxRadio">
                            <h4>Investment</h4>
                            <p>Please choose your preferred investment in the beSure organization. Members
                                following our system have received average revenue in excess of $10,000 with
                                some members receiving revenue in the hundreds of thousands of dollars! (this is
                                not a guarantee and your individual results will vary based on business
                                category, personal engagement and other factors) </p>
                            <p>By choosing a membership term below
                                you agree that you are entering into a contract for the term of your
                                choice. Should you vacate your membership in beSure or if your
                                membership in beSure is terminated due to your failure to maintain
                                minimum requirements, you will be charged for the full term of your
                                membership and will be billed periodically until your Investment is
                                paid in full.</p>
                            <div class="row">
                                <div class="form-group col-12">
                                    <div class="form-check">
                                        <input class="form-check-input" checked type="checkbox" value="" id="investment_1" name="investment_1">
                                        <label class="form-check-label" for="investment_1">I have read, understand & agree to the terms of membership as described above.</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-md-6 parment-info-new">
                                    <h6>1-Year Membership</h6>
                                    <div class="row">
                                        <div class="form-group col-12">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="4" id="first_payment" name="six_month_payment">
                                                <label class="form-check-label" for="first_payment">$59 Twelve Monthly Payments</label>
                                            </div>
                                        </div>
                                        <div class="form-group col-12">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="5" id="second_payment" name="six_month_payment">
                                                <label class="form-check-label" for="second_payment">$149 Four Quarterly Payments (15% Savings!)</label>
                                            </div>
                                        </div>
                                        <div class="form-group col-12">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="6" id="third_payment" name="six_month_payment">
                                                <label class="form-check-label" for="third_payment">$499 Single, One-Time Payment (29% Savings!)</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 parment-info-new">
                                    <h6>2-Year Membership - BEST VALUE!</h6>
                                    <div class="row">
                                        <div class="form-group col-12">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="7" id="forth_payment" name="six_month_payment">
                                                <label class="form-check-label" for="forth_payment">$49 Twenty-Four Monthly Payments</label>
                                            </div>
                                        </div>
                                        <div class="form-group col-12">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="8" id="fifth_payment" name="six_month_payment">
                                                <label class="form-check-label" for="fifth_payment">$119 Eight Quarterly Payments (15% Savings!)</label>
                                            </div>
                                        </div>
                                        <div class="form-group col-12">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="9" id="sixth_payment" name="six_month_payment">
                                                <label class="form-check-label" for="sixth_payment">$799 Single, One-Time Payment (29% Savings!)</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 parment-info-new 6month_membership" style="display: none;">
                                    <h6>6-Month Membership 
                                        <div>for current, paying
                                        members of other networking organizations</div>
                                    </h6>
                                    <div class="row">
                                        <div class="form-group col-12">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="1" id="seventh_payment" name="six_month_payment">
                                                <label class="form-check-label" for="seventh_payment">$59 Six Monthly Payments</label>
                                            </div>
                                        </div>
                                        <div class="form-group col-12">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="2" id="eighth_payment" name="six_month_payment">
                                                <label class="form-check-label" for="eighth_payment">$149 Two Quarterly Payments (15% Savings)</label>
                                            </div>
                                        </div>
                                        <div class="form-group col-12">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="3" id="ninth_payment" name="six_month_payment">
                                                <label class="form-check-label" for="ninth_payment">$249 Single, One-Time Payment (29% Savings!)</label>
                                            </div>
                                        </div>
                                        <h6>
                                            <div>proof of active, paid-in-full membership required</div>
                                        </h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="multisteps-form__content checkboxRadio payMentMethodCard">
                            <h4>Payment/Biling Address</h4>
                            <div class="row">
                                <div class="form-group col-12">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="address_billing_type" id="" name="address_billing_type">
                                        <label class="form-check-label" for="">Use Business Address as Billing Address</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row innerCardSec">
                                <div class="form-group col-12 col-md-6">
                                    <label class="form-control-label">Street Address</label>
                                    <input type="text" id="company_address" name="payment_company_address" placeholder="Street Address" class="form-control">

                                </div>

                                <div class="form-group col-12 col-md-6">
                                    <label class="form-control-label">Suite</label>
                                    <input type="text" id="company_address" name="payment_company_address_second" placeholder="Suite" class="form-control">

                                </div>
                                <div class="form-group col-12 col-md-6">
                                    <label>City</label>
                                    <input type="text" class="chapter_country form-control" name="payment_city" value="" placeholder="Enter City">
                                </div>

                                <div class="form-group col-12 col-md-6">
                                    <label>State/Area</label>
                                    <select name="payment_chapter_state" class="chapter_state form-control custom-select">

                                        <?php
                                        $city_data = get_states_america(231);

                                        foreach ($city_data as $key => $city_data) {

                                        ?>
                                            <option value="<?php echo $city_data->id; ?>">
                                                <?php echo $city_data->name; ?></option>
                                        <?php
                                        }
                                        ?>

                                    </select>
                                </div>

                                <div class="form-group col-12 col-md-6">
                                    <label>Zip Code </label>
                                    <input type="number" class="form-control" placeholder="Zip Code" name="payment_zip_code" value="<?php echo isset($res->zip_code) ? $res->zip_code : ''; ?>">
                                </div>

                            </div>
                        </div>
                        <div class="bottom-new-box">
                            <div class="disclaimer-text">
                                <p><b>Disclaimer:</b> I attest that the information provided in this
                                    application is true and accurate to the best of my knowledge. I
                                    understand that if my application is approved by beSure and I am
                                    selected as a Associate, I will be charged for my entire chosen
                                    membership term either one-time, quarterly, or monthly based on my
                                    selection above.</p>
                            </div>
                            <div class="row button-box">
                                <div class="col-12 col-sm-6">
                                    <button class="button btn_col js-btn-prev" type="button" title="Prev" onclick="submitform(this)">
                                        <span class="fa fa-spinner fa-spin hide"></span> Submit Form</button>
                                </div>
                                <div class="col-12 col-sm-6">
                                    <p><span class="redstar">*</span> Indicates the required fields</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@include('frontend.common.footer')

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css">

<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>

<script>
    function checkMail(e) {
        var email = $(e).val();

        $.ajax({
            url: "/checkEmail",
            method: "POST",
            data: {
                "_token": "{{ csrf_token() }}",
                "data": email,
            },
            success: function(data) {
                if (data.data) {
                    toastr.error('This Email Already Taken.');
                    $(e).val('');
                }
            },
            error: function(data) {
                var errors = data.responseJSON;
                var cnt = 0;
                $.each(errors.errors, function(key, val) {
                    if (val[cnt] != '' && val[cnt] != undefined)
                        toastr.error(val[cnt]);
                    cnt++;
                });
            }
        })
    }
    $('input[name="address_billing_type"]').click(function() { 
        if (this.checked) {
            $('.innerCardSec').css('display', 'none');
        } else {
            $('.innerCardSec').css('display', 'flex');
        }

    });

    $("#select2").select2({
        minimumInputLength: 2
    });

    $('select[name="where_submit_review[]"]').select2({
        tags: true,
        placeholder: "Select where client submit review",
        tokenSeparators: [',', ' ']
    });

    $('.extarBusinessCat').on('change', function() {
        $('#select2').select2('val', 1);
    })


    $('.selectpicker').change(function() {
        var selectedItem = $('.selectpicker').val();
        $('.selectpickerhidden').val(selectedItem);
    });

    $(document).ready(function() {
        $('.selectpicker').selectpicker();

        // $("#datepicker").datepicker({
        //   format: "mm-yyyy",
        //   viewMode: "months", 
        //   minViewMode: "months",
        //   // todayHighlight: true,
        //   // autoclose: true,
        //   // clearBtn: datepicker,
        //   endDate: "today"
        // });
    });

    $(document).on('change', 'select.where_submit_review', function() {
        var value = $(this).val();
        // if (value == 'other') {
        //     $('.where_submit_review_text').css('display', 'block');
        // } else {
        //     $('.where_submit_review_text').css('display', 'none');
        //     $('.where_submit_review_text').val('');
        // }
    });


    $(document).on('change', 'select.currently_member_of_network', function() {
        var value = $(this).val();
      
        if (value == 'YES') {
            $('.currently_member_of_network_text').css('display', 'block');
            $('.6month_membership').css('display', 'block');
        } else {
            $('.currently_member_of_network_text').css('display', 'none');
            $("input[name*='currently_member_of_network_text']").val('');
            $('.6month_membership').css('display', 'none');
        }
    });


    // $(document).on('change', '.chapter_country', function() {
    //    var id = $(this).val();
    //    $.ajax({
    //     url:"/getState",
    //     method:"POST",
    //     data: {
    //         "_token": "{{ csrf_token() }}",
    //         "data": id,
    //     },
    //     success:function(data)
    //     {
    //         $('#registration_form select[name="chapter_city"]').empty();
    //         $('#registration_form select[name="chapter_state"]').empty();  
    //         $.each(data.data, function(e,v) {
    //             $("#registration_form .chapter_state").append("<option value='"+v.id+"'>"+v.name+"</option>");
    //         });
    //     } 
    //     })
    // });


    // $(document).on('change', 'select.chapter_state', function() {
    //    var id = $(this).val();
    //    $.ajax({
    //     url:"/getCity",
    //     method:"POST",
    //     data: {
    //         "_token": "{{ csrf_token() }}",
    //         "data": id,
    //     },
    //     success:function(data)
    //     {
    //        // $(".chapter_city select").empty();
    //         //$('select[name="chapter_city"]').empty();
    //         $.each(data.data, function(e,v) {
    //             $(".chapter_city").append("<option value='"+v.state_id+"'>"+v.name+"</option>");
    //         });
    //     } 
    //     })
    // });

 
    function submitform() {
        var isValid = true;
        $('.fa-spinner.fa-spin').removeClass('hide');
        $('.register input,.register textarea,.register select,.bussiness-info-div input').each(function() {
            console.log($(this).attr('name'));

            var offset_top = $('.mt-5.register').offset().top;
            if ($(this).attr('name') != 'your_image1' &&
            $(this).attr('name') != 'your_image2' &&
            $(this).attr('name') != 'company_address_second' && $(this).attr('name') != 'business_website' && $(this).attr('name') != 'business_card_upload' && $(this).attr('name') != 'your_image' && $(this).attr('name') != 'shipping_address' && $(this).attr(
                    'name') != 'webpage' && $(this).attr('name') != 'suffix' && $(this).val() == '' || $(this)
                .val() == undefined) { 
                  
                $(this).css('border', '1px solid red');
                $("html").scrollTop(offset_top - 20);
                isValid = false;
                $('.fa-spinner.fa-spin').addClass('hide');
            } else {
                $(this).css('border', '1px solid #fff');
                isValid = true;
                $('.fa-spinner.fa-spin').removeClass('hide');
            }
        });

        if (isValid) {
            $.ajax({
                url: "/saveregistrationform",
                method: "POST",
                data: new FormData(document.getElementById("registration_form")),
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,
                success: function(data) {
                    $('.fa-spinner.fa-spin').addClass('hide');
                    debugger;

                    if (data.data) {
                        toastr.success('Success.');
                        setTimeout(function() {
                            window.location.replace("/thankyoupage?name=" + data.name);
                        }, 1000);
                    } else {
                        toastr.error('Error.');
                    }
                },
                error: function(data) {
                    $('.fa-spinner.fa-spin').addClass('hide');
                    var errors = data.responseJSON;
                    var cnt = 0;
                    $.each(errors.errors, function(key, val) {
                        if (val[cnt] != '' && val[cnt] != undefined)
                            toastr.error(val[cnt]);
                        cnt++;
                    });
                }
            })
        }
    }

    $("input:checkbox").on('click', function() {
        var $box = $(this);
        if ($box.is(":checked")) {
            var group = "input:checkbox[name='" + $box.attr("name") + "']";
            $(group).prop("checked", false);
            $box.prop("checked", true);
        } else {
            $box.prop("checked", false);
        }
    });
</script>