@extends('layout.master')
@section('title', 'Affiliate List')

@section('content')

<?php 
   // echo "<pre>";print_R($res);die;
?>

<div class="col-lg-12 col-md-12">
    <div class="section-body mt-3">
        <div class="container-fluid">
            <div class="tab-content mt-3">
                <div class="row clearfix">
                    <div class="col-12">
                        <div class="table-responsive">
                        <form>
                        {{ csrf_field() }}
                            <table id="associatelist_table" class="table table-hover table-vcenter table_custom text-nowrap spacing5 border-style mb-0">
                                <thead style="display:none;">
                                    <tr>
                                        <th class="text-left"><strong>Associate</strong></th>
                                    
                                        <th class="text-left"><strong>Appl Dt</strong></th>
                                        <th class=><strong><p class="text-left mb-0">Affiliate</strong></p></th>
                                        <th class="text-left"><strong>Category</strong></th>
                                        <th class="text-left"><strong>City</strong></th>
                                        <th><strong>Action</strong></th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php 
                                    //echo "<pre>";print_R($res);die;
                                        if($res)
                                        foreach( $res as $key=>$data ){
                                    ?>
                                        <tr>
                                            <td class="w90 ">
                                                <a href="/affiliacte_member_info/{{$data->id}}">
                                                    <img class="imguser avatar avatar-xl mr-3"
                                                        onerror="this.src='/images/user.png'"
                                                        src="/images/{{ $data->your_image }}" alt="avatar">
                                                </a>
                                            </td>
                                            <td><span>
                                                <a href="/affiliacte_member_info/{{$data->id}}">
                                                    <?php echo ($data->first_name)?$data->first_name:'...' ?>
                                                    <?php echo ($data->last_name)?$data->last_name:'...' ?>

                                                </a>
                                            </span></td>
                                            <td>
                                                <div class="font-15">
                                                    <?php                                                        
                                                    
                                                    echo (isset($data->business_name) && $data->business_name !='')?$data->business_name:' ...';
                                                    ?> 
                                                </div>
                                                <span class="text-muted">
                                                    <?php $bizcatname =  getBusinessName(@$data->business_category);
                                                        echo isset($bizcatname->bizcatname)?$bizcatname->bizcatname:"...";
                                                    ?>
                                                </span>
                                            </td>
                                            <td>
                                                <span>
                                                        <?php $chapter_name =  getChapterName($data->affiliate_for); 
                                                            echo isset($chapter_name->chapter_name)?$chapter_name->chapter_name:"...";
                                                        ?>  
                                                </span>
                                            </td>

                                            <td>
                                                <span>
                                                <?php 
                                                        $state_name = get_states($data->state);
                                                        echo (isset($state_name->name) && $state_name->name !='')?$state_name->name:' ...';
                                                ?>

                                                    <?php 
                                                        echo (isset($data->city))?', </br>'.$data->city:' ...';
                                                    ?>
                                                    
                                                </span>
                                            </td>

                                            <td>
                                                <a href="/affiliacte_member_info/{{$data->id}}">
                                                    <button type="button" style="margin-left: 0px !important;" class="btn btn-success btn-block" data-toggle="modal"
                                                        data-target="#exampleModal"><i class="fa fa-send"></i>&nbsp;&nbsp;&nbsp;View Detail</button>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php } ?>

                                </tbody>
                            </table>
                            </form>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop 

@section('page-styles')

<link href='http://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css' rel='stylesheet' type='text/css'>
<link href='https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css' rel='stylesheet' type='text/css'>
<link href='https://cdn.datatables.net/colreorder/1.5.2/js/dataTables.colReorder.min.js' rel='stylesheet' type='text/css'>
@stop
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
@section('page-script')
<script src="{{ asset('assets/bundles/fullcalendar.bundle.js') }}"></script>

<script src="{{ asset('assets/plugins/sweetalert/sweetalert.min.js') }}"></script>

<script src="{{ asset('assets/js/core.js') }}"></script>
<script src="{{ asset('assets/js/page/calendar.js') }}"></script>

<script type="text/javascript" src="https://cdn.datatables.net/1.10.8/js/jquery.dataTables.min.js"></script>
@stop