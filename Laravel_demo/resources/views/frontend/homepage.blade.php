
@extends('layout.member_master')
@section('parentPageTitle', 'Member')
@section('title', 'Dashboard')


@section('content')

    <div class="section-body mt-3">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="mb-4">
                        <h4>Welcome 
                            {{($res->first_name)?$res->first_name:''}}  {{($res->last_name)?$res->last_name:''}}
                        </h4>
                    </div>                        
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-md-4 col-sm-6 col-12">
                    <div class="card">
                        <div class="card-body ribbon">
                            <div class="ribbon-box green">3</div>
                            <!-- <a href="{{route('hrms.users')}}" class="my_sort_cut text-muted"> -->
                            <a href="javascript:;" class="my_sort_cut text-muted">
                                <i class="icon-users"></i>
                                <span>Referral Passed</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-12">
                    <div class="card">
                        <div class="card-body ribbon">
                            <div class="ribbon-box orange">0</div>
                            <!-- <a href="{{route('hrms.holidays')}}" class="my_sort_cut text-muted"> -->
                            <a href="javascript:;" class="my_sort_cut text-muted">
                                <i class="fa fa-money"></i>
                                <span>Referral Received</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-12">
                    <div class="card">
                        <div class="card-body ribbon">
                            <div class="ribbon-box green">0</div>
                            <!-- <a href="{{route('hrms.events')}}" class="my_sort_cut text-muted"> -->
                            <a href="javascript:;" class="my_sort_cut text-muted">
                                <i class="icon-calendar"></i>
                                <span>Sponsored</span>
                            </a>
                        </div>
                    </div>
                </div>               
            </div>
        </div>
    </div>
    <div class="section-body grid-dashboard">
        <div class="col-md-4 col-sm-6 col-12">
            <div class="card">
                <strong class="profile-tiles">My Profile</strong>
                <div class="card-body">
                    <div class="media mb-4">
                        <!-- <img class="avatar avatar-xl mr-3" src="../assets/images/profile.png" alt="avatar"> -->
                        <img class="imguser avatar avatar-xl mr-3" id="imguser" class="avatar avatar-xl mr-3" src="/images/{{ $res->your_image }}" alt="" />
                        <div class="media-body">
                            <h5 class="m-0">
                            {{ $res->prefix }} {{ $res->first_name }} {{ $res->last_name }} {{ $res->suffix }}  </h5>
                            <p class="text-muted mb-0">---</p>
                            <p class="text-muted mb-0"> <i class="fa fa-id-card text-muted"></i> <strong>---</strong></p>
                            <p class="text-muted mb-0"> <i class="fa fa-envelope-o"></i> {{($res->your_email)?$res->your_email:'---'}} </p>
                            <p class="text-muted mb-0"> <i class="fa fa-phone"></i> {{($res->phone_no)?$res->phone_no:'---'}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4 col-sm-6 col-12">
            <div class="card">
                <strong class="profile-tiles">My Business</strong>
                <div class="card-body">
                    <div class="media mb-4">
                        <img class="imguser avatar avatar-xl mr-3" id="imguser" class="avatar avatar-xl mr-3" src="/images/{{ $res->your_image }}" alt="" />
                        <div class="media-body">
                            <h5 class="m-0"><strong>{{($res->business_name)?$res->business_name:'---'}}</strong></h5>
                            <p class="text-muted mb-0">{{($res->company_address)?$res->company_address:'---'}}</p>
                            <p class="text-muted mb-0"> <i class="fa fa-id-card text-muted"></i>{{($res->business_phone_no)?$res->business_phone_no:'---'}}</p>
                            <p class="text-muted mb-0"> <i class="fa fa-envelope-o"></i>  {{($res->business_email)?$res->business_email:'---'}}</p>
                            <p class="text-muted mb-0"> {{($res->business_website)?$res->business_website:'---'}} </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4 col-sm-6 col-12">
            <div class="card">
                <strong class="profile-tiles">My Affiliate</strong>
                <div class="card-body">
                    <div class="media mb-4">
                        <div class="media-body">
                            <p class="m-0">LeTip of Upper Montclair, NJ</p>
                            <p class="text-muted mb-0">Category: App Developer</p>
                            <p class="text-muted mb-0">Area: NEW JERSEY</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <strong class="profile-tiles">Area Support</strong>
                <div class="card-body">
                    <div class="media mb-4">
                        <div class="media-body">
                            <p class="m-0"><strong>NEW JERSEY East Coast VP</strong></p>
                            <img class="avatar avatar-xl mr-3" src="../assets/images/user.png" alt="avatar">
                            <p class="text-muted mb-0">Paul Della Valle</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    
    
@stop

@section('page-styles')

@stop

@section('page-script')
<script src="{{ asset('assets/bundles/apexcharts.bundle.js') }}"></script>
<script src="{{ asset('assets/bundles/counterup.bundle.js') }}"></script>
<script src="{{ asset('assets/bundles/knobjs.bundle.js') }}"></script>

<script src="{{ asset('assets/js/core.js') }}"></script>
<script src="{{ asset('assets/js/page/index.js') }}"></script>
@stop


