
@extends('layout.member_master')
@section('parentPageTitle', 'Member')
@section('title', 'Dashboard')


@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Change Your Password</div>
   
                <div class="card-body">
                    <form method="POST">
                        @csrf 
   
                         @foreach ($errors->all() as $error)
                            <p class="text-danger">{{ $error }}</p>
                         @endforeach 
  
                        
                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">New Password</label>
  
                            <div class="col-md-6">
                                <input id="new_password" type="password" class="form-control" name="new_password" autocomplete="current-password">
                            </div>
                        </div>
  
                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">New Confirm Password</label>
    
                            <div class="col-md-6">
                                <input id="new_confirm_password" type="password" class="form-control" name="new_confirm_password" autocomplete="current-password">
                            </div>
                        </div>
   
                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="button" class="btn btn-primary" onClick="changePass();">
                                    Update Password
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
    
    
@stop

@section('page-styles')

@stop

@section('page-script')
<script src="{{ asset('assets/bundles/apexcharts.bundle.js') }}"></script>
<script src="{{ asset('assets/bundles/counterup.bundle.js') }}"></script>
<script src="{{ asset('assets/bundles/knobjs.bundle.js') }}"></script>

<script src="{{ asset('assets/js/core.js') }}"></script>
<script src="{{ asset('assets/js/page/index.js') }}"></script>
@stop


<script>
    function changePass(){
        var new_password = $('input[name=new_password]').val();
        var new_confirm_password = $('input[name="new_confirm_password"]').val();
    // console.log(new_password.length);

        if(new_password == '' && new_confirm_password == ''){
            toastr.error('Password and new confirm password required');
        }else if(new_password != new_confirm_password){
            toastr.error('password and new passwrod not matched.');
        }else if(parseInt(new_password.length) < 6){
            toastr.error('password lenth atleast 6.');
        }else{
            $.ajax({
                type: 'POST',
                url: '/memberChangePassword',
                data: {
                    "_token": $('input[name=_token]').val(),
                    "data": new_password,
                },
                success: function(e, xhr, data) {
                   // console.log(data);
                    if (data.status == 200) {                       
                        toastr.success('Success.');
                        setTimeout(function(){
                             window.location.href = "/logouts";
                        },1000);
                    } else {
                        toastr.success('Please Try Again.');
                    }
                }
            });
        }
       
    }
</script>