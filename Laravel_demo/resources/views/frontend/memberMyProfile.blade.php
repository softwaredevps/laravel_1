@extends('layout.member_master')
@section('parentPageTitle', 'Member')
@section('title', 'My Profile')

@section('content')
<?php 
    $data = $res; 
?> 


<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="nav nav-tabs page-header-tab">
                <li class="nav-item"><a class="nav-link active" id="dashboard-tab" data-toggle="tab"
                        href="#Personal-dashboard">Personal Information</a></li>
                <li class="nav-item"><a class="nav-link" id="Accounts-tab" data-toggle="tab"
                        href="#Business-payemnt">Business / Payment</a></li>
                <li class="nav-item"><a class="nav-link" id="Accounts-tab" data-toggle="tab"
                        href="#Affiliate-Information">Affiliate Information</a></li>
            </ul>

        </div>
    </div>
</div>

<!-- <div class="section-body">
    <div class="container-fluid">
        
        <div class="card">
        
            <div class="form-group" style="padding-top:20px;padding-right:20px;padding-left:40px;"> 
                <div class="float-right editbtn">
                    <button type="button" class="btn btn-primary edit-btn" onclick="setEdit()"><i class="fe fe-edit mr-2"></i>Edit</button>
                </div>
            </div>                
        </div>
    </div>
</div> -->

<div class="section-body mt-3 member-profile-view">
    <div class="container-fluid">
        <div class="tab-content mt-3">

            <div class="tab-pane fade show active" id="Personal-dashboard" role="tabpanel">
                <div class="section-body">
                    <div class="container-fluid">
                        <div class="tab-content">
                            <div class="table-view-wrapper">
                                <div class="card">   
                                
                                <div style="z-index:1;padding-top:20px;position: absolute;right:0;padding-right:20px;padding-left:40px;"> 
                                    <div class="float-right editbtn">
                                        <button type="button" class="btn btn-primary edit-btn" onclick="setEdit()"><i class="fe fe-edit mr-2"></i>Edit</button>
                                    </div>
                                </div>    

                                    <div class="row">
                                        <div class="col-lg-5 col-md-12">                    
                                            <div class="card">
                                                <h5 class="applicaiton_heading">Personal Information</h5>
                                                    <ul class="social-links list-inline mb-0 mt-2">
                                                        @if(@$data->fb_social_url)
                                                            <li class="list-inline-item">
                                                                <a target="_blank" href="{{$data->fb_social_url}}" title=""
                                                                    data-toggle="tooltip" data-original-title="Facebook"><i
                                                                        class="fa fa-facebook"></i></a>
                                                            </li>
                                                        @endif
                                                        @if(@$data->instagram_social_url)
                                                        <li class="list-inline-item">
                                                            <a target="_blank" href="{{$data->instagram_social_url}}"
                                                                title="" data-toggle="tooltip" data-original-title="Instagram"><i
                                                                    class="fa fa-instagram"></i>
                                                            </a>
                                                        </li>
                                                        @endif
                                                        @if(@$data->linkedin_social_url)
                                                        <li class="list-inline-item">
                                                            <a target="_blank" href="{{$data->linkedin_social_url}}" title=""
                                                                data-toggle="tooltip" data-original-title="Linkedin"><i
                                                                    class="fa fa-linkedin"></i>
                                                            </a>
                                                        </li>
                                                        @endif
                                                        @if(@$data->youtube_social_url)
                                                        <li class="list-inline-item">
                                                            <a target="_blank" href="{{$data->youtube_social_url}}" title=""
                                                                data-toggle="tooltip" data-original-title="Youtube"><i
                                                                    class="fa fa-youtube-play"></i>
                                                            </a>
                                                        </li>
                                                        @endif
                                                        @if(@$data->tiktok_user_name)
                                                        <li class="list-inline-item">
                                                            <a target="_blank" href="{{$data->tiktok_user_name}}" title=""
                                                                data-toggle="tooltip" data-original-title="Tiktok"><i
                                                                    class="fa fa-at"></i>
                                                            </a>
                                                        </li>
                                                        @endif
                                                    </ul>
                                                <div class="card-body">
                                                    <div class="media mb-4">
                                                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

                                                        <input type="hidden" name="updated_id" value="<?php echo $data->id; ?>">

                                                        <div id="content">
                                                            <!-- <div id="magilla">
                                                                <img class="avatar avatar-xl mr-3" onerror="this.src='/images/user.png'" src="/images/{{ $data->your_image }}" alt="avatar">
                                                            </div> -->
                                                            <form method="post" id="upload_form1" enctype="multipart/form-data">
                                                                {{ csrf_field() }}
                                                                <input onchange="SetDefaultImg(this);" type="file" name="your_image"
                                                                    style="display:none" id="select_file1" />

                                                                <input type="hidden" name="updated_id" value="<?php echo $data->id; ?>">

                                                                <label for="select_file1" class="float-right">
                                                                    <div id="profile-magilla">

                                                                        <a class="magilla" title="">

                                                                            <img class="imguser avatar avatar-xl mr-3"
                                                                                onerror="this.src='/images/user.png'"
                                                                                src="/images/{{ $data->your_image }}" alt="avatar">

                                                                        </a>

                                                                    </div>
                                                                </label>
                                                            </form>
                                                        </div>


                                                        <div class="media-body">
                                                            <h5 class="m-0">
                                                                <h6>First Name</h6>
                                                                <input disabled onchange="SetDefaultApplication(this);" class="form-control"
                                                                    type="text" name="first_name" value="<?php echo $data->first_name; ?>">

                                                                <h6>Last Name</h6>
                                                                <input disabled onchange="SetDefaultApplication(this);" class="form-control"
                                                                    type="text" name="last_name" value="<?php echo $data->last_name; ?>">


                                                            </h5>

                                                            <p class="text-muted mb-0" title="Dob">
                                                                <h6>Dob</h6>
                                                                <input onchange="SetDefaultApplication(this);"  name="dob" data-date-format="mm/yyyy" data-provide="datepicker"
                                                                    data-date-autoclose="true" class="box-shadow  "
                                                                    placeholder="Date of Birth" autocomplete="off"
                                                                    value="<?php echo ($data->dob)?$data->dob:'...'; ?>">
                                                            </p>

                                                            <p class="text-muted mb-0" title="Suffix">
                                                                <h6>Suffix</h6>
                                                                <input onchange="SetDefaultApplication(this);"  type="text" id="name" name="suffix" placeholder="Preferred Name"
                                                                    class="box-shadow"
                                                                    value="<?php echo ($data->suffix)?$data->suffix:'...'; ?>">
                                                            </p>

                                                        
                                                            <p class="text-muted mb-0" title="Phone">
                                                                <h6>Phone No</h6>
                                                                <input  onchange="SetDefaultApplication(this);" placeholder="Phone Number"
                                                                    class="form-control" type="text" name="phone_no"
                                                                    value="<?php echo ($data->phone_no)?$data->phone_no:'...'; ?>">

                                                            </p>
                                                            <p class="text-muted mb-0" title="Email">
                                                                <h6>Email</h6>
                                                                <input disabled onchange="SetDefaultApplication(this);" placeholder="Email"
                                                                    class="form-control" type="text" name="your_email"
                                                                    value="<?php echo ($data->your_email)?$data->your_email:'...'; ?>">
                                                            </p>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-7 col-md-12">        
                                            <div class="card-body">
                                                <h5>Profile/Social Media Information</h5>
                                                <ul class="new_timeline application_view_page mt-3">
                                                    <li>
                                                        <div class="bullet pink"></div>
                                                        
                                                        <div class="desc">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label class="form-control-label">Facebook URL(business page if
                                                                            available)</label>
                                                                        <input onchange="SetDefaultApplication(this);" type="text" id="social_url" name="fb_social_url"
                                                                            placeholder="Facebook Url" class="box-shadow" value="{{( ! empty($data->fb_social_url) ? $data->fb_social_url : '...') }}">
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label class="form-control-label">Instagram Account (business
                                                                            account if available)</label>
                                                                        <input onchange="SetDefaultApplication(this);" type="text" id="instagram_social_url"
                                                                            name="instagram_social_url" placeholder="Instagram url"
                                                                            class="box-shadow" value="{{( ! empty($data->instagram_social_url) ? $data->instagram_social_url : '...') }}">
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label class="form-control-label">LinkedIn URL</label>
                                                                        <input onchange="SetDefaultApplication(this);" type="text" id="linkedin_social_url"
                                                                            name="linkedin_social_url" placeholder="Linkedin social url"
                                                                            class="box-shadow" value="{{( ! empty($data->linkedin_social_url) ? $data->linkedin_social_url : '...') }}">
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label class="form-control-label">YouTube Channel URL</label>
                                                                        <input onchange="SetDefaultApplication(this);" type="text" id="youtube_social_url" name="youtube_social_url"
                                                                            placeholder="Youtube social url" class="box-shadow" value="{{( ! empty($data->youtube_social_url) ? $data->youtube_social_url : '...') }}">
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label class="form-control-label">TikTok Username</label>
                                                                        <input onchange="SetDefaultApplication(this);" type="text" id="tiktok_user_name" name="tiktok_user_name"
                                                                            placeholder="TikTok social url" class="box-shadow" value="{{( ! empty($data->tiktok_user_name) ? $data->tiktok_user_name : '...') }}">
                                                                    </div>
                                                                </div>
                                                        </div>

                                                        
                                                        <div class="desc">
                                                        
                                                            <h3>Clients submit reviews for your business</h3>
                                                            <h4>

                                                                <select onchange="SetDefaultApplication(this);" name="where_submit_review"
                                                                    id="multiselect1" class="form-control where_submit_review ">
                                                                    <option value="">Select Submit Review</option>
                                                                    <option value="yelp"
                                                                        <?php echo ($data->where_submit_review == 'yelp')?'selected':'' ?>>
                                                                        Yelp</option>
                                                                    <option value="google"
                                                                        <?php echo ($data->where_submit_review == 'google')?'selected':'' ?>>
                                                                        Google</option>
                                                                    <option value="bbb"
                                                                        <?php echo ($data->where_submit_review == 'bbb')?'selected':'' ?>>
                                                                        BBB (Better Business Bureau)</option>
                                                                    <option value="houzz"
                                                                        <?php echo ($data->where_submit_review == 'houzz')?'selected':'' ?>>
                                                                        Houzz</option>
                                                                    <option value="zillow"
                                                                        <?php echo ($data->where_submit_review == 'zillow')?'selected':'' ?>>
                                                                        Zillow</option>
                                                                    <option value="tripadvisor"
                                                                        <?php echo ($data->where_submit_review == 'tripadvisor')?'selected':'' ?>>
                                                                        Tripadvisor</option>
                                                                    <option value="other"
                                                                        <?php echo ($data->where_submit_review == 'other')?'selected':'' ?>>
                                                                        Other</option>
                                                                </select>

                                                                <input type="text" name="where_submit_review_text"
                                                                            placeholder="Where do your clients submit reviews"
                                                                            class="box-shadow where_submit_review_text"
                                                                            style="display:none;">
                                                                        <input type="hidden" class="selectpickerhidden"
                                                                            name="selectpickerhidden">

                                                            </h4>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>

                                        </div>
                                        <div class="col-lg-12 col-md-12">
                                                <div class="card-body">
                                                    <h5>Experience</h5>
                                                    <ul class="new_timeline application_view_page mt-3">
                                                        <li>
                                                            <div class="bullet pink"></div>
                                                            <div class="desc">
                                                                <h3>Position in the company</h3>
                                                                <h4>
                                                                    <!-- <input onchange="SetDefaultApplication(this);" class="form-control" type="text" name="position_in_comp" value="{{($data->position_in_comp)?$data->position_in_comp:'...'}}">
                                                                            -->
                                                                    <select onchange="SetDefaultApplication(this);" name="position_in_comp"
                                                                        class="form-control">
                                                                        <option value="owner"
                                                                            <?php echo ($data->position_in_comp == 'owner')?'selected':''?>>
                                                                            Owner</option>
                                                                        <option value="partner"
                                                                            <?php echo ($data->position_in_comp == 'partner')?'selected':''?>>
                                                                            Partner</option>
                                                                        <option value="manager"
                                                                            <?php echo ($data->position_in_comp == 'manager')?'selected':''?>>
                                                                            Manager</option>
                                                                        <option value="sales_representative"
                                                                            <?php echo ($data->position_in_comp == 'sales_representative')?'selected':''?>>
                                                                            Sales Representative</option>
                                                                        <option value="other"
                                                                            <?php echo ($data->position_in_comp == 'other')?'selected':''?>>
                                                                            Other</option>
                                                                    </select>

                                                                </h4>
                                                            </div>

                                                            <div class="bullet pink"></div>
                                                            <div class="desc">
                                                                <h3>How long have you owned/worked for this company</h3>
                                                                <h4>
                                                                    <!-- <input onchange="SetDefaultApplication(this);" class="form-control" type="text" name="position_working_year" value="{{($data->position_working_year)?$data->position_working_year.' Year':'...'}}"> -->
                                                                    <select onchange="SetDefaultApplication(this);" name="position_working_year"
                                                                        class="form-control">
                                                                        <option value="1"
                                                                            <?php echo ($data->position_working_year == '1')?'selected':''?>>
                                                                            Less than 1 year</option>
                                                                        <option value="1-3"
                                                                            <?php echo ($data->position_working_year == '1-3')?'selected':''?>>
                                                                            1-3 Years</option>
                                                                        <option value="3-10"
                                                                            <?php echo ($data->position_working_year == '3-10')?'selected':''?>>
                                                                            3-10 Years</option>
                                                                        <option value="10+"
                                                                            <?php echo ($data->position_working_year == '10+')?'selected':''?>>
                                                                            Over 10 years</option>
                                                                    </select>

                                                                </h4>
                                                            </div>


                                                            <div class="bullet pink"></div>
                                                            <div class="desc">
                                                                <h3>How long have you worked in this industry</h3>
                                                                <h4>
                                                                    <select onchange="SetDefaultApplication(this);" name=""
                                                                        class="form-control">
                                                                        <option value="1"
                                                                            <?php echo ($data->position_working_industry == '1')?'selected':''?>>
                                                                            Less than 1 year</option>
                                                                        <option value="1-3"
                                                                            <?php echo ($data->position_working_industry == '1-3')?'selected':''?>>
                                                                            1-3 Years</option>
                                                                        <option value="3-10"
                                                                            <?php echo ($data->position_working_industry == '3-10')?'selected':''?>>
                                                                            3-10 Years</option>
                                                                        <option value="10+"
                                                                            <?php echo ($data->position_working_industry == '10+')?'selected':''?>>
                                                                            Over 10 years</option>
                                                                    </select>

                                                                </h4>
                                                            </div>

                                                            <div class="bullet pink"></div>
                                                            <div class="desc">
                                                                <h3>Full-time, primary occupation</h3>
                                                                <h4>
                                                                    <select onchange="SetDefaultApplication(this);" name="primary_occupation"
                                                                        class="form-control">
                                                                        <option value="1"
                                                                            <?php echo ($data->primary_occupation == '1')?'selected':''?>>YES
                                                                        </option>
                                                                        <option value="0"
                                                                            <?php echo ($data->primary_occupation == '0')?'selected':''?>>NO
                                                                        </option>
                                                                    </select>

                                                                </h4>
                                                            </div>


                                                            <div class="bullet pink"></div>
                                                            <div class="desc">
                                                                <h3>Currently licensed</h3>
                                                                <h4>
                                                                    <!-- <input  class="form-control" type="text" name="bonded" value="{{($data->bonded)?$data->bonded:'...'}}"> -->

                                                                    <select onchange="SetDefaultApplication(this);" name="bonded"
                                                                        class="form-control">
                                                                        <option value="YES" <?php echo ($data->bonded == 'YES')?'selected':''?>>
                                                                            YES</option>
                                                                        <option value="NO" <?php echo ($data->bonded == 'NO')?'selected':''?>>NO
                                                                        </option>
                                                                        <option value="N/A" <?php echo ($data->bonded == 'N/A')?'selected':''?>>
                                                                            N/A</option>
                                                                    </select>

                                                                </h4>
                                                            </div>

                                                            <div class="bullet pink"></div>
                                                            <div class="desc">
                                                                <h3>Professional licenses</h3>
                                                                <h4>
                                                                    <input onchange="SetDefaultApplication(this);" class="form-control"
                                                                        type="text" name="any_professional_licenses"
                                                                        value="{{($data->any_professional_licenses)?$data->any_professional_licenses:'...'}}">

                                                                </h4>
                                                            </div>

                                                            <div class="bullet pink"></div>
                                                            <div class="desc">
                                                                <h3>Currently a Associate of any business network</h3>
                                                                <h4>
                                                                    <select onchange="SetDefaultApplication(this);"
                                                                        name="currently_member_of_network"
                                                                        class="form-control currently_member_of_network">
                                                                        <option value="">Your Answer</option>
                                                                        <option value="YES"
                                                                            <?php echo ($data->currently_member_of_network == 'YES')?'selected':''?>>
                                                                            YES</option>
                                                                        <option value="NO"
                                                                            <?php echo ($data->currently_member_of_network == 'NO')?'selected':''?>>
                                                                            NO</option>
                                                                    </select>
                                                                </h4>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="Business-payemnt" role="tabpanel">
                <div class="section-body">
                    <div class="container-fluid">
                        <div class="tab-content">
                            <div class="table-view-wrapper">
                                <div class="card"> 
                                <div style="z-index:1;padding-top:20px;position: absolute;right:0;padding-right:20px;padding-left:40px;"> 
                                    <div class="float-right editbtn">
                                        <button type="button" class="btn btn-primary edit-btn" onclick="setEdit()"><i class="fe fe-edit mr-2"></i>Edit</button>
                                    </div>
                                </div>   
                                    <div class="row">
                                        <div class="col-lg-5 col-md-12">                    
                                            <div class="card">
                                                <h5 class="applicaiton_heading">Business Information</h5>
                                                <div class="card-body">
                                                    <div class="media mb-4">
                                                    <form method="post" id="upload_form2" enctype="multipart/form-data">
                                                            {{ csrf_field() }}
                                                            <input onchange="SetDefaultImgCard(this);" type="file"
                                                                name="business_card_upload" style="display:none" id="select_file2" />

                                                            <input type="hidden" name="updated_id" value="<?php echo $data->id; ?>">

                                                            <label for="select_file2" class="float-right">
                                                                <div id="bussiness-magilla">

                                                                    <a class="magilla" title="">
                                                                        <img class="imgusercards avatar avatar-xl mr-3"
                                                                            src="/images/{{ $data->business_card_upload }}"
                                                                            onerror="this.src='/images/busineess_pic.png ' " alt="avatar" />
                                                                        
                                                                    </a>
                                                                </div>
                                                            </label>
                                                        </form>

                                                    <div class="media-body">    

                                                    


                                                        <h5 class="m-0" title="Business Name">
                                                            <h6>Business Name</h6>
                                                            <input onchange="SetDefaultApplication(this);" placeholder="Business Name"
                                                                class="form-control" type="text" name="business_name"
                                                                value="<?php echo ($data->business_name)?$data->business_name:'...'; ?> ">

                                                            <?php //echo ($data->business_name) ?>
                                                        </h5>
                                                        <p class="text-muted mb-0" title="Business Website">
                                                            <h6>Business Website</h6>
                                                            <input onchange="SetDefaultApplication(this);" placeholder="Business Website"
                                                                class="form-control" type="text" name="business_website"
                                                                value="<?php echo ($data->business_website)?$data->business_website:'...'; ?> ">

                                                        </p>
                                                        <p class="text-muted mb-0" title="Business Address">
                                                            <h6>Business Address</h6>
                                                            <input onchange="SetDefaultApplication(this);" placeholder="Company Address"
                                                                class="form-control" type="text" name="company_address"
                                                                value="<?php  echo ($data->company_address)?$data->company_address:'...';  ?>">
                                                        </p>

                                                        <p class="text-muted mb-0" title="Suite">
                                                            <h6>Suite</h6>
                                                            <input onchange="SetDefaultApplication(this);" placeholder="Suite"
                                                                class="form-control" type="text" name="company_address_second"
                                                                value="<?php echo ($data->company_address_second)?$data->company_address_second:'...'; ?>">
                                                        </p>

                                                        <p class="text-muted mb-0" title="Business Phone">
                                                            <h6>Business Phone</h6>
                                                            <input onchange="SetDefaultApplication(this);" placeholder="Business Phone No"
                                                                class="form-control" type="text" name="business_phone_no"
                                                                value="<?php echo ($data->business_phone_no)?$data->business_phone_no:'...'; ?>">
                                                        </p>


                                                        <p class="text-muted mb-0" title="city">
                                                            <h6>city</h6>
                                                            <input onchange="SetDefaultApplication(this);" type="text" class="box-shadow"
                                                                name="city" placeholder="Enter City"
                                                                value="<?php echo ($data->city)?$data->city:'...'; ?>">
                                                        </p>

                                                        <p class="text-muted mb-0" title="State/Area">
                                                            <h6>State</h6>
                                                            <select onchange="SetDefaultApplication(this);" name="state"
                                                                class="form-control chapter_state box-shadow">

                                                                <?php
                                                                $city_data = get_states_america(231); 

                                                                foreach($city_data as $key=>$city_data){    
                                                                            
                                                                ?>
                                                                <option value="<?php echo $city_data->id; ?>"
                                                                    <?php echo ($city_data->id == $data->state)?'selected':''; ?>>
                                                                    <?php echo $city_data->name; ?>
                                                                </option>
                                                                <?php
                                                                } 
                                                                ?>

                                                            </select>
                                                        </p>


                                                        <p class="text-muted mb-0" title="Zip Code">
                                                            <h6>Zip Code</h6>
                                                            <input onchange="SetDefaultApplication(this);" type="number" class="box-shadow"
                                                                placeholder="Zip Code" name="zip_code"
                                                                value="<?php echo ($data->zip_code)?$data->zip_code:'...'; ?>">
                                                        </p>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                        <div class="col-lg-7 col-md-12">        
                                            <div class="card-body">
                                                <h5>Payment/Biling Address</h5>

                                                <div class="row innerCardSec">
                                                    <div class="col-md-12">

                                                        <div class="row address_billing_typecardPayment mt-12">

                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="form-control-label">Street Address</label>
                                                                    <input onchange="SetDefaultApplication(this);" type="text" id="company_address" name="payment_company_address"
                                                                        placeholder="Street Address" class="form-control" value="<?php echo ($data->payment_company_address)?$data->payment_company_address:'...'; ?>">
                                                                </div>
                                                            </div>

                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="form-control-label">Suite</label>
                                                                    <input onchange="SetDefaultApplication(this);" type="text" id="company_address"
                                                                        name="payment_company_address_second" placeholder="Suite"
                                                                        class="form-control" value="<?php echo ($data->payment_company_address_second)?$data->payment_company_address_second:'...'; ?>">
                                                                </div>
                                                            </div>

                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label>City</label>
                                                                    <input onchange="SetDefaultApplication(this);" type="text" class="chapter_country form-control"
                                                                        name="payment_city" placeholder="Enter City" value="<?php echo ($data->payment_city)?$data->payment_city:'...'; ?>">
                                                                </div>
                                                            </div>

                                                            <div class="col-md-4">
                                                                <div class="form-group ">
                                                                    <label>State/Area</label>
                                                                    <select onchange="SetDefaultApplication(this);" name="payment_chapter_state" class="chapter_state form-control">

                                                                        <?php
                                                                            $city_data = get_states_america(231); 
                                                                            
                                                                            foreach($city_data as $key=>$city_data){    
                                                                                                    
                                                                        ?>
                                                                        <option value="<?php echo $city_data->id; ?>"
                                                                        <?php echo ($city_data->id == $data->payment_chapter_state)?'selected':''; ?>
                                                                        >
                                                                            <?php echo $city_data->name; ?></option>
                                                                        <?php
                                                                            } 
                                                                        ?>

                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="col-lg-4  col-md-3">
                                                                <div class="form-group">
                                                                    <label>Zip Code<span class="redstar">*<span> </label>
                                                                    <input onchange="SetDefaultApplication(this);" type="number" class="form-control" placeholder="Zip Code"
                                                                        name="payment_zip_code"
                                                                        value="<?php echo ($data->payment_zip_code)?$data->payment_zip_code:'...'; ?>" >
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="Affiliate-Information" role="tabpanel">
                <div class="section-body">
                    <div class="container-fluid">
                        <div class="tab-content">
                            <div class="table-view-wrapper">
                                <div class="card"> 
                                        <div class="row">     
                                            <div class="col-lg-12 col-md-12"> 
                                                <div class="">
                                                    <div class="card-body">
                                                        <h5>Affiliate Information</h5>
                                                        <ul class="new_timeline application_view_page mt-3">
                                                            <li>
                                                                <div class="bullet pink"></div>
                                                                <div class="desc">
                                                                    <h3>Affiliate</h3>
                                                                    <h4>

                                                                        <select disabled name="affiliate_for" class="form-control"
                                                                            onchange="SetDefaultApplication(this);">
                                                                            <option value="">Select Affiliate</option>
                                                                            <?php foreach(chapter_tbl_fetch() as $key=>$val){?>
                                                                            <option value="<?php echo isset($val->id)?$val->id:''; ?>"
                                                                                <?php echo ($data->affiliate_for == $val->id)?'selected':''; ?>>
                                                                                <?php echo isset($val->chapter_name)?$val->chapter_name:''; ?>
                                                                            </option>
                                                                            <?php } ?>
                                                                        </select>
                                                                    </h4>
                                                                </div>
                                                            </li>

                                                            <li>
                                                                <div class="bullet pink"></div>
                                                                <div class="desc">
                                                                    <h3>Business Category </h3> 
                                                                    <select disabled  id="select2" name="business_category" class="form-control"
                                                                        onchange="SetDefaultApplication(this);">
                                                                        <option value="">Select Business Category From List</option>
                                                                        <?php foreach(business_categories_fetch() as $key=>$val){?>
                                                                        <option value="<?php echo isset($val->id)?$val->id:''; ?>"
                                                                            <?php echo ($data->business_category == $val->id)?'selected':''; ?>>
                                                                            <?php echo isset($val->bizcatname)?$val->bizcatname:''; ?>
                                                                        </option>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <button style="width:60%;" type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-send"></i>&nbsp;Request Affiliate Transfer</button>
                                                                <button style="width:60%;" type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-send"></i>&nbsp;Request Category Change</button>
        
                                                            </li>

                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- <div class="col-lg-12 col-md-12"> 
                                                <div class="card-body">
                                                    <h5>Sponsership</h5>
                                                    <ul class="new_timeline application_view_page mt-3">
                                                    
                                                    
                                                        <div class="col-lg-12 col-md-12">
                                                                <div class="form-group">
                                                                    <h3>Which beSure Associate introduced you to our organization?</h3>
                                                                    <h4>
                                                                        <input onchange="SetDefaultApplication(this);" type="text"
                                                                            name="besure_organization" class="form-control"
                                                                            value="{{( ! empty($data->besure_organization) ? $data->besure_organization : '...') }}">

                                                                    </h4>
                                                                </div>
                                                            </div>

                                                            <div class="col-lg-12 col-md-12">
                                                                <div class="form-group">
                                                                    <div>Select Affiliate </div>

                                                                    <select class="form-control" name="ChapterID">
                                                                        <option value="">Select Affiliate</option>
                                                                        <?php foreach(chapter_tbl_fetch() as $key=>$val){?>
                                                                        <option value="<?php echo isset($val->id)?$val->id:''; ?>">
                                                                            <?php echo isset($val->chapter_name)?$val->chapter_name:''; ?>
                                                                        </option>
                                                                        <?php } ?>
                                                                    </select>

                                                                </div>
                                                            </div>

                                                            <div class="col-lg-12 col-md-12">
                                                                <div class="form-group">
                                                                    <div>Select Associate<span class="redstar"><span> </div>
                                                                    <select onchange="SetDefaultApplication(this);" class="form-control"
                                                                        name="sponsershipMemberid">
                                                                        <?php 
                                                                            if(isset($data->sponsershipMemberid) && $data->sponsershipMemberid > 0){
                                                                            
                                                                                $sponser_name = getsponsername($data->sponsershipMemberid);
                                                                                
                                                                            }
                                                                        ?>

                                                                        <option
                                                                            value="<?php echo isset($sponser_name->id)?$sponser_name->id:''; ?>">
                                                                            <?php echo isset($sponser_name->first_name)?$sponser_name->first_name:''; ?>
                                                                            <?php echo isset($sponser_name->last_name)?$sponser_name->last_name:''; ?>
                                                                        </option>

                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div> -->

                                            <div class="col-lg-12 col-md-12"> 
                                                <div class="card-body">
                                                    <h5>Investment</h5>
                                                    <ul class="new_timeline application_view_page mt-3">
                                                        <li title="Payment Info">
                                                            <div class="bullet pink"></div>
                                                            <div class="desc">
                                                                <span>
                                                                    <?php 
                                                                        echo 'Member since '. date("d-M-Y", strtotime($data->created_at));
                                                                    ?>
                                                                </span>
                                                                <div class="desc">

                                                                    <h4>

                                                                        <select disabled onchange="SetDefaultApplication(this);" name="six_month_payment"
                                                                            id="multiselect1"
                                                                            class="form-control where_submit_review">
                                                                            <optgroup label="1s-Year Membership">
                                                                                <option value="4"
                                                                                    <?php echo ($data->six_month_payment == 4)?'selected':'' ?>>
                                                                                    $59 Twelve Monthly Payments</option>
                                                                                <option value="5"
                                                                                    <?php echo ($data->six_month_payment == 5)?'selected':'' ?>>
                                                                                    $149 Two Quarterly Payments (15% Savings)</option>
                                                                                <option value="6"
                                                                                    <?php echo ($data->six_month_payment == 6)?'selected':'' ?>>
                                                                                    $249 Single, One-Time Payment (29% Savings!)</option>
                                                                            </optgroup>
                                                                            <optgroup label="2-Year Membership - BEST VALUE!">
                                                                                <option value="7"
                                                                                    <?php echo ($data->six_month_payment == 7)?'selected':'' ?>>
                                                                                    $49 Twenty-Four Monthly Payments</option>
                                                                                <option value="8"
                                                                                    <?php echo ($data->six_month_payment == 8)?'selected':'' ?>>
                                                                                    $119 Eight Quarterly Payments (15% Savings!)</option>
                                                                                <option value="9"
                                                                                    <?php echo ($data->six_month_payment == 9)?'selected':'' ?>>
                                                                                    $799 Single, One-Time Payment (29% Savings!)</option>
                                                                            </optgroup>

                                                                            <optgroup label="6-Month Membership">
                                                                                <option value="1"
                                                                                    <?php echo ($data->six_month_payment == 1)?'selected':'' ?>>
                                                                                    $59 Six Monthly Payments</option>
                                                                                <option value="2"
                                                                                    <?php echo ($data->six_month_payment == 2)?'selected':'' ?>>
                                                                                    $119 Eight Quarterly Payments (15% Savings!)</option>
                                                                                <option value="3"
                                                                                    <?php echo ($data->six_month_payment == 3)?'selected':'' ?>>
                                                                                    $799 Single, One-Time Payment (29% Savings!)</option>
                                                                            </optgroup>
                                                                        </select>
                                                                    </h4>
                                                                </div>

                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>                                 
                                            </div>
 
                                            <div class="col-lg-12 col-md-12">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <h5>Sponsored By</h5>
                                                        
                                                        <div class="lazy-resource-panel">
                                                            <div class="lazy-resource-panel">
                                                                <table class="table table-striped">
                                                                    <thead>
                                                                        <tr>
                                                                        <th>Member</th>
                                                                        <th>Affiliate</th>
                                                                        <th>Category</th>
                                                                        <th>Phone</th>
                                                                        <th>Email Address</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    <?php 
                                                                      if(isset($data->sponsershipMemberid) && $data->sponsershipMemberid > 0){
                                                    
                                                                        $sponser_name = getsponsernamebyme($data->sponsershipMemberid);
                                                                        
                                                                        foreach($sponser_name as $key=>$val){
                                                                            
                                                                                // if(isset($data->id) && $data->id > 0){
                                                                                
                                                                                //     $sponser_name = getsponsernamebyme($data->id);
                                                                                    
                                                                                //     foreach($sponser_name as $key=>$val){
                                                                                     //   echo "";print_R($val->first_name);die;
                                                                            ?>
                                                                        <tr>
                                                                       
                                                                        <td style="vertical-align: middle;">
                                                                            <div class="d-flex align-items-center">
                                                                                <div>
                                                                                    <a href="/affiliacte_member_view_info/{{$val->uuid}}" class="sponser_name"> 
                                                                                        <img class="sponser_img" onerror="this.src='/images/user.png'"  src="/images/{{ $val->your_image }}" style="width: 35px; height: 35px; object-fit: cover; object-position: center top; background-color: rgb(204, 204, 204);">
                                                                                    </a>    
                                                                                </div>
                                                                                <div class="ml-2">
                                                                                    <a href="/affiliacte_member_view_info/{{$val->uuid}}" class="sponser_name"> 
                                                                                        <?php echo isset($val->first_name)?$val->first_name:''; ?>
                                                                                        <?php echo isset($val->last_name)?$val->last_name:''; ?>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </td>

                                                                        <td>
                                                                            <?php $chapter_name =  getChapterName(@$val->affiliate_for); 
                                                                                echo isset($chapter_name->chapter_name)?$chapter_name->chapter_name:"...";
                                                                            ?>  
                                                                        </td>

                                                                        <td>
                                                                            <?php $bizcatname =  getBusinessName(@$val->business_category);
                                                                                echo isset($bizcatname->bizcatname)?$bizcatname->bizcatname:"...";
                                                                            ?>
                                                                        </td>

                                                                        <td class="sponser_phone" style="vertical-align: middle;"><?php echo isset($val->phone_no)?$val->phone_no:''; ?></td>
                                                                        <td class="sponser_email" style="vertical-align: middle;"><?php echo isset($val->your_email)?$val->your_email:''; ?></td>
                                                                        
                                                                      
                                                                    </tr>
                                                                    <?php } } ?>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>


                                                    </div>
                                                </div>
                                            </div>
                                      

                                            <div class="col-lg-12 col-md-12">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <h5>Sponsored</h5>
                                                        
                                                      
                                                        <div class="lazy-resource-panel">
                                                            <div class="lazy-resource-panel">
                                                                <table class="table table-striped">
                                                                    <thead>
                                                                        <tr>
                                                                        <th>Member</th>
                                                                        <th>Affiliate</th>
                                                                        <th>Category</th>
                                                                        <th>Phone</th>
                                                                        <th>Email Address</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    <?php 
                                                                        if(isset($data->id) && $data->id > 0){
                                                                                                                            
                                                                            $sponser_name = getsponsername($res->id);
                                                                            
                                                                            foreach($sponser_name as $key=>$val){

                                                                                // if(isset($data->sponsershipMemberid) && $data->sponsershipMemberid > 0){
                                                                                
                                                                                //     $sponser_name = getsponsername($data->sponsershipMemberid);
                                                                                    
                                                                                //     foreach($sponser_name as $key=>$val){
                                                                                     //   echo "";print_R($val->first_name);die;
                                                                            ?>
                                                                        <tr>
                                                                       
                                                                        <td style="vertical-align: middle;">
                                                                            <div class="d-flex align-items-center">
                                                                                <div>
                                                                                    <a href="/affiliacte_member_view_info/{{$val->uuid}}" class="sponser_name"> 
                                                                                        <img class="sponser_img" onerror="this.src='/images/user.png'"  src="/images/{{ $val->your_image }}" style="width: 35px; height: 35px; object-fit: cover; object-position: center top; background-color: rgb(204, 204, 204);">
                                                                                    </a>
                                                                                </div>
                                                                                <div class="ml-2">
                                                                                    <a href="/affiliacte_member_view_info/{{$val->uuid}}" class="sponser_name"> 
                                                                                        <?php echo isset($val->first_name)?$val->first_name:''; ?>
                                                                                        <?php echo isset($val->last_name)?$val->last_name:''; ?>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </td>

                                                                        <td>
                                                                            <?php $chapter_name =  getChapterName(@$val->affiliate_for); 
                                                                                echo isset($chapter_name->chapter_name)?$chapter_name->chapter_name:"...";
                                                                            ?>  
                                                                        </td>

                                                                        <td>
                                                                            <?php $bizcatname =  getBusinessName(@$val->business_category);
                                                                                echo isset($bizcatname->bizcatname)?$bizcatname->bizcatname:"...";
                                                                            ?>
                                                                        </td>

                                                                        <td class="sponser_phone" style="vertical-align: middle;"><?php echo isset($val->phone_no)?$val->phone_no:''; ?></td>
                                                                        <td class="sponser_email" style="vertical-align: middle;"><?php echo isset($val->your_email)?$val->your_email:''; ?></td>
                                                                        
                                                                      
                                                                    </tr>
                                                                    <?php } } ?>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>     


            
        </div>
    </div>
</div>



     
@stop


@section('page-styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css">

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<link rel="stylesheet" href="{{ asset('assets/plugins/fullcalendar/fullcalendar.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/sweetalert/sweetalert.css') }}">

<link href='http://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css' rel='stylesheet' type='text/css'>
<link href='https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css' rel='stylesheet'
    type='text/css'>
<link href='https://cdn.datatables.net/colreorder/1.5.2/js/dataTables.colReorder.min.js' rel='stylesheet'
    type='text/css'>

@stop

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
@section('page-script')
<script src="{{ asset('assets/bundles/fullcalendar.bundle.js') }}"></script>

<script src="{{ asset('assets/plugins/sweetalert/sweetalert.min.js') }}"></script>

<script src="{{ asset('assets/js/core.js') }}"></script>
<script src="{{ asset('assets/js/page/calendar.js') }}"></script>

<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

@stop

<script>

        $(function() {
            var imgval = $("#profile-magilla .magilla").find('img').attr('src');
            $("#profile-magilla .magilla").tooltip({
                content: '<img src="' + imgval + '" />'
            });

            var imgval = $("#bussiness-magilla .magilla").find('img').attr('src');
            $("#bussiness-magilla .magilla").tooltip({
                content: '<img src="' + imgval + '" />'
            });

        });

        $(document).on('click','.edit-btn',function(){
            $('.editbtn').removeClass('pointerEvent');
            if (!$('.Employee-view-border-one').is(":visible")) {
              //  $('.member-profile-view input,.member-profile-view select').css('border:1px solid;pointer-events:auto');
                $('input').addClass('Employee-view-border-one');
                $('select').addClass('Employee-view-border-one');
                $('.approve-app').css('display','none');
                $('.reject-app').css('display','none');
                $('.edit-btn').html('Done Editing.');
            } else {
                $('input').removeClass('Employee-view-border-one');
                $('select').removeClass('Employee-view-border-one');
                $('.approve-app').css('display','block');
                $('.reject-app').css('display','block');
                $('.edit-btn').html('Edit.');
            }
        })

        function SetDefaultApplication(res) {
            var name = res.name;
            var data = res.value;
            var updated_id = $('input[name=updated_id]').val();
            $.ajax({
                type: 'POST',
                url: '/ajaxRequestApplicationMember',
                data: {
                    "_token": $('#token').val(),
                    "data": data,
                    "type": name,
                    "updated_id": updated_id
                },
                success: function(e, xhr, data) {
                    if (data.status == 200) {
                        toastr.success('Success.');
                    } else {
                        toastr.success('Please Try Again.');
                    }
                }
            });
        }

        
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('.imguser').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    function SetDefaultImgCard(event) {
        readURLCard(event);
        $.ajax({
            url: "{{ route('ajaxupload.action_your_card') }}",
            method: "POST",
            data: new FormData(document.getElementById("upload_form2")),
            dataType: 'JSON',
            contentType: false,
            cache: false,
            processData: false,
            success: function(data) {
                if (data.class_name == 'alert-danger') {
                    toastr.error('The select file not supported try diffrent image.');
                } else {
                    toastr.success('Success.');
                }
            }
        })
    };

    function SetDefaultImg(event) {
        readURL(event);
        $.ajax({
            url: "{{ route('ajaxupload.action_your_image') }}",
            method: "POST",
            data: new FormData(document.getElementById("upload_form1")),
            dataType: 'JSON',
            contentType: false,
            cache: false,
            processData: false,
            success: function(data) {
                if (data.class_name == 'alert-danger') {
                    toastr.error('The select file not supported try diffrent image.');
                } else {
                    toastr.success('Success.');
                }
            }
        })
    };

    function readURLCard(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('.imgusercards').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }


    $(document).on('change', 'select[name="ChapterID"]', function() {
        var id = $(this).val();
        $.ajax({ 
            url: "/getMembers",
            method: "POST",
            data: {
                "_token": "{{ csrf_token() }}",
                "data": id,
            }, 
            success: function(data) {
                $('select[name="sponsershipMemberid"]').empty();
                $('select[name="sponsershipMemberid"]').append(
                    "<option value=''>Select Associate</option>");
                $.each(data, function(e, v) {
                    $('select[name="sponsershipMemberid"]').append("<option value='" + v.id +
                        "'>" + v.first_name + ' ' + v.last_name + ' (' + v.your_email +
                        ')' + "</option>");
                        
                });
            }
        })
    });

    $(document).on('change', 'select[name="ChapterID"]', function() {
    var id = $(this).val();
        $.ajax({
            url: "/getMembersProfile",
            method: "POST",
            data: {
                "_token": "{{ csrf_token() }}",
                "data": id,
            },
            success: function(data) {
                $('select[name="sponsershipMemberid"]').empty();
                $('select[name="sponsershipMemberid"]').append(
                    "<option value=''>Select Associate</option>");
                $.each(data, function(e, v) {
                    $('select[name="sponsershipMemberid"]').append("<option value='" + v.id +
                        "'>" + v.first_name + ' ' + v.last_name + ' (' + v.your_email +
                        ')' + "</option>");
                });
            }
        })
    });
</script>