@extends('layout.member_master')
@section('title', 'Invite a Guest')
@section('content')
<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center mb-3">
            <ul class="nav nav-tabs page-header-tab">

                <li class="nav-item"><a class="{{ (Request::segment(1) === 'invite_guest' && Request::segment(1) != 'chapter_guest' && Request::segment(1) != 'my_invitees') ? 'nav-link active' : 'nav-link'  }}" id="Employee-tab" href="/invite_guest">Invite a Guest </a></li>
                <li class="nav-item"><a class="{{ (Request::segment(1) === 'chapter_guest' ) ? 'nav-link active' : 'nav-link'  }}" id="Employee-tab" href="/chapter_guest">My Affiliate Guest </a></li>
                <!--li class="nav-item"><a class="{{ (Request::segment(1) === 'my_invitees') ? 'nav-link active' : 'nav-link'  }}" id="Employee-tab" href="/my_invitees"> My Invitees </a></li-->
            </ul>
        </div>
    </div>
</div>
<div class="col-lg-12 col-md-12">
   <div class="col-lg-12 col-md-12">
      
      <div class="section-body mt-3">
         <div class="container-fluid">
            <div class="tab-content mt-3">
              

              <!-- Send An Escrow Referral start here -->
             
                     <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Inviting Associate</h3>
                            </div>
                            <div class="card-body">
                                <form method="post" id="">
                                    <div class="row clearfix">
                                        <style type="text/css">
                                            .tip-form-header{box-sizing: border-box; color: #000000; padding: 1rem;width: 100%;}
                                            .tip-form-header .tip-form-member{display: -webkit-box;display: flex; -webkit-box-flex: 1; flex: auto; align-items: center;    border: 1px solid #ccc;
                                            }
                                            .tip-form-header .profile-image{width: 120px; height: 120px; object-fit: cover; object-position: top center; margin-right: 20px; }

                                        </style>

                                          <div class="tip-form-header">
                                             <div class="tip-form-member">
                                                <img class="profile-image" src="../assets/images/xs/avatar1.jpg" alt="">
                                                <div class="member-info">
                                                   <div class="member-name">Alice Agyemang-Badu</div>
                                                   <!--div class="business-name">New York Life</div>
                                                   <div class="text-muted category-name">Recruiter</div -->
                                                   <div class="chapter-name">Besure of Upper Montclair, NJ</div>
                                                </div>
                                             </div>
                                          </div>


                                       <div class="col-lg-12 col-lg-12">
                                          <div class="form-group">
                                             
                                          </div>
                                       </div>
                                       <div class="col-lg-12 col-lg-12">
                                          <div class="form-group">
                                             
                                          </div>
                                       </div>
                                      
                                       <div class="col-lg-6 col-md-6">
                                          <label>Guest First Name <span class="redstar">*<span></label>
                                          <div class="form-group">
                                             <input type="text" class="form-control" placeholder="First Name" required>
                                          </div>
                                       </div>

                                       <div class="col-lg-6 col-md-6">
                                          <label>Guest Last Name <span class="redstar">*<span></label>
                                          <div class="form-group">
                                             <input type="text" class="form-control" placeholder="Last Name" required>
                                          </div>
                                       </div>


                                       <div class="col-lg-6 col-md-6">
                                          <label>Guest Phone Number <span class="redstar">*<span></label>
                                          <div class="form-group">
                                             <input type="text" class="form-control" placeholder="Phone Number" required>
                                          </div>
                                       </div>

                                       <div class="col-lg-6 col-md-6">
                                          <label>Guest Email Address <span class="redstar">*<span></label>
                                          <div class="form-group">
                                             <input type="text" class="form-control" placeholder="Email Address" required>
                                          </div>
                                       </div>


                                       <div class="col-lg-12 col-lg-12">
                                        <label>Guest Company Name</label>
                                          <div class="form-group">
                                             <input type="text" class="form-control" placeholder="Company Name">
                                          </div>
                                       </div>



                                       <div class="col-lg-6 col-md-6">
                                          <label>Affiliate</label>
                                          <div class="form-group">
                                             <select class="custom-select">
                                                <option value="0">Select Affiliate</option>
                                                <option value="1">Alice Agyemang-Badu</option>
                                                <option value="2">Celestina Ando</option>
                                             </select>
                                          </div>
                                       </div>
                                       <div class="col-lg-6 col-md-6">
                                          <label>Category</label>
                                          <div class="form-group">
                                             <select class="custom-select">
                                                <option value="0">Select Category</option>
                                                <option value="1">Business Advisor</option>
                                                <option value="2">Financial Advisor</option>
                                             </select>
                                          </div>
                                       </div>
                                      



                                      <div class="col-lg-6 col-md-6">
                                          <div class="form-group">
                                              <label class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1">
                                                <span class="custom-control-label">Invite guest to a meeting</span>
                                                <span style="position: absolute; margin: 0 0 0 12px;"><i class="fa fa-question-circle" style="font-size: 22px;"></i></span>
                                            </label>

                                          </div>
                                      </div>
                                      <div class="col-lg-6 col-md-6">
                                          <div class="form-group">
                                              
                                          </div>
                                      </div>


                                      <div class="col-lg-12 col-lg-12">
                                        <label>Event</label>
                                          <div class="form-group">
                                             <select class="custom-select">
                                                <option value="0">(choose event)</option>
                                                <option value="1">Weekly Chapter Meeting</option>
                                             </select>
                                          </div>
                                       </div>



                                       <div class="col-lg-6 col-md-6">
                                          <label>Phone Confirmation</label>
                                          <div class="form-group">
                                             <select class="custom-select">
                                                <option value="0">Select</option>
                                                <option value="1">Celestina Ando</option>
                                                <option value="2">Celestina Ando</option>
                                             </select>
                                          </div>
                                       </div>

                                       <div class="col-lg-6 col-md-6">
                                          <label>Email Confirmation</label>
                                          <div class="form-group">
                                             <select class="custom-select">
                                                <option value="0">Select</option>
                                                <option value="1">Celestina Ando</option>
                                                <option value="2">Celestina Ando</option>
                                             </select>
                                          </div>
                                       </div>

                                       <div class="col-lg-12 col-md-12">
                                          <label>Personal Note / Message <span style="position: absolute; margin: 0 0 0 12px;"><i class="fa fa-question-circle" style="font-size: 22px;"></i></span></label>
                                          <div class="form-group">
                                             <textarea rows="8" cols="100" type="text" class="form-control" placeholder="Details" name="terminationRemarks"></textarea>
                                          </div>
                                       </div>
                                    </div>
                                <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1">
                                        <span class="custom-control-label">Send a copy to the inviting email</span>
                                    </label>
                                     
                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      
                                  </div>
                              </div>
                           
                                    <button type="button" class="btn btn-round btn-primary" style="float:right;">Send Invitation </button>
                                    
                                </form>
                           </div>
                     </div>
               
               <!-- Send An Escrow Referral end here -->

            </div>
         </div>
      </div>
   </div>
</div>

@stop




@section('popup')


@stop


@section('page-styles')

<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }} ">

@stop
@section('page-script')
<script src="{{ asset('assets/bundles/apexcharts.bundle.js') }}"></script>
<script src="{{ asset('assets/bundles/counterup.bundle.js') }}"></script>
<script src="{{ asset('assets/bundles/knobjs.bundle.js') }}"></script>
<script src="{{ asset('assets/js/core.js') }}"></script>
<script src="{{ asset('assets/js/page/index.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
@stop
