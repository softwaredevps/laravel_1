@extends('layout.member_master')
@section('title', 'My Affiliate Guest')

@section('content')

<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center mb-3">
            <ul class="nav nav-tabs page-header-tab">

                <li class="nav-item"><a class="{{ (Request::segment(1) === 'invite_guest' && Request::segment(1) != 'chapter_guest' && Request::segment(1) != 'my_invitees') ? 'nav-link active' : 'nav-link'  }}" id="Employee-tab" href="/invite_guest">Invite a Guest </a></li>
                <li class="nav-item"><a class="{{ (Request::segment(1) === 'chapter_guest' ) ? 'nav-link active' : 'nav-link'  }}" id="Employee-tab" href="/chapter_guest">My Affiliate Guest </a></li>
                <!--li class="nav-item"><a class="{{ (Request::segment(1) === 'my_invitees') ? 'nav-link active' : 'nav-link'  }}" id="Employee-tab" href="/my_invitees"> My Invitees </a></li-->
            </ul>
        </div>
    </div>
</div>

<div class="section-body">
    <div class="container-fluid">
        <div class="tab-content">
            <div class="tab-pane fade show active" id="Employee-list" role="tabpanel">                        
                <div class="card">
                    <div class="card-header">
                        <!--h3 class="card-title">My Affiliate List</h3 -->
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1">
                                    <span class="custom-control-label"><b>Show My Invitee Only</b></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <form>
                               
                                <table class="table table-hover table-striped table-vcenter text-nowrap mb-0 trtd" id="chapter_list_table">
                                    <thead>
                                        <tr>
                                            <th class="text-left"><strong>#</strong></th>
                                            <th class="text-left"><strong>Date</strong></th>
                                            <th class="text-left"><strong>Guest</strong></th>
                                            <th class="text-left"><strong>Email</strong></th>
                                            <th class="text-left"><strong>Phone Number</strong></th>
                                            <th class="text-left"><strong>My Invitee</strong></th>
                                            <th class="text-left"><strong>Status</strong></th>
                                           
                                        </tr>
                                    </thead>
                                    <tbody>
                                      
                                    
                                        <tr class="">
                                            <td class="text-left"><span>1</span></td>
                                            <td class="text-left">02/04/2020</td>
                                            <td class="text-left">
                                                <a href="javascript:;">Leslie Morrison</a>
                                            </td>
                                            <td class="text-left">lesliemorrison2@gmail.com</td>
                                            <td class="text-left">+1 917-912-6907</td>
                                            <td class="text-left">Yes</td>
                                            <td class="text-left">New</td>
                                        </tr>  

                                        <tr class="">
                                            <td class="text-left"><span>2</span></td>
                                            <td class="text-left">03/01/2020</td>
                                            <td class="text-left">
                                                <a href="javascript:;">Carline Colquhoun</a>
                                            </td>
                                            <td class="text-left">unknown@gmail.com</td>
                                            <td class="text-left">+1 973-902-8114</td>
                                            <td class="text-left">Yes</td>
                                            <td class="text-left">Invited</td>
                                        </tr> 

                                        <tr class="">
                                            <td class="text-left"><span>3</span></td>
                                            <td class="text-left">01/23/2020</td>
                                            <td class="text-left">
                                                <a href="javascript:;">Pauline Koni</a>
                                            </td>
                                            <td class="text-left">misskoni@yahoo.com</td>
                                            <td class="text-left">+1 973-902-8114</td>
                                            <td class="text-left">Yes</td>
                                            <td class="text-left">Application Sent</td>
                                        </tr>   

                                         <tr class="">
                                            <td class="text-left"><span>1</span></td>
                                            <td class="text-left">02/04/2020</td>
                                            <td class="text-left">
                                                <a href="javascript:;">Leslie Morrison</a>
                                            </td>
                                            <td class="text-left">lesliemorrison2@gmail.com</td>
                                            <td class="text-left">+1 917-912-6907</td>
                                            <td class="text-left">Yes</td>
                                            <td class="text-left">New</td>
                                        </tr>  

                                        <tr class="">
                                            <td class="text-left"><span>2</span></td>
                                            <td class="text-left">03/01/2020</td>
                                            <td class="text-left">
                                                <a href="javascript:;">Carline Colquhoun</a>
                                            </td>
                                            <td class="text-left">unknown@gmail.com</td>
                                            <td class="text-left">+1 973-902-8114</td>
                                            <td class="text-left">Yes</td>
                                            <td class="text-left">Invited</td>
                                        </tr> 
                                        


                                </tbody>
                                </table>
                            </form>                            
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </div>            
</div>

@stop

@section('page-styles')
<link rel="stylesheet" href="{{ asset('assets/plugins/fullcalendar/fullcalendar.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/sweetalert/sweetalert.css') }}">
<link href='http://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css' rel='stylesheet' type='text/css'>

@stop

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

@section('page-script')


<script src="{{ asset('assets/bundles/fullcalendar.bundle.js') }}"></script>


<script src="{{ asset('assets/js/page/calendar.js') }}"></script>
<script src="{{ asset('assets/plugins/sweetalert/sweetalert.min.js') }}"></script>

<script src="{{ asset('assets/js/core.js') }}"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.8/js/jquery.dataTables.min.js"></script>

@stop
