@extends('layout.member_master')
@section('title', 'Rewards')
@section('content')
<div class="col-lg-12 col-md-12">
   <div class="col-lg-12 col-md-12">
      <div class="section-body">
         <div class="container-fluid">
            <!--div class="d-flex justify-content-between align-items-center">
               <ul class="nav nav-tabs page-header-tab">
                  <li class="nav-item"><a class="nav-link active" id="li-my-rewards" data-toggle="tab" href="#my-rewards">Rewards</a></li>
               </ul>
            </div-->
         </div>
      </div>
      <div class="section-body mt-3">
         <div class="container-fluid">
            <div class="tab-content mt-3">

               <!-- My Rewards start here -->

                  <div class="row clearfix">
                     <div class="col-12">
                        <div class="card">
                           <div class="card-header">
                              <div class="card-options">
                                 <h6>Current Reward Balance: <b>$5000</b></h6>
                              </div>
                           </div>
                           <div class="card-body">
                              <div class="row">
                                 <div class="col-lg-5 col-md-6 col-sm-6">
                                    <label>Search By Service or Product</label>
                                    <div class="input-group">
                                       <input type="text" class="form-control" placeholder="Search...">
                                    </div>
                                 </div>

                                 <div class="col-lg-5 col-md-6 col-sm-6">
                                    <label>Search By Category</label>
                                    <div class="form-group">
                                       <select class="custom-select">
                                          <option selected="">Newest first</option>
                                          <option value="1">Oldest first</option>
                                          <option value="2">Low salary first</option>
                                          <option value="3">High salary first</option>
                                          <option value="3">Sort by name</option>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="col-lg-2 col-md-4 col-sm-6">
                                    <label>&nbsp;</label>
                                    <a href="javascript:void(0);" class="btn btn-sm btn-primary btn-block" title="">Filter</a>
                                 </div>
                              </div>
                              <div class="row">
                              <div style="margin-top:25px;" class="table-responsive">
                              <form>
                              {{ csrf_field() }}
                              <table class="table table-hover table-striped table-vcenter text-nowrap mb-0 trtd" id="rewards_list_table">
                              <thead>
                              <tr>
                              <th class="text-left"><strong>#</strong></th>
                              <!--th class="text-left"><strong>Select</strong></th-->
                              <th class="text-left"><strong>Service Name</strong></th>
                              <th class="text-center"><strong>Service<br>Cost</strong></th>
                              <th class="text-center"><strong>Reward<br>Points</strong></th>
                              <th class="text-center"><strong>Service Provider<br> Information</strong></th>
                              <th><strong>Action</strong></th>
                              </tr>
                              </thead>
                              <tbody>
                              @for($i = 1 ; $i <= 25; $i++)
                                 <tr class="row-{{ $i }}">
                                    <td class="text-left"><span>{{ $i }}</span></td>
                                    <!--td class="text-center">
                                      <label class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1"><span class="custom-control-label"></span>
                                      </label>
                                    </td-->
                                    <td class="text-left">Tax planning and planning</td>
                                    <td class="text-center">300</td>
                                    <td class="text-center">200</td>
                                    <td class="text-center">
                                      <div class="d-flex">  
                                          <div class="ml-3 text-center">
                                              <h6 class="mb-1" ><i class="fa fa-user" aria-hidden="true"></i> David S</h6>
                                              <span class="text-muted" title="">
                                                <i class="fa fa-building" aria-hidden="true"></i> BeSure</span>
                                          </div>
                                      </div>
                                    </td>
                                    <td class="text-center">
                                      <a href="#"><button type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#exampleModal" title="Redeem Now">Redeem Now</button></a>
                                    </td>
                                 </tr> 
                              @endfor       
                              </tbody>
                              </table>
                              </form>                            
                              </div>
                              </div>
                           </div>
                           <div class="card-footer" style="text-align:right">
                              <div style="text-align:right">
                                 <h6>Total: <b>$250</b></h6>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>

               <!-- My Rewards end here -->        

            </div>
         </div>
      </div>
   </div>
</div>

@stop




@section('popup')
<div class="modal fade " id="editModal" tabindex="-1" role="dialog">
   
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Referral</h5>
                    <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>-->
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
                <div class="modal-body">

                        <form>
                          <div class="row clearfix">
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <input type="text" class="form-control" placeholder="Person To Contact">
                                  </div>
                              </div>

                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">                                   
                                      <input type="text" class="form-control" placeholder="Company Name">
                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <input type="text" class="form-control" placeholder="Phone Number">
                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <input type="text" class="form-control" placeholder="Email Address">
                                  </div>
                              </div>

                              <div class="col-lg-12 col-md-12">
                                  <div class="form-group">
                                     <textarea class="form-control" name="" id="" placeholder="Referral Details"></textarea>
                                  </div>
                              </div>

                              
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <input type="text" class="form-control" placeholder="$">
                                  </div>
                              </div>

                             <div class="col-lg-6 col-md-6">
                                 <div class="form-group">
                                       <select class="custom-select">
                                          <option selected="">Status</option>
                                          <option value="1">New</option>
                                          <option value="2">Bogus/Invalid</option>
                                          <option value="2">Closed</option>
                                          <option value="2">Working</option>
                                       </select>
                                 </div>
                              </div>

                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1">
                                        <span class="custom-control-label">This is a private Referral</span>
                                        <span style="position: absolute; margin: 0 0 0 12px;"><i class="fa fa-question-circle" style="font-size: 22px;"></i></span>
                                    </label>

                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      
                                  </div>
                              </div>

                          </div>
                        </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary">Submit Referral</button>
                </div>
            </div>
        </div>
</div>

<div class="modal fade " id="exampleModal" tabindex="-1" role="dialog">   
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Redeem Form</h5>
                    <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>-->
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
                <div class="modal-body">

                        <form>
                          <div class="row clearfix">

                           <style type="text/css">
                                 .tip-form-header{box-sizing: border-box; background-color: #e0a258; color: #fff; padding: 1rem;width: 100%;}
                                 .tip-form-header .tip-form-member{display: -webkit-box;display: flex; -webkit-box-flex: 1; flex: auto; align-items: center;
                                 }
                              .tip-form-header .profile-image{width: 120px; height: 120px; object-fit: cover; object-position: top center; margin-right: 20px; background-color: #ccc;}

                           </style>

                              <div class="tip-form-header">
                                 <div class="tip-form-member">
                                    <img class="profile-image" src="../assets/images/xs/avatar1.jpg" alt="">
                                    <div class="member-info">
                                       <div class="member-name">Alice Agyemang-Badu</div>
                                       <div class="business-name">New York Life</div>
                                       <div class="text-muted category-name">Recruiter</div>
                                       <div class="chapter-name">Besure of Upper Montclair, NJ</div>
                                    </div>
                                 </div>
                              </div>

                              <div class="text-center" style="padding: 10px; font-size: 90%; font-weight: 500;"><p>My qualified Business Referral is for a company or person who is interested in a specific service or product and is expecting a call from a Besure member.</p></div>


                              <div class="col-lg-4 col-md-4">
                                  <label>Service Name</label>
                                  <div class="form-group">
                                      <input type="text" class="form-control" value="Tax planning and planning" readonly>
                                  </div>
                              </div>

                              <div class="col-lg-4 col-md-4">
                                  <label>Service Cost</label>
                                  <div class="form-group">                                   
                                      <input type="text" class="form-control" value="300" readonly>
                                  </div>
                              </div>

                              <div class="col-lg-4 col-md-4">
                                  <label>Reward Points</label>
                                  <div class="form-group">                                   
                                      <input type="text" class="form-control" value="200" readonly>
                                  </div>
                              </div>

                              <div class="col-lg-12 col-md-12">
                                  <label>Notes</label>
                                  <div class="form-group">
                                     <textarea rows="6" class="form-control" name="" id="" placeholder="Any Specific Notes"></textarea>
                                  </div>
                              </div>
                        </div>
                        </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary">Redeem Now</button>
                </div>
            </div>
        </div>
</div>
@stop


@section('page-styles')

<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }} ">
<link href='http://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css' rel='stylesheet' type='text/css'>

@stop
@section('page-script')
<script src="{{ asset('assets/bundles/apexcharts.bundle.js') }}"></script>
<script src="{{ asset('assets/bundles/counterup.bundle.js') }}"></script>
<script src="{{ asset('assets/bundles/knobjs.bundle.js') }}"></script>
<script src="{{ asset('assets/js/core.js') }}"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.8/js/jquery.dataTables.min.js"></script>
<script src="{{ asset('assets/js/page/index.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<script>
   $(document).ready(function()
   {
      $('#rewards_list_table').DataTable();
   });
</script>
@stop
