<?php 
$register = registrationCount();
?>

<div class="section-body row ml-0 mr-0">
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="card-body w_sparkline">
                <div class="details">
                    <span>Total Registration</span>
                    <h3 class="mb-0 counter"><?php echo $register->total_count ?></h3>
                </div>
                <div class="w_chart">
                    <img src="../assets/images/Group2.png">
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="card-body w_sparkline">
                <div class="details">
                    <span>Male</span>
                    <h3 class="mb-0 counter"><?php echo $register->male_cnt ?></h3>
                </div>
                <div class="w_chart">
                <img src="../assets/images/Group3.png">
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="card-body w_sparkline">
                <div class="details">
                    <span>Female</span>
                    <h3 class="mb-0 counter"><?php echo $register->female_cnt ?></h3>
                </div>
                <div class="w_chart">
                <img src="../assets/images/Group4.png">
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="card-body w_sparkline">
                <div class="details">
                    <span>Other</span>
                    <h3 class="mb-0 counter"><?php echo $register->other_cnt ?></h3>
                </div>
                <div class="w_chart">
                <img src="../assets/images/Group5.png">
                </div>
            </div>
        </div>
    </div>
</div>
