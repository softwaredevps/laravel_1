@extends('layout.master')
@section('title', 'Categories List')


@section('content')
<?php 
   // echo "<pre>";print_r($res);die;
?>

<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center mb-3">
            <ul class="nav nav-tabs page-header-tab">
                <li class="nav-item"><a class="{{ (Request::segment(1) === 'categorieslist' && Request::segment(1) != 'categories') ? 'nav-link active' : 'nav-link'  }}" id="Employee-tab" href="/categorieslist">Categories List</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="section-body">
    <div class="container-fluid">
        <div class="tab-content">
            <div class="tab-pane fade show active" id="Employee-list" role="tabpanel">                        
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Categories List</h3>
                        <!-- <div class="card-options">
                            <form>
                                <div class="input-group">
                                    <input type="text" class="form-control form-control-sm" placeholder="Search something..." name="s">
                                    <span class="input-group-btn ml-2"><button class="btn btn-icon btn-sm" type="submit"><span class="fe fe-search"></span></button></span>
                                </div>
                            </form>
                        </div> -->
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <form>
                                {{ csrf_field() }}
                                <table class="table table-hover table-striped table-vcenter text-nowrap mb-0 trtd" id="businessCatInfo">
                                    <thead>
                                        <tr>
                                            <th class="text-left"><strong>#</th>
                                            <th class="text-left"><strong>Category name</strong></th>
                                            <th class="text-left"><strong>Category description</strong></th>
                                            <th class="text-left"><strong>Member Count</strong></th>
                                        </tr>
                                    </thead> 
                                    <tbody> 
                                    <?php $i=1; ?>
                                    @foreach($res as $val)   
                                        <tr class="row-{{$val->id}}">
                                            <td class="text-left"><span><?php echo $i;?></span></td>
                                            <td class="text-left">{{$val->bizcatname}}</td>
                                        
                                            <td class="text-left"><?php echo strip_tags(substr($val->bizcatdescription,0,50)); ?></td>
                                            <td>
                                            <a href="/associateslistByCategory/{{$val->id}}">
                                                <?php 
                                                    $member_count = get_member_category_count($val->id);
                                                    echo (isset($member_count))?$member_count:' ...';
                                            
                                                ?> 
                                            </a>
                                            </td>
                                        </tr>  
                                    <?php $i++;?>
                                    @endforeach       
                                </tbody>
                                </table>
                            </form>
                         
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>            
</div>

@stop 

@section('page-styles')
<link rel="stylesheet" href="{{ asset('assets/plugins/fullcalendar/fullcalendar.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/sweetalert/sweetalert.css') }}">
<link href='http://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css' rel='stylesheet' type='text/css'>
<link href='https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css' rel='stylesheet' type='text/css'>
<link href='https://cdn.datatables.net/colreorder/1.5.2/js/dataTables.colReorder.min.js' rel='stylesheet' type='text/css'>
@stop
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
@section('page-script')
<script src="{{ asset('assets/bundles/fullcalendar.bundle.js') }}"></script>

<script src="{{ asset('assets/plugins/sweetalert/sweetalert.min.js') }}"></script>

<script src="{{ asset('assets/js/core.js') }}"></script>
<script src="{{ asset('assets/js/page/calendar.js') }}"></script>

<script type="text/javascript" src="https://cdn.datatables.net/1.10.8/js/jquery.dataTables.min.js"></script>
@stop
 
<script>
    function submitAjax(id){          
        $.ajax({
        url:"/actioncategorysdelete",
        method:"POST",
        data: {
                "_token": "{{ csrf_token() }}",
                "data": id,
            },
            success:function(data)
            {
                if(data.data){
                    $(".row-"+id).remove();
                    toastr.success('Success.');
                    swal.close()
                }else{
                    toastr.error('Error.');
                }
            }
        })          
    }

    function deleteCategory(id){
        var form = event.target.form; 
        swal({
            title: "Are you sure?",
            text: "But you will still be able to retrieve this file.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#dc3545",
            confirmButtonText: "Delete",
            cancelButtonText: "No, cancel please!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){
            if (isConfirm) {
                submitAjax(id); 
            } else {
                swal("Cancelled", "Your Request Is Cancelled.. :)", "error");
            }
        });
    }
</script>