@extends('layout.master')
@section('title', '')

@section('content')
<!-- 
@include('hrms.countinfo')    -->
<?php 
   //echo"<pre>"; print_r($other);die;
?>

<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center mb-3">
            <ul class="nav nav-tabs page-header-tab">
                <li class="nav-item"><a class="{{ (Request::segment(1) === 'associateApplication' && Request::segment(2) != '2' && Request::segment(2) != '3') ? 'nav-link active' : 'nav-link'  }}" id="Employee-tab" href="/associateApplication">New Received</a></li>
                <li class="nav-item"><a class="{{ Request::segment(2) === '2' ? 'nav-link active' : 'nav-link' }}" id="Employee-tab"  href="/associateApplication/2">Approved Applications</a></li>
                <li class="nav-item"><a class="{{ Request::segment(2) === '3' ? 'nav-link active' : 'nav-link' }}" id="Employee-tab" href="/associateApplication/3">Rejected Applications</a></li>
            </ul>
            <div class="header-action">
                <button type="button" class="btn btn-primary" onclick="setEdit()"><i class="fe fe-edit mr-2"></i>Edit</button>
            </div>
        </div>
    </div>
</div>

<div class="section-body">
    <div class="container-fluid">
        <div class="tab-content">
            <div class="tab-pane fade show active" id="Employee-list" role="tabpanel">                        
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                        <?php if(isset($other) &&  $other== 2){ 
                                $res_status = 'approved'
                            ?>
                            Approved Application
                           
                        <?php }else if(isset($other) &&  $other == 3){  
                            $res_status = 'pending' ?>
                            Rejected Application
                        <?php } else { $res_status = 'all' ?>
                            New Application
                        <?php } ?>
                        </h3>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <form>
                                {{ csrf_field() }}
                                <table class="table table-striped table-bordered trtd" id="associate_application_table">
                                    <thead>
                                        <tr>
                                            <th><strong>#</th>
                                            <th class="text-left"><strong>Associate</strong></th>
                                     
                                            <th class="text-left"><strong>Appl Dt</strong></th>
                                            <th class=><strong><p class="text-left mb-0">Affiliate</strong></p></th>
                                            <th class="text-left"><strong>Category</strong></th>
                                            <th class="text-left"><strong>City</strong></th>
                                            <th class="text-left"><strong>State</strong></th>
                                            <?php if(isset($other)){ ?>
                                                <th class="text-left"><strong>Status</strong></th>
                                            <?php } ?>
                                            <th><strong>Action</strong></th>
                                        </tr>
                                    </thead>
                                    <tbody> 
                                    <?php $i=1; ?>
                                    @foreach($res as $val)   
                                        <tr class="row-{{$val->id}}">
                                            <td class="w40"><span><?php echo $i;?></span></td>
                                           
                                            <td class="" title="{{$val->first_name}} {{$val->last_name}}">  
                                                <div class="d-flex">
                                                    <img class="avatar avatar-blue" onerror="this.src='/images/user.png'"  id="imguser" src="/images/{{ $val->your_image }}" alt="avatar" />    
                                                    <div class="ml-3 text-left">
                                                        <h6 class="mb-1" ><i class="fa fa-user" aria-hidden="true"></i> {{$val->first_name}} {{$val->last_name}} </h6>
                                                        <span class="text-muted" title="{{$val->your_email}}">
                                                        <i class="fa fa-envelope" aria-hidden="true"></i> {{$val->your_email}}</span>
                                                        <div class="text-muted" title="Number- {{$val->phone_no}}">
                                                        <i class="fa fa-phone" aria-hidden="true"></i> {{$val->phone_no}}</div>
                                                    </div>
                                                </div>
                                            </td>
                                   
                                            <td title=" <?php echo $val->created_at; ?>">
                                           <p class="text-left mb-0">  <?php echo date('d-M-Y', strtotime($val->created_at)); ?>    
                                           </p> </td>

                                            <td>

                                              <p class="text-left mb-0"> <?php $chapter_name =  getChapterName($val->affiliate_for); 
                                                    echo isset($chapter_name->chapter_name)?$chapter_name->chapter_name:"...";
                                                ?>  
                                           </p> </td>

                                            <td>
                                               <p class="text-left mb-0">
                                                <?php $bizcatname =  getBusinessName($val->business_category);
                                                    echo isset($bizcatname->bizcatname)?$bizcatname->bizcatname:"...";
                                                ?>    </p> 
                                            </td>     

                                            <td>
                                                <p class="text-left mb-0"> 
                                                {{($val->city)?$val->city:'...'}}
                                                </p>   
                                            </td> 

                                            <td>
                                                <p class="text-left mb-0"> 
                                                    <?php                                                        
                                                        $state_name = get_states($val->state);
                                                        echo (isset($state_name->name) && $state_name->name !='')?$state_name->name:' ...';
                                                    ?>  
                                                </p>   
                                            </td> 
    

                                            <?php if(isset($other)){ ?>
                                                <td>
                                                    <?php
                                                        echo ($val->status == 2)?'Approve':'Rejected';
                                                    ?>    
                                                </td>
                                            <?php } ?>                                                                               
                            
                                            <td>
                                                <a href="/application/{{$val->id}}?res_status={{$res_status}}">
                                                    <button type="button" class="btn btn-icon btn-sm" title="View"><i class="fa fa-eye"></i></button>
                                                </a>
                                            </td>
                                        </tr>  
                                    <?php $i++;?> 
                                    @endforeach       
                                </tbody>

                                <tfoot>
                                    <tr>
                                        <th><strong>#</th>
                                        <th class="text-left"><strong>Associate</strong></th>
                                    
                                        <th class="text-left"><strong>Appl Dt</strong></th>
                                        <th class=><strong><p class="text-left mb-0">Affiliate</strong></p></th>
                                        <th class="text-left"><strong>Category</strong></th>
                                        <th class="text-left"><strong>Location</strong></th>
                                        <th class="text-left"><strong>Status</strong></th>
                                        <th><strong>Action</strong></th>
                                    </tr>
                                </tfoot>
                                
                                </table>
                            </form>                         
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>            
</div>

@stop

@section('page-styles')

<link rel="stylesheet" href="{{ asset('assets/plugins/fullcalendar/fullcalendar.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/sweetalert/sweetalert.css') }}">
<link href='http://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css' rel='stylesheet' type='text/css'>


@stop
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
@section('page-script')
<script src="{{ asset('assets/bundles/fullcalendar.bundle.js') }}"></script>

<script src="{{ asset('assets/plugins/sweetalert/sweetalert.min.js') }}"></script>

<script src="{{ asset('assets/js/core.js') }}"></script>
<script src="{{ asset('assets/js/page/calendar.js') }}"></script>

<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
@stop
 
<script>

$(function() {
     
    function statusChange($id,$action){
        $.ajax({
        url:"/application_status_change",
        method:"POST",
        data: {
                "_token": "{{ csrf_token() }}",
                "data": {'id':$id,'status':$action},
            },
            success:function(data)
            {
                if(data.data){
                    var text = $(".row-"+$id+" .btn-block.btn-primary").text();
                    if(text == 'NO'){
                        $(".row-"+$id+" .btn-block.btn-primary").text('YES');
                    }else{
                        $(".row-"+$id+" .btn-block.btn-primary").text('NO');
                    }
                    toastr.success('Success.');
                    swal.close()
                }else{
                    toastr.error('Error.');
                }
            }
        })          
    }

    function submitAjax(id){          
        $.ajax({
        url:"/actionapplicationdelete",
        method:"POST",
        data: {
                "_token": "{{ csrf_token() }}",
                "data": id,
            },
            success:function(data)
            {
                if(data.data){
                    $(".row-"+id).remove();
                    toastr.success('Success.');
                    swal.close()
                }else{
                    toastr.error('Error.');
                }
            }
        })          
    }

    function deleteApplication(id){
        var form = event.target.form; 
        swal({
            title: "Are you sure?",
            text: "But you will still be able to retrieve this file.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#dc3545",
            confirmButtonText: "Delete",
            cancelButtonText: "No, cancel please!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){
            if (isConfirm) {
                submitAjax(id); 
            } else {
                swal("Cancelled", "Your Request Is Cancelled.. :)", "error");
            }
        });
    })
</script>