@extends('layout.master')
@section('parentPageTitle', 'HRMS')
@section('title', 'Dashboard')


@section('content')

    <div class="section-body mt-3">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="mb-4">
                        <h4>Welcome 
                            {{($res->first_name)?$res->first_name:''}}  {{($res->last_name)?$res->last_name:''}}
                        </h4>
                    </div>                        
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-md-4 col-sm-6 col-12">
                    <div class="card">
                        <div class="card-body ribbon">
                            <div class="ribbon-box green">3</div>
                            <!-- <a href="{{route('hrms.users')}}" class="my_sort_cut text-muted"> -->
                            <a href="javascript:;" class="my_sort_cut text-muted">
                                <i class="icon-users"></i>
                                <span>Referral Passed</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-12">
                    <div class="card">
                        <div class="card-body ribbon">
                            <div class="ribbon-box orange">0</div>
                            <!-- <a href="{{route('hrms.holidays')}}" class="my_sort_cut text-muted"> -->
                            <a href="javascript:;" class="my_sort_cut text-muted">
                                <i class="fa fa-money"></i>
                                <span>Referral Received</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-12">
                    <div class="card">
                        <div class="card-body ribbon">
                            <div class="ribbon-box green">0</div>
                            <!-- <a href="{{route('hrms.events')}}" class="my_sort_cut text-muted"> -->
                            <a href="javascript:;" class="my_sort_cut text-muted">
                                <i class="icon-calendar"></i>
                                <span>Sponsored</span>
                            </a>
                        </div>
                    </div>
                </div>               
            </div>
        </div>
    </div>
    <div class="section-body grid-dashboard">
        <div class="col-md-4 col-sm-6 col-12">
            <div class="card">
                <strong class="profile-tiles">My Profile</strong>
                <div class="card-body">
                    <div class="media mb-4">
                        <!-- <img class="avatar avatar-xl mr-3" src="../assets/images/profile.png" alt="avatar"> -->
                        <img class="imguser avatar avatar-xl mr-3" id="imguser" class="avatar avatar-xl mr-3" src="/images/{{ $res->img_url }}" alt="avatar" />
                        <div class="media-body">
                            <h5 class="m-0">
                            {{ $res->prefix }} {{ $res->first_name }} {{ $res->last_name }} {{ $res->suffix }}  </h5>
                            <p class="text-muted mb-0">{{($res->member_since)?$res->member_since:'---'}}</p>
                            <p class="text-muted mb-0"> <i class="fa fa-id-card text-muted"></i> <strong>{{($res->badge_color)?$res->badge_color:'---'}}</strong></p>
                            <p class="text-muted mb-0"> <i class="fa fa-envelope-o"></i> {{($res->email)?$res->email:'---'}} </p>
                            <p class="text-muted mb-0"> <i class="fa fa-phone"></i> {{($res->phone)?$res->phone:'---'}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4 col-sm-6 col-12">
            <div class="card">
                <strong class="profile-tiles">My Business</strong>
                <div class="card-body">
                    <div class="media mb-4">
                        <img class="imguser avatar avatar-xl mr-3" id="imguser" class="avatar avatar-xl mr-3" src="/images/{{ $res->business_img }}" alt="avatar" />
                        <div class="media-body">
                            <h5 class="m-0"><strong>{{($res->name)?$res->name:'---'}}</strong></h5>
                            <p class="text-muted mb-0">{{($res->business_address)?$res->business_address:'---'}}</p>
                            <p class="text-muted mb-0"> <i class="fa fa-id-card text-muted"></i>{{($res->business_phone_no)?$res->business_phone_no:'---'}}</p>
                            <p class="text-muted mb-0"> <i class="fa fa-envelope-o"></i>  {{($res->business_email)?$res->business_email:'---'}}</p>
                            <p class="text-muted mb-0"> {{($res->url)?$res->url:'---'}} </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4 col-sm-6 col-12">
            <div class="card">
                <strong class="profile-tiles">My Affiliate</strong>
                <div class="card-body">
                    <div class="media mb-4">
                        <div class="media-body">
                            <p class="m-0">LeTip of Upper Montclair, NJ</p>
                            <p class="text-muted mb-0">Category: App Developer</p>
                            <p class="text-muted mb-0">Area: NEW JERSEY</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <strong class="profile-tiles">Area Support</strong>
                <div class="card-body">
                    <div class="media mb-4">
                        <div class="media-body">
                            <p class="m-0"><strong>NEW JERSEY East Coast VP</strong></p>
                            <img class="avatar avatar-xl mr-3" src="../assets/images/user.png" alt="avatar">
                            <p class="text-muted mb-0">Paul Della Valle</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    
    <!-- <div class="section-body">
        <div class="container-fluid">
            
             
            <div class="row clearfix">
                <div class="col-12 col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Project Summary</h3>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-hover table-striped text-nowrap table-vcenter mb-0">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Client Name</th>
                                            <th>Team</th>
                                            <th>Project</th>
                                            <th>Project Cost</th>
                                            <th>Payment</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>#AD1245</td>
                                            <td>Sean Black</td>
                                            <td>
                                                <ul class="list-unstyled team-info sm margin-0 w150">
                                                    <li><img src="../assets/images/xs/avatar1.jpg" alt="Avatar"></li>
                                                    <li><img src="../assets/images/xs/avatar2.jpg" alt="Avatar"></li>
                                                    <li><img src="../assets/images/xs/avatar3.jpg" alt="Avatar"></li>
                                                    <li><img src="../assets/images/xs/avatar4.jpg" alt="Avatar"></li>
                                                    <li class="ml-2"><span>2+</span></li>
                                                </ul>
                                            </td>
                                            <td>Angular Admin</td>
                                            <td>$14,500</td>
                                            <td>Done</td>
                                            <td><span class="tag tag-success">Delivered</span></td>
                                        </tr>
                                        <tr>
                                            <td>#DF1937</td>
                                            <td>Sean Black</td>
                                            <td>
                                                <ul class="list-unstyled team-info sm margin-0 w150">
                                                    <li><img src="../assets/images/xs/avatar1.jpg" alt="Avatar"></li>
                                                    <li><img src="../assets/images/xs/avatar2.jpg" alt="Avatar"></li>
                                                    <li><img src="../assets/images/xs/avatar3.jpg" alt="Avatar"></li>
                                                    <li><img src="../assets/images/xs/avatar4.jpg" alt="Avatar"></li>
                                                    <li class="ml-2"><span>2+</span></li>
                                                </ul>
                                            </td>
                                            <td>Angular Admin</td>
                                            <td>$14,500</td>
                                            <td>Pending</td>
                                            <td><span class="tag tag-success">Delivered</span></td>
                                        </tr>
                                        <tr>
                                            <td>#YU8585</td>
                                            <td>Merri Diamond</td>
                                            <td>
                                                <ul class="list-unstyled team-info sm margin-0 w150">
                                                    <li><img src="../assets/images/xs/avatar1.jpg" alt="Avatar"></li>
                                                    <li><img src="../assets/images/xs/avatar2.jpg" alt="Avatar"></li>
                                                </ul>
                                            </td>
                                            <td>One page html Admin</td>
                                            <td>$500</td>
                                            <td>Done</td>
                                            <td><span class="tag tag-orange">Submit</span></td>
                                        </tr>
                                        <tr>
                                            <td>#AD1245</td>
                                            <td>Sean Black</td>
                                            <td>
                                                <ul class="list-unstyled team-info sm margin-0 w150">
                                                    <li><img src="../assets/images/xs/avatar1.jpg" alt="Avatar"></li>
                                                    <li><img src="../assets/images/xs/avatar2.jpg" alt="Avatar"></li>
                                                    <li><img src="../assets/images/xs/avatar3.jpg" alt="Avatar"></li>
                                                    <li><img src="../assets/images/xs/avatar4.jpg" alt="Avatar"></li>
                                                </ul>
                                            </td>
                                            <td>Wordpress One page</td>
                                            <td>$1,500</td>
                                            <td>Done</td>
                                            <td><span class="tag tag-success">Delivered</span></td>
                                        </tr>
                                        <tr>
                                            <td>#GH8596</td>
                                            <td>Allen Collins</td>
                                            <td>
                                                <ul class="list-unstyled team-info sm margin-0 w150">
                                                    <li><img src="../assets/images/xs/avatar1.jpg" alt="Avatar"></li>
                                                    <li><img src="../assets/images/xs/avatar2.jpg" alt="Avatar"></li>
                                                    <li><img src="../assets/images/xs/avatar3.jpg" alt="Avatar"></li>
                                                    <li><img src="../assets/images/xs/avatar4.jpg" alt="Avatar"></li>
                                                    <li class="ml-2"><span>2+</span></li>
                                                </ul>
                                            </td>
                                            <td>VueJs Application</td>
                                            <td>$9,500</td>
                                            <td>Done</td>
                                            <td><span class="tag tag-success">Delivered</span></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
@stop

@section('page-styles')

@stop

@section('page-script')
<script src="{{ asset('assets/bundles/apexcharts.bundle.js') }}"></script>
<script src="{{ asset('assets/bundles/counterup.bundle.js') }}"></script>
<script src="{{ asset('assets/bundles/knobjs.bundle.js') }}"></script>

<script src="{{ asset('assets/js/core.js') }}"></script>
<script src="{{ asset('assets/js/page/index.js') }}"></script>
@stop
