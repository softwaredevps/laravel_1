@extends('layout.master')
@section('title', 'My Profiles')


@section('content')

<?php 
 //echo "<pre>";print_R($res);die; 
    // foreach($res as $user){
    //     echo "<pre>";print_R($user);
    // };die;
?>
<div class="emailmodal" style="display:none" id='modal'>
   <div class="modal fade show" role="dialog" tabindex="-1" style="display: block;">
      <div class="modal-dialog modal-md" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <h5 class="modal-title">Email Addresses</h5>
            </div>
            <div class="modal-body">
               <form method="post" action="" id="emailform" accept-charset="UTF-8">
               {{ csrf_field() }}
                  <div class="row">
                     <div class="col-sm-3">
                        <div class="form-group">
                           <div class="input-group">
                              <select class="form-control" placeholder="(choose)" name="email_type_modal" id="label" required>
                                 <option value="">(choose)</option>
                                 <option value="Home">Home</option>
                                 <option value="Business">Business</option>
                                 <option value="E-Mail">E-Mail</option>
                                 <option value="Other">Other</option>
                              </select>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-6">
                        <div class="form-group">
                           <div class="input-group"><input type="email" class="form-control" placeholder="acme@example.com" name="email_values" id="value" value="" required></div> 
                        </div>
                     </div>
                     <div class="col-sm-3"><button onclick="SetEmailSave(this,'emailform')" type="button" class="inline-form-button btn btn-primary btn-block">Add</button></div>
                  </div>
               </form>
               <div class="form-buttons"><button type="button" class="btn btn-" onclick="closeModal(this,'emailmodal');">Done</button></div>
            </div>
         </div>
      </div>
   </div>
   <div class="modal-backdrop fade show"></div>
</div>

<div class="numbermodal" style="display:none">
   <div class="modal fade show" role="dialog" tabindex="-1" style="display: block;">
      <div class="modal-dialog modal-md" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <h5 class="modal-title">Phone Numbers</h5>
            </div>
            <div class="modal-body">
               <form method="post" action="" accept-charset="UTF-8">
                  <div class="row">
                     <div class="col-sm-3">
                        <div class="form-group">
                           <div class="input-group">
                              <select class="form-control" placeholder="(choose)" name="phonetype" id="label" required>
                                 <option value="">(choose)</option>
                                 <option value="Cell">Cell</option>
                                 <option value="Home">Home</option>
                                 <option value="Office">Office</option>
                                 <option value="Fax">Fax</option>
                                 <option value="Business">Business</option>
                                 <option value="Other">Other</option>
                                 <option value="Toll Free">Toll Free</option>
                                 <option value="Alternate">Alternate</option>
                                 <option value="Phone 2">Phone 2</option>
                                 <option value="Phone 3">Phone 3</option>
                              </select>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-6">
                        <div class="form-group">
                           <div class="input-group"><input type="text" class="form-control" placeholder="(555) 555-5555" name="number" id="value" value="" required></div>
                        </div>
                     </div>
                     <div class="col-sm-3"><button onclick="SetNumberSave(this,'numbermodal')" type="button" class="inline-form-button btn btn-primary btn-block">Add</button></div>
                  </div>
               </form>
               <div class="form-buttons"><button type="button" class="btn btn-"  onclick="closeModal(this,'numbermodal');">Done</button></div>
            </div>
         </div>
      </div>
   </div>
   <div class="modal-backdrop fade show"></div>
</div>
    <div class="section-body">
        <div class="container-fluid">
            <div class="d-flex justify-content-between align-items-center mb-3">
                <ul class="nav nav-tabs page-header-tab">
                    <li class="nav-item"><a class="nav-link active" id="Employee-tab" data-toggle="tab" href="#Employee-list">My Profile Information</a></li>
                    <li class="nav-item"><a class="nav-link" id="Employee-tab" data-toggle="tab" href="#Employee-view">Business Information</a></li>
                    <li class="nav-item"><a class="nav-link" id="Employee-tab" data-toggle="tab" href="#Employee-Request">Network Information</a></li>
                </ul>
                <!-- <div class="header-action">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"><i class="fe fe-plus mr-2"></i>Add</button>
                </div> -->
                <div class="header-action">
                    <button type="button" class="btn btn-primary" onclick="setEdit()"><i class="fe fe-edit mr-2"></i>Edit</button>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
    <div class="section-body">
        <div class="container-fluid">
            <div class="tab-content">
                <div class="tab-pane fade show active" id="Employee-list" role="tabpanel">
                    <div class="row">
                        <div class="col-lg-4 col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="media mb-4">
                                        <form method="post" id="upload_form1" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                            <input onchange="SetDefaultImg(this);" type="file" name="select_file" style="display:none" id="select_file1" />

                                            <label for="select_file1" class="float-right pointerEvent">
                                                <img class="imguser avatar avatar-xl mr-3" id="imguser" class="avatar avatar-xl mr-3" src="/images/{{ $res->img_url }}" alt="avatar" />
                                            </label>
                                        </form>

                                        <div class="media-body">
                                            <div>{{ $res->prefix }} {{ $res->first_name }} {{ $res->last_name }} {{ $res->suffix }}</div>
                                            <!-- <input readonly class="form-edit pointerEvent" type="text" name="user_name" value="{{ $res->prefix }} {{ $res->first_name }} {{ $res->last_name }} {{ $res->suffix }}" onchange="SetDefault(this);" > -->
                                        </div>
                                    </div>


                                    <span>Add a short video to showcase your business!<span>

                                    <div class="input-group">
                                        <input type="text" class="form-control form-edit pointerEvent" placeholder="https://youtu.be/videoid" name="url" id="videoUrl" value="{{($res->url)?$res->url:''}}" onchange="SetDefault(this);">

                                        @if($res->url)
                                            <button onclick="Copy();" class="btn btn-success btn-xs" data-clipboard-target="#videoUrl">
                                                Copy Url
                                            </button>
                                        @endif
                                    </div>
 
                                 </div>
                            </div>
                            <div class="card">
                                <div class="card-body">
                                <h5>Business Card</h5>
                                    <div class="media mb-4 card-img-div">                                     
                                        <form method="post" id="upload_form3" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                            <input onchange="SetDefaultImgCard(this);" type="file" name="select_file" style="display:none" id="select_file1_card" />

                                            <label for="select_file1_card" class="float-right pointerEvent">
                                                <img class="imgusercard avatar avatar-xl mr-3" id="imgusercard" class="avatar avatar-xl mr-3" src="/images/{{ $res->business_img_card }}" alt="avatar" />
                                            </label>
                                        </form>
                                    </div>
                                 </div>
                            </div>

                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">Statistics</h3>
                                    <div class="card-options">
                                        <a href="#" class="card-options-collapse" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a>
                                        <a href="#" class="card-options-remove" data-toggle="card-remove"><i class="fe fe-x"></i></a>
                                    </div>
                                </div>
                                <div class="card-body statis">         
                                   
                                    <div class="form-group">
                                        <label class="d-block">Associate Since
                                            <span class="float-right pointerEvent">
                                                <input class="form-edit" type="text" name="member_since" value="{{ $res->member_since }}" onchange="SetDefault(this);" >
                                            </span>
                                        </label>
                                        <div class="progress progress-xs">
                                            <div class="progress-bar bg-danger" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%;"></div>
                                        </div>
                                    </div>
                                   
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-12">
                            <div class="card">
                                <div class="card-body profile-page col-md-12">
                                    <ul class="new_timeline mt-3 col-md-5">
                                        <li>
                                            <div class="bullet pink"></div>
                                            <div class="desc">
                                                <h3>Prefix</h3>
                                                <h4>
                                                    <span class="float-left pointerEvent">
                                                        <!-- <input class="form-edit" type="text" name="prefix" value="{{ ($res->prefix)?$res->prefix:'----' }}" onchange="SetDefault(this);" > -->

                                                        <select class="form-control" id="sel1" name="prefix" onchange="SetDefault(this);">
                                                            <option value="">(choose)</option>
                                                            <option value="Mr." <?php echo ($res->prefix == 'Mr.')?'selected':''?>>Mr.</option>
                                                            <option value="Mrs." <?php echo ($res->prefix == 'Mrs.')?'selected':''?>>Mrs.</option>
                                                            <option value="Dr." <?php echo ($res->prefix == 'Dr.')?'selected':''?>>Dr.</option>
                                                            <option value="M." <?php echo ($res->prefix == 'M.')?'selected':''?>>M.</option>
                                                        </select>

                                                    </span>
                                                </h4>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="bullet pink"></div>
                                            <div class="desc">
                                                <h3>First Name</h3>
                                                <h4>
                                                    <span class="float-left pointerEvent">
                                                        <input class="form-edit" type="text" name="first_name" value="{{ ($res->first_name)?$res->first_name:'----' }}" onchange="SetDefault(this);" >
                                                    </span>
                                                </h4>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="bullet green"></div>
                                            <div class="desc">
                                                <h3>Last Name</h3>
                                                <h4>
                                                <h4>
                                                    <span class="float-left pointerEvent">
                                                        <input class="form-edit" type="text" name="last_name" value="{{ ($res->last_name)?$res->last_name:'----' }}" onchange="SetDefault(this);" >
                                                    </span>
                                                </h4> 
                                            </div>
                                        </li>
                                        <li>
                                            <div class="bullet green"></div>
                                            <div class="desc">
                                                <h3>Preferred Name</h3>
                                                <h4>
                                                    <span class="float-left pointerEvent">
                                                        <select class="form-control" id="sel1" name="suffix" onchange="SetDefault(this);">
                                                            <option value="">(choose)</option>
                                                            <option value="Jr." <?php echo ($res->suffix == 'Jr.')?'selected':''?>>Jr.</option>
                                                            <option value="Sr." <?php echo ($res->suffix == 'Sr.')?'selected':''?>>Sr.</option>
                                                        </select>
                                                    </span>
                                                </h4>
                                            </div>
                                        </li>
                                        <li> 
                                            <div class="bullet green"></div>
                                            <div class="desc profile-no">
                                                <h3>Phone Numbers  
                                                    <a href="javascript:;" onclick="addNew(this,'numbermodal')"> <i class="fa fa-plus pointerEvent"></i> </a>
                                                </h3>
                                                <h4>Phone:  </h4>
                                                    <span class="float-left pointerEvent">
                                                        <input class="form-edit" type="text" name="phone" value="{{ ($res->phone)?$res->phone:'----' }}" onchange="SetDefault(this);" >
                                                    </span>
                                               
                                                
                                                <h4>Cell: </h4>
                                                    <span class="float-left pointerEvent">
                                                        <input class="form-edit" type="text" name="cell_no" value="{{ ($res->cell_no)?$res->cell_no:'----' }}" onchange="SetDefault(this);" >
                                                    </span> 
                                              
                                            </div>
                                        </li>

                                        <li>
                                            <div class="bullet green"></div>
                                            <div class="desc">
                                                <h3>Biography</h3>
                                                <h4>
                                                    <span class="float-left pointerEvent">
                                                        <input class="form-edit" type="text" name="biography" value="{{ ($res->biography)?$res->biography:'You have not entered a bio yet' }}" onchange="SetDefault(this);" >
                                                    </span>
                                                </h4>
                                            </div>
                                        </li>
                                    </ul>
                                    <ul class="new_timeline mt-3 col-md-5">
                                        <li>
                                            <div class="bullet green"></div>
                                            <div class="desc">
                                                <h3>Email Addresses 
                                                    <a href="javascript:;" onclick="addNew(this,'emailmodal')"> <i class="fa fa-plus pointerEvent"></i></a>
                                                </h3>
                                                <h4 class="email-content">
                                                    <span class="float-left pointerEvent">
                                                        <input class="form-edit" type="email" name="email" value="{{ ($res->email)?$res->email:'---' }}" onchange="SetDefault(this);" >
                                                    </span>
                                                </h4>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="bullet orange"></div>
                                            <div class="desc">
                                                <h3>Birth Date</h3>
                                                <h4>
                                                    <span class="float-left pointerEvent">
                                                        <input class="form-edit" type="text" name="dob" value="{{ ($res->dob)?$res->dob:'---' }}" onchange="SetDefault(this);" >
                                                    </span>
                                                </h4>
                                            </div>
                                        </li>
                                     
                                    </ul>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="Employee-view" role="tabpanel">
                    <div class="row">
                        <div class="col-lg-4 col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="media mb-4">
                                        <!-- <img class="avatar avatar-xl mr-3" src="../assets/images/bussiness_logo.png" alt="avatar"> -->

                                        <form method="post" id="upload_form2" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <input onchange="SetDefaultImgBusiness(this);" type="file" name="select_file" style="display:none" id="select_file2" />

                                            <label for="select_file2" class="float-right pointerEvent">
                                                <img class="imguserbussiness avatar avatar-xl mr-3" id="imguserbussiness" class="avatar avatar-xl mr-3" src="/images/{{ $res->business_img }}" alt="avatar" />
                                            </label>
                                        </form>

                                        <div class="media-body">
                                            <h5 class="m-0">
                                                <span class="float-left pointerEvent">
                                                    <div> {{ ($res->name)?$res->name:'---' }} </div>
                                                    <!-- <input class="form-edit" type="text" name="name" value="{{ ($res->name)?$res->name:'---' }}" onchange="SetDefaultBusiness(this);" >  -->
                                                </span>   
                                            </h5>
                                        </div>
                                    </div>
                                 </div>
                            </div>
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">Statistics</h3>
                                    <div class="card-options">
                                        <a href="#" class="card-options-collapse" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a>
                                        <a href="#" class="card-options-remove" data-toggle="card-remove"><i class="fe fe-x"></i></a>
                                    </div>
                                </div>
                                <div class="card-body">         
                                    <div class="form-group">
                                        <label class="d-block">Hours of Operation <span class="float-right">Not provided</span></label>
                                        <div class="progress progress-xs">
                                            <div class="progress-bar bg-blue" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%;"></div>
                                        </div>                                   
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <ul class="new_timeline mt-3">
                                        <li>
                                            <div class="bullet pink"></div>
                                            <div class="desc">
                                                <h3>Name</h3>
                                                <h4 class="m-0">
                                                    <span class="float-left pointerEvent">
                                                        <input class="form-edit" type="text" name="name" value="{{ ($res->name)?$res->name:'---' }}" onchange="SetDefaultBusiness(this);" > 
                                                    </span>   
                                                </h4>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="bullet pink"></div>
                                            <div class="desc">
                                                <h3>Social Media Profiles</h3>
                                                <h4 class="m-0">
                                                    <span class="float-left pointerEvent">
                                                        <input class="form-edit" type="text" name="social_links" value="{{ ($res->social_links)?$res->social_links:'Social media links have not been entered' }}" onchange="SetDefaultBusiness(this);" > 
                                                    </span>   
                                                </h4>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="bullet green"></div>
                                            <div class="desc">
                                                <h3>Business Phone Numbers</h3>
                                                <h4>
                                                    <div class="business-phone">
                                                        <h3>Phone: </h3>
                                                        <span class="float-left pointerEvent col-lg-8 col-md-12">
                                                            <input class="form-edit" type="text" name="business_phone_no" value="{{ ($res->business_phone_no)?$res->business_phone_no:'Social media links have not been entered' }}" onchange="SetDefaultBusiness(this);" > 
                                                        </span>  
                                                    </div>     
                                                
                                                    <div class="business-office">
                                                        <h3>Office: </h3>
                                                        <span class="float-left pointerEvent col-lg-8 col-md-12">
                                                            <input class="form-edit" type="text" name="business_office_no" value="{{ ($res->business_office_no)?$res->business_office_no:'Social media links have not been entered' }}" onchange="SetDefaultBusiness(this);" > 
                                                        </span>     
                                                    </div>                                                
                                                </h4>   
                                            </div>
                                        </li>
                                        <li>
                                            <div class="bullet green"></div>
                                            <div class="desc">
                                                <h3>About My Business </h3>
                                                <span class="float-left pointerEvent">
                                                    <input class="form-edit" type="text" name="business_about" value="{{ ($res->business_about)?$res->business_about:'You have not entered a business description yet' }}" onchange="SetDefaultBusiness(this);" > 
                                                </span>  
                                            </div>
                                        </li>
                                        <li>
                                            <div class="bullet orange"></div>
                                            <div class="desc">
                                                <h3>Website</h3>
                                                <span class="float-left pointerEvent">
                                                    <input class="form-edit" type="text" name="website" value="{{ ($res->website)?$res->website:'' }}" onchange="SetDefaultBusiness(this);" > 
                                                </span>  
                                            </div>
                                        </li>
                                        <li>
                                            <div class="bullet green"></div>
                                            <div class="desc">
                                                <h3>Business Email Addresses</h3>
                                                <h4>
                                                    <span class="float-left pointerEvent">
                                                        <input class="form-edit" type="text" name="business_email" value="{{ ($res->business_email)?$res->business_email:'' }}" onchange="SetDefaultBusiness(this);" > 
                                                    </span>      
                                                </h4>
                                            </div>
                                        </li>                                       
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>                        
                </div>
                <div class="tab-pane fade" id="Employee-Request" role="tabpanel">
                    <div class="row">
                        <div class="col-lg-4 col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="media mb-4">
                                        <form method="post" id="upload_form1" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <input onchange="SetDefaultImg(this);" type="file" name="select_file" style="display:none" id="select_file1" />

                                            <label for="select_file1" class="float-right pointerEvent">
                                                <img class="imguser avatar avatar-xl mr-3" id="imguser" class="avatar avatar-xl mr-3" src="/images/{{ $res->img_url }}" alt="avatar" />
                                            </label>
                                        </form>
                                        <div class="media-body">
                                            {{ $res->prefix }} {{ $res->first_name }} {{ $res->last_name }} {{ $res->suffix }}
                                            <!-- <input readonly class="form-edit pointerEvent" type="text" name="user_name" value="{{ $res->prefix }} {{ $res->first_name }} {{ $res->last_name }} {{ $res->suffix }}" onchange="SetDefault(this);" > -->
                                        </div>
                                    </div>
                                    <span>Add a short video to showcase your business!<span>

                                    <div class="input-group">
                                        <input type="text" class="form-control form-edit pointerEvent" placeholder="https://youtu.be/videoid" name="url" id="videoUrl" value="{{($res->url)?$res->url:''}}" onchange="SetDefault(this);">

                                        @if($res->url)
                                            <button onclick="Copy();" class="btn btn-success btn-xs" data-clipboard-target="#videoUrl">
                                                Copy Url
                                            </button>
                                        @endif
 
                                    </div>

                                 </div>
                            </div>
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">Statistics</h3>
                                    <div class="card-options">
                                        <a href="#" class="card-options-collapse" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a>
                                        <a href="#" class="card-options-remove" data-toggle="card-remove"><i class="fe fe-x"></i></a>
                                    </div>
                                </div>
                                <div class="card-body statis">         
                                   
                                    <div class="form-group">
                                        <label class="d-block">Associate Since
                                            <span class="float-right pointerEvent">
                                                <input class="form-edit" type="text" name="member_since" value="{{ $res->member_since }}" onchange="SetDefault(this);" >
                                            </span>
                                        </label>
                                        <div class="progress progress-xs">
                                            <div class="progress-bar bg-danger" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%;"></div>
                                        </div>
                                    </div>
                                   
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-12">
                            <div class="card">
                                <div class="card-body profile-page col-md-12">
                                    <ul class="new_timeline mt-3 col-md-5">
                                        <li>
                                            <div class="bullet pink"></div>
                                            <div class="desc">
                                                <h3>Chapter Name</h3>
                                                <h4>
                                                    <span class="float-left pointerEvent">
                                                        <input class="form-edit" type="text" name="chapter_name" value="{{ $res->chapter_name }}" onchange="SetDefaultNetwork(this);" >
                                                    </span>  
                                                </h4>  
                                            </div>
                                        </li>
                                        <li>
                                            <div class="bullet pink"></div>
                                            <div class="desc">
                                                <h3>Category</h3>
                                                <h4>
                                                    <span class="float-left pointerEvent">
                                                        <input class="form-edit" type="text" name="category" value="{{ $res->category }}" onchange="SetDefaultNetwork(this);" >
                                                    </span>     
                                                </h4>
                                            </div>
                                        </li>  
                                        <li>
                                            <div class="bullet green"></div>
                                            <div class="desc">
                                                <h3>Type</h3>
                                                <span class="float-left pointerEvent">
                                                    <input class="form-edit" type="text" name="type" value="{{ $res->type }}" onchange="SetDefaultNetwork(this);" >
                                                </span>                                             
                                            </div>
                                        </li>

                                       

                                        <li>
                                            <div class="bullet green"></div>
                                            <div class="desc">
                                                <h3>NTS Completed:</h3>
                                                <h4>
                                                    <span class="float-left pointerEvent">
                                                        <input class="form-edit" type="text" name="nts_completed" value="{{ $res->nts_completed }}" onchange="SetDefaultNetwork(this);" >
                                                    </span>    
                                                </h4>
                                            </div>
                                        </li>
                                    </ul>
                                    <ul class="new_timeline mt-3 col-md-5">
                                        <li>
                                            <div class="bullet green"></div>
                                            <div class="desc">
                                                <h3>State</h3>
                                                <h4>
                                                    <span class="float-left pointerEvent">
                                                        <input class="form-edit" type="text" name="area" value="{{ $res->area }}" onchange="SetDefaultNetwork(this);" >
                                                    </span>    
                                                </h4>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="bullet orange"></div>
                                            <div class="desc">
                                                <h3>Application Date</h3>
                                                <h4>
                                                    <span class="float-left pointerEvent">
                                                        <input class="form-edit" type="text" name="join_date" value="{{ $res->join_date }}" onchange="SetDefaultNetwork(this);" >
                                                    </span>    
                                                </h4>
                                            </div>
                                        </li>
                                        <!-- <li>
                                            <div class="bullet green"></div>
                                            <div class="desc">
                                                <h3>Membership</h3>
                                                <h4>---</h4>
                                            </div>
                                        </li> -->
                                    </ul>
                                </div>
                            </div>

                            <div class="card">
                                <h5>Sponsored By</h5>
                                <div class="card-body">
                                    <ul class="new_timeline mt-3">
                                        <li>
                                            <div class="bullet pink"></div>
                                            <div class="desc">
                                                <h3>Associate</h3>
                                                <h4>
                                                    <img class="avatar avatar-xl mr-3" src="../assets/images/user.png" alt="avatar">
                                                    <span class="sponser-img" style="position:relative;top:-30px;">Joseph Michura</span>                                            
                                                </h4>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="bullet pink"></div>
                                            <div class="desc">
                                                <h3>Category</h3>
                                                <h4>
                                                    <span class="float-left pointerEvent">
                                                        <input class="form-edit" type="text" name="sponser_category" value="{{ $res->sponser_category }}" onchange="SetDefaultNetwork(this);" >
                                                    </span>    
                                                </h4>
                                            </div>
                                        </li>
                                     
                                        <li>
                                            <div class="bullet green"></div>
                                            <div class="desc">
                                                <h3>Phone Number</h3>
                                                <h4>
                                                    <span class="float-left pointerEvent">
                                                        <input class="form-edit" type="text" name="sponser_phone_no" value="{{ $res->sponser_phone_no }}" onchange="SetDefaultNetwork(this);" >
                                                    </span>  (Business)
                                                </h4>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="bullet green"></div>
                                            <div class="desc">
                                                <h3>Email Address</h3>
                                                <h4>
                                                    <span class="float-left pointerEvent">
                                                        <input class="form-edit" type="text" name="sponser_email" value="{{ $res->sponser_email }}" onchange="SetDefaultNetwork(this);" >
                                                    </span>    
                                                </h4>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <!-- <div class="card">
                                <h5>Network Activity</h5>
                                <div class="card-body">
                                    <ul class="new_timeline mt-3">
                                        <li>
                                            <div class="bullet pink"></div>
                                            <div class="desc">
                                                <h3>08/07/2019</h3>
                                                <h3>Membership created until ---</h3>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>            
    </div>
@stop

@section('popup')
   <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Departments</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="row clearfix">
                        <div class="col-md-4 col-sm-6">
                            <div class="form-group">                                   
                                <input type="text" class="form-control" placeholder="Employee ID">
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Name">
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Email ID">
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="form-group">
                                <input type="number" class="form-control" placeholder="Phone Number">
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="form-group">
                            <input type="text" data-provide="datepicker" data-date-autoclose="true" class="form-control" placeholder="Start date *">
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Role">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group mt-2 mb-3">
                                <input type="file" class="dropify">
                                <small id="fileHelp" class="form-text text-muted">This is some placeholder block-level help text for the above input. It's a bit lighter and easily wraps to a new line.</small>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Facebook">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">                                   
                                <input type="text" class="form-control" placeholder="Twitter">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Linkedin">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="instagram">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('page-styles')
<link rel="stylesheet" href="{{ asset('assets/plugins/sweetalert/sweetalert.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/dropify/css/dropify.min.css') }}">
@stop

@section('page-script')
<script src="{{ asset('assets/bundles/counterup.bundle.js') }}"></script>
<script src="{{ asset('assets/plugins/sweetalert/sweetalert.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/plugins/dropify/js/dropify.min.js') }}"></script>

<script src="{{ asset('assets/js/core.js') }}"></script>
<script src="{{ asset('assets/js/page/dialogs.js') }}"></script>
<script src="{{ asset('assets/js/form/dropify.js') }}"></script>
<script>
    $(function() {
        "use strict";
        $('.counter').counterUp({
            delay: 10,
            time: 1000
        });

        function getRandomValues() {
            // data setup
            var values = new Array(20);

            for (var i = 0; i < values.length; i++) {
                values[i] = [5 + randomVal(), 10 + randomVal(), 15 + randomVal(), 20 + randomVal(), 30 + randomVal(),
                    35 + randomVal(), 40 + randomVal(), 45 + randomVal(), 50 + randomVal()
                ];
            }

            return values;
        }    
        function randomVal() {
            return Math.floor(Math.random() * 80);
        }

        // MINI BAR CHART
        var values2 = getRandomValues();    
        var paramsBar = {
            type: 'bar',
            barWidth: 5,
            height: 25,
        };

        $('#mini-bar-chart1').sparkline(values2[0], paramsBar);
        paramsBar.barColor = '#6c757d';
        $('#mini-bar-chart2').sparkline(values2[1], paramsBar);
        paramsBar.barColor = '#6c757d';
        $('#mini-bar-chart3').sparkline(values2[2], paramsBar);
        paramsBar.barColor = '#6c757d';
        $('#mini-bar-chart4').sparkline(values2[3], paramsBar);
        paramsBar.barColor = '#6c757d';    
    });
</script>
@stop
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>


<script>

function Copy() 
{
    var copyText = document.getElementById("videoUrl");
    copyText.select();
    copyText.setSelectionRange(0, 99999); 
    document.execCommand("copy");
}

function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();   
    reader.onload = function(e) {
      $('.imguser').attr('src', e.target.result);
    }
    reader.readAsDataURL(input.files[0]);
  }
}

function readURLBusiness(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();   
    reader.onload = function(e) {
      $('.imguserbussiness').attr('src', e.target.result);
    }
    reader.readAsDataURL(input.files[0]);
  }
}

function readURLCard(input){
    console.log(input.files);
    if (input.files && input.files[0]) {
    var reader = new FileReader();   
    reader.onload = function(e) {
      $('.imgusercard').attr('src', e.target.result);
    }
    reader.readAsDataURL(input.files[0]);
  }
}


function SetDefaultImg(event){
    readURL(event);
    $.ajax({
    url:"{{ route('ajaxupload.action') }}",
    method:"POST",
    data:new FormData(document.getElementById("upload_form1")),
    dataType:'JSON',
    contentType: false,
    cache: false,
    processData: false,
    success:function(data)
    {
        if(data.class_name == 'alert-danger'){
            toastr.error('The select file not supported try diffrent image.');
        }else{
            toastr.success('Success.');
        }
    }
    })
 };


function SetDefaultImgBusiness(event){
    readURLBusiness(event);
    $.ajax({
    url:"{{ route('ajaxuploadbusiness.actionbusiness') }}",
    method:"POST",
    data:new FormData(document.getElementById("upload_form2")),
    dataType:'JSON',
    contentType: false,
    cache: false,
    processData: false,
        success:function(data)
        {
            if(data.class_name == 'alert-danger'){
                toastr.error('The select file not supported try diffrent image.');
            }else{
                toastr.success('Success.');
            }
        }
    })
}

function SetDefaultImgCard(event){
    readURLCard(event);

    $.ajax({
    url:"{{ route('ajaxuploadcard.actioncard') }}",
    method:"POST",
    data:new FormData(document.getElementById("upload_form3")),
    dataType:'JSON',
    contentType: false,
    cache: false,
    processData: false,
        success:function(data)
        {
            if(data.class_name == 'alert-danger'){
                toastr.error('The select file not supported try diffrent image.');
            }else{
                toastr.success('Success.');
            }
        }
    })
}

function addNew(e,val){
    $("."+val).css('display','block');
}

function closeModal(e,val){
    //alert();
    $("."+val).css('display','none');
}

function SetEmailSave(e){   
    $.ajax({
    url:"{{ route('saveemail.actionemail') }}",
    method:"POST",
    data:new FormData(document.getElementById("emailform")),
    dataType:'JSON',
    contentType: false,
    cache: false,
    processData: false,
        success:function(data)
        {
            console.log(data);
        }
    })
}
</script>