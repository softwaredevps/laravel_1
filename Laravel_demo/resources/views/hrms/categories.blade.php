@extends('layout.master')
@section('title', 'Categories')


@section('content')
<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center mb-3">
            <ul class="nav nav-tabs page-header-tab">
                <li class="nav-item"><a class="{{ (Request::segment(1) === 'categorieslist' && Request::segment(1) != 'categories') ? 'nav-link active' : 'nav-link'  }}" id="Employee-tab" href="/categorieslist">Categories List</a></li>
                <li class="nav-item"><a class="{{ Request::segment(1) === 'categories' ? 'nav-link active' : 'nav-link' }}" id="Employee-tab"  href="/categories">Add New Categories</a></li>
            </ul>
            <div class="header-action">
                <button type="button" class="btn btn-primary" onclick="setEdit()"><i class="fe fe-edit mr-2"></i>Edit</button>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-12 col-md-12">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Category</h3>
        </div>
        <div class="card-body">
            <form method="post" id="categorys">
                {{ csrf_field() }}
                <input type="hidden" name="update_id" value="<?php echo isset($res->id)?$res->id:''; ?>">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12">
                        <div class="form-group">
                            <div> Category Name<span class="redstar">*<span> </div>
                            <input type="text" class="form-control" placeholder="Category Name" name="bizcatname" 
                            
                            value="<?php echo isset($res->bizcatname)?$res->bizcatname:''; ?>"
                            >
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12">
                        <div class="form-group">
                            <div>  Category Description<span class="redstar">*<span> </div>

                            <textarea rows="8" cols="100" type="text" class="form-control" placeholder=" Category Description" value="<?php echo isset($res->bizcatdescription)?$res->bizcatdescription:''; ?>" name="bizcatdescription"><?php echo isset($res->bizcatdescription)?$res->bizcatdescription:''; ?></textarea>

                            <input type="hidden" name="hiddenbizcatdescription"  value="<?php echo isset($res->bizcatdescription)?strip_tags($res->bizcatdescription):''; ?>">
                        </div>
                    </div>

                    
                    <div class="col-lg-2 col-md-2">
                        <div class="form-group">
                        <div>Active<span class="redstar">*<span> </div>
                        <select size="1" class="form-control select-border-true  custom-select" name="isActive">
                                <option value="1" 
                                <?php echo ((isset($res->isactive) &&  $res->isactive == 1))?'selected="selected"':'';?>>YES</option>
                                <option value="0" 
                                <?php echo ((isset($res->isactive) &&  $res->isactive == 0))?'selected="selected"':'';?>>NO</option>
                            </select>
                        </div>
                    </div>
 
                </div>
                <?php if(isset($res->id)){ ?>
                    <button type="button" class="btn btn-round btn-primary" onclick="updateCategorys(this);">Update Category</button> &nbsp;&nbsp;
                <?php } else { ?>
                    <button type="button" class="btn btn-round btn-primary" onclick="saveCategorys(this);">Add Category</button> &nbsp;&nbsp;
                <?php } ?>
                <div class="col-lg-10 col-md-10 star-indigate"><span class="redstar">*<span> Indicates the required fields</div>

            </form>
        </div>
    </div>
</div>
@stop

@section('page-styles')
<link rel="stylesheet" href="{{ asset('assets/plugins/sweetalert/sweetalert.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/dropify/css/dropify.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/summernote/dist/summernote.css') }}">

@stop

@section('page-script')
<script src="{{ asset('assets/bundles/counterup.bundle.js') }}"></script>
<script src="{{ asset('assets/plugins/sweetalert/sweetalert.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/plugins/dropify/js/dropify.min.js') }}"></script>

<script src="{{ asset('assets/js/core.js') }}"></script>
<script src="{{ asset('assets/js/page/dialogs.js') }}"></script>
<script src="{{ asset('assets/js/form/dropify.js') }}"></script>
<!-- 
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>tinymce.init({ selector:'textarea' });</script> -->

<!-- <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/jquery.tinymce.min.js" referrerpolicy="origin"></script> -->

<script>
    //   $('textarea#tiny').tinymce({
    //     height: 500,
    //     menubar: false,
    //     plugins: [
    //       'advlist autolink lists link image charmap print preview anchor',
    //       'searchreplace visualblocks code fullscreen',
    //       'insertdatetime media table paste code help wordcount'
    //     ],
    //     toolbar: 'undo redo | formatselect | bold italic backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help'
    //   });
      
    $(function() {
        "use strict";
        $('.counter').counterUp({
            delay: 10,
            time: 1000
        });

        function getRandomValues() {
            // data setup
            var values = new Array(20);

            for (var i = 0; i < values.length; i++) {
                values[i] = [5 + randomVal(), 10 + randomVal(), 15 + randomVal(), 20 + randomVal(), 30 + randomVal(),
                    35 + randomVal(), 40 + randomVal(), 45 + randomVal(), 50 + randomVal()
                ];
            }

            return values;
        }    
        function randomVal() {
            return Math.floor(Math.random() * 80);
        }

        // MINI BAR CHART
        var values2 = getRandomValues();    
        var paramsBar = {
            type: 'bar',
            barWidth: 5,
            height: 25,
        };

        $('#mini-bar-chart1').sparkline(values2[0], paramsBar);
        paramsBar.barColor = '#6c757d';
        $('#mini-bar-chart2').sparkline(values2[1], paramsBar);
        paramsBar.barColor = '#6c757d';
        $('#mini-bar-chart3').sparkline(values2[2], paramsBar);
        paramsBar.barColor = '#6c757d';
        $('#mini-bar-chart4').sparkline(values2[3], paramsBar);
        paramsBar.barColor = '#6c757d';    
    });
</script>
@stop
<script>
    function saveCategorys(e){
       // $("input[name='hiddenbizcatdescription']" ).val(tinymce.activeEditor.getContent());

       var isValid = true;
        $('.form-control').each(function() {      
            if ( $(this).val() == '' && $(this).val() === '' &&  $(this).attr('name') != undefined){
                $(this).addClass('borderColor');
                isValid = false;
            }else{
                $(this).removeClass('borderColor');
                isValid = true;
            }
                
        });
        if(isValid){
            $.ajax({
            url:"{{ route('savecategorys.actioncategorys') }}",
            method:"POST",
            data:new FormData(document.getElementById("categorys")),
            dataType:'JSON',
            contentType: false,
            cache: false,
            processData: false,
                success:function(data)
                {
                    if(data.data){
                        $("#categorys")[0].reset();
                        toastr.success('Success.');
                    }else{
                        toastr.error('Error.');
                    }
                }
            })
        }
    }

    function updateCategorys(e){
      //  $("input[name='hiddenbizcatdescription']" ).val(tinymce.activeEditor.getContent());
        $.ajax({
        url:"/actioncategorysupdate",
        method:"POST",
        data:new FormData(document.getElementById("categorys")),
        dataType:'JSON',
        contentType: false,
        cache: false,
        processData: false,
            success:function(data)
            {
                if(data.data){
                    toastr.success('Success.');
                    setTimeout(function(){
                        window.location.replace("/categorieslist");
                    },1000);
                }else{
                    toastr.error('Error.');
                }
            }
        })
    }

</script>
