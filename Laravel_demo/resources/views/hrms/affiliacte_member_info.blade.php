@extends('layout.master')
@section('title', 'Member Detail')


@section('content')
<?php 
    //echo "<pre>";print_r($res);die;
?>
<a href="/associateslist" style="margin-left:10px;width:100%;float:left">
    <button style="width:15%;margin:10px 0px 10px 10px;" type="button" class="btn btn-success btn-block" data-toggle="modal"
        data-target="#exampleModal"><i class="fa fa-arrow-left "></i>&nbsp;&nbsp;&nbsp;Back</button>
</a>
<div class="section-body">
    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <h6 class="text-muted">Member Profile</h6>
                    <div class="d-flex flex-row">
                        <div style="padding-right: 20px;">
                            <img class="imguser avatar avatar-xl mr-3" onerror="this.src='/images/user.png'"
                                src="/images/{{ $res->your_image }}" alt="avatar">
                        </div>
                        <div class="flex-fill">
                            <div class="media-body">
                                <h5 class="m-0">
                                    <h6>First Name</h6>
                                    <?php echo $res->first_name; ?>
                                    <?php echo $res->last_name; ?>
                                </h5>

                                <p class="text-muted mb-0" title="Dob">
                                    <h6>Dob</h6>
                                    <span><?php echo ($res->dob)?$res->dob:'...'; ?></span>
                                </p>

                                <p class="text-muted mb-0" title="Suffix">
                                    <h6>Suffix</h6>
                                    <span><?php echo ($res->suffix)?$res->suffix:'...'; ?></span>
                                </p>


                                <p class="text-muted mb-0" title="Phone">
                                    <h6>Phone No</h6>
                                    <span><?php echo ($res->phone_no)?$res->phone_no:'...'; ?></span>

                                </p>
                                <p class="text-muted mb-0" title="Email">
                                    <h6>Email</h6>
                                    <span><?php echo ($res->your_email)?$res->your_email:'...'; ?></span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <h6 class="text-muted">Company Profile</h6>
                    <div class="row">
                        <div class="col-6">
                            <div class="mb-2"><b><?php echo $res->business_name; ?></b></div>
                            <div class="mb-2">
                                <div><?php echo $res->company_address; ?></div>
                                <div><?php echo $res->city; ?></div>
                            </div>
                            <div><?php echo $res->business_phone_no; ?></div>

                            <div>
                                <?php echo $res->business_about; ?>
                            </div>
                        </div>
                        <div class="col-6"></div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="col-lg-12 col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <h5>Sponsored By</h5>

                                <div class="lazy-resource-panel">
                                    <div class="lazy-resource-panel">
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Member</th>
                                                    <th>Affiliate</th>
                                                    <th>Category</th>
                                                    <th>Phone</th>
                                                    <th>Email Address</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php 
                                                    if(isset($res->sponsershipMemberid) && $res->sponsershipMemberid > 0){
                                                    
                                                        $sponser_name = getsponsernamebyme($res->sponsershipMemberid);
                                                        
                                                        foreach($sponser_name as $key=>$val){
                                                ?>
                                                <tr>

                                                    <td style="vertical-align: middle;">
                                                        <div class="d-flex align-items-center">
                                                            <div>
                                                            <a href="/affiliacte_member_info/{{$val->uuid}}">
                                                                <img class="sponser_img"
                                                                    onerror="this.src='/images/user.png'"
                                                                    src="/images/{{ $val->your_image }}"
                                                                    style="width: 35px; height: 35px; object-fit: cover; object-position: center top; background-color: rgb(204, 204, 204);">
                                                            </a>
                                                            </div>
                                                            <div class="ml-2">
                                                                <a href="/affiliacte_member_info/{{$val->uuid}}" class="sponser_name">
                                                                    <?php echo isset($val->first_name)?$val->first_name:''; ?>
                                                                    <?php echo isset($val->last_name)?$val->last_name:''; ?>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </td>

                                                    <td>
                                                        <?php $chapter_name =  getChapterName(@$val->affiliate_for); 
                                                                                echo isset($chapter_name->chapter_name)?$chapter_name->chapter_name:"...";
                                                                            ?>
                                                    </td>

                                                    <td>
                                                        <?php $bizcatname =  getBusinessName(@$val->business_category);
                                                                                echo isset($bizcatname->bizcatname)?$bizcatname->bizcatname:"...";
                                                                            ?>
                                                    </td>

                                                    <td class="sponser_phone" style="vertical-align: middle;">
                                                        <?php echo isset($val->phone_no)?$val->phone_no:''; ?></td>
                                                    <td class="sponser_email" style="vertical-align: middle;">
                                                        <?php echo isset($val->your_email)?$val->your_email:''; ?></td>


                                                </tr>
                                                <?php } } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                              

                            </div>
                        </div>
                    </div>


                    <div class="col-lg-12 col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <h5>Sponsored</h5>
                                <div class="lazy-resource-panel">
                                    <div class="lazy-resource-panel">
                                        <table class="table table-striped" id="affiliatesInfo">
                                            <thead>
                                                <tr>
                                                    <th>Member</th>
                                                    <th>Affiliate</th>
                                                    <th>Category</th>
                                                    <th>Phone</th>
                                                    <th>Email Address</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php 
                                            //  echo "<pre>";print_r($res->uuid);die;
                                                    if(isset($res->uuid) && $res->uuid > 0){
                                                    
                                                        $sponser_name = getsponsername($res->uuid);
                                                        
                                                        foreach($sponser_name as $key=>$val){
                                                            //  echo "";print_R($val->uuid);die;
                                                ?>
                                                <tr>

                                                    <td style="vertical-align: middle;">
                                                        <div class="d-flex align-items-center">
                                                            <div>
                                                            <a href="/affiliacte_member_info/{{$val->uuid}}">
                                                                <img class="sponser_img"
                                                                    onerror="this.src='/images/user.png'"
                                                                    src="/images/{{ $val->your_image }}"
                                                                    style="width: 35px; height: 35px; object-fit: cover; object-position: center top; background-color: rgb(204, 204, 204);">
                                                            </a>
                                                            </div>
                                                            <div class="ml-2">
                                                                <a href="/affiliacte_member_info/{{$val->uuid}}" class="sponser_name">
                                                                    <?php echo isset($val->first_name)?$val->first_name:''; ?>
                                                                    <?php echo isset($val->last_name)?$val->last_name:''; ?>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </td>

                                                    <td>
                                                        <?php $chapter_name =  getChapterName(@$val->affiliate_for); 
                                                                                echo isset($chapter_name->chapter_name)?$chapter_name->chapter_name:"...";
                                                                            ?>
                                                    </td>

                                                    <td>
                                                        <?php $bizcatname =  getBusinessName(@$val->business_category);
                                                                                echo isset($bizcatname->bizcatname)?$bizcatname->bizcatname:"...";
                                                                            ?>
                                                    </td>

                                                    <td class="sponser_phone" style="vertical-align: middle;">
                                                        <?php echo isset($val->phone_no)?$val->phone_no:''; ?></td>
                                                    <td class="sponser_email" style="vertical-align: middle;">
                                                        <?php echo isset($val->your_email)?$val->your_email:''; ?></td>


                                                </tr>
                                                <?php } } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('page-script')
<script src="{{ asset('assets/bundles/counterup.bundle.js') }}"></script>

<script src="{{ asset('assets/js/core.js') }}"></script>

@stop