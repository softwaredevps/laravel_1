@extends('layout.master')
@section('title', 'Affiliate')

@section('content')

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<?php 
    //echo "<pre>";print_R($res);die;
?>
<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center mb-3">
            <ul class="nav nav-tabs page-header-tab">
                <li class="nav-item"><a class="{{ (Request::segment(1) === 'chapterlist' && Request::segment(1) != 'chapter' && Request::segment(1) != 'board_chair_members') ? 'nav-link active' : 'nav-link'  }}" id="Employee-tab" href="/chapterlist">Affiliate List</a></li>
                <li class="nav-item"><a class="{{ Request::segment(1) === 'chapter' ? 'nav-link active' : 'nav-link' }}" id="Employee-tab"  href="/chapter">Add New Affiliates</a></li>
             </ul>
            <div class="header-action">
                <button type="button" class="btn btn-primary" onclick="setEdit()"><i class="fe fe-edit mr-2"></i>Edit</button>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-12 col-md-12"> 
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Affiliate</h3>
        </div>
        <div class="card-body">
            <form method="post" id="Affiliates">
                {{ csrf_field() }}
                <input type="hidden" name="update_id" value="<?php echo isset($res->id)?$res->id:''; ?>">
                <div class="row clearfix">
                    <div class="col-lg-6 col-md-6">
                        <div class="form-group">
                            <div>Affiliate Name<span class="redstar">*<span> </div>
                            <input type="text" class="form-control" placeholder="Affiliate Name" name="chapter_name" 
                            
                            value="<?php echo isset($res->chapter_name)?$res->chapter_name:''; ?>"
                            >
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="form-group">
                             <div>Affiliate Founded On <span class="redstar">*<span> </div>

                             <input name="chapter_founded_On" data-provide="datepicker" data-date-autoclose="true" class="form-control" placeholder="Affiliate Founded On" value="<?php echo isset($res->chapter_founded_On)?$res->chapter_founded_On:''; ?>">

                        </div>
                    </div>


                    <div class="col-lg-12 col-md-12">
                        <div class="form-group">
                            <div>Website<span class="redstar">*<span> </div>
                            <input type="text" class="form-control" placeholder="Affiliate Website" name="chapter_website" 
                            
                            value="<?php echo isset($res->chapter_website)?$res->chapter_website:''; ?>"
                            >
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12">
                        <input type="hidden" name="hidden_chapter_description" value="<?php echo isset($res->chapter_description)?strip_tags($res->chapter_description):''; ?>">

                        <div class="form-group">
                             <div>Affiliate Description<span class="redstar">*<span> </div>                         
                            <textarea rows="4" cols="50" class="form-control" name="chapter_description" value="" value="<?php echo isset($res->chapter_description)?$res->chapter_description:''; ?>"><?php echo isset($res->chapter_description)?$res->chapter_description:''; ?></textarea>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12">
                        <div class="form-group">
                            <div>Street<span class="redstar">*<span> </div>
                            <input type="text" class="form-control" placeholder="Street" name="chapter_address" 
                            
                            value="<?php echo isset($res->chapter_address)?$res->chapter_address:''; ?>"
                            >
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12">
                        <div class="form-group">
                            <div>Suite </div>
                            <input type="text" class="form-control" placeholder="Suite" name="chapter_suite" 
                            
                            value="<?php echo isset($res->chapter_suite)?$res->chapter_suite:''; ?>"
                            >
                        </div>
                    </div>

                    <!-- <div class="col-lg-12 col-md-12">
                        <div class="form-group">
                            <div>Affiliate Country</div>
                            <select class="form-control chapter_country select-border-true  custom-select" name="chapter_country" >

                            </select>
                            <input type="hidden" value="<?php //echo isset($res->chapter_country)?$res->chapter_country:''; ?>" name="hidden_chapter_country"> 
                        </div>
                    </div> -->
                        
                    <div class="col-lg-12 col-md-12">
                        <div class="form-group">
                            <div>Affiliate City<span class="redstar">*<span>  </div>
                            <input type="text" class="form-control" name="chapter_city" value="<?php echo isset($res->chapter_city)?$res->chapter_city:''; ?>" placeholder="Enter City">                          
                        </div> 
                    </div>
                    


                    <div class="col-lg-12 col-md-12">
                        <div class="form-group">
                            <div>Affiliate State/Area </div>
                            <select class="form-control chapter_state select-border-true  custom-select" name="chapter_state">

                                <?php
                                if(isset($res->chapter_state) && $res->chapter_state != ''){
                               
                                    $city_data = isset($res->chapter_state)?get_states($res->chapter_state):'';
                              
                                  // print_r($res->chapter_country);die;
                                ?>
                                    <option value="<?php echo $city_data->id; ?>"><?php echo $city_data->name; ?></option>
                                <?php
                                    }
                                ?>
                                
                            </select>
                        </div>
                    </div>

                   
                    <div class="col-lg-6 col-md-6">
                        <div class="form-group">
                            <div>Zip Code<span class="redstar">*<span> </div>
                            <input type="number" class="form-control" placeholder="Zip Code" name="zip_code" 
                            
                            value="<?php echo isset($res->zip_code)?$res->zip_code:''; ?>"
                            >
                        </div>
                    </div>

                    <!-- <div class="col-lg-6 col-md-6">
                        <div class="form-group">
                            <div>Affiliate Phone No<span class="redstar">*<span> </div>
                            <input type="text" class="form-control" placeholder="Phone Number" name="chapter_phone_no" 
                            
                            value="<?php //echo isset($res->chapter_phone_no)?$res->chapter_phone_no:''; ?>"
                            >
                        </div>
                    </div> -->

                    <div class="col-lg-3 col-md-3">
                        <div class="form-group">
                            <div>Meeting Day<span class="redstar">*<span> </div>

                            <select size="1" class="form-control select-border-true  custom-select" name="chapter_general_meeting_day">
                                <?php 
                              
                                    foreach(get_days() as $key=>$val){
                                ?>
                                <option  value="<?php echo isset($val->day_name)?$val->day_name:'' ?>" 
                                    <?php echo ((isset($res->chapter_general_meeting_day) &&  $res->chapter_general_meeting_day == $val->day_name))?'selected="selected"':'';?>>
                                    <?php echo isset($val->day_name)?$val->day_name:'' ?>
                                 </option>
                                <?php 
                                    }
                                ?>
                            </select>

                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3">
                        <div class="form-group" id='datetimepicker3'>    
                            <div>Meeting Time From(hh:mm)<span class="redstar">*<span> </div>
                      
                            <input name="chapter_general_meeting_time_from" class="form-control datetimepicker3" placeholder="Affiliate Metting Time Upto" value="<?php echo isset($res->chapter_general_meeting_time_from)?$res->chapter_general_meeting_time_from:''; ?>">
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3">
                        <div class="form-group" id='datetimepicker4'> 
                            <div>Meeting Time To(hh:mm)<span class="redstar">*<span> </div>                        
                            <input name="chapter_general_meeting_time_on" class="form-control datetimepicker4" placeholder="Affiliate Metting Time Up"  value="<?php echo isset($res->chapter_general_meeting_time_on)?$res->chapter_general_meeting_time_on:''; ?>">
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-4">
                        <div class="form-group">
                            <div>Meeting Location MAP (please input url)</div>
                            <input type="text" class="form-control" placeholder="Meeting Location" name="meeting_location" 
                            
                            value="<?php echo isset($res->meeting_location)?$res->meeting_location:''; ?>"
                            >
                        </div> 
                    </div>


                </div>
                <?php if(isset($res->id)){?>
                    <button type="button" class="btn btn-round btn-primary" onclick="updateChapter(this);">Update Affiliate</button> &nbsp;&nbsp;
                <?php } else { ?>
                    <button type="button" class="btn btn-round btn-primary" onclick="saveChapter(this);">Add Affiliate</button> &nbsp;&nbsp;
                <?php } ?>
                    <div class="col-lg-10 col-md-10 star-indigate"><span class="redstar">*<span> Indicates the required fields</div>
            </form>
        </div>
    </div>
</div>
@stop

@section('page-styles')
<link rel="stylesheet" href="{{ asset('assets/plugins/sweetalert/sweetalert.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/dropify/css/dropify.min.css') }}">
@stop

@section('page-script')
<script src="{{ asset('assets/bundles/counterup.bundle.js') }}"></script>
<script src="{{ asset('assets/plugins/sweetalert/sweetalert.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/plugins/dropify/js/dropify.min.js') }}"></script>

<script src="{{ asset('assets/js/core.js') }}"></script>
<script src="{{ asset('assets/js/page/dialogs.js') }}"></script>
<script src="{{ asset('assets/js/form/dropify.js') }}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>

<script>
    $(function() {
        "use strict";
        $('.counter').counterUp({
            delay: 10,
            time: 1000
        });

        function getRandomValues() {
            // data setup
            var values = new Array(20);

            for (var i = 0; i < values.length; i++) {
                values[i] = [5 + randomVal(), 10 + randomVal(), 15 + randomVal(), 20 + randomVal(), 30 + randomVal(),
                    35 + randomVal(), 40 + randomVal(), 45 + randomVal(), 50 + randomVal()
                ];
            }

            return values;
        }    
        function randomVal() {
            return Math.floor(Math.random() * 80);
        }

        // MINI BAR CHART
        var values2 = getRandomValues();    
        var paramsBar = {
            type: 'bar',
            barWidth: 5,
            height: 25,
        };

        $('#mini-bar-chart1').sparkline(values2[0], paramsBar);
        paramsBar.barColor = '#6c757d';
        $('#mini-bar-chart2').sparkline(values2[1], paramsBar);
        paramsBar.barColor = '#6c757d';
        $('#mini-bar-chart3').sparkline(values2[2], paramsBar);
        paramsBar.barColor = '#6c757d';
        $('#mini-bar-chart4').sparkline(values2[3], paramsBar);
        paramsBar.barColor = '#6c757d';    
    });
</script>
@stop

<!-- <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>tinymce.init({ selector:'textarea' });</script> -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>



<script>

    $(document).ready(function(){ 

        // $('.datetimepicker3').timepicker({
        //     timeFormat: 'h:mm p',
        //     interval: 60,
        //     minTime: '10',
        //     maxTime: '6:00pm',
        //     startTime: '10:00',
        //     dynamic: false,
        //     dropdown: true,
        //     scrollbar: true
        // });

        // $('.datetimepicker4').timepicker({
        //     timeFormat: 'h:mm p',
        //     interval: 60,
        //     minTime: '10',
        //     maxTime: '6:00pm',
        //     startTime: '11:00',
        //     dynamic: false,
        //     dropdown: true,
        //     scrollbar: true
        // });

        $.ajax({
        url:"/getCountry",
        method:"get",
        processData: false,
            success:function(data)
            {
                $(".chapter_country option").empty();
                $(".chapter_country").append("<option value=''>Select</option>");
                $.each(data.data, function(e,v) {
                    var selected = '';
                    if($("input[name='hidden_chapter_country']").val() == v.id){
                        selected = 'selected';
                    }
                    if(v.code == 'US' || v.code == 'CA')
                    $(".chapter_country").append("<option value='"+v.id+"' "+selected+">"+v.name+"</option>");
                });
            }
        })    
    })

    function saveChapter(e){
        //$("input[name='hidden_chapter_description']" ).val(tinymce.activeEditor.getContent());

        var isValid = true;
        $('.form-control').each(function() {   
            if ( $(this).attr('name') != 'chapter_suite' && $(this).attr('name') != 'meeting_location' && $(this).val() == '' && $(this).val() === '' && $(this).attr('name') != 'chapter_country' && $(this).attr('name') != undefined){
                $(this).addClass('borderColor');
                isValid = false;
            }else{
                $(this).removeClass('borderColor');
                isValid = true;
            }               
        });

        if(isValid){
        $.ajax({
            url:"{{ route('savechapter.actionchapter') }}",
            method:"POST",
            data:new FormData(document.getElementById("Affiliates")),
            dataType:'JSON',
            contentType: false,
            cache: false,
            processData: false,
                success:function(data)
                {
                    if(data.data){
                        $("#Affiliates")[0].reset();
                        toastr.success('Success.');
                    }else{
                        toastr.error('Error.');
                    }
                }
            })
        }       
    }

    function updateChapter(e){
        //$("input[name='hidden_chapter_description']" ).val(tinymce.activeEditor.getContent());
        var isValid = true;
        $('.form-control').each(function() {   
            if ($(this).attr('name') != 'chapter_suite' && $(this).attr('name') != 'meeting_location' && $(this).val() == '' && $(this).val() === '' && $(this).attr('name') != 'chapter_country' && $(this).attr('name') != undefined){
                $(this).addClass('borderColor');
                isValid = false;
            }else{
                $(this).removeClass('borderColor');
                isValid = true;
            }               
        });

        if(isValid){
        $.ajax({
            url:"/actionchaptersupdate",
            method:"POST",
            data:new FormData(document.getElementById("Affiliates")),
            dataType:'JSON',
            contentType: false,
            cache: false,
            processData: false,
                success:function(data)
                {
                    if(data.data){
                        toastr.success('Success.');
                        setTimeout(function(){
                            window.location.replace("/chapterlist");
                        },1000);
                    }else{
                        toastr.error('Error.');
                    }
                }
            })
        }
    }
   
    // $(document).on('change', 'select.chapter_country', function() { 
    //    var id = $(this).val();
    //    $.ajax({
    //     url:"/getState",
    //     method:"POST",
    //     data: {
    //         "_token": "{{ csrf_token() }}",
    //         "data": id,
    //     },
    //     success:function(data)
    //     {
    //         $('select[name="chapter_city"]').empty();
    //         $('select[name="chapter_state"]').empty();  
    //         $.each(data.data, function(e,v) {
    //             $(".chapter_state").append("<option value='"+v.id+"'>"+v.name+"</option>");
    //         });
    //     } 
    //     })
    // });


    // $(document).on('change', 'select.chapter_state', function() {
    //    var id = $(this).val();
    //    $.ajax({
    //     url:"/getCity",
    //     method:"POST",
    //     data: {
    //         "_token": "{{ csrf_token() }}",
    //         "data": id,
    //     },
    //     success:function(data)
    //     {
    //         $(".chapter_city select").empty();
    //         $('select[name="chapter_city"]').empty();
    //         $.each(data.data, function(e,v) {
    //             $(".chapter_city").append("<option value='"+v.state_id+"'>"+v.name+"</option>");
    //         });
    //     } 
    //     })
    // });

       $.ajax({
        url:"/getState",
        method:"POST",
        data: {
            "_token": "{{ csrf_token() }}",
            "data": 231,
        },
        success:function(data)
        {
            // $('select[name="chapter_city"]').empty();
            // $('select[name="chapter_state"]').empty();  
            $.each(data.data, function(e,v) {
                setTimeout(function(){
                    $('select[name="chapter_state"]').append("<option value='"+v.id+"'>"+v.name+"</option>");
                },1000)
            });
        } 
        })

</script>
