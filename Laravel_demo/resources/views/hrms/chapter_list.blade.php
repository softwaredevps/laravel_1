@extends('layout.master')
@section('title', 'Affiliate List')

@section('content')
<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center mb-3">
            <ul class="nav nav-tabs page-header-tab">
                <li class="nav-item"><a class="{{ (Request::segment(1) === 'chapterlist' && Request::segment(1) != 'chapter' && Request::segment(1) != 'board_chair_members') ? 'nav-link active' : 'nav-link'  }}" id="Employee-tab" href="/chapterlist">Affiliate List</a></li>
                <li class="nav-item"><a class="{{ Request::segment(1) === 'chapter' ? 'nav-link active' : 'nav-link' }}" id="Employee-tab"  href="/chapter">Add New Affiliates</a></li>
             </ul>
            <div class="header-action">
                <button type="button" class="btn btn-primary" onclick="setEdit()"><i class="fe fe-edit mr-2"></i>Edit</button>
            </div>
        </div>
    </div>
</div>
<div class="section-body">
    <div class="container-fluid">
        <div class="tab-content">
            <div class="tab-pane fade show active" id="Employee-list" role="tabpanel">                        
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Affiliate List</h3>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <form>
                                {{ csrf_field() }}
                                <table class="table table-hover table-striped table-vcenter text-nowrap mb-0 trtd" id="chapter_list_table">
                                    <thead>
                                        <tr>
                                            <th class="text-left"><strong>#</strong></th>
                                            <th class="text-left"><strong>Affiliate</strong></th>
                                       
                                            <th class="text-left"><strong>City</strong></th>
                                            <th class="text-left"><strong>State</strong></th>
                                            <th class="text-left"><strong>Is Active</strong></th>
                                            <th><strong>Action</strong></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      
                                    <?php $i=1; ?>
                                    @foreach($res as $val)  
                                    
                                        <tr class="row-{{$val->id}}">
                                            <td class="text-left"><span><?php echo $i;?></span></td>
                                            
                                            <td class="text-left">
                                                <a href="/chapter_roster_list/<?php echo $val->id; ?>">
                                                    {{$val->chapter_name}}
                                                </a>
                                            </td>
<!--                                            
                                            <td class="text-left">
                                                <?php 
                                                    //$country_name = country_name($val->chapter_country);
                                                   // echo (isset($country_name->name) && $country_name->name !='')?$country_name->name:'';

                                               
                                                ?>                                           
                                            </td> -->

                                            <td class="text-left">
                                                
                                                {{($val->chapter_city)?$val->chapter_city:'...'}}
                                                                                        
                                          </td>

                                            <td class="text-left">
                                                <?php 
                                         
                                                    $state_name = get_states($val->chapter_state);
                                                    echo (isset($state_name->name) && $state_name->name !='')?$state_name->name:' ...';
                                               
                                                ?>                                           
                                            </td>                                

                                            <td class="text-left">{{($val->isActive == 1)?'Active':'Inactive'}}</td>

                                            <td>
                                                <a href="/chapter/{{$val->id}}"><button type="button" class="btn btn-icon btn-sm" title="Edit"><i class="fa fa-edit"></i></button></a>
                                                <button onclick="deleteChapter({{$val->id}});" type="button" class="btn btn-icon btn-sm js-sweetalert" title="Delete" data-type="confirm"><i class="fa fa-trash-o text-danger"></i></button>
                                             </td>
                                        </tr>  
                                    <?php $i++;?>
                                    @endforeach       
                                </tbody>
                                </table>
                            </form>                            
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </div>            
</div>

@stop

@section('page-styles')
<link rel="stylesheet" href="{{ asset('assets/plugins/fullcalendar/fullcalendar.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/sweetalert/sweetalert.css') }}">
<link href='http://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css' rel='stylesheet' type='text/css'>

@stop

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

@section('page-script')


<script src="{{ asset('assets/bundles/fullcalendar.bundle.js') }}"></script>


<script src="{{ asset('assets/js/page/calendar.js') }}"></script>
<script src="{{ asset('assets/plugins/sweetalert/sweetalert.min.js') }}"></script>

<script src="{{ asset('assets/js/core.js') }}"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.8/js/jquery.dataTables.min.js"></script>

@stop



<script>
    function submitAjax(id){  
        $.ajax({
            url:"/actionchapterdelete",
            method:"POST",
            data: {
                    "_token": "{{ csrf_token() }}",
                    "data": id,
                },
            success:function(data)
            {
                if(data.data){
                    $(".row-"+id).remove();
                    toastr.success('Success.');
                    swal.close()
                }else{
                    toastr.error('Error.');
                }
            }
        })   
    }

    function deleteChapter(id){
        var form = event.target.form; 
        swal({
            title: "Are you sure?",
            text: "But you will still be able to retrieve this file.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#dc3545",
            confirmButtonText: "Delete",
            cancelButtonText: "No, cancel please!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){
            if (isConfirm) {
                submitAjax(id); 
            } else {
                swal("Cancelled", "Your Request Is Cancelled.. :)", "error");
            }
        });
    }
</script>