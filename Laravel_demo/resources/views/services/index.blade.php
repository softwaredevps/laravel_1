@extends('layout.member_master')
@section('title', 'Services')
@section('content')

<?php 
 //echo "<pre>";print_r($updateForm);die;
?>
<div class="col-lg-12 col-md-12">
   <div class="col-lg-12 col-md-12">
      <div class="section-body">
         <div class="container-fluid">
            <div class="d-flex justify-content-between align-items-center">
               <ul class="nav nav-tabs page-header-tab">
                  <li class="nav-item"><a class="nav-link <?php echo ($updateForm)?'':'active';?>" id="li-ref-tab" data-toggle="tab" href="#ref-tab">Dashboard</a></li>
                  <li class="nav-item"><a class="nav-link  <?php echo ($updateForm)?'active':'';?>" id="li-add-new-service" data-toggle="tab" href="#add-new-service">Add New Service</a></li>
                  <li class="nav-item"><a class="nav-link" id="li-my-services" data-toggle="tab" href="#my-services">My Services</a></li>
               </ul>
            </div>
         </div>
      </div>

      @if ($errors->any())
         <div class="alert alert-danger" style="margin: 0px 35px;">
            <ul>
                  @foreach ($errors->all() as $error)
                     <li>{{ $error }}</li>
                  @endforeach
            </ul> 
         </div>
      @endif

      <div class="flash-message"  style="margin: 0px 35px;">
         @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
         @endforeach
      </div>

      <div class="section-body mt-3">
         <div class="container-fluid">
            <div class="tab-content mt-3">

               <!-- Dashboard Start Here -->
               <div class="tab-pane fade  <?php echo ($updateForm)?'':'show active';?>" id="ref-tab" role="tabpanel">
                  <div class="card">
                     <div class="card-header">
                        <div class="card-options">
                           <form>
                              <div class="input-group">
                                 <select class="form-control">
                                    <option value="past_12_months">Past 12 Months</option>
                                    <option value="this_year">This Year</option>
                                    <option value="last_year">Last Year</option>
                                 </select>
                              </div>
                           </form>
                        </div>
                     </div>
                     <div class="card-body">
                        <div id="apex-basic-column" style="min-height: 365px;">
                           
                        </div>
                     </div>
                  </div>
               </div>
               <!-- Dashboard end here -->
               <!-- Add New Service start here -->
               <div class="tab-pane fade <?php echo ($updateForm)?'show active':'';?>" id="add-new-service" role="tabpanel">
               <div class="card">
               <div class="card-header">
               <!--h3 class="card-title">123 6th St. Melbourne, FL 32904</h3-->
               </div>
               <div class="card-body">
               <form method="post" id="">
               @csrf
                  <div class="row clearfix">
                     <div class="col-lg-6 col-md-6">
                        <label>Service  Name <span class="redstar">*<span></label>
                        <div class="form-group">
                           <input value="<?php echo ($updateForm && $res[0]->service_name)?$res[0]->service_name:'';?>" type="text" name="service_name" class="form-control" placeholder="Service Name" required>
                        </div>
                     </div>

                     <div class="col-lg-6 col-md-6">
                        <label>Service Category <span class="redstar">*<span></label>
                        <div class="form-group">
                           <select class="custom-select" name="service_category" required>
                              <option value="">Select Category</option>

                              <?php foreach(business_categories_fetch() as $key=>$val){?>
                                 <option value="<?php echo isset($val->id)?$val->id:''; ?>"
                                    <?php echo ($updateForm && $res[0]->service_category == $val->id)?'selected':''; ?>>
                                    <?php echo isset($val->bizcatname)?$val->bizcatname:''; ?>
                                 </option>
                              <?php } ?>
                           </select>
                        </div>
                     </div>

                     <div class="col-lg-6 col-md-6">
                        <label>Tentative Price($) <span class="redstar">*<span></label>
                        <div class="form-group">
                           <input value="<?php echo ($updateForm && $res[0]->tentative_price)?$res[0]->tentative_price:'';?>" type="text" name="tentative_price" class="form-control" placeholder="Tentative Price" required>
                        </div>
                     </div>

                     <div class="col-lg-6 col-md-6">
                        <label>BeSure Rewards (required to redeem this service) <span class="redstar">*<span></label>
                        <div class="form-group">
                           <input value="<?php echo ($updateForm && $res[0]->besure_rewards_points)?$res[0]->besure_rewards_points:'';?>" type="text" name="besure_rewards_points" class="form-control" placeholder="BeSure Rewards" required>
                        </div>
                     </div>

                     <div class="col-lg-12 col-md-12">
                        <label>Service Description <span class="redstar">*<span></label>
                        <div class="form-group">
                           <textarea  value="<?php echo ($updateForm && $res[0]->service_description)?$res[0]->service_description:'';?>" rows="8" cols="100" type="text" class="form-control" placeholder="Details" name="service_description" required> <?php echo ($updateForm && $res[0]->service_description)?$res[0]->service_description:'';?>
                           </textarea>
                        </div>
                     </div>
                  </div>

                  <?php if(isset($updateForm)){?>
                     <button type="submit" class="btn btn-round btn-primary">Update Service</button>
                     <input type="hidden" name="hiddenUpdate" value="1">
                  <?php } else {?>
                     <button type="submit" class="btn btn-round btn-primary">Submit Service</button>
                  <?php } ?>
                  <div class="col-lg-10 col-md-10 star-indigate"><span class="redstar">*<span> Indicates the required fields</span></span></div>
               </form>
               </div>
               </div>
               </div>
               <!-- Add New Service end here -->


               <!-- My Services start here -->

               <div class="tab-pane fade" id="my-services" role="tabpanel">
                  <div class="row clearfix">
                     <div class="col-12">
                        <div class="card">
                           <div class="card-body">
                              <div class="table-responsive">
                              <form>
                              {{ csrf_field() }}
                              <table class="table table-hover table-striped table-vcenter text-nowrap mb-0 trtd" id="services_list_table">
                              <thead>
                              <tr>
                              <th class="text-left"><strong>#</strong></th>
                              <th class="text-left"><strong>Service Name</strong></th>
                              <th class="text-left"><strong>Category</strong></th>
                              <th class="text-center"><strong>Tentative Price <br>($)</strong></th>
                              <th class="text-center"><strong>BeSure Rewards<br> to Redeem</strong></th>
                              <!--th class="text-centger"><strong>Turnaround<br> Time</strong></th-->
                              <th><strong>Action</strong></th>
                              </tr>
                              </thead>
                              <tbody>
                                 <?php if($res){ $i = 1; ?>
                                    @foreach ($res as $data)
                                       <tr class="row-{{ $data->id }}">
                                          <td class="text-left"><span>{{ $i }}</span></td>
                                          <td class="text-left">{{$data->service_name}}</td>
                                          <td class="text-left">
                                             <?php 
                                                   $business_cat_name =  business_categories_fetch_byid($data->service_category); 
                                                   echo isset($business_cat_name->bizcatname)?$business_cat_name->bizcatname:"...";
                                             ?>
                                          </td>
                                          <td class="text-center">
                                             <?php echo number_format((float)$data->besure_rewards_points, 2, '.', ''); ?>
                                          </td>

                                          <td class="text-center">
                                             <?php echo number_format((float)$data->tentative_price, 2, '.', ''); ?>
                                          </td>

                                          <!--td class="text-center">7 days</td-->
                                          <td>
                                             <a href="/services/{{$data->id}}"><button type="button" class="btn btn-icon btn-sm" title="Edit"><i class="fa fa-edit"></i></button></a>
                                             <button onClick="deleteService({{$data->id}})" type="button" class="btn btn-icon btn-sm js-sweetalert"
                                                title="Delete" data-type="confirm"><i
                                                    class="fa fa-trash-o text-danger"></i></button>                                          </td>
                                       </tr> 
                                       <?php $i++; ?>
                                    @endforeach 
                                 <?php } ?>     
                              </tbody>
                              </table>
                              </form>                            
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- My Services end here -->

               


            </div>
         </div>
      </div>
   </div>
</div>

@stop




@section('popup')
<div class="modal fade " id="editModal" tabindex="-1" role="dialog">
   
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Referral</h5>
                    <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>-->
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
                <div class="modal-body">

                        <form>
                          <div class="row clearfix">

                              <div class="col-lg-6 col-md-6">
                                 <div class="form-group">
                                    <input type="text" data-provide="datepicker" data-date-autoclose="true" class="form-control" placeholder="Start date *">
                                 </div>
                              </div>

                              <div class="col-lg-6 col-md-6">
                                 <div class="form-group">
                                       <select class="custom-select">
                                          <option selected="">Referral Type</option>
                                          <option value="1">Inside</option>
                                          <option value="2">OutSide</option>
                                       </select>
                                 </div>
                              </div>

                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <input type="text" class="form-control" placeholder="Person To Contact">
                                  </div>
                              </div>

                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">                                   
                                      <input type="text" class="form-control" placeholder="Company Name">
                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <input type="text" class="form-control" placeholder="Phone Number">
                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <input type="text" class="form-control" placeholder="Email Address">
                                  </div>
                              </div>

                              <div class="col-lg-12 col-md-12">
                                  <div class="form-group">
                                     <textarea class="form-control" name="" id="" placeholder="Referral Details"></textarea>
                                  </div>
                              </div>

                              
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <input type="text" class="form-control" placeholder="$">
                                  </div>
                              </div>

                             <div class="col-lg-6 col-md-6">
                                 <div class="form-group">
                                       <select class="custom-select">
                                          <option selected="">Status</option>
                                          <option value="1">New</option>
                                          <option value="2">Bogus/Invalid</option>
                                          <option value="2">Closed</option>
                                          <option value="2">Working</option>
                                       </select>
                                 </div>
                              </div>

                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1">
                                        <span class="custom-control-label">This is a private Referral</span>
                                        <span style="position: absolute; margin: 0 0 0 12px;"><i class="fa fa-question-circle" style="font-size: 22px;"></i></span>
                                    </label>

                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      
                                  </div>
                              </div>

                          </div>
                        </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary">Submit Referral</button>
                </div>
            </div>
        </div>
</div>

<div class="modal fade " id="exampleModal" tabindex="-1" role="dialog">
   
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Referral Form</h5>
                    <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>-->
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
                <div class="modal-body">

                        <form>
                          <div class="row clearfix">

                           <style type="text/css">
                                 .tip-form-header{box-sizing: border-box; background-color: #e0a258; color: #fff; padding: 1rem;width: 100%;}
                                 .tip-form-header .tip-form-member{display: -webkit-box;display: flex; -webkit-box-flex: 1; flex: auto; align-items: center;
                                 }
                              .tip-form-header .profile-image{width: 120px; height: 120px; object-fit: cover; object-position: top center; margin-right: 20px; background-color: #ccc;}

                           </style>

                              <div class="tip-form-header">
                                 <div class="tip-form-member">
                                    <img class="profile-image" src="../assets/images/xs/avatar1.jpg" alt="">
                                    <div class="member-info">
                                       <div class="member-name">Alice Agyemang-Badu</div>
                                       <div class="business-name">New York Life</div>
                                       <div class="text-muted category-name">Recruiter</div>
                                       <div class="chapter-name">Besure of Upper Montclair, NJ</div>
                                    </div>
                                 </div>
                              </div>

                              <div class="text-center" style="padding: 10px; font-size: 90%; font-weight: 500;"><p>My qualified Business Referral is for a company or person who is interested in a specific service or product and is expecting a call from a Besure member.</p></div>

                              <div class="col-lg-6 col-md-6">
                                 <div class="form-group">
                                    <input type="text" data-provide="datepicker" data-date-autoclose="true" class="form-control" placeholder="Start date *">
                                 </div>
                              </div>

                              <div class="col-lg-6 col-md-6">
                                 <div class="form-group">
                                       <select class="custom-select">
                                          <option selected="">Referral Type</option>
                                          <option value="1">Inside</option>
                                          <option value="2">OutSide</option>
                                       </select>
                                 </div>
                              </div>

                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <input type="text" class="form-control" placeholder="Person To Contact">
                                  </div>
                              </div>

                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">                                   
                                      <input type="text" class="form-control" placeholder="Company Name">
                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <input type="text" class="form-control" placeholder="Phone Number">
                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <input type="text" class="form-control" placeholder="Email Address">
                                  </div>
                              </div>

                              <div class="col-lg-12 col-md-12">
                                  <div class="form-group">
                                     <textarea class="form-control" name="" id="" placeholder="Referral Details"></textarea>
                                  </div>
                              </div>

                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1">
                                        <span class="custom-control-label">Specify the Referral value</span>
                                    </label>
                                     
                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      
                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1">
                                        <span class="custom-control-label">This is a private Referral</span>
                                        <span style="position: absolute; margin: 0 0 0 12px;"><i class="fa fa-question-circle" style="font-size: 22px;"></i></span>
                                    </label>

                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                                  <div class="form-group">
                                      
                                  </div>
                              </div>

                          </div>
                        </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary">Submit Referral</button>
                </div>
            </div>
        </div>
</div>
@stop


@section('page-styles')

<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }} ">
<link href='http://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="{{ asset('assets/plugins/sweetalert/sweetalert.css') }}">

@stop

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

@section('page-script')
<script src="{{ asset('assets/bundles/apexcharts.bundle.js') }}"></script>
<script src="{{ asset('assets/bundles/counterup.bundle.js') }}"></script>
<script src="{{ asset('assets/bundles/knobjs.bundle.js') }}"></script>
<script src="{{ asset('assets/js/core.js') }}"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.8/js/jquery.dataTables.min.js"></script>
<script src="{{ asset('assets/js/page/index.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/plugins/sweetalert/sweetalert.min.js') }}"></script>

<script>
   $(document).ready(function()
   {
      $('#services_list_table').DataTable();
   });

   function deleteService(id) {
        var form = event.target.form;
        swal({
                title: "Are you sure?",
                text: "But you will still be able to retrieve this file.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#dc3545",
                confirmButtonText: "Delete",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    submitAjax(id);
                } else {
                    swal("Cancelled", "Your Request Is Cancelled.. :)", "error");
                }
            });
    }

    function submitAjax(id) {
        $.ajax({
            url: "/actionDeleteService",
            method: "POST",
            data: {
                "_token": "{{ csrf_token() }}",
                "data": id,
            },
            success: function(data) {
                if (data.data) {
                    $(".row-" + id).remove();
                    toastr.success('Success.');
                    swal.close()
                } else {
                    toastr.error('Error.');
                }
            }
        })
    }

</script>
@stop
