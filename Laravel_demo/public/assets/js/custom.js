function SetDefault(res) {
    var name = res.name;
    var data = res.value;

    $.ajax({
        type: 'POST',
        url: '/ajaxRequest',
        data: {
            "_token": $('#token').val(),
            "data": data,
            "type": name
        },
        success: function(e, xhr, data) {
            if (data.status == 200) {
                toastr.success('Success.');
            } else {
                alert('sdf');
                toastr.success('Please Try Again.');
            }
        }
    });
}


function SetDefaultBusiness(res) {
    var name = res.name;
    var data = res.value;

    $.ajax({
        type: 'POST',
        url: '/ajaxRequestBusiness',
        data: {
            "_token": $('#token').val(),
            "data": data,
            "type": name
        },
        success: function(e, xhr, data) {
            if (data.status == 200) {
                toastr.success('Success.');
            } else {
                alert('sdf');
                toastr.success('Please Try Again.');
            }
        }
    });
}

function SetDefaultNetwork(res) {
    var name = res.name;
    var data = res.value;

    $.ajax({
        type: 'POST',
        url: '/ajaxRequestNetwork',
        data: {
            "_token": $('#token').val(),
            "data": data,
            "type": name
        },
        success: function(e, xhr, data) {
            if (data.status == 200) {
                toastr.success('Success.');
            } else {
                alert('sdf');
                toastr.success('Please Try Again.');
            }
        }
    });
}


function setEdit() {
    if (!$('.form-borders').is(':visible')) {
        $('.form-edit').removeClass('pointerEvent');
        $('.float-right').removeClass('pointerEvent');
        $('.float-left.pointerEvent').removeClass('pointerEvent');
        $('.form-edit').addClass('form-borders');
        $('select').addClass('form-borders');
    } else {
        $('.form-edit').addClass('pointerEvent');
        $('.float-right').addClass('pointerEvent');
        $('.float-left.pointerEvent').addClass('pointerEvent');
        $('.form-edit').removeClass('form-borders');
        $('select').removeClass('form-borders');
    }
}

$("input,select").click(function() {
    $(this).removeClass('form-borders');
});

function SetEmailSave(e, modalType) {
    //console.log()
    var type = $("select[name*='email_type_modal']").val();
    var data = $("input[name*='email_values']").val();

    $.ajax({
        type: 'POST',
        url: '/ajaxRequestModalEmail',
        data: {
            "_token": $('#token').val(),
            "data": data,
            "type": type
        },
        success: function(e, xhr, data) {
            if (data.status == 200) {
                toastr.success('Success.');
                $('.modal-body .btn.btn-').trigger('click');
                $("#emailform").trigger("reset");
            } else {
                toastr.error('Please Try Again.');
            }
        }
    });
}

function SetNumberSave(e, modalType) {

    var type = $("select[name*='phonetype']").val();
    var data = $("input[name*='number']").val();

    $.ajax({
        type: 'POST',
        url: '/ajaxRequestModalMobile',
        data: {
            "_token": $('#token').val(),
            "data": data,
            "type": type
        },
        success: function(e, xhr, data) {
            if (data.status == 200) {
                toastr.success('Success.');
                $('.modal-body .btn.btn-').trigger('click');
                $("#emailform").trigger("reset");
            } else {
                toastr.error('Please Try Again.');
            }
        }
    });
}