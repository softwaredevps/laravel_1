<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return redirect('/'); });

/* HR */
Route::any('/home','HrmsController@index');

Route::group(['middleware' => 'auth'], function () {

Route::get('hrms', function ()                { return redirect('hrms/index'); });
Route::get('index',                      'HrmsController@index')->name('hrms.index');
Route::get('users',                      'HrmsController@users')->name('hrms.users');
Route::get('departments',                'HrmsController@departments')->name('hrms.departments');
// Route::get('hrms/employee',                   'HrmsController@employee')->name('hrms.employee');


Route::get('categories', 'HrmsController@categories')->name('hrms.categories');
Route::get('categorieslist', 'HrmsController@categorieslist')->name('hrms.categorieslist');


Route::get('chapter', 'HrmsController@chapter')->name('hrms.chapter');
Route::get('chapterlist', 'HrmsController@chapterlist')->name('hrms.chapterlist');

Route::get('associateApplication', 'HrmsController@associateApplication');


Route::get('profle',                     'HrmsController@employee')->name('hrms.employee');
Route::get('activities',                 'HrmsController@activities')->name('hrms.activities');
Route::get('holidays',                   'HrmsController@holidays')->name('hrms.holidays');
Route::get('events',                     'HrmsController@events')->name('hrms.events');
Route::get('payroll',                    'HrmsController@payroll')->name('hrms.payroll');
Route::get('accounts',                   'HrmsController@accounts')->name('hrms.accounts');
Route::get('report',                     'HrmsController@report')->name('hrms.report');

/* Project */
Route::get('project', function ()             { return redirect('project/index2'); });
Route::get('project/index2',                  'ProjectController@index2')->name('project.index2');
Route::get('project/list',                    'ProjectController@list')->name('project.list');
Route::get('project/taskboard',               'ProjectController@taskboard')->name('project.taskboard');
Route::get('project/ticket',                  'ProjectController@ticket')->name('project.ticket');
Route::get('project/ticketdetails',           'ProjectController@ticketdetails')->name('project.ticketdetails');
Route::get('project/clients',                 'ProjectController@clients')->name('project.clients');
Route::get('project/todo',                    'ProjectController@todo')->name('project.todo');

/* Job */
Route::get('job', function ()                 { return redirect('job/index3'); });
Route::get('job/index3',                      'JobController@index3')->name('job.index3');
Route::get('job/positions',                   'JobController@positions')->name('job.positions');
Route::get('job/applicants',                  'JobController@applicants')->name('job.applicants');
Route::get('job/resumes',                     'JobController@resumes')->name('job.resumes');
Route::get('job/jobsettings',                 'JobController@jobsettings')->name('job.jobsettings');

/* Authentication  */
Route::get('authentication', function ()        { return redirect('authentication/login'); });
Route::get('authentication/login',              'AuthenticationController@login')->name('authentication.login');
Route::get('authentication/register',           'AuthenticationController@register')->name('authentication.register');
Route::get('authentication/forgotpassword',     'AuthenticationController@forgotpassword')->name('authentication.forgotpassword');
Route::get('authentication/error404',           'AuthenticationController@error404')->name('authentication.error404');
Route::get('authentication/error500',           'AuthenticationController@error500')->name('authentication.error500');

/* Extra pages  */
Route::get('pages', function ()                 { return redirect('pages/search'); });
Route::get('pages/search',                      'PagesController@search')->name('pages.search');
Route::get('pages/calendar',                    'PagesController@calendar')->name('pages.calendar');
Route::get('pages/contact',                     'PagesController@contact')->name('pages.contact');
Route::get('pages/filemanager',                 'PagesController@filemanager')->name('pages.filemanager');

/* Chat app  */
Route::get('chatapp', function ()                 { return redirect('chatapp/chat'); });
Route::get('chatapp/chat',                      'ChatappController@chat')->name('chatapp.chat');

// ajax request to edit profile
Route::post('ajaxRequest', 'HrmsController@ajaxRequest');
Route::post('ajaxRequestBusiness', 'HrmsController@ajaxRequestBusiness');
Route::post('ajaxRequestNetwork', 'HrmsController@ajaxRequestNetwork');

Route::get('/ajax_upload', 'HrmsController@indexn');

Route::post('/ajax_upload/action', 'HrmsController@action')->name('ajaxupload.action');

Route::post('/ajax_upload/action_your_image', 'HrmsController@action_your_image')->name('ajaxupload.action_your_image');

Route::post('/ajax_upload/action_your_card', 'HrmsController@action_your_card')->name('ajaxupload.action_your_card');

Route::get('/ajaxuploadbusiness', 'HrmsController@indexbusiness');

Route::post('/ajaxuploadbusiness/actionbusiness', 'HrmsController@actionbusiness')->name('ajaxuploadbusiness.actionbusiness');

Route::post('/saveemail/actionemail', 'HrmsController@actionemail')->name('saveemail.actionemail');

Route::post('/ajaxRequestModalEmail', 'HrmsController@ajaxRequestModalEmail');
Route::post('/ajaxRequestModalMobile', 'HrmsController@ajaxRequestModalMobile');


Route::post('/ajaxuploadcard/actioncard', 'HrmsController@ajaxuploadcard')->name('ajaxuploadcard.actioncard');

Route::post('/savecategorys/actioncategorys', 'HrmsController@savecategorys')->name('savecategorys.actioncategorys');

Route::post('/actioncategorysdelete', 'HrmsController@actiondelete');

Route::get('/categories/{post}', 'HrmsController@editCategory');

Route::post('/actioncategorysupdate', 'HrmsController@actioncategorysupdate');

Route::post('/applicationRejected', 'HrmsController@applicationRejected');


Route::post('/savechapter/actionchapter', 'HrmsController@savechapter')->name('savechapter.actionchapter');
Route::get('/chapter/{post}', 'HrmsController@editChapter');
Route::post('/actionchaptersupdate', 'HrmsController@actionchaptersupdate');
Route::post('/actioncategorysdelete', 'HrmsController@actioncategorysdelete');


Route::post('/actionapplicationdelete', 'HrmsController@actionapplicationdelete');
Route::post('/application_status_change', 'HrmsController@application_status_change');

Route::post('/applicationApprove', 'HrmsController@application_approve');


Route::get('/getCountry', 'HrmsController@getCountry');
Route::any('/getState', 'HrmsController@getState');
Route::any('/getCity', 'HrmsController@getCity');

Route::post('/actionchapterdelete', 'HrmsController@actionchapterdelete');

Route::get('/application/{post}', 'HrmsController@applicationview');

Route::get('/members',   'PagesController@members');
Route::get('/member_view/{post}', 'PagesController@member_view');

Route::any('/updateBardChair',   'PagesController@updateChairMember');
Route::any('/deleteBoardMember',   'PagesController@deleteBoardMember');

Route::get('/chapter_roster',   'PagesController@chapter_roster');
Route::any('/chapter_roster_list/{post}',   'PagesController@chapter_roster_list');


Route::get('/board_chair',   'PagesController@board_chair');
Route::get('/editBoardChair/{post}', 'PagesController@editBoardChair');

Route::any('/BardChair',   'PagesController@BardChairAdd');

Route::get('/board_chair_members',   'PagesController@board_chair_members');

Route::any('/getDesignation',   'PagesController@getDesignation');
Route::any('/getMembers',   'PagesController@getMembers');
Route::any('/getMembersTerminate',   'PagesController@getMembersTerminate');


Route::any('/insertAppointment',   'PagesController@insertAppointment');

Route::any('/terminationForm',   'PagesController@terminationForm');

Route::get('/associateApplication/{type}', 'HrmsController@associateApplicatioNew');

Route::any('/ajaxRequestApplication', 'HrmsController@ajaxRequestApplication');

//Route::get('referral', 'ReferralController@index');

Route::any('getMembersbyid', 'PagesController@getMembersbyid');
Route::any('setting_notification', 'PagesController@setting_notification');
Route::any('associateslist', 'HrmsController@associateslist');
Route::get('/affiliacte_member_info/{id}', 'HrmsController@affiliacte_member_info');

Route::get('/affiliatesInfo', 'HrmsController@affiliatesInfo');
Route::get('/businessCatInfo', 'HrmsController@businessCatInfo');

Route::any('associateslist/{id}', 'HrmsController@associateslistByid');

Route::any('associateslistByCategory/{id}', 'HrmsController@associateslistByCategory');

});

Auth::routes();
Route::any('/', 'Frontend\FrontendController@index')->name('Frontend.index');

Route::any('/ajaxRequestApplicationMember', 'HrmsController@ajaxRequestApplication');

Route::any('/getMembersProfile',   'PagesController@getMembers');

Route::any('/checkEmail',   'PagesController@checkEmail');
Route::any('/memberResetPassword',   'PagesController@memberResetPassword');

Route::post('/ajax_upload/action_your_image', 'HrmsController@action_your_image')->name('ajaxupload.action_your_image');

Route::post('/ajax_upload/action_your_card', 'HrmsController@action_your_card')->name('ajaxupload.action_your_card');

Route::get('member_affiliates', 'HrmsController@member_affiliates');

Route::get('myAffiliate', 'PagesController@myAffiliate');

Route::any('associateslistMember', 'HrmsController@associateslistMember');
Route::any('businessCatInfoMembers', 'HrmsController@businessCatInfoMembers');

Route::get('/affiliacte_member_view_info/{id}', 'HrmsController@affiliacte_member_view_info');
Route::any('associateslistMember/{id}', 'HrmsController@associateslistMemberByid');
Route::any('associateslistMemberCategory/{id}', 'HrmsController@associateslistMemberCategory');
Route::get('/myAffiliate/{id}', 'PagesController@myAffiliatebyid');


    Route::get('/registration', 'HomenewController@register');

    Route::get('/getCountry', 'HrmsController@getCountry');
    Route::any('/getState', 'HrmsController@getState');
    Route::any('/getCity', 'HrmsController@getCity');

    Route::any('/saveregistrationform', 'Registration@saveinfo');
    Route::any('/thankyoupage', 'Registration@thankyoupage');

    Route::any('/memberlogin', 'Frontend\Member@memberLogin')->middleware('CheckLogin');

    Route::any('/member_forgot_pass', 'Frontend\Member@member_forgot_pass');
    
    Route::any('/member_login', 'Frontend\Member@memberLogin');

    Route::any('/users', 'Frontend\Member@users');

    Route::any('/logouts', 'Frontend\Member@logouts' );

    Route::any('/member_profile', 'Frontend\Member@memberMyProfile');

    Route::get('/changePass', 'Frontend\Member@changePass');
    Route::any('/memberChangePassword', 'Frontend\Member@memberChangePassword');

    
    // admin setting start here  
    
    Route::any('/MemberShipPmtType', 'Admin\SettingController@MemberShipPmtType' );
    Route::any('/ActivityPoints', 'Admin\SettingController@ActivityPoints' );
    Route::any('/RewardsPercentage', 'Admin\SettingController@RewardsPercentage' );
    Route::any('/MeetingType', 'Admin\SettingController@MeetingType' );
    Route::any('/MeetingStructure', 'Admin\SettingController@MeetingStructure' );

    Route::any('/memberChangePass', 'Frontend\Member@memberChangePass' );


// Guest setting start here  

  Route::any('invite_guest', 'Admin\GuestController@invite_guest' );
  Route::any('chapter_guest', 'Admin\GuestController@chapter_guest' );
  Route::any('my_invitees', 'Admin\GuestController@my_invitees' );
  

// Guest setting end here    


  // referral setting end here 

  Route::get('referral', 'ReferralController@index');
  Route::get('send_Referral', 'ReferralController@send_Referral');
  Route::get('send_escrow_Referral', 'ReferralController@send_escrow_Referral');
  Route::get('received_Referral', 'ReferralController@received_Referral');
  Route::get('my_send_Referral', 'ReferralController@my_send_Referral');

  Route::get('affiliate-referrals', 'ReferralController@affiliate_referrals')->name('affiliate-referrals');
  Route::get('affiliate-calendar', 'ReferralController@affiliate_calendar')->name('affiliate-calendar');

  // referral setting end here 

  // services setting end here 

  Route::any('services', 'ServicesController@index');
  Route::any('services/{id}', 'ServicesController@editServices');
  Route::any('actionDeleteService', 'ServicesController@actionDeleteService');

  
  Route::get('rewards', 'RewardsController@index');

  // services setting end here 
  
  // meetings setting start here

  Route::get('meetings', 'MeetingsController@index');

  // meetings setting end here 

  Route::get('get_associate_details_from_id', 'ReferralController@get_associate_details_from_id')->name('get_associate_details_from_id');
  Route::post('add_referral_from_referral_form', 'ReferralController@add_referral_from_referral_form')->name('add_referral_from_referral_form');
