<?php

namespace App\Listeners;

use App\Events\Registertion;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;


use App\Mail\RegistrationEmail;


class SendResigtrationEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Registertion  $event
     * @return void
     */
    public function handle(Registertion $event)
    {
        \Mail::to($event->email)->send(
            new RegistrationEmail($event->message, $event->email)
        );
    }
}
