<?php

namespace App\Listeners;

use App\Events\TeacherAdded;
use App\Mail\TeacherConfirmationEmail;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendTeacherConfirmationEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TeacherAdded  $event
     * @return void
     */
    public function handle(TeacherAdded $event)
    {
      //  dd($event->teacher);
        \Mail::to($event->student)->send(
            new TeacherConfirmationEmail($event->teacher, $event->student)
        );
    }
}
