<?php 
use Illuminate\Support\Facades\DB;
use App\ActivityPointsLogs;

if (!function_exists('getNameChairType')) {
 
    function getNameChairType($id)
    {
        try { 
            $get = DB::table('boardchairdesignationtbl')->select('*')->where('id',$id)->get()[0];

        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }
}


if (!function_exists('getsponsername')) {
 
    function getsponsername($id)
    {
        try {
            $get = DB::table('member_info_tbl')->select('*')->where('sponsershipMemberid',$id)->get();
          //  echo "<pre>";print_r($get);die();
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }
}

if (!function_exists('getsponsernamebyme')) {
 
    function getsponsernamebyme($id)
    {
        try { 
            $get = DB::table('member_info_tbl')->select('*')->where('uuid',$id)->get();

        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }
}

if (!function_exists('get_states_america')) {
 
    function get_states_america($id)
    {
        try { 
            $get = DB::table('states')->select('*')->where('country_id',$id)->get();

        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }
}


if (!function_exists('getmembers')) {
 
    function getmembers($id) 
    {
        try { 
            $get = DB::table('boardchairdesignationtbl')->select('*')->get();
     //  echo "<pre>";print_R($get);die;
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }
}


if (!function_exists('getuserInfo')) {
 
    function getuserInfo($id)
    {
        try { 
            $get = DB::table('associate_application_tbl')->select('*')->where('id',$id)->get()[0];
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }
}


if (!function_exists('get_members_name')) {
 
    function get_members_name($id)
    {
        try { 
         
            $get = DB::table('member_info_tbl')->select('*')->where('uuid',$id)->get();
            if(isset($get) && isset($get[0])){
                $return=  $get[0];
            }else{
                $return=  '';
            }
            
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $return;
    }
}

if (!function_exists('termination_designated_member')) {
 
    function termination_designated_member()
    {
        try { 
            $get = DB::table('chapterboardchairmembertbl')->select('*')->groupBy('MemberID')->where('MemberID','!=','')->get();
     // echo "<pre>";print_R($get);die;
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }
}

if (!function_exists('get_states')) {
 
    function get_states($id)
    {
      
        try { 
            $get = DB::table('states')->select('*')->where('id',$id)->first();
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }
}

if (!function_exists('country_fetch')) {
 
    function country_fetch() 
    {
        try { 
            $get = DB::table('countries')->select('*')->get();
      
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }
}
 

if (!function_exists('business_categories_fetch')) {
  
    function business_categories_fetch()
    {
        try { 
            $get = DB::table('business_categories_tbl')->select('*')->where(['isdelete'=>0,'isactive'=>1])->get();
           
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }
}


if (!function_exists('business_categories_fetch_byid')) {
 
    function business_categories_fetch_byid($id)
    {
        try { 
            $get = DB::table('business_categories_tbl')->select('*')->where('id',$id)->where('isdelete',0)->first();
      
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }
}

if (!function_exists('randomPassword')) {

    function randomPassword() {
        $string = "";
        $chars = "abcdefgh";
        $size = strlen($chars);
        for ($i = 0; $i < $size; $i++) {
            $string .= $chars[rand(0, $size - 1)];
        }
        return $string;
    }

}

if (!function_exists('chapter_tbl_fetch')) {
 
    function chapter_tbl_fetch()
    {
        try { 
            $get = DB::table('chapter_tbl')->select('*')->where(['isDelete'=>'NO','isActive'=>1,'status'=>1])->get();
      
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }
}

if (!function_exists('get_days')) {
    function get_days()
    {
        try { 
            $get = DB::table('operation_days_tbl')->select('*')->orderBy('day_display_order')->get();
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }
}

 
if (!function_exists('get_city')) {
    function get_city($id)
    {
        try { 
            $get = DB::table('states')->select('*')->where('id',$id)->first();
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }
}

if (!function_exists('country_name')) {
    function country_name($id)
    {
        try { 
            $get = DB::table('countries')->select('name')->where('id',$id)->first();
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }
}


if (!function_exists('formatNumber')) {
 
    function formatNumber($number, $currency = 'IDR')
    {
    if($currency == 'USD') {
            return number_format($number, 2, '.', ',');
    }
    return number_format($number, 0, '.', '.');
    }
}


if (!function_exists('getChapterName')) {

    function getChapterName($id) {
        try { 
            $get = DB::table('chapter_tbl')->select('chapter_name')->where('id',$id)->first();
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }
}




if (!function_exists('getBusinessName')) {
    function getBusinessName($id) {
        try { 
            $get = DB::table('business_categories_tbl')->select('bizcatname')->where('id',$id)->first();
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }   
}

if (!function_exists('registrationCount')) {

    function registrationCount() {
        try { 
            $get = DB::select("select
            count(*) as total_count,
            count(case when gender='Male' then 1 end) as male_cnt,
            count(case when gender='Female' then 1 end) as female_cnt,
            count(case when gender='Do Not Disclose' then 1 end) as other_cnt
            from associate_application_tbl")[0];
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }

}

if (!function_exists('getAcitveStatus')) {

    function getAcitveStatus($id) {
        try { 
            $get = DB::table('associate_application_tbl')->select('isactive')->where('id',$id)->first();
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }
}

if (!function_exists('getApplicationViewList')) { // after approve 
    function getApplicationViewList($id) {
        try {
            $get = DB::table('associate_application_tbl')
                ->select('associate_application_tbl.*','member_info_tbl.password','associate_application_tbl.id')
                ->leftJoin('member_info_tbl', 'member_info_tbl.uuid', '=', 'associate_application_tbl.id')
                ->where('associate_application_tbl.id',$id)->first();
            // dd($get);
        } catch(\Illuminate\Database\QueryException $ex){
            dd($ex->getMessage()); 
        }
        return $get;
    }
}

if (!function_exists('getApplicationViewListnew')) { // after approve 
    function getApplicationViewListnew($id) {
        try {
        
            $get = DB::table('associate_application_tbl')
                ->select('associate_application_tbl.*','member_info_tbl.password','member_info_tbl.uuid')
                ->leftJoin('member_info_tbl', 'member_info_tbl.uuid', '=', 'associate_application_tbl.id')
                ->where('member_info_tbl.uuid',$id)->first();
            // dd($get);
        } catch(\Illuminate\Database\QueryException $ex){
            dd($ex->getMessage()); 
        }
        return $get;
    }
}

if (!function_exists('getApplicationMembers')) {
    function getApplicationMembers() {
        try {
            $get = DB::table('associate_application_tbl')
                ->select('associate_application_tbl.*','member_info_tbl.password','member_info_tbl.uuid')
                ->leftJoin('member_info_tbl', 'member_info_tbl.uuid', '=', 'associate_application_tbl.id')
                ->where('member_info_tbl.status',2)
                ->where('member_info_tbl.isdelete',0)->get();
          
        } catch(\Illuminate\Database\QueryException $ex){
            dd($ex->getMessage()); 
        }
        return $get;
    }
}


if (!function_exists('checkEmailHelper')) {
    function checkEmailHelper($email) {
        try {
            $get = DB::table('associate_application_tbl')->where('your_email',$email)->first();
        } catch(\Illuminate\Database\QueryException $ex){
            dd($ex->getMessage()); 
        }
        return $get;
    }
}

if (!function_exists('getResetPassword')) {
    function getResetPassword($email) {
        try {
            $get = DB::table('member_info_tbl')->where('your_email',$email)->first();
        } catch(\Illuminate\Database\QueryException $ex){
            dd($ex->getMessage()); 
        }
        return $get;
    }
}

if (!function_exists('get_member_count')) {
    function get_member_count($id) {
        try {
            $get = DB::table('member_info_tbl')->select('*')->where('affiliate_for',$id)->where('status',2)->get();
        } catch(\Illuminate\Database\QueryException $ex){
            dd($ex->getMessage()); 
        }
        return $get->count();
    }
}


if (!function_exists('get_member_category_count')) {

    function get_member_category_count($id) {
        try { 
            $get = DB::table('member_info_tbl')->select('*')->where('business_category',$id)->where('status',2)->get();
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get->count();
    }
}

if (!function_exists('getApplicationMembersByid')) {

    function getApplicationMembersByid($id) {
        try { 
            $get = DB::table('associate_application_tbl')
            ->select('associate_application_tbl.*','member_info_tbl.password')
            ->leftJoin('member_info_tbl', 'member_info_tbl.uuid', '=', 'associate_application_tbl.id')
            ->where('member_info_tbl.status',2)
            ->where('member_info_tbl.isdelete',0)
            ->where('member_info_tbl.affiliate_for',$id)->get();
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }
}
if (!function_exists('associateslistByCategory')) {

    function associateslistByCategory($id) {
        try { 
            $get = DB::table('associate_application_tbl')
            ->select('associate_application_tbl.*','member_info_tbl.password')
            ->leftJoin('member_info_tbl', 'member_info_tbl.uuid', '=', 'associate_application_tbl.id')
            ->where('member_info_tbl.status',2)
            ->where('member_info_tbl.isdelete',0)
            ->where('member_info_tbl.business_category',$id)->get();
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }
}

if (!function_exists('activity_point_updates')) 
{
    function activity_point_updates($activity_type,$activity_by,$activity_for,$activity_nature,$remarks,$created_by,$referral_id) 
    {
        try 
        { 
            $activity_points = DB::table('ActivityPointsTbl')->select('ActivityPoints')->where('ActivityShortCode',$activity_type)->first();
            if($activity_points)
            {
                $points = new ActivityPointsLogs();
                $points->activity_points = $activity_points->ActivityPoints;
                $points->activity_type   = $activity_type;
                $points->activityby      = $activity_by;
                $points->activityto      = $activity_for;
                $points->activity_nature = $activity_nature;
                $points->remarks         = $remarks;
                $points->created_by      = $created_by;
                $points->referral_id     = $referral_id;
                $result = $points->save();
                if($result)
                {
                    $message = "Saved Successfully";
                }
                else
                {
                    $message = "Error Saving Data";
                }
            }

        } 
        catch(\Illuminate\Database\QueryException $ex)
        { 
            dd($ex->getMessage()); 
        }
        return $message;
    }
}

?>

