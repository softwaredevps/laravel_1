<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Settings extends Model
{
    
    static function getMemberShipPmtType(){
        try { 
            
            $get =  DB::table('MemberShipPmtTypeTbl')->get();
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }

    static function getActivityPoints(){
        try { 
            
            $get =  DB::table('ActivityPointsTbl')->select('*')->where('IsActive','Yes')->get();
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }

	static function getRewardsPercentage(){
        try { 
            
            $get =  DB::table('RewardsPercentageTbl')->select('*')->where('IsActive','Yes')->get();
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }

    static function getMeetingType(){
        try { 
            
            $get =  DB::table('MeetingTypeTbl')->select('*')->where('IsActive','Yes')->get();
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }

    static function getMeetingStructure(){
        try { 
            
            $get =  DB::table('MeetingStructureTbl')->select('*')->where('IsActive','Yes')->get();
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }




}
 