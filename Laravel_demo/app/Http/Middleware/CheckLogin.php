<?php

namespace App\Http\Middleware;

use Closure;
use Session;
class CheckLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $value = Session::get('frontend_name');

        if(isset($value) && !empty($value)){    
            return redirect('/users');
           // return route('memberlogin');
            //return $next($request);
        }
        else{ 
            return $next($request);
            //return redirect('/member_login');
            //return $next($request);
        }
       
    }
}
