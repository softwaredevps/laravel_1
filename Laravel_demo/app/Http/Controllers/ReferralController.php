<?php

namespace App\Http\Controllers;

use App\User;
use App\MemberReferral;
use Illuminate\Http\Request;
use App\Usersprofile;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Events\Registertion;
use Illuminate\Support\Facades\Crypt;
use Session;
use DB;

class ReferralController extends BaseController
{ 
    public function index()
    {
      $id = User::all()[0];
      $all_associates = getApplicationMembers();
      if(Auth::user() && Auth::user()->id == 1)
      { 
        $logged_in_user_id = Auth::user()->id;
      }
      else
      {
        $data = Session::all();
        $logged_in_user_id = $data['UUID'];
        $res = Usersprofile::getChapterList($data['UUID']);
      }
      /**************************** Sent Referral Data *******************************/
      $sent_referral_data = MemberReferral::where('referral_from',$logged_in_user_id)->get();
      if($sent_referral_data)
      {
        $sent_referral_details_array = array();
        foreach($sent_referral_data as $sent_item)
        {
          $referred_to_details = DB::table('member_info_tbl')->select('member_info_tbl.first_name','member_info_tbl.last_name','business_categories_tbl.bizcatname','member_info_tbl.your_image','chapter_tbl.chapter_name')->where('member_info_tbl.uuid',$sent_item->referral_to)->join('business_categories_tbl','member_info_tbl.business_category','=','business_categories_tbl.id')->join('chapter_tbl','member_info_tbl.affiliate_for','=','chapter_tbl.id')->first();
          $resultant_array = array(
            "referral_to_name" => $referred_to_details->first_name." ".$referred_to_details->last_name,
            "referral_to_business_cat" => $referred_to_details->bizcatname,
            "referral_to_affiliate" => $referred_to_details->chapter_name,
            "referral_to_your_image" => $referred_to_details->your_image,
            "contact_person" => $sent_item->contact_person,
            "company_name" => $sent_item->company_name,
            "phone" => $sent_item->phone,
            "email" => $sent_item->email,
            "referral_details" => $sent_item->referral_details,
            "status" => $sent_item->status,
          );
          array_push($sent_referral_details_array, $resultant_array);
        }
      }     
      /**************************** Received Referral Data *******************************/
      $received_referral_data = MemberReferral::where('referral_to',$logged_in_user_id)->get();
      if($received_referral_data)
      {
        $received_referral_details_array = array();
        foreach($received_referral_data as $received_item)
        {
          $referred_to_details = DB::table('member_info_tbl')->select('member_info_tbl.first_name','member_info_tbl.last_name','business_categories_tbl.bizcatname','member_info_tbl.your_image','chapter_tbl.chapter_name')->where('member_info_tbl.uuid',$received_item->referral_to)->join('business_categories_tbl','member_info_tbl.business_category','=','business_categories_tbl.id')->join('chapter_tbl','member_info_tbl.affiliate_for','=','chapter_tbl.id')->first();
          $received_resultant_array = array(
            "referral_to_name" => $referred_to_details->first_name." ".$referred_to_details->last_name,
            "referral_to_business_cat" => $referred_to_details->bizcatname,
            "referral_to_affiliate" => $referred_to_details->chapter_name,
            "referral_to_your_image" => $referred_to_details->your_image,
            "contact_person" => $received_item->contact_person,
            "company_name" => $received_item->company_name,
            "phone" => $received_item->phone,
            "email" => $received_item->email,
            "referral_details" => $received_item->referral_details,
            "status" => $received_item->status,
          );
          array_push($received_referral_details_array, $received_resultant_array);
        }
      } 
      return view('referral.index',compact('all_associates','logged_in_user_id','sent_referral_details_array','received_referral_details_array'));       
    }
    
     public function send_Referral(){
      return view('referral.send_Referral');
       
    }

    public function send_escrow_Referral()
    {
      return view('referral.send_escrow_Referral');       
    }

    public function received_Referral(){
      return view('referral.received_Referral');
       
    }

    public function my_send_Referral(){
      return view('referral.my_send_Referral');
       
    }

    public function affiliate_referrals()
    {
        return view('referral.affiliate_referrals');
    }

    public function affiliate_calendar()
    {
        return view('referral.affiliate_calendar');
    }    

    public function add_referral_from_referral_form(Request $request)
    {
      $referral_to = $request->post('referral_to');
      $contact_person = $request->post('contact_person');
      $company_name = $request->post('company_name');
      $phone = $request->post('phone');
      $email = $request->post('email');
      $referral_details = $request->post('referral_details');
      $status = $request->post('status');
      $created_by = $request->post('created_by');
      $referral_from = $request->post('referral_from');
      $referral_type = $request->post('referral_type');

      $memberreferral = new MemberReferral();

      $memberreferral->referral_to = (!empty($referral_to)) ? $referral_to : "";
      $memberreferral->contact_person = (!empty($contact_person)) ? $contact_person : "";
      $memberreferral->company_name = (!empty($company_name)) ? $company_name : "";
      $memberreferral->phone = (!empty($phone)) ? $phone : "";
      $memberreferral->email = (!empty($email)) ? $email : "";
      $memberreferral->referral_details = (!empty($referral_details)) ? $referral_details : "";
      $memberreferral->status = (!empty($status)) ? $status : "New";
      $memberreferral->created_by = (!empty($created_by)) ? $created_by : "";
      $memberreferral->referral_from = (!empty($referral_from)) ? $referral_from : "";
      $memberreferral->referral_type = (!empty($referral_type)) ? $referral_type : "member";

      $result = $memberreferral->save();
      if($result)
      {
          $save_activity_points = activity_point_updates("PR",$referral_from,$referral_to,$referral_type,"Referral Passed Successfully",$created_by,$memberreferral->id);
          $response = array(
             "message" => "success"
          );
      }
      else
      {
          $response = array(
             "message" => "error"
          );        
      }
      return json_encode($response);
    }

    public function get_associate_details_from_id(Request $request)
    {
      $associate_id = $request->get('associate_id');
      $associate_data = DB::table('member_info_tbl')->select('business_categories_tbl.bizcatname','member_info_tbl.first_name','member_info_tbl.last_name','member_info_tbl.payment_city','chapter_tbl.chapter_name','member_info_tbl.uuid','member_info_tbl.your_image')->where('member_info_tbl.uuid',$associate_id)->join('business_categories_tbl','member_info_tbl.business_category','=','business_categories_tbl.id')->join('chapter_tbl','member_info_tbl.affiliate_for','=','chapter_tbl.id')->first();
      if($associate_data)
      {
        $response = array(
            "message"=> "success",
            "bizcatname"=> $associate_data->bizcatname,
            "name"=> $associate_data->first_name." ".$associate_data->last_name,
            "payment_city"=> $associate_data->payment_city,
            "chapter_name"=> $associate_data->chapter_name,
            "uuid"=> $associate_data->uuid,
            "image"=> $associate_data->your_image,
        );
      }
      else
      {
        $response = array(
            "message"=> "error"
        );
      }
      return json_encode($response);
    }
}
