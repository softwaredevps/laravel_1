<?php

namespace App\Http\Controllers\Frontend;


use App\Memberprofile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Hash;
use Redirect;
use Session;
use Validator;
use Illuminate\Support\Facades\Input;
use App\Frontedndlogin as Auth;
use Illuminate\Support\Facades\Crypt;

class Member extends Controller 
{
    public function memberLogin(Request $request){  
       
        if(isset($_POST) && !empty($_POST)){ 
            $rules = array (                
                'useremail' => 'required',
                'password' => 'required' 
            );
            $validator = Validator::make ( Input::all (), $rules );
            if ($validator->fails ()) {
                return Redirect::back ()->withErrors ( $validator, 'login' )->withInput();
            } else {
             
                if (Auth::attempt ( array (
                        
                        'name' => $request->get ( 'useremail' ),
                        'password' => $request->get ( 'password' ) 
                ) )) {
               
                    // session ( [ 

                    //         'frontend_name' => $request->get ( 'useremail' ) ,
                    //         'UUID' => $request->get ( 'password' )
                    // ] );
                    return redirect('/users');
                } else {
                    Session::flash ( 'message', "Invalid Credentials , Please try again." );
                    return Redirect::back ();
                }
            }
        } else{
            return view('frontend.memberlogin');
        }
    }
 
    public function logouts(Request $request){
        //Session::flush();
        Session::forget('frontend_name');
        Session::forget('UUID');
       // Auth::logout ();
       return redirect('/memberlogin');
        //return Redirect::back ('/');
    }

    public function users(Request $request){
 
        $data = Session::all();

        $res = Memberprofile::getMember($data['frontend_name'] ,$data['UUID'] );

        return view('frontend.homepage', compact('res'));

        //return view('frontend.homepage');
    }

    public function memberChangePass(){
        return view('frontend.memberChangePass');
    }

    public function memberMyProfile(){
        $data = Session::all();
        $res = Memberprofile::getMember($data['frontend_name'] ,$data['UUID'] );
       
        return view('frontend.memberMyProfile',compact('res'));
    }

    public function changePass(){
        return view('frontend.changePass');
    }

    public function memberChangePassword(Request $request){
        $data = Session::all();
    
        $res = Memberprofile::memberChangePassword($data['frontend_name'] ,$_REQUEST );

        if($res){
            return response()->json(['status'=>'ok','data'=>$res], 200);
         }else{
            return response()->json(['status'=>'error','data'=>'data_example'], 300);
        }
    }

    public function member_forgot_pass(){
        return view('frontend.memberForgotPass');
    }

}
