<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Routing\Controller as BaseController;
use App\Usersprofile;
use Illuminate\Http\Request;
use Validator;
use App\Events\Registertion;
use Illuminate\Support\Facades\Crypt;
use Session; 

class PagesController extends BaseController
{
    function search(){
        return view('pages.search');
    }
    function calendar(){
        return view('pages.calendar');
    }
    function contact(){
        return view('pages.contact');
    }
    function filemanager(){
        return view('pages.filemanager');
    }
	function members(){
		 return view('layout.member');
    }
    function setting_notification(){
        return view('layout.setting_notification');
    }
    function board_chair(){
        $res = Usersprofile::getBoradChair();
        return view('layout.board_chair',compact('res'));
    }

    function member_view($id){
        return view('layout.member_view');
    }

    function chapter_roster(){
        return view('layout.chapter_roster');
    }

    function checkEmail(){
      $res =  checkEmailHelper($_REQUEST['data']);
       if($res){
            return response()->json(['status'=>'ok','data'=>$res], 200);
        }else{
            return response()->json(['status'=>'error','data'=>'data_example'], 300);
        }
    }

    function memberResetPassword(){
        $res =  ($_REQUEST['data'])?$_REQUEST['data']:'';
        $data = getResetPassword($res);
        if($data){
           
            $password = Crypt::decrypt($data->password);
            $email_array = array(
                'first_name'=>$data->first_name,
                'last_name'=>$data->first_name,
                'uuid'=>$password,
                'your_email'=>$data->your_email,
                'resetpass'=>true
            );

            event(new Registertion($email_array,$data->your_email)); 

            return response()->json(['status'=>'ok','data'=>$res], 200);
        
        }else{
            return response()->json(['status'=>'error','data'=>'email not exists'], 320);
        }
    }

    function chapter_roster_list($id){
        $res = Usersprofile::getChapterRosterData($id);
        return view('layout.chapter_roster',compact('res'));
    }

    function BardChairAdd(Request $request){
        $id     = User::all()[0];
        $ChairType  = isset($_POST['ChairType'])?$_POST['ChairType']:'';
        $DesignationName   = isset($_POST['DesignationName'])?$_POST['DesignationName']:'';
        $Hierarchy   = isset($_POST['Hierarchy'])?$_POST['Hierarchy']:'';
        $isActive   = isset($_POST['isActive'])?$_POST['isActive']:'';
      
        $inset_array = [
            'ChairType'=>$ChairType,
            'DesignationName'=>trim($DesignationName),
            'Hierarchy'=>$Hierarchy,
            'isActive'=>$isActive
        ];        
        $res = Usersprofile::insertChairMember($id['id'],$inset_array);

        if($res){
            return response()->json(['status'=>'ok','data'=>$res], 200);
         }else{
            return response()->json(['status'=>'error','data'=>'data_example'], 300);
        }
    }

    function editBoardChair($id){
        $res = Usersprofile::editBoardChair($id);
        return view('layout.editBoardChair',compact('res'));

    }

    function updateChairMember (Request $request){
        $ChairType  = isset($_POST['ChairType'])?$_POST['ChairType']:'';
        $DesignationName   = isset($_POST['DesignationName'])?$_POST['DesignationName']:'';
        $Hierarchy   = isset($_POST['Hierarchy'])?$_POST['Hierarchy']:'';
        $isActive   = isset($_POST['isActive'])?$_POST['isActive']:'';

        $hiddenUpdateId   = isset($_POST['hiddenUpdateId'])?$_POST['hiddenUpdateId']:'';

        
        $udpate_data = [
            'ChairType'=>$ChairType,
            'DesignationName'=>trim($DesignationName),
            'Hierarchy'=>$Hierarchy,
            'isActive'=>$isActive
        ]; 
        
        $res = Usersprofile::updateChairMember($hiddenUpdateId,$udpate_data);

        if($res){
            return response()->json(['status'=>'ok','data'=>$res], 200);
         }else{
            return response()->json(['status'=>'error','data'=>'data_example'], 300);
        }
    }

    function deleteBoardMember(Request $request){
        $udpate_data = [
            'isDelete'=>2,
        ]; 
        $res = Usersprofile::updateChairMember($_REQUEST['data'],$udpate_data);

        if($res){
            return response()->json(['status'=>'ok','data'=>$res], 200);
         }else{
            return response()->json(['status'=>'error','data'=>'data_example'], 300);
        }
    }

    function board_chair_members(){
        $res = Usersprofile::chapterBoardChairMember();
        return view('layout.chapterBoardChairMember',compact('res'));  
    }

    function getDesignation(Request $request){
        $res = Usersprofile::getDesignation($_REQUEST['data']);
        return $res;
    }

    function getMembers(Request $request){
        $user_id = isset($_REQUEST['user_id'])?$_REQUEST['user_id']:'';
        $res = Usersprofile::getMembers($_REQUEST['data'],$user_id);
        return $res;
    }

    function getMembersbyid(Request $request){
        $res = Usersprofile::getMembersbyid($_REQUEST['data']);
        return $res;
    } 

    function getMembersTerminate(Request $request){
        $user_id = isset($_REQUEST['user_id'])?$_REQUEST['user_id']:'';
        $res = Usersprofile::getMembersTerminate($_REQUEST['data'],$user_id);
        return $res;
    }

    function insertAppointment(Request $request){
        $validation = Validator::make($request->all(), [
        'AppointmentDt' => 'required',
        'ChapterID' => 'required',
        'DesignationID' => 'required',
        // 'AppointmentRemarks' => 'required',
       // 'member' => 'required'
        ]);
        if($validation->passes())
        {

            $id     = User::all()[0];
            $AppointmentDt  = isset($_POST['AppointmentDt'])?$_POST['AppointmentDt']:'';
            $ChapterID   = isset($_POST['ChapterID'])?$_POST['ChapterID']:'';
            $member   = isset($_POST['member'])?$_POST['member']:'';
            $DesignationID   = isset($_POST['DesignationID'])?$_POST['DesignationID']:'';
            $ChairType   = isset($_POST['ChairType'])?$_POST['ChairType']:'';
            $AppointmentRemarks   = isset($_POST['AppointmentRemarks'])?$_POST['AppointmentRemarks']:'';
            
            $inset_array = [
                'AppointmentDt'=>$AppointmentDt,
                'ChapterID'=>trim($ChapterID),           
                'DesignationID'=>$DesignationID,
                'AppointmentRemarks'=>$AppointmentRemarks,
                'created_by'=>$id['id'],
                'MemberID'=>$member,
                'ChairType'=>$ChairType
            ];       
            $check_appointment = Usersprofile::checkAppointment($inset_array);

            if(!$check_appointment){
                $res = Usersprofile::insertAppointment($inset_array);

                if($res){
                    return response()->json(['status'=>'ok','data'=>$res], 200);
                }else{
                    return response()->json(['status'=>'error','data'=>'data_example'], 300);
                }
            }else{
                return response()->json(['status'=>'error','data'=>'already_assigned'], 350);
            }
           
        }else{
            return response()->json(['status'=>'error','data'=>'data_example'], 320);
        }
    }

    public function terminationForm(Request $request){
        $validation = Validator::make($request->all(), [
            'Chapter_termination' => 'required',
            'TerminationDt' => 'required',
            'Chapter_members' => 'required',
            'TerminationReason' => 'required',
          //  'terminationRemarks' => 'required'
            ]);
            if($validation->passes())
            {
    
                $id     = User::all()[0];
                $Chapter_termination  = isset($_POST['Chapter_termination'])?$_POST['Chapter_termination']:'';
                $TerminationDt   = isset($_POST['TerminationDt'])?$_POST['TerminationDt']:'';
                $Chapter_members   = isset($_POST['Chapter_members'])?$_POST['Chapter_members']:'';
                $TerminationReason   = isset($_POST['TerminationReason'])?$_POST['TerminationReason']:'';
                $terminationRemarks   = isset($_POST['terminationRemarks'])?$_POST['terminationRemarks']:'';
            
                $update_array = [
                    'ChapterID'=>$Chapter_termination,
                    'TerminationDt'=>trim($TerminationDt),           
                    'MemberID'=>$Chapter_members,
                    'TerminationReason'=>$TerminationReason,
                    'TerminationRemarks'=>$terminationRemarks,
                    'updated_by'=>$id['id'],
                    'isTerminated'=>1,
                ]; 

                $res = Usersprofile::updateTermination($Chapter_members,$update_array);
        
                if($res){
                    return response()->json(['status'=>'ok','data'=>$res], 200);
                }else{
                    return response()->json(['status'=>'error','data'=>'data_example'], 300);
                }
            }else{
                return response()->json(['status'=>'validation error','data'=>'data_example'], 320);
            }
    }

    public function myAffiliate(){
        $res = Usersprofile::getChapterRosterDatabyID();
        return view('layout.myAffiliate',compact('res'));
    }

    public function myAffiliatebyid($id){ 
        $res = Usersprofile::getChapterRosterDataMemberId($id);
        return view('layout.myAffiliateMember',compact('res'));
    }
}