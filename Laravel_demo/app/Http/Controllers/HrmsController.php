<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Usersprofile;
use Illuminate\Routing\Controller as BaseController;
use Validator;
use App\Events\Registertion;
use Illuminate\Support\Facades\Crypt;
use Session;

class HrmsController extends BaseController
{ 
    function index(){
        $id = User::all()[0];
        $res = Usersprofile::getinfo($id['id']);
        return view('hrms.index', compact('res'));
    }
    function users(){
        $users = User::all();
        return view('hrms.users');
    }
    function departments(){
        return view('hrms.departments');
    }
    function employee(){
        $id = User::all()[0];
        $res = Usersprofile::getinfo($id['id']);
        return view('hrms.employee',compact('res'));
    }

    public function associateslist(){
        $id = User::all()[0];
        $res = getApplicationMembers();
        return view('frontend.associateslist',compact('res'));
    }

    public function associateslistByCategory($id){
        $res = associateslistByCategory($id);
        return view('frontend.associateslist',compact('res'));
    } 

    public function associateslistByid($id){ 
        $res = getApplicationMembersByid($id);
        return view('frontend.associateslist',compact('res'));
    }

    public function associateslistMemberByid($id){
        $res = getApplicationMembersByid($id);
        return view('frontend.associateslistMember',compact('res'));
    }

    
    public function associateslistMember(){
        $id = User::all()[0];
        $data = Session::all();
        $res = getApplicationMembers();
        return view('frontend.associateslistMember',compact('res'));
    }

    public function businessCatInfoMembers(){
          $id = User::all()[0];
          $data = Session::all();
          $res = Usersprofile::businessCatInfo($data['UUID']);
         
          return view('frontend.businessCatInfoMembers',compact('res'));
    }

    public function associateslistMemberCategory($id){
       // $res = associateslistMemberCategory($id);
        $res = associateslistByCategory($id);
        return view('frontend.associateslistMember',compact('res'));
    }

    public function affiliacte_member_info($id){
        $res = getApplicationViewListnew($id);
        return view('hrms.affiliacte_member_info',compact('res'));
    }

    public function affiliacte_member_view_info($id){
        $res = getApplicationViewListnew($id);
        return view('hrms.affiliacte_member_view_info',compact('res'));
    }

    public function affiliatesInfo(){
        $id = User::all()[0];
        $res = Usersprofile::getChapterListAffiliatesInfo($id['id']);
        return view('hrms.affiliatesInfo',compact('res'));
    }

    function activities(){
        return view('hrms.activities');
    }
    function holidays(){
        return view('hrms.holidays');
    }
    function events(){
        return view('hrms.events');
    }
    function payroll(){
        return view('hrms.payroll');
    }
    function accounts(){
        return view('hrms.accounts');
    }
    
    function report(){
        return view('hrms.report');
    }

   
    function ajaxRequest(Request $request){
        $id     = User::all()[0];
        $type  = isset($_POST['type'])?$_POST['type']:'';
        $name   = isset($_POST['data'])?$_POST['data']:'';
        $new_va = explode(" ",$name);
    
        if($type == 'user_name'){
            $update_array = [
                'first_name'=>(isset($new_va[0]))?$new_va[0]:'',
                'last_name'=>(isset($new_va[1]))?$new_va[1]:''
            ];        
            $res = Usersprofile::updateInfo($id['id'],$update_array,$type);
        }else{
            $update_array = [
                $type=>$name,
            ];        
            $res = Usersprofile::updateInfoNew($id['id'],$update_array);
        }
        if($res){
            return response()->json(['status'=>'ok','data'=>$res], 200);
         }else{
            return response()->json(['status'=>'error','data'=>'data_example'], 300);
        }
    }

    function ajaxRequestBusiness(){
        $id     = User::all()[0];
        $type   = isset($_POST['type'])?$_POST['type']:'';
        $name   = isset($_POST['data'])?$_POST['data']:'';

        $update_array = [
            $type=>$name,
        ];  
        $res = Usersprofile::updateInfoBusiness($id['id'],$update_array);
        if($res){
            return response()->json(['status'=>'ok','data'=>$res], 200);
         }else{
            return response()->json(['status'=>'error','data'=>'data_example'], 300);
        }
    }

    function ajaxRequestNetwork(){
        $id     = User::all()[0];
        $type   = isset($_POST['type'])?$_POST['type']:'';
        $name   = isset($_POST['data'])?$_POST['data']:'';

        $update_array = [
            $type=>$name,
        ];  
        $res = Usersprofile::updateInfoNetwork($id['id'],$update_array);
        if($res){
            return response()->json(['status'=>'ok','data'=>$res], 200);
         }else{
            return response()->json(['status'=>'error','data'=>'data_example'], 300);
        }
    }

    function indexn()
    {
     return view('ajax_upload');
    }

    function action(Request $request)
    {
     $validation = Validator::make($request->all(), [
      'select_file' => 'required|image|mimes:jpeg,png,jpg,gif|max:10000'
     ]);
     if($validation->passes())
     {
      $image = $request->file('select_file');
      $new_name = rand() . '.' . $image->getClientOriginalExtension();
      $image->move(public_path('images'), $new_name);
        $id  = User::all()[0];
        $update_array = [
            'img_url'=>$new_name,
        ];  
        $res = Usersprofile::updateInfoNew($id['id'],$update_array);
       
        if($res){
          return response()->json([
            'message'   => 'Image Upload Successfully',
            'uploaded_image' => '<img src="/images/'.$new_name.'" class="img-thumbnail" width="300" />',
            'class_name'  => 'alert-success'
            ]);
        }else{
            return response()->json(['status'=>'error','data'=>'data_example'], 300);
        }
     }
     else
     {
      return response()->json([
       'message'   => $validation->errors()->all(),
       'uploaded_image' => '',
       'class_name'  => 'alert-danger'
      ]);
     }
    }

    function indexbusiness(){
     return view('ajax_upload');
    }

    function actionbusiness(Request $request)
    {
     $validation = Validator::make($request->all(), [
      'select_file' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048'
     ]);
     if($validation->passes())
     {
      $image = $request->file('select_file');
      $new_name = rand() . '.' . $image->getClientOriginalExtension();
      $image->move(public_path('images'), $new_name);
        $id  = User::all()[0];
        $update_array = [
            'business_img'=>$new_name,
        ];  
        $res = Usersprofile::updateInfoBusiness($id['id'],$update_array);
       
        if($res){
          return response()->json([
            'message'   => 'Image Upload Successfully',
            'uploaded_image' => '<img src="/images/'.$new_name.'" class="img-thumbnail" width="300" />',
            'class_name'  => 'alert-success'
            ]);
        }else{
            return response()->json(['status'=>'error','data'=>'data_example'], 300);
        }
     }
     else
     {
      return response()->json([
       'message'   => $validation->errors()->all(),
       'uploaded_image' => '',
       'class_name'  => 'alert-danger'
      ]);
     } 
    }

    public function ajaxuploadcard(Request $request){
        $validation = Validator::make($request->all(), [
            'select_file' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048'
           ]);
           if($validation->passes())
           {
            $image = $request->file('select_file');
            $new_name = rand() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images'), $new_name);
              $id  = User::all()[0];
              $update_array = [
                  'business_img_card'=>$new_name,
              ];  
    
              $res = Usersprofile::updateInfoBusiness($id['id'],$update_array);
             
              if($res){
                return response()->json([
                  'message'   => 'Image Upload Successfully',
                  'uploaded_image' => '<img src="/images/'.$new_name.'" class="img-thumbnail" width="300" />',
                  'class_name'  => 'alert-success'
                  ]);
              }else{
                  return response()->json(['status'=>'error','data'=>'data_example'], 300);
              }
           }
           else
           {
            return response()->json([
             'message'   => $validation->errors()->all(),
             'uploaded_image' => '',
             'class_name'  => 'alert-danger'
            ]);
           }
    }

   function ajaxRequestModalEmail(){
        $id     = User::all()[0];
        $type   = isset($_POST['type'])?$_POST['type']:'';
        $email   = isset($_POST['data'])?$_POST['data']:'';

        if($type != '' || $email != ''){
            $update_array = array(
                'email'=>$email,
                'emailtype'=>$type            
            );  
            $res = Usersprofile::ajaxRequestModalEmail($id,$update_array);
            if($res){
                return response()->json(['status'=>'ok','data'=>$res], 200);
            }else{
                return response()->json(['status'=>'error','data'=>'data_example'], 300);
            }
        }else{
            return response()->json(['status'=>'error','data'=>'Please check Required Fields.'], 300);
        }
       
   }

   function ajaxRequestModalMobile(){
        $id     = User::all()[0];
        $type   = isset($_POST['type'])?$_POST['type']:'';
        $mobile   = isset($_POST['data'])?$_POST['data']:'';
        if($type != '' || $mobile != ''){
            $update_array = array(
                'mobile'=>$mobile,
                'mobiletype'=>$type            
            );  
            $res = Usersprofile::ajaxRequestModalMobile($id,$update_array);
            if($res){
                return response()->json(['status'=>'ok','data'=>$res], 200);
            }else{
                return response()->json(['status'=>'error','data'=>'data_example'], 300);
            }
        }else{
            return response()->json(['status'=>'error','data'=>'Please check Required Fields.'], 300);
        }
   }

   public function categories(){
        $id = User::all()[0];
        $res = Usersprofile::getCategoriesInfo($id['id']);
        return view('hrms.categories',compact('res'));
   }

   public function savecategorys(){
        $id = User::all()[0];
        if($_REQUEST['bizcatname'] != '' && $_REQUEST['bizcatdescription'] != ''){
            $save_array = array(
                'bizcatname'=>trim($_REQUEST['bizcatname']),           
                'bizcatdescription'=>trim($_REQUEST['bizcatdescription']), 
                'isActive'=>trim($_REQUEST['isActive']), 
                'isdelete'=>0,
                'created_at'=>date('Y-m-d H:i:s'),
            );  
  
            $res = Usersprofile::addCategoriesInfo($id['id'],$save_array);
            if($res)
                return response()->json(['status'=>'ok','data'=>$res], 200);
        }else{
            return response()->json(['status'=>'error','data'=>'Please check Required Fields.'], 300);
        }
   }

   public function categorieslist(){
        $id = User::all()[0];
        $res = Usersprofile::getCategoriesInfo($id['id']);
        return view('hrms.categories_list',compact('res'));
   }

    public function businessCatInfo(){
        $id = User::all()[0];
        $res = Usersprofile::businessCatInfo($id['id']);
        return view('hrms.businessCatInfo',compact('res'));
    }

   public function actiondelete(){
        if($_REQUEST['data'] != ''){
            $delete = array(
                'status'=>0,
            );  
            $res = Usersprofile::deleteCategories($_REQUEST['data'],$delete);
            if($res)
                return response()->json(['status'=>'ok','data'=>'Deleted Successfully.'], 200);
        }else{
            return response()->json(['status'=>'error','data'=>'Please check Required Fields.'], 300);
        }
   }

   public function editCategory($id){
        //$id = User::all()[0];
        $res = Usersprofile::editCategoriesInfo($id);
        return view('hrms.categories',compact('res'));
   }

   public function actioncategorysupdate(){
        if($_REQUEST['update_id'] != '' && trim($_REQUEST['bizcatname']) && trim($_REQUEST['bizcatdescription'])){
            $update = array(
                'bizcatname'=>trim($_REQUEST['bizcatname']),
                'bizcatdescription'=>trim($_REQUEST['bizcatdescription']), 
                'isactive'=>$_REQUEST['isActive'], 
                'updated_at'=>date('Y-m-d H:i:s'),
            );  
            $res = Usersprofile::deleteCategories($_REQUEST['update_id'],$update);
            if($res)
                return response()->json(['status'=>'ok','data'=>'update Successfully.'], 200);
        }else{
            return response()->json(['status'=>'error','data'=>'Please check Required Fields.'], 300);
        }
   }

   public function chapter(){
    return view('hrms.chapter');
   }

   public function savechapter(Request $request){
    $validation = Validator::make($request->all(), [
        'chapter_city' => 'required'
    ]);
    if($validation->passes())
    {
        $id = User::all()[0];
        if($_REQUEST['chapter_name'] != ''){
            $save_array = array(
                'chapter_general_meeting_time_on'=>@$_REQUEST['chapter_general_meeting_time_on'],
                'uuid'=> $id->toArray()['id'],
                'chapter_founded_On'=>trim(@$_REQUEST['chapter_founded_On']),      
                'chapter_website'=>trim(@$_REQUEST['chapter_website']), 
                'chapter_description'=>trim(@$_REQUEST['chapter_description']), 
                'chapter_city'=>trim(@$_REQUEST['chapter_city']), 
                'chapter_country'=>trim(@$_REQUEST['chapter_country']), 
                'chapter_phone_no'=>@$_REQUEST['chapter_phone_no'], 
                'chapter_general_meeting_day'=>trim(@$_REQUEST['chapter_general_meeting_day']), 
                'chapter_general_meeting_time_from'=>@$_REQUEST['chapter_general_meeting_time_from'], 
                'zip_code'=>@$_REQUEST['zip_code'], 
                'isActive'=>1, 
                'status'=>1, 
                'isDelete'=>'NO',
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s'),
                'chapter_name'=>trim(@$_REQUEST['chapter_name']), 
                'chapter_address'=>@$_REQUEST['chapter_address'],
                'chapter_state'=>@$_REQUEST['chapter_state'],
                'chapter_suite'=>@$_REQUEST['chapter_suite'],
                'meeting_location'=>@$_REQUEST['meeting_location']
            );  
            $res = Usersprofile::addChapterInfo($id['id'],$save_array);
            if($res)
                return response()->json(['status'=>'ok','data'=>$res], 200);
        }else{
            return response()->json(['status'=>'error','data'=>'Please check Required Fields.'], 300);
        }
    }
   }

   public function chapterlist(){
    $id = User::all()[0];
    $res = Usersprofile::getChapterList($id['id']);
    return view('hrms.chapter_list',compact('res'));
   }

   public function member_affiliates(){
    $id = User::all()[0];
    $data = Session::all();
    $res = Usersprofile::getChapterListAffiliatesInfo($data['UUID']);
   // $res = Usersprofile::getChapterList($data['UUID']);
    return view('hrms.member_affiliates',compact('res'));
   }

   public function editChapter($id){
        $res = Usersprofile::getChapterListbyid($id);
        return view('hrms.chapter',compact('res'));
   }
 
   public function actionchaptersupdate(Request $request){ 
    $validation = Validator::make($request->all(), [
        'chapter_city' => 'required'
    ]);
    if($validation->passes())
    {

    $id = User::all()[0];
    if($_REQUEST['chapter_name'] != ''){
        $update_array = array( 
            'chapter_general_meeting_time_on'=>@$_REQUEST['chapter_general_meeting_time_on'],
            'uuid'=> $id->toArray()['id'],
            'chapter_founded_On'=>trim(@$_REQUEST['chapter_founded_On']),      
            'chapter_website'=>trim(@$_REQUEST['chapter_website']), 
            'chapter_description'=>trim(@$_REQUEST['chapter_description']), 
            'chapter_city'=>trim(@$_REQUEST['chapter_city']), 
            'chapter_country'=>trim(@$_REQUEST['chapter_country']), 
            'chapter_phone_no'=>@$_REQUEST['chapter_phone_no'], 
            'chapter_general_meeting_day'=>trim(@$_REQUEST['chapter_general_meeting_day']), 
            'chapter_general_meeting_time_from'=>@$_REQUEST['chapter_general_meeting_time_from'], 
            'isActive'=>1, 
            'status'=>1, 
            'isDelete'=>'NO',
            'zip_code'=>@$_REQUEST['zip_code'],
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s'),
            'chapter_name'=>trim(@$_REQUEST['chapter_name']), 
            'chapter_address'=>@$_REQUEST['chapter_address'],
            'chapter_state'=>@$_REQUEST['chapter_state'],
            'chapter_suite'=>@$_REQUEST['chapter_suite'],
            'meeting_location'=>@$_REQUEST['meeting_location']
        );  
        $res = Usersprofile::updateChapterInfo($_REQUEST['update_id'],$update_array);
        if($res)
            return response()->json(['status'=>'ok','data'=>$res], 200);
    }else{
        return response()->json(['status'=>'error','data'=>'Please check Required Fields.'], 300);
    }
}
   }

   public function actioncategorysdelete(){  
        if($_REQUEST['data'] != ''){
            $delete = array(
                'isdelete'=>1,
            );  
            $res = Usersprofile::deleteCategories($_REQUEST['data'],$delete);
            if($res)
                return response()->json(['status'=>'ok','data'=>'Deleted Successfully.'], 200);
        }else{
            return response()->json(['status'=>'error','data'=>'Please check Required Fields.'], 300);
        }
   }

   public function getCountry(){
    $res = Usersprofile::getCountry();
    return response()->json(['status'=>'ok','data'=>$res], 200);
   }


   public function getState(){
    $res = Usersprofile::getState($_REQUEST['data']);
    return response()->json(['status'=>'ok','data'=>$res], 200);
   }

   public function getCity(){
    $res = Usersprofile::getCity($_REQUEST['data']);
    return response()->json(['status'=>'ok','data'=>$res], 200);
   }
   
   public function actionchapterdelete(){
    $id = User::all()[0];
    $update_array = array( 
        'status'=>0,
    );  
    $res = Usersprofile::updateChapterInfo($_REQUEST['data'],$update_array);
    if($res)
        return response()->json(['status'=>'ok','data'=>$res], 200);
   }

   public function associateApplication(){
        $id = User::all()[0];
        $res = Usersprofile::getAssociateApplication($id['id']); 
        return view('hrms.associateApplication',compact('res'));
   }

   public function associateApplicatioNew($status){
        $id = User::all()[0];
        $res = Usersprofile::getAssociateApplicationNew($id['id'],$status);
        $other = $status;
        return view('hrms.associateApplication',compact('res','other'));
   }

   public function actionapplicationdelete(){
    if($_REQUEST['data'] != ''){
        $delete = array(
            'isdelete'=>1,
        );  
        $res = Usersprofile::actionapplicationdelete($_REQUEST['data'],$delete);
        if($res)
            return response()->json(['status'=>'ok','data'=>'Status Changed Successfully.'], 200);
    }else{
        return response()->json(['status'=>'error','data'=>'Please Try Again.'], 300);
    }
   }

   public function application_status_change(){
        if($_REQUEST['data']['id'] != ''){
            $getstatus = getAcitveStatus($_REQUEST['data']['id']);
            $status = ($getstatus->isactive == 0)?1:0;
            $delete = array(
                'isactive'=>$status,
            );  
            $res = Usersprofile::actionapplicationdelete($_REQUEST['data']['id'],$delete);
            if($res)
                return response()->json(['status'=>'ok','data'=>'Status Changed Successfully.'], 200);
        }else{
            return response()->json(['status'=>'error','data'=>'Please Try Again.'], 300);
        }
   }

   public function applicationview($id){
        $res = $id;
        return view('hrms.application_view',compact('res'));
   }

   public function application_approve(){ 
        if($_REQUEST['data']['id'] != ''){
            $status = array(
                'status'=>2,
            );  
         
            $res = Usersprofile::actionapplicationdelete($_REQUEST['data']['id'],$status);
           
            $data = Usersprofile::saveDataOtherTables($_REQUEST['data']['id']);  
        
            $get_id = getApplicationViewList($_REQUEST['data']['id']);
          //dd($get_id);
            $password = Crypt::decrypt($get_id->uuid);
           
            $email_array = array(
                'first_name'=>$get_id->first_name,
                'last_name'=>$get_id->first_name,
                'uuid'=>$password,
                'your_email'=>$get_id->your_email,
            );

           $data =  event(new Registertion($email_array,$get_id->your_email)); 

            //echo "<pre>";print_r($data);die;

            //event(new Registertion($_REQUEST['name'].' Token:'.$rand, $_REQUEST['your_email']));
            
            return response()->json(['status'=>'ok','data'=>'Successfully Approved Application.'], 200);
        }else{
            return response()->json(['status'=>'error','data'=>'Please Try Again.'], 300);
        }
    }

    public function applicationRejected(){
        if($_REQUEST['data']['id'] != ''){
            $status = array(
                'status'=>3,
            );  
            $res = Usersprofile::actionapplicationdelete($_REQUEST['data']['id'],$status);
            return response()->json(['status'=>'ok','data'=>'Successfully Approved Application.'], 200);
        }else{
            return response()->json(['status'=>'error','data'=>'Please Try Again.'], 300);
        }
    }

    public function ajaxRequestApplication(Request $request){ 
        $id     = User::all()[0];
        $type   = isset($_POST['type'])?$_POST['type']:'';
        $name   = isset($_POST['data'])?$_POST['data']:'';
        $updated_id = isset($_POST['updated_id'])?$_POST['updated_id']:'';
        $new_va = explode(" ",$name);
    
        if($type == 'user_name'){ 
            $update_array = [
                'first_name'=>(isset($new_va[0]))?$new_va[0]:'',
                'last_name'=>(isset($new_va[1]))?$new_va[1]:''
            ];        
            $res = Usersprofile::updateInfoApplicant($updated_id,$update_array,$type);
        }else{
            $update_array = [
                $type=>$name,
            ];      
           // dd($name);
            $res = Usersprofile::updateInfoApplicant($updated_id,$update_array);
        }
        if($res){
            return response()->json(['status'=>'ok','data'=>$res], 200);
         }else{
            return response()->json(['status'=>'error','data'=>'data_example'], 300);
        }
    }


    function action_your_image(Request $request)      
    {
     $validation = Validator::make($request->all(), [
      'your_image' => 'required|image|mimes:jpeg,png,jpg,gif|max:10000'
     ]);
     if($validation->passes())
     {
      $image = $request->file('your_image');
      $new_name = rand() . '.' . $image->getClientOriginalExtension();
      $image->move(public_path('images'), $new_name);
        $id  = User::all()[0];
        $update_array = [
            'your_image'=>$new_name,
        ];  
        $res = Usersprofile::updateInfoApplicant($_REQUEST['updated_id'],$update_array);
       
        if($res){
          return response()->json([
            'message'   => 'Image Upload Successfully',
            'uploaded_image' => '<img src="/images/'.$new_name.'" class="img-thumbnail" width="300" />',
            'class_name'  => 'alert-success'
            ]);
        }else{
            return response()->json(['status'=>'error','data'=>'data_example'], 300);
        }
     }
     else
     {
      return response()->json([
       'message'   => $validation->errors()->all(),
       'uploaded_image' => '',
       'class_name'  => 'alert-danger'
      ]);
     }
    }

    function action_your_Card(Request $request)      
    {
     $validation = Validator::make($request->all(), [
      'business_card_upload' => 'required|image|mimes:jpeg,png,jpg,gif|max:10000'
     ]);
     if($validation->passes())
     {
      $image = $request->file('business_card_upload');
      $new_name = rand() . '.' . $image->getClientOriginalExtension();
      $image->move(public_path('images'), $new_name);
        $id  = User::all()[0];
        $update_array = [
            'business_card_upload'=>$new_name,
        ];  
        $res = Usersprofile::updateInfoApplicant($_REQUEST['updated_id'],$update_array);
       
        if($res){
          return response()->json([
            'message'   => 'Image Upload Successfully',
            'uploaded_image' => '<img src="/images/'.$new_name.'" class="img-thumbnail" width="300" />',
            'class_name'  => 'alert-success'
            ]);
        }else{
            return response()->json(['status'=>'error','data'=>'data_example'], 300);
        }
     }
     else
     {
      return response()->json([
       'message'   => $validation->errors()->all(),
       'uploaded_image' => '',
       'class_name'  => 'alert-danger'
      ]);
     }
    }



  
    
}
