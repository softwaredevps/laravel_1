<?php

namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;
use App\Usersprofile;
use Illuminate\Routing\Controller as BaseController;
use Validator;


class GuestController extends BaseController
{ 
    function index(){
      
       
    }

    public function invite_guest()
    {
    	
    	return view('guest.invite_guest');

    }

    public function chapter_guest()
    {
    	
    	return view('guest.chapter_guest');

    }

    public function my_invitees()
    {
        
    	return view('guest.my_invitees');

    }
    

}
