<?php

namespace App\Http\Controllers\Admin;

use App\Settings;
use Illuminate\Http\Request;
use App\Usersprofile;
use Illuminate\Routing\Controller as BaseController;
use Validator;
use App\Events\Registertion;

class SettingController extends BaseController
{ 
    function index(){
      
       return view('settings.index');
    }

    public function MemberShipPmtType()
    {
    	
        $res = Settings::getMemberShipPmtType();
    	return view('settings.MemberShipPmtType',  compact('res'));

    }

    public function ActivityPoints()
    {
    	
        $res = Settings::getActivityPoints();
    	return view('settings.ActivityPoints',  compact('res'));

    }

    public function RewardsPercentage()
    {
    	
        $res = Settings::getRewardsPercentage();
    	return view('settings.RewardsPercentage',  compact('res'));

    }

    public function MeetingType()
    {
    	
        $res = Settings::getMeetingType();
    	return view('settings.MeetingType',  compact('res'));

    }

    public function MeetingStructure()
    {
    	
        $res = Settings::getMeetingStructure();
    	return view('settings.MeetingStructure',  compact('res'));

    }

    

}
