<?php

namespace App\Http\Controllers;

use App\Registration as register;
//use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Input;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

use App\Events\TeacherAdded;
use App\Events\Registertion;

use App\Listeners\SendTeacherConfirmationEmail;
//use Validator;
use Illuminate\Support\Facades\Crypt;

use Validator;


class Registration extends Controller
{
   

    public function saveinfo(Request $request){
  
            $validatedData = $request->validate([
                'your_email' => 'required|email|unique:associate_application_tbl',
                'name' => 'required',
                'last_name' => 'required',
                'dob' => 'required',
                'phone_no' => 'required', 
                'business_email'=>'nullable|email',  
                'head_shot'=>'nullable|url',
                'business_name'=>'required', 
                'business_phone_no'=>'required',
                'company_address'=>'required',
                'city'=>'required',
                'zip_code'=>'required',
            ]);
          
                $rand_pass = randomPassword();
            
                $rand = Crypt::encrypt($rand_pass);
                //dd($rand);
                $validation = Validator::make($request->all(), [
                    'your_image1' => 'image|mimes:jpeg,png,jpg,gif|max:2048'
                ]);
                if($validation->passes())
                {
                // user profile image upload
                $image = $request->file('your_image1');
                $new_name = '';
                if($image){
                    $new_name = rand() . '.' . $image->getClientOriginalExtension();
                    $image->move(public_path('images'), $new_name);
                }
                 
                //busineess image upload
                $business_card_upload = $request->file('your_image2');
                //$head_shot_upload = $request->file('head_shot');
                $card_upload='';
                //$head_shot=''; 
               
                if($business_card_upload){
                     $card_upload = rand() . '.' . $business_card_upload->getClientOriginalExtension(); 
                     $business_card_upload->move(public_path('images'), $card_upload);
                 }

                // if($head_shot_upload){
                //     $head_shot = rand() . '.' . $head_shot_upload->getClientOriginalExtension(); 
                //     $head_shot_upload->move(public_path('images'), $head_shot);
                // }

                if(isset($_REQUEST['where_submit_review'])){
                    $_REQUEST['where_submit_review'] =  implode(',',$_POST['where_submit_review']);

                }
                
                // if(isset($_REQUEST['selectpickerhidden']) && $_REQUEST['selectpickerhidden'] != ''){
                //     $_REQUEST['where_submit_review'] = (isset($_REQUEST['selectpickerhidden']) && $_REQUEST['selectpickerhidden']!='')?$_REQUEST['selectpickerhidden']:'';
                // }

                if(isset($_REQUEST['currently_member_of_network']) && $_REQUEST['currently_member_of_network'] == 'YES'){
                    $_REQUEST['currently_member_of_network'] =  trim((isset($_REQUEST['currently_member_of_network_text']) && $_REQUEST['currently_member_of_network_text']!='')?$_REQUEST['currently_member_of_network_text']:'' );
                }

                if(isset($_REQUEST['address_billing_type'])){
                    $payment_company_address = trim((isset($_REQUEST['company_address']) && $_REQUEST['company_address']!='')?$_REQUEST['company_address']:'' );
                    $payment_company_address_second = trim((isset($_REQUEST['company_address_second']) && $_REQUEST['company_address_second']!='')?$_REQUEST['company_address_second']:'' );
               
                    $payment_city = trim((isset($_REQUEST['city']) && $_REQUEST['city']!='')?$_REQUEST['city']:'' );
                    $payment_chapter_state = trim((isset($_REQUEST['state']) && $_REQUEST['state']!='')?$_REQUEST['state']:'' );
                    $payment_zip_code = trim((isset($_REQUEST['zip_code']) && $_REQUEST['zip_code']!='')?$_REQUEST['zip_code']:'' );
                  
                }else{
                    $payment_company_address = trim((isset($_REQUEST['payment_company_address']) && $_REQUEST['payment_company_address']!='')?$_REQUEST['payment_company_address']:'' );
                    $payment_company_address_second = trim((isset($_REQUEST['payment_company_address_second']) && $_REQUEST['payment_company_address_second']!='')?$_REQUEST['payment_company_address_second']:'' );
               
                    $payment_city = trim((isset($_REQUEST['payment_city']) && $_REQUEST['payment_city']!='')?$_REQUEST['payment_city']:'' );
                    $payment_chapter_state = trim((isset($_REQUEST['payment_chapter_state']) && $_REQUEST['payment_chapter_state']!='')?$_REQUEST['payment_chapter_state']:'' );
                    $payment_zip_code = trim((isset($_REQUEST['payment_zip_code']) && $_REQUEST['payment_zip_code']!='')?$_REQUEST['payment_zip_code']:'' );
                  
                }
               //echo "<pre>";print_r($_REQUEST);die('sdf');
                    $save_array = array(  
                        'your_image'=>$new_name,
                        'business_card_upload'=>$card_upload,
                        'affiliate_for'=>trim((isset($_REQUEST['affiliate_are']) && $_REQUEST['affiliate_are']!='')?$_REQUEST['affiliate_are']:'' ),
                        'business_category'=>trim((isset($_REQUEST['business_category']) && $_REQUEST['business_category']!='')?$_REQUEST['business_category']:'' ),
                        'extra_business_category'=>trim((isset($_REQUEST['extra_business_category']) && $_REQUEST['extra_business_category']!='')?$_REQUEST['extra_business_category']:'' ),

                        'besure_organization'=>trim((isset($_REQUEST['besure_organization']) && $_REQUEST['besure_organization']!='')?$_REQUEST['besure_organization']:'' ),
        
                        'prefix'=>trim((isset($_REQUEST['prefix']) && $_REQUEST['prefix']!='')?$_REQUEST['prefix']:'' ),
                        'first_name'=>trim((isset($_REQUEST['name']) && $_REQUEST['name']!='')?$_REQUEST['name']:'' ),
                        'last_name'=>trim((isset($_REQUEST['last_name']) && $_REQUEST['last_name']!='')?$_REQUEST['last_name']:'' ),
                        'suffix'=>trim((isset($_REQUEST['suffix']) && $_REQUEST['suffix']!='')?$_REQUEST['suffix']:'' ),
                        'gender'=>trim((isset($_REQUEST['gender']) && $_REQUEST['gender']!='')?$_REQUEST['gender']:'' ),
                        'dob'=>trim((isset($_REQUEST['dob']) && $_REQUEST['dob']!='')?$_REQUEST['dob']:'' ),
                        'phone_no'=>trim((isset($_REQUEST['phone_no']) && $_REQUEST['phone_no']!='')?$_REQUEST['phone_no']:'' ),
                        'home_address'=>trim((isset($_REQUEST['home_address']) && $_REQUEST['home_address']!='')?$_REQUEST['home_address']:'' ),
                        'your_email'=>trim((isset($_REQUEST['your_email']) && $_REQUEST['your_email']!='')?$_REQUEST['your_email']:'' ),
                        'webpage'=>trim((isset($_REQUEST['webpage']) && $_REQUEST['webpage']!='')?$_REQUEST['webpage']:'' ),
                        'country'=>trim((isset($_REQUEST['country']) && $_REQUEST['country']!='')?$_REQUEST['country']:'' ),
                        'state'=>trim((isset($_REQUEST['state']) && $_REQUEST['state']!='')?$_REQUEST['state']:'' ),
                        'city'=>trim((isset($_REQUEST['city']) && $_REQUEST['city']!='')?$_REQUEST['city']:'' ),
                        'zip_code'=>trim((isset($_REQUEST['zip_code']) && $_REQUEST['zip_code']!='')?$_REQUEST['zip_code']:'' ),
                        'biography'=>trim((isset($_REQUEST['bio']) && $_REQUEST['bio']!='')?$_REQUEST['bio']:'' ),
                        'business_name'=>trim((isset($_REQUEST['business_name']) && $_REQUEST['business_name']!='')?$_REQUEST['business_name']:'' ),
                        'business_phone_no'=>trim((isset($_REQUEST['business_phone_no']) && $_REQUEST['business_phone_no']!='')?$_REQUEST['business_phone_no']:'' ),
                        'business_website'=>trim((isset($_REQUEST['business_website']) && $_REQUEST['business_website']!='')?$_REQUEST['business_website']:'' ),
                        'business_email'=>trim((isset($_REQUEST['business_email']) && $_REQUEST['business_email']!='')?$_REQUEST['business_email']:'' ),
                        'company_address'=>trim((isset($_REQUEST['company_address']) && $_REQUEST['company_address']!='')?$_REQUEST['company_address']:'' ),
                        'company_address_second'=>trim((isset($_REQUEST['company_address_second']) && $_REQUEST['company_address_second']!='')?$_REQUEST['company_address_second']:'' ),

                        'business_about'=>trim((isset($_REQUEST['about_business']) && $_REQUEST['about_business']!='')?$_REQUEST['about_business']:'' ),
                        'position_in_comp'=>trim((isset($_REQUEST['position_in_comp']) && $_REQUEST['position_in_comp']!='')?$_REQUEST['position_in_comp']:'' ),
                        'position_working_year'=>trim((isset($_REQUEST['position_working_year']) && $_REQUEST['position_working_year']!='')?$_REQUEST['position_working_year']:'' ),
                        'position_working_industry'=>trim((isset($_REQUEST['position_working_industry']) && $_REQUEST['position_working_industry']!='')?$_REQUEST['position_working_industry']:'' ),
                        'primary_occupation'=>trim((isset($_REQUEST['primary_occupation']) && $_REQUEST['primary_occupation']!='')?$_REQUEST['primary_occupation']:'' ),
                        'bonded'=>trim((isset($_REQUEST['bonded']) && $_REQUEST['bonded']!='')?$_REQUEST['bonded']:'' ),
                        'any_professional_licenses'=>trim((isset($_REQUEST['any_professional_licenses']) && $_REQUEST['any_professional_licenses']!='')?$_REQUEST['any_professional_licenses']:'' ),
                        'currently_member_of_network'=>trim((isset($_REQUEST['currently_member_of_network']) && $_REQUEST['currently_member_of_network']!='')?$_REQUEST['currently_member_of_network']:'' ),
                        'head_shot'=>trim((isset($_REQUEST['head_shot']) && $_REQUEST['head_shot']!='')?$_REQUEST['head_shot']:'' ),
                        'fb_social_url'=>trim((isset($_REQUEST['fb_social_url']) && $_REQUEST['fb_social_url']!='')?$_REQUEST['fb_social_url']:'' ),
                        'instagram_social_url'=>trim((isset($_REQUEST['instagram_social_url']) && $_REQUEST['instagram_social_url']!='')?$_REQUEST['instagram_social_url']:'' ),
                        'linkedin_social_url'=>trim((isset($_REQUEST['linkedin_social_url']) && $_REQUEST['linkedin_social_url']!='')?$_REQUEST['linkedin_social_url']:'' ),
                        'youtube_social_url'=>trim((isset($_REQUEST['youtube_social_url']) && $_REQUEST['youtube_social_url']!='')?$_REQUEST['youtube_social_url']:'' ),
                        'tiktok_user_name'=>trim((isset($_REQUEST['tiktok_user_name']) && $_REQUEST['tiktok_user_name']!='')?$_REQUEST['tiktok_user_name']:'' ),
                        'where_submit_review'=>trim((isset($_REQUEST['where_submit_review']) && $_REQUEST['where_submit_review']!='')?$_REQUEST['where_submit_review']:'' ),
                        'guidelines_0'=>trim((isset($_REQUEST['guidelines_0']) && $_REQUEST['guidelines_0']!='')?$_REQUEST['guidelines_0']:'' ),
                        'guidelines_1'=>trim((isset($_REQUEST['guidelines_1']) && $_REQUEST['guidelines_1']!='')?$_REQUEST['guidelines_1']:'' ),
                        'guidelines_2'=>trim((isset($_REQUEST['guidelines_2']) && $_REQUEST['guidelines_2']!='')?$_REQUEST['guidelines_2']:'' ),
                        'guidelines_3'=>trim((isset($_REQUEST['guidelines_3']) && $_REQUEST['guidelines_3']!='')?$_REQUEST['guidelines_3']:'' ),
                        'guidelines_4'=>trim((isset($_REQUEST['guidelines_4']) && $_REQUEST['guidelines_4']!='')?$_REQUEST['guidelines_4']:'' ),
                        'Investment_1'=>trim((isset($_REQUEST['Investment_1']) && $_REQUEST['Investment_1']!='')?$_REQUEST['Investment_1']:'' ),
                        'six_month_payment'=>trim((isset($_REQUEST['six_month_payment']) && $_REQUEST['six_month_payment']!='')?$_REQUEST['six_month_payment']:'' ),
                        'two_quarterly_payment'=>trim((isset($_REQUEST['single_one_time_payment']) && $_REQUEST['single_one_time_payment']!='')?$_REQUEST['single_one_time_payment']:'' ),
                        'single_one_time_payment'=>trim((isset($_REQUEST['single_one_time_payment']) && $_REQUEST['single_one_time_payment']!='')?$_REQUEST['single_one_time_payment']:'' ),
                        'twelve_month_payment'=>trim((isset($_REQUEST['twelve_month_payment']) && $_REQUEST['twelve_month_payment']!='')?$_REQUEST['twelve_month_payment']:'' ),
                        'four_quarterly_payment'=>trim((isset($_REQUEST['four_quarterly_payment']) && $_REQUEST['four_quarterly_payment']!='')?$_REQUEST['four_quarterly_payment']:'' ),
                        'one_time_payment'=>trim((isset($_REQUEST['one_time_payment']) && $_REQUEST['one_time_payment']!='')?$_REQUEST['one_time_payment']:'' ),
                        'twentyfour_month_payment'=>trim((isset($_REQUEST['twentyfour_month_payment']) && $_REQUEST['twentyfour_month_payment']!='')?$_REQUEST['twentyfour_month_payment']:'' ),
                        'selennine_one_time_payment'=>trim((isset($_REQUEST['selennine_one_time_payment']) && $_REQUEST['selennine_one_time_payment']!='')?$_REQUEST['selennine_one_time_payment']:'' ),
                        'ach_withdrawal'=>trim((isset($_REQUEST['ach_withdrawal']) && $_REQUEST['ach_withdrawal']!='')?$_REQUEST['ach_withdrawal']:'' ),
                        'credit_card'=>trim((isset($_REQUEST['credit_card']) && $_REQUEST['credit_card']!='')?$_REQUEST['credit_card']:'' ),
                        'payment_info'=>trim((isset($_REQUEST['payment_info']) && $_REQUEST['payment_info']!='')?$_REQUEST['payment_info']:'' ),
                        'bank_name'=>trim((isset($_REQUEST['bank_name']) && $_REQUEST['bank_name']!='')?$_REQUEST['bank_name']:'' ),
                        'account_number'=>trim((isset($_REQUEST['account_number']) && $_REQUEST['account_number']!='')?$_REQUEST['account_number']:'' ),
                        'account_name'=>trim((isset($_REQUEST['account_name']) && $_REQUEST['account_name']!='')?$_REQUEST['account_name']:'' ),
                        'rotation_number'=>trim((isset($_REQUEST['rotation_number']) && $_REQUEST['rotation_number']!='')?$_REQUEST['rotation_number']:'' ),
                        'card_holer_name'=>trim((isset($_REQUEST['card_holer_name']) && $_REQUEST['card_holer_name']!='')?$_REQUEST['card_holer_name']:'' ),
                        'card_number'=>trim((isset($_REQUEST['card_number']) && $_REQUEST['card_number']!='')?$_REQUEST['card_number']:'' ),
                        'expiry_date_m'=>trim((isset($_REQUEST['expiry_date_m']) && $_REQUEST['expiry_date_m']!='')?$_REQUEST['expiry_date_m']:'' ),
                        'expiry_date_y'=>trim((isset($_REQUEST['expiry_date_y']) && $_REQUEST['expiry_date_y']!='')?$_REQUEST['expiry_date_y']:'' ),
                        'cv_code'=>trim((isset($_REQUEST['cv_code']) && $_REQUEST['cv_code']!='')?$_REQUEST['cv_code']:'' ),
                        'your_answer'=>trim((isset($_REQUEST['your_answer']) && $_REQUEST['your_answer']!='')?$_REQUEST['your_answer']:'' ),
                        
                        'payment_company_address'=>$payment_company_address,
                        'payment_company_address_second'=>$payment_company_address_second,
                        'payment_city'=>$payment_city,
                        'payment_chapter_state'=>$payment_chapter_state,
                        'payment_zip_code'=>$payment_zip_code,
                      
                        'uuid'=>$rand,
                        'isactive'=>1, 
                        'isdelete'=>0, 
                        'created_at'=>date('Y-m-d H:i:s'),
                        'updated_at'=>date('Y-m-d H:i:s'),
                        'deleted_at'=>0,
                        'created_by'=>1,
                        'updated_by'=>0,
                        'deleted_by'=>0
                    ); 
        
                    $res = register::saveinfo($save_array);
                    if($res)
              //      event(new Registertion($_REQUEST['name'].' Token:'.$rand, $_REQUEST['your_email']));
                    
              //      event(new Registertion('Admin '.$_REQUEST['name'].' Applied In Besure.','admin@gmail.com'));
                    return response()->json(['status'=>'ok','data'=>$res,'name'=> trim((isset($_REQUEST['name']) && $_REQUEST['name']!='')?base64_encode($_REQUEST['name']):'' )], 200);

                    //$res = Usersprofile::updateInfoBusiness($id['id'],$update_array);
                    
                    if($res){
                        return response()->json([
                            'message'   => 'Image Upload Successfully',
                            'uploaded_image' => '<img src="/images/'.$new_name.'" class="img-thumbnail" width="300" />',
                            'class_name'  => 'alert-success',
                            
                        ]);
                    }else{
                        return response()->json(['status'=>'error','data'=>'data_example'], 300);
                    }
                }
                else
                {
                return response()->json([
                    'message'   => $validation->errors()->all(),
                    'uploaded_image' => '',
                    'class_name'  => 'alert-danger'
                ]);
            }

    }

    public function thankyoupage(){
        return view('frontend.thankyoupage');
    }

    
}
