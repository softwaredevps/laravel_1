<?php

namespace App\Http\Controllers;

use App\User;
use App\Settings;
use Illuminate\Http\Request;
use App\Usersprofile;
use Illuminate\Routing\Controller as BaseController;
use Validator;
use App\Events\Registertion;

class MeetingsController extends BaseController
{ 
    public function index()
    {
		$res = Settings::getMeetingType();
      	return view('meetings.index',compact('res'));       
    }
}
