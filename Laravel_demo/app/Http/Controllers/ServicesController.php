<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Usersprofile;
use App\Memberprofile;

use Illuminate\Routing\Controller as BaseController;
use Validator;
use App\Events\Registertion;
use Session;

class ServicesController extends BaseController
{ 
    public function index(Request $request)
    {        
      $data = Session::all();
      if(isset($_POST) && !empty($_POST)){
        $validatedData = $request->validate([
          'service_name' => 'required',
          'service_category' => 'required',
          'tentative_price' => 'required',
          'besure_rewards_points' => 'required',
          'service_description' => 'required',
        ]);
        
          $store = array(
            'service_name'=>$_POST['service_name'],
            'service_category'=>$_POST['service_category'], 
            'tentative_price'=>$_POST['tentative_price'],
            'besure_rewards_points'=>$_POST['besure_rewards_points'],
            'service_description'=>$_POST['service_description'],     
            'member_id'=>$data['UUID'],
            'created_by'=>$data['UUID'],
          );  
          
          $res = Memberprofile::saveServicesData($store);
          if($res){
            $request->session()->flash('alert-success', 'Successful Added!');
            return redirect('services');
          }
       
      }else{
        $res = Memberprofile::getServicesData($data['UUID']);
        $updateForm =false;
        return view('services.index',compact('res','updateForm'));     
      }
       
    }

    public function actionDeleteService(Request $request){
      $updated_array = array(
        'isActive'=>0,
      );  

      $res = Memberprofile::actionDeleteService($_POST['data'],$updated_array);
      if($res){
          return response()->json(['status'=>'ok','data'=>$res], 200);
      }else{
          return response()->json(['status'=>'error','data'=>'data_example'], 300);
      }
    }

    public function editServices($id,Request $request){
     
      if(isset($_POST['hiddenUpdate']) && $_POST && !empty($_POST)){ 
        $validatedData = $request->validate([
          'service_name' => 'required',
          'service_category' => 'required',
          'tentative_price' => 'required',
          'besure_rewards_points' => 'required',
          'service_description' => 'required',
        ]);
        
        $data = Session::all();
        $updated_array = array(
          'service_name'=>$_POST['service_name'],
          'service_category'=>$_POST['service_category'], 
          'tentative_price'=>$_POST['tentative_price'],
          'besure_rewards_points'=>$_POST['besure_rewards_points'],
          'service_description'=>$_POST['service_description'],     
          'member_id'=>$data['UUID'],
          'created_by'=>$data['UUID'],
        );  

        $response = Memberprofile::actionDeleteService($id,$updated_array);
        if($response)
        $res = Memberprofile::getServicesData($data['UUID']);
        $updateForm =false;
        $request->session()->flash('alert-success', 'Success!');
        return redirect('services'); 

      }else{
        $res = Memberprofile::getServicesEditData($id);
        $updateForm =true;
        return view('services.index',compact('res','updateForm')); 
      }

       
    }
}
