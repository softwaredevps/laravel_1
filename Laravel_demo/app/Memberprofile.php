<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;

class Memberprofile extends Model
{
    
    static function getMember($email , $uuid){
        try { 

            $get = DB::table('associate_application_tbl')
            ->select('associate_application_tbl.*','member_info_tbl.password')
            ->leftJoin('member_info_tbl', 'member_info_tbl.uuid', '=', 'associate_application_tbl.id')
            ->where('member_info_tbl.uuid',$uuid)->first();
           // dd($get);
            // $get = DB::table('associate_application_tbl')->select('*')->where('your_email',$email)->where('uuid',$uuid)->first();
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }

    static function memberChangePassword($email_id , $data){
        $pass = Crypt::encrypt($data['data']);
       
        $update_array = [
            'password'=>$pass,
        ];  
        $assos_array = [
            'uuid'=>'',
        ];  
      
        try { 
            $get =  DB::table('member_info_tbl')
            ->where('your_email', $email_id)
            ->update($update_array);

            DB::table('associate_application_tbl')
            ->where('your_email', $email_id)
            ->update($assos_array);
          
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }

    static function saveServicesData($array){
       return  DB::table('services_tbl')->insert($array);    
    }

    static function getServicesData($id){
        try { 
            $get = DB::table('services_tbl')
            ->select('services_tbl.*')
            ->where(['member_id'=>$id,'isDeleted'=>0,'isActive'=>1])->get();
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }

    static function actionDeleteService($id,$array){
        try { 
            $get =  DB::table('services_tbl')
            ->where('id', $id)
            ->update($array);
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }

    static function getServicesEditData($id){
        try { 
            $get = DB::table('services_tbl')
            ->select('services_tbl.*')
            ->where(['id'=>$id,'isDeleted'=>0,'isActive'=>1])->get();
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }
    
}
 