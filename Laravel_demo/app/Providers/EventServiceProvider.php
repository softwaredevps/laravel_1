<?php

namespace App\Providers;

use App\Events\TeacherAdded;
use App\Listeners\SendTeacherConfirmationEmail;

use App\Events\Registertion;
use App\Listeners\SendResigtrationEmail;

use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        // TeacherAdded::class => [
        //     SendTeacherConfirmationEmail::class
        // ],
        Registertion::class => [
            SendResigtrationEmail::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
