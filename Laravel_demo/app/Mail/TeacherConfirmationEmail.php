<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TeacherConfirmationEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $teacher;

    public $student;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($teacher, $student)
    {
        $this->teacher = $teacher;
        $this->student = $student;
    }

    /**
     * Build the    .
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.teacher-confirmation');
    }
}
