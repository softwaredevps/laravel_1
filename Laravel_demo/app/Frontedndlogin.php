<?php

namespace App;
use Session;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;

class Frontedndlogin extends Model
{
   static function attempt($request){
        try { 
            $user =  DB::table('member_info_tbl')
                    ->where('your_email',$request['name'])
                    ->where('status',2)->first(); 
                 
            if(!$user) return false;
           
            $userPwd = $request['password'];  
                
            $decryptPwd = Crypt::decrypt($user->password);
           
            $validLogin = $userPwd == $decryptPwd;
           
            if($validLogin) {
                session ( [                                                     
                    'frontend_name' => $request['name'] ,
                    'UUID' => $user->uuid
                ] );
            }

            return $validLogin;

        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
       //return $get;
   }
}
