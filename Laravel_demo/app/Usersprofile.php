<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Session;
 
class Usersprofile extends Model
{
    static function getinfo($id){
        try {
            $get = DB::table('users as a')->where('a.id',$id)
            ->leftJoin('userprofile as b', 'a.id', '=', 'b.uuid')
            ->leftJoin('userbusinessinfo as c', 'c.uuid', '=', 'b.uuid')
            ->leftJoin('user_networkinfo as d', 'd.uuid', '=', 'b.uuid')
            ->select('*','b.badge_color','c.name','b.url')->first();
          
        } catch(\Illuminate\Database\QueryException $ex){
            dd($ex->getMessage()); 
        }
        return $get;
    }

    static function getChapterRosterData($id){
        try { 
            $get = DB::table('chapter_tbl')->select('*')->where('id',$id)->groupBy('id')->get();
            
            $assos = DB::table('member_info_tbl as a')
                    ->leftJoin('member_biz_tbl as b', 'a.uuid', '=', 'b.uuid')
                    ->select('*')->where('a.status',2)->where('a.affiliate_for',$id)->get();

            $member = DB::table('chapterboardchairmembertbl')->select('*')->where('ChapterID',$id)->get();

        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return array('get'=>$get,'assos'=>$assos,'member'=>$member);
    }

    static function getChapterRosterDataMemberId($id){ 
        try {     
        
            $get = DB::table('chapter_tbl')->select('*')->where('id',$id)->groupBy('id')->get();
      
        
            $assos = DB::table('member_info_tbl as a')
                    ->leftJoin('member_biz_tbl as b', 'a.uuid', '=', 'b.uuid')
                    ->select('*')->where('a.status',2)->where('a.affiliate_for',$id)->get();
                 
            $member = DB::table('chapterboardchairmembertbl')->select('*')->where('ChapterID',$id)->get();

        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return array('get'=>$get,'assos'=>$assos,'member'=>$member);
    }

    static function getChapterRosterDatabyID(){ 

        $data = Session::all();
        $id =  $data['UUID'];
        try { 
            $get = DB::table('member_info_tbl')->select('*')->where('uuid',$id)->get()[0];

            $id = ($get->affiliate_for)?$get->affiliate_for:'';
        
            $get = DB::table('chapter_tbl')->select('*')->where('id',$id)->groupBy('id')->get();
      
        
            $assos = DB::table('member_info_tbl as a')
                    ->leftJoin('member_biz_tbl as b', 'a.uuid', '=', 'b.uuid')
                    ->select('*')->where('a.status',2)->where('a.affiliate_for',$id)->get();
                 
            $member = DB::table('chapterboardchairmembertbl')->select('*')->where('ChapterID',$id)->get();

        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return array('get'=>$get,'assos'=>$assos,'member'=>$member);
    }
    
    static function updateTermination($id,$array){
        try { 
            $get =  DB::table('chapterboardchairmembertbl')
                ->where('MemberID', $id)
                ->update($array);
      
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }

    static function getMembersbyid($id){
        try { 
            $get = DB::table('boardchairdesignationtbl')->select('*')->where('id',$id)->get();

        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }

    static function getMembers($id,$user_id){
        try {
            $get = DB::table('associate_application_tbl')->select('*')->where('affiliate_for',$id)->where('id','!=',$user_id)->get();
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }

    static function getMembersTerminate($id,$user_id){
        try {
            $get = DB::table('member_info_tbl as a')
            ->Join('chapterboardchairmembertbl as b', 'b.MemberID', '=', 'a.uuid')
            ->select('a.*')->where('a.affiliate_for',$id)->where('isTerminated','!=',1)->get();
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }

    static function getDesignation($id){
        try { 
            $get = DB::table('boardchairdesignationtbl')->select('*')->where('ChairType',$id)->get();
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }

    static function chapterBoardChairMember(){
        try { 
            $get = DB::table('chapterboardchairmembertbl')->select('*')->get();
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }

    static function updateChairMember($id,$array){
        try { 
            $get =  DB::table('boardchairdesignationtbl')
                ->where('id', $id)
                ->update($array);
        
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }

    static function editBoardChair($id){
        try { 
            $get = DB::table('boardchairdesignationtbl')->select('*')->where('id',$id)->get();
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }

    static function getBoradChair(){
        try { 
            $get = DB::table('boardchairdesignationtbl')->select('*')->where('isDelete',1)->get();
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }

    static function updateInfo($id,$values,$type){
        try { 
            $get =  DB::table('userprofile')
                ->where('id', $id)
                ->update($values);
        
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }

    static function updateInfoNew($id,$values){
        try { 
            $get =  DB::table('userprofile')
                ->where('id', $id)
                ->update($values);
        
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }

    static function updateInfoApplicant($id,$values){
        try {
            $get =  DB::table('associate_application_tbl')
                ->where('id', $id)
                ->update($values);
            
            $get = DB::table('associate_application_tbl')->select('*')->where('id',$id)->first();
            if($get->status != 1) {
               DB::table('member_info_tbl')->where('uuid', $id)->delete();
               DB::table('member_payment_tbl')->where('uuid', $id)->delete();
               DB::table('member_biz_tbl')->where('uuid', $id)->delete();
               DB::table('member_experience_tbl')->where('uuid', $id)->delete();
               self::saveDataOtherTables($id);
            }
        } catch(\Illuminate\Database\QueryException $ex){
            dd($ex->getMessage()); 
        }
        return $get;
    }

    static function updateInfoBusiness($id,$values){
        try { 
            $get =  DB::table('userbusinessinfo')
                ->where('id', $id)
                ->update($values);
        
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }

    static function updateInfoNetwork($id,$values){
        try { 
            $get =  DB::table('user_networkinfo')
                ->where('id', $id)
                ->update($values);
        
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }

    static function ajaxRequestModalEmail($id,$values){
        try { 
            $get =  DB::table('users_email')->insert(
                array('uuid' => $id->toArray()['id'],'status'=>1, 'email' => $values['email'],'emailtype' => $values['emailtype'])
            );
        
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }

    static function ajaxRequestModalMobile($id,$values){
        try { 
            $get =  DB::table('users_phone')->insert(
                array('uuid' => $id->toArray()['id'],'status'=>1, 'number' => $values['mobile'],'phonetype' => $values['mobiletype'])
            );
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get; 
    }

    static function getCategoriesInfo($id){
        try { 
            $get = DB::table('business_categories_tbl')->select('*')->where(['isdelete'=>0])->get();
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    } 

    static function businessCatInfo($id){
        try { 
            $get = DB::table('business_categories_tbl')->select('*')->where(['isdelete'=>0,'isactive'=>1])->get();
           
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }

    static function checkAppointment($array){
        try { 
            $get = DB::table('chapterboardchairmembertbl')->where('ChapterID', $array['ChapterID'])
            ->where('MemberID', $array['MemberID'])->where('isTerminated',0)->first();
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }
    
    static function insertAppointment($array){
        try { 
           
            $get =  DB::table('chapterboardchairmembertbl')->insert(
                $array
            );
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }

    static function insertChairMember($id,$array){
        try { 
            $get =  DB::table('boardchairdesignationtbl')->insert(
                $array
            );
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }

    static function addCategoriesInfo($id,$array){
        try { 
            $get =  DB::table('business_categories_tbl')->insert(
                $array
            );
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }

    static function deleteCategories($id,$array){
     
        try { 
            $get =  DB::table('business_categories_tbl')
                ->where('id', $id)
                ->update($array);
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }

    static function editCategoriesInfo($id){
        try { 
            $get = DB::table('business_categories_tbl')->select('*')->where(['id'=>$id])->first();
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }

    static function addChapterInfo($id,$save_array){
        try { 
            $get =  DB::table('chapter_tbl')->insert(
                $save_array
            );
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }

    static function getChapterListAffiliatesInfo($id){
        try { 
            $get = DB::table('chapter_tbl')->select('*')->where(['status'=>1,'isActive'=>1,'isDelete'=>'NO'])->orderby('chapter_name')->get();
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }

    static function getChapterList($id){
        try { 
            $get = DB::table('chapter_tbl')->select('*')->where(['status'=>1,'uuid'=>$id])->orderby('chapter_name')->get();
           
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }

    static function getChapterListbyid($id){
        try { 
            $get = DB::table('chapter_tbl')->select('*')->where(['status'=>1,'id'=>$id])->first();
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }

    static function updateChapterInfo($id,$array){
        try { 
            $get =  DB::table('chapter_tbl')
                ->where('id', $id)
                ->update($array);
              
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }

    static function getCountry(){
        try { 
            $get = DB::table('countries')->select('*')->get();
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }

    static function getState($id){
        try { 
            $get = DB::table('states')->select('*')->where('country_id',$id)->get();
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get; 
    }

    static function getCity($id){
        try { 
            $get = DB::table('cities')->select('*')->where('state_id',$id)->get();
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get; 
    }

    static function getAssociateApplication($id){
        try { 
            $get = DB::table('associate_application_tbl')->select('*')->where('isdelete',0)->where('status',1)->orderby('id','desc')->get();
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get; 
    }

    static function getAssociateApplicationNew($id, $status){
        try { 
            $get = DB::table('associate_application_tbl')->select('*')->where('isdelete',0)->where('status',$status)->orderby('id','desc')->get();
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get; 
    }

    static function actionapplicationdelete($id,$array){
        try { 
            $get =  DB::table('associate_application_tbl')
                ->where('id', $id)
                ->update($array);
              
        } catch(\Illuminate\Database\QueryException $ex){ 
            dd($ex->getMessage()); 
        }
        return $get;
    }

    static function saveDataOtherTables($id){
        $get = DB::table('associate_application_tbl')->select('*')->where('id',$id)->first();
        $res = DB::table('member_info_tbl')->select('*')->where('uuid',$id)->first();
 
        if(!$res){
            $array = [
                'uuid'=>$get->id,
                'password'=>$get->uuid,
                'first_name'=>$get->first_name,
                'last_name'=>$get->last_name,
                'prefix'=>$get->first_name,
                'suffix'=>$get->suffix,
                'gender'=>$get->gender,
                'dob'=>$get->dob,
                'phone_no'=>$get->phone_no,
                'home_address'=>$get->home_address,
                'your_email'=>$get->your_email,
                'webpage'=>$get->webpage,
                'your_image'=>$get->your_image,
                'affiliate_for'=>$get->affiliate_for,
                'business_category'=>$get->business_category,
                'extra_business_category'=>$get->extra_business_category,
                'besure_organization'=>$get->besure_organization,
                'sponsershipMemberid'=>$get->sponsershipMemberid,
                'payment_company_address'=>$get->payment_company_address,
                'payment_company_address_second'=>$get->payment_company_address_second,
                'payment_city'=>$get->payment_city,
                'payment_chapter_state'=>$get->payment_chapter_state,
                'payment_zip_code'=>$get->payment_zip_code,

                'head_shot'=>$get->head_shot,
                'fb_social_url'=>$get->fb_social_url,
                'instagram_social_url'=>$get->instagram_social_url,   
                'linkedin_social_url'=>$get->linkedin_social_url,   
                'youtube_social_url'=>$get->youtube_social_url,   
                'tiktok_user_name'=>$get->tiktok_user_name,   
                'where_submit_review'=>$get->where_submit_review,  

                'isactive'=>1,
                'created_at'=>$get->created_at, 
                'isdelete'=>$get->isdelete, 
                'deleted_at'=>$get->deleted_at, 
                'created_by'=>$get->created_by, 
                'updated_by'=>$get->updated_by, 
                'deleted_by'=>$get->deleted_by, 
                'status'=>$get->status, 
                'updated_at'=>$get->updated_at,
            ];
            
            DB::table('member_info_tbl')->insert(
                $array
            );               
            
           // self::saveAffiliateInof($get);
            self::saveBusinessInof($get);
            self::saveExperienceInof($get);
           // self::saveInvestmentInof($get);
            self::savePaymentInof($get);
            //self::saveSociaInof($get);
            //self::saveRequirementsInof($get);
        }
    }

    static function savePaymentInof($get){
        $array = [
            'uuid'=>$get->id,
            'payment_info'=>$get->payment_info,
            'bank_name'=>$get->bank_name,
            'account_number'=>$get->account_number,   
            'account_name'=>$get->account_name,   
            'rotation_number'=>$get->rotation_number,   
            'card_holer_name'=>$get->card_holer_name,   
            'card_number'=>$get->card_number,   
            'expiry_date_m'=>$get->expiry_date_m,   
            'expiry_date_y'=>$get->expiry_date_y,  
            'cv_code'=>$get->cv_code,   
            'isactive'=>1,
            'created_at'=>$get->created_at, 
            'isdelete'=>$get->isdelete, 
            'deleted_at'=>$get->deleted_at, 
            'created_by'=>$get->created_by, 
            'updated_by'=>$get->updated_by, 
            'deleted_by'=>$get->deleted_by, 
            'status'=>1,
            'updated_at'=>$get->updated_at,
        ];
    
        DB::table('member_payment_tbl')->insert(
            $array
        );      
    }

    static function saveBusinessInof($get){
        $array = [
            'uuid'=>$get->id,
            'state'=>$get->state,
            'city'=>$get->city,
            'zip_code'=>$get->zip_code, 
            'biography'=>$get->biography, 
            'business_name'=>$get->business_name, 
            'business_phone_no'=>$get->business_phone_no, 
            'business_website'=>$get->business_website, 
            'business_email'=>$get->business_email, 
            'company_address'=>$get->company_address, 
            'company_address_second'=>$get->company_address_second, 
            'business_card_upload'=>$get->business_card_upload,
            'business_about'=>$get->business_about,            
            'isactive'=>1,
            'created_at'=>$get->created_at, 
            'isdelete'=>$get->isdelete, 
            'deleted_at'=>$get->deleted_at, 
            'created_by'=>$get->created_by, 
            'updated_by'=>$get->updated_by, 
            'deleted_by'=>$get->deleted_by, 
            'status'=>1,
            'updated_at'=>$get->updated_at,
        ];
    
        DB::table('member_biz_tbl')->insert(
            $array
        );    
    }

    static function saveExperienceInof($get){
        $array = [
            'uuid'=>$get->id,
            'position_in_comp'=>$get->position_in_comp,
            'position_working_year'=>$get->position_working_year,
            'position_working_industry'=>$get->position_working_industry, 
            'primary_occupation'=>$get->primary_occupation, 
            'bonded'=>$get->bonded, 
            'any_professional_licenses'=>$get->any_professional_licenses, 
            'currently_member_of_network'=>$get->currently_member_of_network, 


            'six_month_payment'=>$get->six_month_payment,
            'two_quarterly_payment'=>$get->two_quarterly_payment,
            'single_one_time_payment'=>$get->single_one_time_payment, 
            'twelve_month_payment'=>$get->twelve_month_payment, 
            'four_quarterly_payment'=>$get->four_quarterly_payment, 
            'ach_withdrawal'=>$get->ach_withdrawal, 
            'twentyfour_month_payment'=>$get->twentyfour_month_payment, 
            'one_time_payment'=>$get->one_time_payment,
            'credit_card'=>$get->credit_card,


            'guidelines_0'=>$get->guidelines_0,
            'guidelines_1'=>$get->guidelines_1,
            'guidelines_2'=>$get->guidelines_2,   
            'guidelines_3'=>$get->guidelines_3,   
            'guidelines_4'=>$get->guidelines_4,   

            
            'isactive'=>1,
            'created_at'=>$get->created_at, 
            'isdelete'=>$get->isdelete, 
            'deleted_at'=>$get->deleted_at, 
            'created_by'=>$get->created_by, 
            'updated_by'=>$get->updated_by, 
            'deleted_by'=>$get->deleted_by, 
            'status'=>1,
            'updated_at'=>$get->updated_at,
        ];
    
        DB::table('member_experience_tbl')->insert(
            $array
        ); 
    }

}
 