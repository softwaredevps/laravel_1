<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AssociateApplication extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('associate_application', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->index();
            $table->string('uuid');
            $table->text('affiliate_for');
            $table->text('first_name');
            $table->text('last_name');
            $table->text('business_category');
            $table->text('besure_organization');  
            $table->text('selennine_one_time_payment'); 
            $table->text('prefix');
            $table->text('suffix');
            $table->text('gender');
            $table->text('dob');
            $table->text('phone_no');
            $table->text('home_address');
            $table->text('your_email');
            $table->text('webpage');
            $table->text('country');
            $table->text('state');
            $table->text('city');
            $table->text('zip_code');
            $table->text('biography');
            $table->text('business_name');
            $table->text('business_phone_no');
            $table->text('business_website');
            $table->text('business_email');
            $table->text('company_address');
            $table->text('business_card_upload');
            $table->string('business_about');
            $table->text('position_in_comp');
            $table->string('position_working_year');
            $table->text('position_working_industry');
            $table->text('primary_occupation');
            $table->string('bonded');
            $table->text('any_professional_licenses');
            $table->string('currently_member_of_network');
            $table->text('head_shot');
            $table->string('fb_social_url');
            $table->text('instagram_social_url');
            $table->string('linkedin_social_url');
            $table->text('youtube_social_url');
            $table->text('tiktok_user_name');
            $table->text('where_submit_review');
            $table->string('guidelines_0');
            $table->text('guidelines_1');
            $table->string('guidelines_2');
            $table->string('guidelines_3');
            $table->string('guidelines_4');
            $table->string('Investment_1');
            $table->text('six_month_payment');
            $table->text('two_quarterly_payment');
            $table->text('single_one_time_payment');
          
          
            $table->text('twelve_month_payment');
          
         
            $table->string('four_quarterly_payment');
            $table->string('ach_withdrawal');
            $table->string('one_time_payment');
            $table->string('twentyfour_month_payment');

            $table->text('credit_card');
            $table->text('payment_info');
            $table->text('bank_name');
            $table->text('account_number');
            $table->text('account_name');
            $table->text('rotation_number');
            $table->text('card_holer_name');
            $table->text('card_number');
            $table->text('expiry_date_m');
            $table->text('expiry_date_y');
            $table->text('cv_code');
            $table->text('your_answer');

            $table->string('isactive');
            $table->string('isdelete');
            $table->string('created_at');
            $table->string('updated_at');
            $table->string('deleted_at');
            $table->string('created_by');
            $table->string('updated_by');
            $table->string('deleted_by');
            $table->string('status')->default(1);  
        });  
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('associate_application');
    }
}
