<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUserNetworkinfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_networkinfo', function (Blueprint $table) {
            $table->increments('id',true);
            $table->integer('uuid')->unsigned();
            $table->string('chapter_name');
            $table->string('category');
            $table->string('area');
            $table->string('join_date');
            $table->string('type');
            $table->string('badge_color');
            $table->string('nts_completed');
            $table->string('video_url');
            $table->string('sponser_id');
            $table->string('sponser_name');
            $table->string('sponser_img');
            $table->string('sponser_category');
            $table->string('sponser_badge_color');
            $table->string('sponser_phone_no');
            $table->string('sponser_email');
            $table->timestamps();
        });

        Schema::table('user_networkinfo', function($table) {
            $table->foreign('uuid')->references('id')->on('userprofile');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
