<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserbusinessinfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('userbusinessinfo', function (Blueprint $table) {
            $table->increments('id',true);
            $table->integer('uuid')->unsigned();
            $table->string('name');
            $table->string('website');
            $table->string('social_links');
            $table->string('business_img');
            $table->string('business_phone_no');
            $table->string('business_office_no');
            $table->string('business_email');
            $table->string('business_address');
            $table->string('business_about');
            $table->string('business_close_time');
            $table->string('business_open_time');
            $table->string('url');
            $table->timestamps();
        });

        Schema::table('userbusinessinfo', function($table) {
            $table->foreign('uuid')->references('id')->on('userprofile');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('userbusinessinfo');
    }
}
