<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCardImgInBusinesstable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('userbusinessinfo', function(Blueprint $table) {
            $table->string('business_img_card'); // url
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('userbusinessinfo', function (Blueprint $table) {
            Schema::dropIfExists('business_img_card');
        });
    }
}
