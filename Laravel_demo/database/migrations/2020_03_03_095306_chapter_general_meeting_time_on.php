<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChapterGeneralMeetingTimeOn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('chapter_tbl', function(Blueprint $table) {
            $table->string('chapter_general_meeting_time_on'); // url
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('chapter_tbl', function (Blueprint $table) {
            Schema::string('chapter_general_meeting_time_on');
        });
    }
}
