<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Businesscategoriestbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_categories_tbl', function (Blueprint $table) 
        {
            $table->bigIncrements('id',true);
            $table->string('bizcatname');
            $table->string('bizcatdescription');
            $table->string('isactive');
            $table->string('isdelete');
            $table->string('updatedon');
            $table->string('updatedt');
            $table->string('deletedon');
            $table->string('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business_categories_tbl');
    }
}
