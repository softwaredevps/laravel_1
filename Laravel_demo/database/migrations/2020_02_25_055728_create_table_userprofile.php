<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableUserprofile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('userprofile', function (Blueprint $table) {
            $table->increments('id',true);
            $table->integer('uuid')->unsigned();
            $table->string('prefix');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->unique();
            $table->string('suffix');
            $table->string('preferred_name');
            $table->string('img_url');
            $table->string('gender');
            $table->string('dob')->nullable();
            $table->string('website');
            $table->string('phone',15)->unique();
            $table->string('cell_no');
            $table->string('shipping_address');
            $table->string('biography');
            $table->string('badge_color');
            $table->string('member_since');
            $table->string('membership_type');
            $table->boolean('is_active')->default(1);
            $table->timestamps();
        });

        Schema::table('userprofile', function($table) {
            $table->foreign('uuid')->references('id')->on('users');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_userprofile');
    }
}
