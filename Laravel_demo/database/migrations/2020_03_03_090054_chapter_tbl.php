<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChapterTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chapter_tbl', function (Blueprint $table) 
        {
            $table->bigIncrements('id',true);
            $table->string('uuid')->nullable();
            $table->string('chapter_founded_On')->nullable();
            $table->string('chapter_website')->nullable();
            $table->string('chapter_description')->nullable();
            $table->string('chapter_address')->nullable();
            $table->string('chapter_city')->nullable();
            $table->string('chapter_state')->nullable();
            $table->string('chapter_country')->nullable();
            $table->string('chapter_phone_no')->nullable();
            $table->string('chapter_general_meeting_day')->nullable();
            $table->string('chapter_general_meeting_time_from')->nullable();
            $table->string('isActive')->nullable();
            $table->string('isDelete')->nullable();
            $table->string('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chapter_tbl');
    }
}
