<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChairMemberPositionTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chair_member_positionTbl', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->index();
            $table->string('uuid');
            $table->string('chair_member_position');
            $table->integer('is_active')->default(1);  
            $table->integer('is_delete')->default(0);  
            $table->string('updated_on');
            $table->string('deleted_on');
            $table->string('status')->default(1);  
            $table->timestamps();   
        });  
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chair_member_positionTbl');
    }
}
