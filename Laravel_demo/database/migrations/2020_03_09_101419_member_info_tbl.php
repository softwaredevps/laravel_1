<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MemberInfoTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_info', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->index();
            $table->string('uuid');
            $table->string('member_id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('suffix');
            $table->string('preferred_name');
            $table->string('birthdate');
            $table->string('biography');
            $table->string('chapter_id');
            $table->string('chapter_joining_date');
            $table->string('BizCategoryID');
            $table->string('BadgeColorID');
            $table->string('membershipIsActive');
            $table->string('membershipSponsoredBy');
            $table->integer('is_active')->default(1);  
            $table->integer('is_delete')->default(0);  
            $table->string('updated_on');
            $table->string('deleted_on');
            $table->string('status')->default(1);  
            $table->timestamps();         
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_info');
    }
}
