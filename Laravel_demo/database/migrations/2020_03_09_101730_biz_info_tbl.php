<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BizInfoTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('biz_info_tbl', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->index();
            $table->string('uuid');
            $table->string('biz_id');
            $table->string('member_id');
            $table->string('biz_name');
            $table->string('biz_website');
            $table->string('biz_description');
            $table->string('biz_video_url');
            $table->string('biz_address1');
            $table->string('biz_address2');
            $table->string('biz_city');
            $table->string('biz_state');
            $table->string('biz_zip');
            $table->string('biz_country');
            $table->integer('is_active')->default(1);  
            $table->integer('is_delete')->default(0);  
            $table->string('updated_on');
            $table->string('deleted_on');
            $table->string('status')->default(1);  
            $table->timestamps();   
        });      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('biz_info_tbl');
    }
}
