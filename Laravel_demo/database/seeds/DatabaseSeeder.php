<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(UsersTableSeeder::class);
        $this->call(countrylist::class);
        $this->call(statelist::class);
        $this->call(citysdata::class);
        $this->call(days_table::class);  
    }
}
