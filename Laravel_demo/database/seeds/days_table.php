<?php

use Illuminate\Database\Seeder;

class days_table extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('operation_days_tbl')->delete();
        $days = array(
            array('day_name' => "Monday",'day_display_name' => 'Monday','day_display_order' => 1),
            array('day_name' => "Tuesday",'day_display_name' => 'Tuesday','day_display_order' => 2),
            array('day_name' => "Wednesday",'day_display_name' => 'Wednesday','day_display_order' => 3),
            array('day_name' => "Thursday",'day_display_name' => 'Thursday','day_display_order' => 4),
            array('day_name' => "Friday",'day_display_name' => 'Friday','day_display_order' => 5),
            array('day_name' => "Saturday",'day_display_name' => 'Saturday','day_display_order' => 6),
            array('day_name' => "Sunday",'day_display_name' => 'Sunday','day_display_order' => 7),
		);
        DB::table('operation_days_tbl')->insert($days);
    }
}

