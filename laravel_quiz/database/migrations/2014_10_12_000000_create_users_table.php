<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email', 250)->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('password_hash');
            $table->string('google_token');
            $table->string('fb_token');
            $table->string('mobile');
            $table->string('picture');
            $table->string('gender');
            $table->enum('status', ['0','1', '2', '3'])->comment = '0=>InActive, 1=>Active, 2=> Blocked, 3=>Spam';
            $table->enum('userRole', ['1', '2', '3', '4'])->comments = '1=>Admin, 2=> Editor, 3=>Registered User, 4=>Guest';
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
