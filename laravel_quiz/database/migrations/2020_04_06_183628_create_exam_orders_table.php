<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExamOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exam_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('order_num');
            $table->integer('user_id');
            $table->string('order_amount', 100);
            $table->string('order_actual_amount', 100);
            $table->string('order_discount', 100)->default('0');
            $table->integer('coupon_id')->default(0);
            $table->string('transaction_id', 100)->nullable();
            $table->string('order_response_code', 20)->nullable();
            $table->string('payment_mode', 100)->nullable();
            $table->date('transaction_date')->nullable();
            $table->string('bank_transaction_id', 100)->nullable();
            $table->string('bank_name', 100)->nullable();
            $table->string('currency', 10)->nullable();
            $table->string('payment_gateway', 10)->nullable();
            $table->string('order_message')->nullable();
            $table->enum('status', ['0','1', '2', '3'])->comment = '0=>Pending, 1=>Done, 2=> Cancel, 3=>Refund';
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exam_orders');
    }
}
