<?php

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::group(['prefix' => 'v1' ], function() {
    Route::post('/login', 'Api\V1\AuthController@login');

    Route::post('/register', 'Api\V1\AuthController@register');

    Route::post('/forgot/password', 'Api\V1\AuthController@forgotPasswordVerifyCode'); //send reset password link in mail
    Route::post('/reset/password', 'Api\V1\AuthController@passwordResetEmail'); //geyt email from token to validate token
    Route::post('/account/password/reset', 'Api\V1\AuthController@forgotPasswordReset'); // change password api route

    Route::post('/socialLogin', 'Api\V1\AuthController@socialLogin'); //social login

    Route::group(['middleware' => 'auth:api'], function() {
        
        Route::post('/getquestion/{id}', 'Api\V1\ExamController@getQuestions')->middleware('decodeIdParam'); //GET QUESTION
        Route::post('/updateProfile', 'Api\V1\UserController@updateProfile');
        Route::post('/updatePassword', 'Api\V1\UserController@updatePassword');
        Route::post('/exam/submitTest', 'Api\V1\ExamController@submitTest'); //SUBMIT TEST
        Route::get('/test/result/{id}', 'Api\V1\ExamController@testResult')->middleware('decodeIdParam'); //TEST RESULT
        Route::post('/exam/purchase', 'Api\V1\OrderController@purchaseExam');
        Route::post('/exam/getOrderDetail/{id}', 'Api\V1\OrderController@orderDetail'); //ORDER DETAIL
        Route::post('/myOrders', 'Api\V1\OrderController@myOrders'); //GET LOGIN USER ORDERS
        Route::post('/getOrderDetail/{id}', 'Api\V1\OrderController@orderDetail')->middleware('decodeIdParam'); //GET LOGIN USER ORDERS
        Route::post('/exam/reportQuestion', 'Api\V1\ExamController@reportQuestion'); //GET LOGIN USER ORDERS
        

       
    });
    
    Route::post('/report', 'Api\V1\ExamController@getScoreList'); //GET RESULT OF LOGIN USER
        
    Route::get('/categories', 'Api\V1\CategoryController@listCategories'); //FETCH CATEGORIES
    Route::get('/exams/{slug}/{id}', 'Api\V1\ExamController@listSubCategories'); //FETCH SUB CATEGORIES 
    Route::get('/exams/modules/{slug}/{id}', 'Api\V1\ExamController@listExams')->middleware('decodeIdParam'); //FETCH EXAMS 
    Route::get('/exam/detail/{id}', 'Api\V1\ExamController@examDetail')->middleware('decodeIdParam'); //FETCH EXAMS
    
    Route::post('/cart/getData', 'Api\V1\OrderController@getCartItems'); //FETCH CART ITEMS DETAIL
    
    Route::any('/search', 'Api\V1\SearchController@search');

    Route::post('/todo/add', 'Api\V1\BaseController@createTodo');
    
    Route::post('/contactUs', 'Api\V1\ContactusController@add');


//    Route::get('/getquestion/{id}', 'Api\V1\ExamController@getQuestions')->middleware('decodeIdParam');
    

});
