<?php

use App\User;

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

/* auth code */
Auth::routes();
Route::get('/', 'Auth\LoginController@index')->name('login');
Route::get('/register', 'Auth\LoginController@index')->name('login');

Route::group(['prefix' => 'admin'], function() {
    

    Route::group(['middleware' => ['auth', 'roles'], 'roles'=>['admin']], function() {
        Route::get('/', 'AdminController@dashboard')->name('home');
        Route::resource('/users', 'UsersController');
        Route::get('/users/changeStatus/{id}/{status}', 'UsersController@changeStatus');
        Route::get('/users/delete/{id}', 'UsersController@deleteUser');
        Route::post('/users/changePassword', 'UsersController@changePassword');
         
        
        Route::any('/category/add', 'CategoryController@add');
        Route::any('/category/list', 'CategoryController@listCategory');
        Route::any('/category/listedit/{id}', 'CategoryController@listedit');
        Route::any('/category/listdelete/{id}', 'CategoryController@listdelete');
        Route::any('/category/listactive/{id}/{status}', 'CategoryController@listactive');
        Route::any('/category/sub-category/{id}', 'CategoryController@listSubcategory');
        
        
        Route::any('/questions/add', 'QuestionsController@add');
        Route::any('/questions/list', 'QuestionsController@listQuestions');
        Route::any('/questions/listedit/{id}', 'QuestionsController@listedit');
        Route::any('/questions/listdelete/{id}', 'QuestionsController@listdelete');
        Route::any('/questions/listactive/{id}/{status}', 'QuestionsController@listactive');
        Route::get('/import_excel', 'ImportExcelController@index');
        Route::post('/import_excel/import', 'ImportExcelController@import');
        Route::get('/questions/reports', 'QuestionsController@reports');
        Route::get('/questions/reports/{id}', 'QuestionsController@reports');
        Route::post('/questions/deleteImage', 'QuestionsController@deleteImage');
        
        Route::any('/exams/add', 'ExamsController@add');
        Route::any('/exams/edit/{id}', 'ExamsController@update');
        Route::any('/exams/list', 'ExamsController@listExams');
        Route::any('/exams/delete', 'ExamsController@delete');
        Route::any('/exams/listactive/{id}/{status}', 'ExamsController@listactive');


        Route::any('/articles/add', 'ArticlesController@add');
        Route::any('/articles/list', 'ArticlesController@listArticles')->name('articles');
        Route::any('/articles/listedit/{id}', 'ArticlesController@listedit');
        Route::any('/articles/listdelete/{id}', 'ArticlesController@listdelete');
        Route::any('/articles/listactive/{id}/{status}', 'ArticlesController@listactive');


        Route::any('/contact/list', 'ContactController@listContact')->name('contact');
        Route::any('/contact/listdelete/{id}', 'ContactController@listdelete');
        Route::any('/contact/listactive/{id}/{status}', 'ContactController@listactive');
        
        Route::any('/orders/couponAdd', 'OrderController@couponAdd');
        Route::any('/orders/couponList', 'OrderController@couponList')->name('couponList');
        Route::any('/orders/couponActive/{id}/{status}', 'OrderController@couponActive');
        Route::any('/orders/CouponEdit/{id}', 'OrderController@CouponEdit');

        
        Route::any('/articles/addNews', 'ArticlesController@addNews');
        Route::any('/articles/listNews', 'ArticlesController@listNews')->name('listNews');
        Route::any('/articles/newsActive/{id}/{status}', 'ArticlesController@newsActive');
        Route::any('/articles/newsEdit/{id}', 'ArticlesController@newsEdit');
        Route::any('/articles/newsdelete/{id}', 'ArticlesController@newsdelete');

        Route::get('/orders/list', 'OrderController@listOrders');
        Route::get('/orders/detail/{id}', 'OrderController@listOrders');
        Route::get('/orders/refund/{id}', 'OrderController@refund');

    });
    Route::get('/clear-cache', 'UsersController@clearCache'); // Clear Cache

});

//ORDER ROUTES STARTS
Route::group(['prefix' => 'order'], function() {
    Route::get('/purchase', '\App\Http\Controllers\OrderController@purchaseExam');
    Route::post('/payment', 'OrderController@eventOrderGen')->name('orderPayment');
    Route::any('/payment/status', 'OrderController@paymentCallback');
    Route::get('/billing-error', 'OrderController@paymentCallback');
    Route::get('/billing-thankyou', 'OrderController@paymentCallback');
});
//ORDER ROUTES ENDS


Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');
