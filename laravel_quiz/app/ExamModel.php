<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;
use App\ExamCategoryModel;

class ExamModel extends Model
{
    protected $table = "exams";
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'description', 'type', 'price', 'end_date', 'total_marks', 'total_time', 'status'
    ];
    protected $attributes = [
        'status' => '1',
    ];
    public static function boot(){
        parent::boot();

        static::creating(function ($instance){
            $instance->status = '1';
            $instance->created_at = $instance->updated_at = date('Y-m-d h:i:s');
            
        });
        
        static::saved(function ($instance){ //dd($instance);
            $getCategories = Self::select('id')->with('examCategories','examCategories.examCategory')->get();
            
            if(!empty($getCategories)) {
                foreach($getCategories->toArray() as $cats){
                    try{
                        \Cache::forget($cats['exam_categories'][0]['exam_category']['category_slug'].'Exams');
                    }catch(\Exception $e) {
                        
                    }
                }
            }
            
        });
    }
    
    public function examCategories()
    {
        return $this->hasMany('App\ExamCategoryModel', 'exam_id');
    }
    
    public function examQuestions()
    {
        return $this->hasMany('App\QuestionExamModel', 'exam_id');
    }
    
    public function savedQuestion() {
        return $this->hasMany('App\SavedQuestion', 'examId', 'id');
    }

    static function saveData($data){       // dd($data);
        $examData = $data;
        unset($examData['category_id']);
        $examData['status'] = '0';
        $examData['created_at'] = $examData['updated_at'] = date('Y-m-d h:i:s');
         $id = self::insertGetId($examData);
        if($id){
            if(isset($data['category_id']) && !empty($data['category_id'])){
                $examCat = [];
                foreach ($data['category_id'] as $k => $value) {
                    $examCat[$k]['category_id'] = $value;
                    $examCat[$k]['exam_id'] = $id;
                    $examCat[$k]['created_at'] = $examCat[$k]['updated_at'] = date('Y-m-d h:i:s');
                    try{
                        ExamCategoryModel::create($examCat[$k]);
                    }catch(\Exception $e){
                        echo $e->getMessage();die;
                    }
                }
                return true; 

            }else{
                return true;
            }
        }
    }
    static function listData(){
       return self::with('examQuestions')->orderBy('id', 'DESC')->get()->toArray();
    }
    static function listAllData($id){
        return self::with('examCategories:category_id,exam_id')->where('id',$id)->first()->toArray();
    }
    static function updateData($data){
         self::find($data['update_id'])->update($data);
        if(isset($data['category_id']) && !empty($data['category_id'])){
            
            DB::table('exam_categories')->where('exam_id', $data['update_id'])->delete();
            
            $examCat = [];
            foreach ($data['category_id'] as $k => $value) {
                $examCat[$k]['exam_id'] = $data['update_id'];
                $examCat[$k]['category_id'] = $value;
            }
            return DB::table('exam_categories')->insert($examCat); 

        }else{
            return true;
        }
    }
    static function listDelete($id){
        return self::where('id', $id)->delete();
    }
    static function listactive($id,$status){
        $res = array('status'=>$status);
        return self::find($id)->update($res);
    }
    static function getCategory($slug, $id=null){
        if($id){
            return self::where('category_slug',$slug)->where('id','!=',$id)->first();
        }else{
            return self::where('category_slug',$slug)->first();            
        }
    }

    
    static function getExamQuestion($examId, $params) {
        
        $questions = DB::select("select  SQL_CALC_FOUND_ROWS question.id, question, optionFirst, optionSecond, optionThird, optionFourth, (CASE when (question.type=1) THEN 'EN' ELSE 'HI' END) as questionType , question.description, question.image, exams.type as examType, exams.total_marks, exams.total_time, exams.title as examTitle, saved_questions.id as examResult from question 
LEFT JOIN question_exams ON question_exams.question_id = question.id
LEFT JOIN exams ON exams.id = question_exams.exam_id
LEFT JOIN saved_questions ON saved_questions.examId = question_exams.exam_id
where question_exams.exam_id = $examId and question.status = '1' 
ORDER BY question.id ASC limit ".$params['start'].",1");
        
        $rowCount = DB::select(DB::raw("SELECT FOUND_ROWS() AS 'total';"))[0]->total;
        
        return ['data'=> $questions, 'total'=> $rowCount];
    }

    static function saveQuestion($data){
        $user = DB::table('saved_questions')->where('examId', $data['examId'])->where('user_id', $data['user_id'])->first();
    
//        if(isset($user->examId) && $user->examId){        
//            $data['updated_at'] =  date('Y-m-d h:i:s');
//            $result = DB::table('saved_questions')
//            ->where('examId', $data['examId'])
//            ->where('user_id',  $data['user_id'])
//            ->update($data);
//        }else{
            $data['created_at'] =  date('Y-m-d h:i:s');
            $data['updated_at'] =  date('Y-m-d h:i:s');
            $result = DB::table('saved_questions')->insertGetId($data);
//        }
        return $result;
    }

    static function getScoreList($params){
        $res = DB::table('saved_questions')
                ->select('saved_questions.id as resultId','category_name','examId',\DB::raw('CONCAT("'. url('/public').'/", image) as image'),'json_data_save','user_id','title')
        ->join('exams', 'exams.id', '=', 'saved_questions.examId')
        ->join('category', 'category.id', '=', 'saved_questions.examId')->where('user_id', $params['user_id'])->orderBy('saved_questions.id','DESC')->offset($params['page'])->limit($params['limit'])->get();
        return $res;
    }
    
    static function getExamDetail($id, $userId='') {
        $res = self::with(['savedQuestion'=> function ($q) use($userId) {
            $q->select('id','examId')->where('user_id', $userId);
        } ])->select('id','title','description','total_time')->withCount('examQuestions')->find($id);
        return $res;
    }

    static function saveCoupons($array){
        try{
            return DB::table('coupons')->insert($array); 
        }catch(\Exception $e) {
            return 0;
        }
        
    }

    static function getCouponList(){
        return DB::table('coupons')->where('status', 0)->get();
    }

    static function couponActive($id,$status){
        $res = array('status'=>!$status);
        return DB::table('coupons')->where('id',$id)->update($res);
    }

    static function listAllCoupon($id){
       return  DB::table('coupons')->where('id', $id)->first();
    }

    static function updateCoupon($id,$array){
        return DB::table('coupons')->where('id',$id)->update($array);
    }

    static function saveNews($array){
        try{
            return DB::table('news')->insert($array); 
        }catch(\Exception $e) {
            return 0;
        }
    }
}
