<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Request;
use DB; 


class QuestionExamModel extends Model
{
        protected $table = "question_exams";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'question_id', 'exam_id', 'created_at', 'updated_at'
    ];
    
    public static function boot(){
        parent::boot();

        static::creating(function (QuestionExamModel $item){
//            $instance->created_at = $instance->updated_at = \Carbon\Carbon::now();
//            dd($item);
        });
    }
    
    public function examData()
    {
        return $this->hasOne('App\ExamCategoryModel', 'id', 'exam_id');
    }
    
    public function getQuestions()
    {
        return $this->hasOne('App\Question', 'id', 'question_id');
    }
    
    public static function listQuestionWithExams($examId) {
        return self::with('getQuestions')->where('exam_id',$examId)->get()->toArray();
    }
}


