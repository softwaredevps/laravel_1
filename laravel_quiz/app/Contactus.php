<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Contactus extends Model
{
    protected $table = 'contactus';
    
    protected $fillable = [
        'name', 'email', 'subject', 'message', 'status', 'created_at', 'updated_st'
    ];
    
    public static function boot(){
        parent::boot();

        static::creating(function ($instance){
            $instance->status = '1';
            $instance->created_at = $instance->updated_at = date('Y-m-d h:i:s');
            
        });
        
    }
    
    static function saveData($data){
        $data = array(
        'name'=>trim($data['name']),
        'email'=>trim($data['email']),
        'subject'=>trim($data['subject']),
        'message'=>trim($data['message']),
        'status'=>1);
        return self::create($data);
    }

    static function listContact(){
        return self::get()->toArray();
     }
     static function listDelete($id){
         return  self::find($id)->delete();
     }
     static function listactive($id,$status){
         $res = array('status'=>!$status);
         return self::where('id',$id)->update($res);
     }
}
