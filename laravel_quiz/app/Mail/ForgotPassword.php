<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ForgotPassword extends Mailable
{

    
    use Queueable, SerializesModels;

    public $content;
    public $subject;
    public $receiver;
          /**
     * Create a new message instance.
     *      
     * @return void
     */  
        public function __construct($subject, $data, $receiver){

            $this->content = $data;
            $this->subject = $subject;
            $this->receiver = $receiver;
           
        }



    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {  
        
        return $this->markdown('mails.forgot_password')
                ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
                ->subject($this->subject)
                ->with('token', $this->content);
    }
}
