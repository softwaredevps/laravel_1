<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ResetPasswordMail extends Mailable
{

    
    use Queueable, SerializesModels;

        public $event; 
          /**
     * Create a new message instance.
     *      
     * @return void
     */  
        public function __construct($userRec,$token){
            $this->event = $userRec; 
            $this->token = $token; 
           
        }



    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {  
    $dataShow = $this->event;
    $token = $this->token;

        return $this->subject('Reset Password') 
           ->view('emails.reset_password',compact('dataShow','token'));
    }
}
