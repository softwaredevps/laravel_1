<?php 
use Illuminate\Support\Facades\DB;

    if (!function_exists('usersList')) {
    
        function usersList()
        {
            try { 
                $get = DB::table('users')->select('*')->where('userRole','!=',1)->get();

            } catch(\Illuminate\Database\QueryException $ex){ 
                dd($ex->getMessage()); 
            }
            return $get;
        }
    }

    if (!function_exists('getCategory')) {
    
        function getCategory()
        {
            try { 
                $get = DB::table('category')->select('*')->where('status',1)->get();

            } catch(\Illuminate\Database\QueryException $ex){ 
                dd($ex->getMessage()); 
            }
            return $get;
        }
    }

    if (!function_exists('getCoupon')) {
    
        function getCoupon()
        {
            try { 
                $get = DB::table('coupons')->select('*')->orderBy('id', 'desc')->get();

            } catch(\Illuminate\Database\QueryException $ex){ 
                dd($ex->getMessage()); 
            }
            return $get;
        }
    }

    if (!function_exists('getUserNameById')) {
    
        function getUserNameById($id)
        {
            try { 
                $get = DB::table('users')->select('*')->where('id', $id)->first();

            } catch(\Illuminate\Database\QueryException $ex){ 
                dd($ex->getMessage()); 
            }
            return $get;
        }
    }
 
    if (!function_exists('getCategoryNameById')) {
    
        function getCategoryNameById($id)
        {
            try { 
                $get = DB::table('category')->select('*')->where('id', $id)->first();

            } catch(\Illuminate\Database\QueryException $ex){ 
                dd($ex->getMessage()); 
            }
            return $get;
        }
    }
    
?>