<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\QuestionExamModel;
class Question extends Model
{
        protected $table = "question";
        protected $primaryKey = 'id';
        protected $foreignKey = 'question_id';


     public function questionExams()
    {
        return $this->hasMany('App\QuestionExamModel', 'question_id');
    }
    
    
    static function saveData($data){
    
        $dataSave = array(
            'question'=>trim($data['questionDesc']),
            'optionFirst'=>trim($data['optionFirst']),
            'optionSecond'=>trim($data['optionSecond']),
            'optionThird'=>trim($data['optionThird']),
            'optionFourth'=>trim($data['optionFourth']),
            'correctAnswer'=>trim($data['rightAnswer']),
            'description'=>trim($data['description']),
            'type'=>trim($data['type']),
            'status'=>isset($data['status']) ? $data['status'] : '1',
        );
        $id = self::insertGetId($dataSave);
        if($id){
            if(isset($data['exams']) && !empty($data['exams'])){
                $questionCat = [];
                foreach ($data['exams'] as $k => $value) {
                    $questionCat[$k]['question_id'] = $id;
                    $questionCat[$k]['exam_id'] = $value;
                    $questionCat[$k]['created_at'] = $questionCat[$k]['updated_at'] = date('Y-m-d h:i:s');
                    try{
                        QuestionExamModel::create($questionCat[$k]);
                    }catch(\Exception $e){
                        echo $e->getMessage();die;
                    }
                }
                return $id; 

            }else{
                return $id;
            }
        }
    }

    static function listData(){
        return self::orderBy('id', 'DESC')->get()->toArray();
    }

    static function listAllData($id){
//        DB::enableQueryLog(); // Enable query log

        $question = DB::select('select `question`.*, `question_categories`.`category_id` from `question` left join `question_categories` on `question_categories`.`question_id` = `question`.`id` where `question`.`id` = '.$id);
//        dd(DB::getQueryLog()); // Show results of log

        echo '<pre>'; print_r($question);die;
        if(isset($question) && !empty($question)){
            return $question;
        }else{
            return false;
        }
    }
    static function updateData($data){
        $res = array(
            'question'=>trim($data['questionDesc']),
            'optionFirst'=>trim($data['optionFirst']),
            'optionSecond'=>trim($data['optionSecond']),
            'optionThird'=>trim($data['optionThird']),
            'optionFourth'=>trim($data['optionFourth']),
            'correctanswer'=>trim($data['rightAnswer']),
            'description'=>trim($data['description']),
            'type'=>trim($data['type']),
        );
        $updateQuestion = self::where('id',$data['update_id'])->update($res);
        
        if(isset($data['exams']) && !empty($data['exams'])){
            
            DB::table('question_exams')->where('question_id', $data['update_id'])->delete();
            
            $questionCat = [];
            foreach ($data['exams'] as $k => $value) {
                $questionCat[$k]['question_id'] = $data['update_id'];
                $questionCat[$k]['exam_id'] = $value;
            }
            return DB::table('question_exams')->insert($questionCat); 

        }else{
            return true;
        }
    }
    static function listDelete($id){
        return self::where('id', $id)->delete();
    }
    static function listactive($id,$status){
        $res = array('status'=>$status);
        return self::where('id',$id)->update($res);
    }
    
    static function getLinks($id) {
        $prevUrl = DB::select("SELECT id FROM `question` WHERE id < $id order by id desc limit 0,1");
        $nextUrl = DB::select("SELECT id FROM `question` WHERE id > $id order by id ASC limit 0,1");
        return ['prevUrl'=>$prevUrl, 'nextUrl'=>$nextUrl];
    }
}
