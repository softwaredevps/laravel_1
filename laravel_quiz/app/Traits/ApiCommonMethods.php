<?php
namespace App\Traits;
use App\User;
use App\OauthAccessToken;

trait ApiCommonMethods
{
    protected $newDateFormat = 'd.m.Y H:i';


    // save the date in UTC format in DB table
    public function setCreatedAtAttribute($date){

        $this->attributes['created_at'] = Carbon::parse($date);

    }
    
     public function sendResponse($result, $message) {

        $response = [
            'status' => true,
            'data' => $result,
            'message' => $message,
        ];


        echo json_encode($response);
    }

    /**

     * return error response.

     *

     * @return \Illuminate\Http\Response

     */
    public function sendError($error, $errorMessages = [], $code = 404) {

        $response = [
            'status' => false,
            'message' => $error,
        ];


        if (!empty($errorMessages)) {

            $response['data'] = $errorMessages;
        }


        echo json_encode($response, $code);
		exit();
    }
    
    public function getAuthUser($token=null){
        echo '<pre>';
        print_r($user);die;
        echo '</pre>';
    }
    
    public function decodeId($id) {
        return ord(substr($id, env('EncodedChar'),-env('EncodedChar')));
    }

}