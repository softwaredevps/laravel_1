<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Request;
use DB; 


class QuestionCategory extends Model
{
        protected $table = "question_categories";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'question_id', 'category_id'
    ];
    
    public static function boot(){
        parent::boot();

        static::creating(function ($instance){
            $instance->status = '1';
            $instance->created_at = $instance->created_at = \Carbon\Carbon::now();
            
        });
    }
    
    public function categoryData()
    {
        return $this->hasOne('App\CategoryModel', 'id', 'category_id');
    }
}


