<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;

class CategoryModel extends Model
{
    protected $table = "category";
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_name', 'category_slug', 'image', 'status', 'parent_category_id', 'price'
    ];
    public static function boot(){
        parent::boot();

        static::saved(function ($instance){
            \Cache::forget('categories');
        });
        static::saving(function ($instance){
            \Cache::forget('categories');
        });
    }
    
    public function parentCategory() {
        return $this->belongsTo('App\CategoryModel', 'parent_category_id', 'id');
    }
    
    public function childCategory() {
        return $this->hasMany('App\CategoryModel', 'parent_category_id', 'id');
    }
    
    public function getOrderCategory() {
        return $this->hasMany('App\ExamOrderDetail', 'category_id', 'id');
    }
    
    public function getCategoryExams() {
        return $this->hasMany('App\ExamCategoryModel', 'category_id');
    }
    
    static function saveData($data){
        $data = array('category_name'=>trim($data['categoryname']),'category_slug'=>trim($data['categoryslug']),'image'=>trim($data['image']),'status'=>1,'parent_category_id'=>trim($data['parent_category_id']),'price'=>trim($data['price']));
        return self::create($data);
    }
    static function listData($id='parent'){
        $cond = ($id == 'parent') ? 0 : $id;
       return self::where('parent_category_id', '=', $cond)->orderBy('id', 'DESC')->withCount('childCategory')->get()->toArray();
    }
    //list category data
    static function listAllData($id){
        return self::with('parentCategory:id,category_name')->where('id',$id)->first()->toArray();
    }
    static function updateData($data){
        $res = array('category_name'=>trim($data['categoryname']),'category_slug'=>trim($data['categoryslug']),'image'=>trim($data['image']),'parent_category_id'=>trim($data['parent_category_id']), 'price'=>trim($data['price']));
        return self::find($data['update_id'])->update($res);
    }
    static function listDelete($id){
        return self::where('id', $id)->delete();
    }
    static function listactive($id,$status){
        $res = array('status'=>$status);
        return self::where('id',$id)->update($res);
    }
    static function getCategory($slug, $id=null){
        if($id){
            return self::where('category_slug',$slug)->where('id','!=',$id)->first();
        }else{
            return self::where('category_slug',$slug)->first();            
        }
    }
    
    static function getParentCategry() {
        $res = self::where(['status'=>'1', 'parent_category_id'=>0])->get();
        $data = [];
        if(!empty($res)){
            $data = $res->toArray();
        }
        return $data;
    }
}
