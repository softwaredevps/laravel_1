<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class SavedQuestion extends Model
{
    protected $table = "saved_questions";
    protected $primaryKey = 'id';
    protected $foreignKey = 'user_id';
    
    public function exams()
    {
        return $this->belongsTo('App\ExamModel', 'examId');
    }
    

}
