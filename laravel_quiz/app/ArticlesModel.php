<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class ArticlesModel extends Model
{
    protected $table = 'article';
    use SoftDeletes;
    
    static function saveData($data){
        $data = array('article_name'=>trim($data['articlename']),'article_desc'=>trim($data['articledesc']),'status'=>1);
        return self::insert($data);
    }
    static function listData(){
       return self::get()->toArray();
    }
    static function listAllData($id){
        return self::where('id',$id)->first()->toArray();
    }
    static function updateData($data){
        $res = array('article_name'=>trim($data['articlename']),'article_desc'=>trim($data['articledesc']));
        return self::where('id',$data['update_id'])->update($res);
    }
    static function listDelete($id){
//        return self::where('id', $id)->delete();
        return  self::find($id)->delete();
    }
    static function listactive($id,$status){
        $res = array('status'=>!$status);
        return self::where('id',$id)->update($res);
    }


    static function saveNews($array){
        try{
            return DB::table('news')->insertGetId($array); 
        }catch(\Exception $e) {
            return 0;
        }
    }

    static function listNews(){
        try{
            return DB::table('news')->get(); 
        }catch(\Exception $e) {
            return 0;
        }
    }

    static function listNewsById($id){
        try{
            return DB::table('news')->where('id',$id)->first(); 
        }catch(\Exception $e) {
            return 0;
        }
    }

    static function updateNews($array,$id){
        return DB::table('news')->where('id',$id)->update($array);
    }

    static function listactiveNews($id,$status){
        $res = array('status'=>!$status);
        return DB::table('news')->where('id',$id)->update($res);
    }

    static function newsdelete($id){
        return DB::table('news')->where('id',$id)->delete();
    }
}
