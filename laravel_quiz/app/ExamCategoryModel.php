<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Request;
use DB; 


class ExamCategoryModel extends Model
{
        protected $table = "exam_categories";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'exam_id', 'category_id', 'created_at', 'updated_at'
    ];
    
    public function examData()
    {
        return $this->hasOne('App\ExamModel', 'id', 'exam_id');
    }
    
    public function examCategory() {
        return $this->belongsTo('App\CategoryModel', 'category_id','id');
    }
    
    public function orderExam() {
        return $this->belongsTo('App\ExamOrder', 'category_id', 'category_id');
    }
}


