<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class QuestionsModel extends Model
{
    protected $table = "question";
    protected $primaryKey = 'id';
    static function saveData($data){
        $data = array(
            'question'=>trim($data['questionDesc']),
            'questionfirst'=>trim($data['questionfirst']),
            'questionsecond'=>trim($data['questionSecond']),
            'questionthrid'=>trim($data['questionThrid']),
            'questionfourth'=>trim($data['questionFourth']),
            'currectanswer'=>trim($data['rightAnswer']),
            'type'=>trim($data['type']),
        );
        return self::insert($data);
    }

    static function listData(){
        return self::get()->toArray();
    }

    static function listAllData($id){
        return self::where('id',$id)->first()->toArray();
    }
    static function updateData($data){
     //   dd($data);
        $res = array(
            'question'=>trim($data['questionDesc']),
            'questionfirst'=>trim($data['questionfirst']),
            'questionsecond'=>trim($data['questionSecond']),
            'questionthrid'=>trim($data['questionThrid']),
            'questionfourth'=>trim($data['questionFourth']),
            'currectanswer'=>trim($data['rightAnswer']),
            'type'=>trim($data['type']),
        );
        return self::where('id',$data['update_id'])->update($res);
    }
    static function listDelete($id){
        return self::where('id', $id)->delete();
    }
    static function listactive($id,$status){
        $res = array('status'=>!$status);
        return self::where('id',$id)->update($res);
    }
}
