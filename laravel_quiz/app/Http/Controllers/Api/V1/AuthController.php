<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;
use App\Traits\ApiCommonMethods;
use App\User;
use App\PasswordReset;
use Auth;
use Validator;
use DB;
use Hash;
use Crypt;
use Mail;
use Config;
use App\Mail\RegisterEmail;
use App\Mail\ForgotPassword;
use App\Jobs\SendForgotPasswordMail;
//use Parser;
class AuthController extends Controller {

    use ApiCommonMethods;

    protected $clientSecret;
    protected $FRONT_URL;

    public function __construct() {
        $this->clientSecret = '3DOsh25EjeBa463siUcP9m4kyhCnifkQ8ZnsQqua'; //GRANT ID
        $this->clientId = '2';
        $this->FRONT_URL = env("FRONT_URL");
    }

    /**

     * Register api

     *

     * @return \Illuminate\Http\Response

     */
    public function register(Request $request) {
        $credentials = $request->only('email', 'password','firstname','lastname');
        $validator = Validator::make($request->all(), [
                    'email'     => 'required',
                    'password'  => 'required',
                    'firstname' => 'required',
                    'lastname'  => 'required',
        ]);


        if ($validator->fails()) {

            $this->sendError('Validation Error.', $validator->errors());
            die;
        }
        $user = User::getUser($credentials['email']);
        
        if (empty($user)) {
            $input = $request->all();
            //SAVE USER DATA
            $email = explode('@', $input['email']);
            $input['email'] = $input['email'];
            $input['password'] = $input['password'];
            $input['first_name'] = $input['firstname'];
            $input['last_name'] = $input['lastname'];
            $input['picture'] = "";
            $input['fb_token'] = "";
            $input['mobile'] = "";
            $input['google_token'] = "";
            $input['gender'] = "M";
            $input['userRole'] = '3';
            $input['status'] = '0';
            $input['userVerifyToken'] = md5(rand(1, 9999999));
//            dd($input);
            $user = User::create($input);
            
            DB::table('password_resets')->insert(['email'=>$input['email'], 'token'=>$input['userVerifyToken']]);
            
            //CREATE ACCOUNT VERIFICATION EMAIL
            $success['firstname'] = $input['first_name'];
            $success['link'] = $input['userVerifyToken'];
            $success['link'] = url('user/account/verify/' . $input['userVerifyToken']);
            $this->sendResponse('', 'User register successfully. Account confirmation link has been sent to your registered email. Please click on the given link to activate your account.');
            
            //SEND ACCOUNT ACTIVATION MAIL
            Mail::to($input['email'])->send(new RegisterEmail($success));

        }else{
            $this->sendError( 'Email already registered', ['email'=>$credentials['email']]);
        }
        
    }

    /**
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */
    public function login(Request $request) {
        try{
        $credentials = $request->only('username', 'password');
//        $credentials['password'] =  angularDataDecoder($credentials['password']);
        
        $user = User::getUser($credentials['username']);
    //    dd($user);
            if (empty($user)) {
                $this->sendError('Please check your email/password', '');
            } else {
                if ($user[0]->status == '0') {
                    $this->sendError('Please check your email for activating your account.', '');
                } elseif (Crypt::decrypt($user[0]->password_hash) != $credentials['password']) {
                    $this->sendError('Invalid password.', '');
                } elseif ($user[0]->status == '1') { //dd('here');
                    $client = new \GuzzleHttp\Client();
                    // dd('here');
                    
                    $response = $client->post(url('/') . '/oauth/token', [
                        'form_params' => [
                            'username' => $credentials['username'],
                            'password' => $credentials['password'],
                            'grant_type' => 'password',
                            'client_id' => $this->clientId,
                            'client_secret' => $this->clientSecret,
                        ],
                    ]);
                //    dd($response);
                    $response = json_decode($response->getBody(), true);
                    // dd($response);
                    if ($response) {
                        $sendData = [];
                        $sendData['user']['firstname'] = $user[0]->first_name;
                        $sendData['user']['lastname'] = $user[0]->last_name;
                        $sendData['user']['email'] = $user[0]->email;
                        $sendData['user']['picture'] = ($user[0]->picture=='') ? url('images/default-user.png') : ((strpos($user[0]->picture,'ttps') >0) ? $user[0]->picture : (\URL::to('public/') .'/'. $user[0]->picture));
                        $sendData['user']['mobile'] = $user[0]->mobile;
                        $sendData['user']['gender'] = $user[0]->gender;
                        $sendData['user']['status'] = $user[0]->status;
                        
                        $sendData['token'] = $response['access_token'];

                        $this->sendResponse($sendData, 'Login successfully');
                    } else {
                        $this->sendError('Unauthenticated', '');
                    }
                } else {
                    $this->sendError('Account blocked, please contact admin.', '');
                }
            }
        }catch(\Exception $e){
            echo $e->getMessage();
        }
        
    }

    public function socialLogin(Request $request) {

        $postdata = $request->post();
        //find user
        $user = User::getUser($postdata['email']);

        $credentials = [];
        if (!empty($user)) {

            if ($postdata['provider'] == 'FACEBOOK') {
                $updateUser['fb_token'] = $postdata['id'];
            } else {
                $updateUser['google_token'] = $postdata['id'];
            }

            User::where('id', $user[0]->id)->update($updateUser);
            $emailUser = $user[0]->email;
            $password = Crypt::decrypt($user[0]->password_hash);

        } else {
            $credentials['fb_token'] = '';
            $credentials['google_token'] = '';
            if ($postdata['provider'] == 'facebook') {
                $credentials['fb_token'] = $postdata['id'];
            } else {
                $credentials['google_token'] = $postdata['id'];
            }

            $name = explode(' ', $postdata['name']);
            $email = explode('@', $postdata['email']);
            $credentials['first_name'] = ($name[0]) ? $name[0] : '';
            $credentials['last_name'] = ($name[1]) ? $name[1] : '';
            $credentials['email'] = $postdata['email'];
            $credentials['password'] = $postdata['id'];
            $credentials['picture'] = isset($postdata['image']) ? $postdata['image'] : '';
            $credentials['gender'] = 'M';
            $credentials['mobile'] = '';
            $credentials['role'] = '3';
            $credentials['status'] = '1';

            $lastId = User::create($credentials)->id; //create user
            
            //get user
            $user = User::getUser($postdata['email']);

            $emailUser = $user[0]->email;
            $password = Crypt::decrypt($user[0]->password_hash);
        }

        $client = new \GuzzleHttp\Client();

        try {
              $response = $client->post(url('/') . '/oauth/token', [
                    'form_params' => [
                        'username' => $emailUser,
                        'password' => $password,
                        'grant_type' => 'password',
                        'client_id' => $this->clientId,
                        'client_secret' => $this->clientSecret,
                    ],
                ]);
            $response = json_decode($response->getBody(), true);
            if ($response) {
                $user = User::getUser($postdata['email']); //get saved user
                //send user data
                $sendData = [];
                $sendData['user']['firstname'] = $user[0]->first_name;
                $sendData['user']['lastname'] = $user[0]->last_name;
                $sendData['user']['email'] = $user[0]->email;
                $sendData['user']['picture'] = $user[0]->picture;
                $sendData['user']['status'] = $user[0]->status;
                $sendData['user']['gender'] = $user[0]->gender;
                $sendData['user']['mobile'] = $user[0]->mobile;
                $sendData['token'] = $response['access_token'];
                $this->sendResponse($sendData, 'Login successfully');
            } else {
                $this->sendError('Unauthenticated', '');
            }
        } catch (\Exception $e) {
            $this->sendError('something went wrong, please try again', '');
        }
    }

    

    /* Forgot password Api send verification code */

    public function forgotPasswordVerifyCode(Request $request) {
        $request_data = $request->post();
        if (empty($request_data['username'])) {
            $this->sendError('false', 'Please enter required fields.');
        }
        $findUser = User::getUser($request_data);

        if (isset($findUser) and ! empty($findUser)) {
            $request_data['token'] = Hash::make(rand(100000, 999999));

            $request_data['tokenLink'] = $this->FRONT_URL . 'reset/password?token=' . $request_data['token'];
            
            $msg = 'Please go to your email for password recovery link';
            SendForgotPasswordMail::dispatch($request_data, $request_data['username']);
            $this->sendResponse('', $msg);
        } else {
            $this->sendError('Email does not exists.', '');
        }
    }

    //get email from token
    public function passwordResetEmail(Request $request) {
        $request_data = $request->post();

        if (empty($request_data['token'])) {
            $this->sendError('Token is required', '');
        }
        $resetEmail = PasswordReset::where('token', $request_data['token'])->first();

        if (!empty($resetEmail)) {
            $resetEmail = $resetEmail->toArray();
            $this->sendResponse($resetEmail['email'], '');
        } else {
            $this->sendError('Link expired.', '');
        }
    }

    /* Forgot password Api reset password */

    public function forgotPasswordReset(Request $request) {
        $request_data = $request->post();
        if (empty($request_data['password']) || empty($request_data['token'])) {
            $this->sendError('Invalid data.', '');
            exit;
        }
        $findUser = \App\PasswordReset::with('user')->where('token', $request_data['token'])->first();
//        dd($findUser);
        if(!empty($findUser)){
            $findUser = $findUser->toArray();
            $password = $request_data['password'];

            User::find($findUser['user']['id'])->update(['status' => '1', 'updated_at' => date("Y-m-d h:i:s")]);

            DB::table('password_resets')->where('token', '=', $request_data['token'])->delete();

            $this->sendResponse('', 'Password changed successfully');
        }else{
            $this->sendError('Token not found or expired, please request for new token and try again.', '');
            
        }
        
    }

    public function authLogout(Request $request) {

        $accessToken = Auth::user()->token();
        DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                    'revoked' => true
        ]);

        $accessToken->revoke();
        $this->sendResponse('', 'Logout successfully');
    }

}
