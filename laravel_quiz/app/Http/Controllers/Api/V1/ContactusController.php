<?php

namespace App\Http\Controllers\api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Contactus;
use Validator;

class ContactusController extends Controller
{
    
    
    public function add(Request $request){
        $request_data = $request->post();
//        if (isset($_POST) && !empty($_POST)) {

            $rules = array(
                'email'     => 'required|max:20|regex:/^.+@.+$/i',
                'name'  => 'required|max:20',
                'subject' => 'required|max:100',
                'message'  => 'required|max:300',
            );

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                $errors = [];
                foreach(json_decode(json_encode($validator->errors()),true) as $err){
                    foreach($err as $er){
                       $errors[] = $er; 
                    }
                }
                
                $this->sendError($errors, '');
            } else {
                $res = Contactus::saveData($request_data);
                if ($res) {
                    $this->sendResponse('','Message sent successfully. We will contact you back shortly.');
                }
            }
//        }

    }
}
