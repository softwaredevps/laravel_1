<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
class SearchController extends Controller
{
    public function search(Request $request) {      
        $data = DB::table('article')
        ->select('*')   
        ->where('article_name', 'like', '%' . $request['search'] . '%')
        ->orWhere('article_desc', 'like', '%' . $request['search'] . '%')
        ->get();
        $this->sendResponse($data, '');
    }
}
