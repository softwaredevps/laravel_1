<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\CategoryModel;
use App\ExamModel;
use App\Question;
use Request;
use Auth;
use Validator;
use DB;
use App\User;
use App\SavedQuestion;
use App\ExamOrder;
use App\ExamCategoryModel;
use App\QuestionReportModel;


class ExamController extends Controller
{
    /**
     * LIST CATEGORY
     */
    
    public function listExams($examSlug, $id) {
        $authUser = auth('api')->user();

        $catData = [];

        //CHECK IF EXAM IS BOUGHT
        $examOrderCond = [];
        $examArray = [];
        $examDetail = [];
        try{
            
            if(!empty($authUser)){
                $authUser = $authUser->toArray();
               $userId = $authUser['id']; 
               $examOrderCond = [ 'user_id'=>$userId, 'status'=>'1'];
               $catDetail = CategoryModel::select('id','category_name','category_slug','price', \DB::raw('CONCAT("'. url('/public').'/", image) as image'))->with(['getCategoryExams:id,category_id,exam_id','getCategoryExams.examData','getOrderCategory','getOrderCategory.getOrder'=> function ($q) use($examOrderCond) {
                    $q->where($examOrderCond);
                }])->where('id', $id)->first();
            }else{
                $catDetail = CategoryModel::select('id','category_name','category_slug','price', \DB::raw('CONCAT("'. url('/public').'/", image) as image'))->with(['getCategoryExams:id,category_id,exam_id','getCategoryExams.examData'])->where('id', $id)->first();
            }

            if(!empty($catDetail)){
                $catData = $catDetail->toArray();
                $examDetail['id'] = $catData['id'];
                $examDetail['category_name'] = $catData['category_name'];
                $examDetail['category_slug'] = $catData['category_slug'];
                $examDetail['price'] = $catData['price'];
                $examDetail['image'] = $catData['image'];
                if((empty($catData['get_order_category']))){
                    $examDetail['isBought'] = false;
                }else{
                    $isbought = false;
                    foreach($catData['get_order_category'] as $order){
                        if(!empty($order['get_order'])){
                            $isbought = true;
                        }
                    }
                    $examDetail['isBought'] = $isbought;
                }
                foreach($catData['get_category_exams'] as $k => $cat){
                    $examArray[$k]['id'] = $cat['exam_data']['id'];
                    $examArray[$k]['title'] = $cat['exam_data']['title'];
                    $examArray[$k]['description'] = $cat['exam_data']['description'];
                    $examArray[$k]['type'] = $cat['exam_data']['type'];
                }
                
                usort($examArray, function ($item1, $item2) {
                    return $item1['type'] <=> $item2['type'];
                });
            }
            
            
        }catch(\Exception $e){
            $catDetail = ['err'=>$e->getMessage()];
        }
        $resp['exam'] = $examArray;
        $resp['examInfo'] = $examDetail;
        
        if (!empty($resp)) {
                $this->sendResponse($resp, \Config::get('constants.success.exams_fetched_successfully'));
        } else {
            $this->sendError(\Config::get('constants.error.no_category_available'), $catDetail);
        }
    }
    
    
    
    /**
     * LIST CATEGORY
     */
    
    public function listSubCategories($examSlug, $parentCatId) {
        
        try{
            $authUser = auth('api')->user();
            
            $examsData = [];
            if(!empty($authUser)){ //FOR LOGGEDIN USERS
                
                $exams = CategoryModel::select('id', 'category_name', 'category_slug', \DB::raw('CONCAT("'. url('/public').'/", image) as image'), 'parent_category_id', 'price')->with(['getOrderCategory:id,order_id,category_id,price' , 'getOrderCategory.getOrder'=> function ($q) use($authUser) {
                $q->select('id','order_num','user_id')->where(['user_id' => $authUser['id'], 'status'=>'1'])->get();
            }])->where(['status' => 1, 'parent_category_id'=>$parentCatId])->orderBy('created_at','ASC')->get();
            }else{ //FOR NON LOGGEDIN USERS
                $exams = CategoryModel::select('id', 'category_name', 'category_slug', \DB::raw('CONCAT("'. url('/public').'/", image) as image'), 'parent_category_id', 'price')->with(['getOrderCategory:id,order_id,category_id,price' ])->where(['status' => 1, 'parent_category_id'=>$parentCatId])->orderBy('created_at','ASC')->get();
            }
            
            if(!empty($exams)){
                $examsData = $exams->toArray();
            }
        }catch(\Exception $e){
            $examsData = ['err'=>$e->getMessage()];
        }
        
        if (!empty($exams)) {
            $this->sendResponse($examsData, \Config::get('constants.success.exams_fetched_successfully'));
        } else {
            $this->sendError(\Config::get('constants.error.categoried_not_available'), $examsData);
        }
    }
    
    /*
     * Exam detail
     */
    
    public function examDetail($id) {
        if(trim($id) == ''){
            $this->sendError(\Config::get('constants.error.exam_does_not_exists'), '');
            exit;
        }
        try{ 
            $authUser = auth('api')->user();
            $userId = '';
            if($authUser && $authUser->id){
                $userId = $authUser->id;
            }
            $detail = ExamModel::getExamDetail($id, $userId);
            
            if(!empty($detail)){
                $detail = $detail->toArray();
                $this->sendResponse($detail, \Config::get('constants.success.exam_detail_fetched_successfully'));
            }else{
                $this->sendError(\Config::get('constants.error.exam_does_not_exists'), '');
            }
            

        }catch(\Exception $e){
            $this->sendError(\Config::get('constants.error.exam_does_not_exists'), $e->getMessage());
        }
    }
    
    
    /**
     * Get single question for test
     * @param type $idG
     */

    public function getQuestions($id){
        if(trim($id) == ''){
            $this->sendError(\Config::get('constants.error.exam_does_not_exists'), '');
            exit;
        }
        try{
            $requestData = Request::all();
            $detail = ExamModel::getExamQuestion($id, $requestData);
            if(!empty($detail['data'][0]->image)){
                $detail['data'][0]->image = url('/public/uploads/question-images/'.$detail['data'][0]->image);
            }
            if(!empty($detail)){ 
                $this->sendResponse($detail, \Config::get('constants.success.question_fetched_successfully'));
            }else{
                $this->sendError(\Config::get('constants.error.question_not_found'), '');
            }
            

        }catch(\Exception $e){
            $this->sendError(\Config::get('constants.error.exam_does_not_exists'), $e->getMessage());
        }
    }
    
    /**
     * Submit test
     */
    
    public function submitTest() {
        $requestData = Request::all();

        $accessToken = Auth::user()->token();
        
        if ($accessToken->user_id) { 
            $decodedChar = config('app.EncodedChar') ?? 15;
            $examId = (substr($requestData['examId'], $decodedChar,-$decodedChar));
            $getQuestion = ExamModel::with(['examQuestions','examQuestions.getQuestions'=> function($q) {
                    $q->where('status', '=', '1'); // '=' is optional
                }])->where('id', $examId)->first();
            

            if(!empty($getQuestion) && !empty($requestData['answers'])){
                $getQuestion = $getQuestion->toArray();
                $correctAnswer = 0;
                $savedAnsers = [];
                $savedAnsersArray = [];
                
                foreach ($getQuestion['exam_questions'] as $key => $dbQuestion){
                    foreach ($requestData['answers'] as $k => $answer){
                        if(isset($dbQuestion['get_questions']) && !empty($dbQuestion['get_questions'])){
                            if($dbQuestion['question_id'] == $answer['questionId']){
                                $savedAnsers[$dbQuestion['question_id']]['questionId'] = $dbQuestion['question_id'];
                                $savedAnsers[$dbQuestion['question_id']]['answer'] = $answer['answer'] ;
                                $savedAnsers[$dbQuestion['question_id']]['answerOption'] = $answer['option'];
                                $savedAnsers[$dbQuestion['question_id']]['answerSaved'] = $answer['saved']; //1 means saved 0 means skipped
                                $savedAnsers[$dbQuestion['question_id']]['isCorrect'] = false; //means wrong
                            }

                            if($dbQuestion['question_id'] == $answer['questionId'] && $dbQuestion['get_questions']['correctAnswer'] == $answer['answer']){
                                $correctAnswer += 1;
                                $savedAnsers[$dbQuestion['question_id']]['isCorrect'] = true;
                            }
                        }
                            
                    }
                }
                $savedAnsersArray['answers']         = array_values($savedAnsers);
                $savedAnsersArray['totalMarks']      = $getQuestion['total_marks'];
                $savedAnsersArray['totalTime']       = $getQuestion['total_time'];
                $savedAnsersArray['timeSpent']       = $requestData['timeSpent'] ?? 0;
                $savedAnsersArray['marksObtained']   = $correctAnswer;

                $sendData = [];
                $sendData['user_id'] = $accessToken->user_id;
                $sendData['examId'] = $examId;
                $sendData['json_data_save'] = json_encode($savedAnsersArray);

                $detail = ExamModel::saveQuestion($sendData);

                $this->sendResponse($detail, \Config::get('constants.success.test_submit_successfully'));
                
                
            }else{
                $this->sendError('', 'Exam not found.');
            }

        } else {
            $this->sendError(\Config::get('constants.error.unauthorized'), '');
        }
    }
    
    /**
     * Test result
     */
    
    public function testResult($id){
        $requestData = Request::all();

        $accessToken = Auth::user()->token();
        try{
            $getResult = SavedQuestion::with(['exams:id,title','exams.examQuestions.getQuestions'=> function($q) {
                    $q->where('status', '1'); // '=' is optional
                }])->where(['user_id'=> $accessToken->user_id, 'id'=> $id])->get()->first();
            if(!empty($getResult)){
                $getResult = $getResult->toArray();
                $this->sendResponse($getResult, \Config::get('constants.success.result_fetched_successfully'));
            }else {
                $this->sendError(\Config::get('constants.error.result_not_found'), '');
            }
        } catch (\Exception $e){
            $this->sendError(\Config::get('constants.error.unauthorized'), $e->getMessage());
        }
    }

    public function getScoreList(){ 
        try{
            $requestData = Request::all();
            $authUser = auth('api')->user();

            $page = (isset($requestData['page']) && ($requestData['page']!='0')) ? ($requestData['total']) : 0;
            $params = [];
            $params['page'] = $page;
            $params['limit'] = 20;
            $params['user_id'] = $authUser->id;
            if($authUser && $authUser->id){
                $detail = ExamModel::getScoreList($params);
                if(!empty($detail)){ 
                    $this->sendResponse($detail, \Config::get('constants.success.report_fetched_successfully'));
                }else{
                    $this->sendError('Report not found!', '');
                }
            }else{
                    $this->sendError('', \Config::get('constants.error.please_login_to_see_your_results'));
            }
            
        }catch(\Exception $e){
            $this->sendError(\Config::get('constants.error.result_not_found'), '');
        }
    }
    
    public function reportQuestion() {
        try{
            $requestData = Request::all();
            $authUser = auth('api')->user();
            if($authUser){
                $postdata= [];
                $postdata['question_id'] = $requestData['questionId'];
                $postdata['comment'] = $requestData['comment'];
                $postdata['status'] = '1';
                $postdata['user_id'] = $authUser->id;
                QuestionReportModel::create($postdata);
                $this->sendResponse('', \Config::get('constants.success.question_reported_successfully'));
            }else{
                $this->sendError('', \Config::get('constants.error.unauthorized'));
            }
            
            
        }catch(\Exception $e){
            $this->sendError(\Config::get('constants.error.please_try_again'), $e->getMessage());
        }
    }

}