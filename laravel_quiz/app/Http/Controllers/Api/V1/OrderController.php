<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\CategoryModel;
use App\ExamModel;
use App\Question;
use Request;    
use Auth;
use Validator;
use DB;
use App\User;
use App\SavedQuestion;
use App\ExamOrder;
use App\ExamCategoryModel;
use App\ExamOrderDetail;


class OrderController extends Controller
{
    
    public function orderDetail($orderNum){
         
        $accessToken = Auth::user()->token();
        
        if ($accessToken->user_id) {
            $orderData = $this->myOrders($orderNum);
        } else {
            $this->sendError(\Config::get('constants.error.unauthorized'), '');
        }
    }
  
    public  function getCartItems(Request $request) {
        $data = Request::all();
        $authUser = auth('api')->user();
        if(!empty($data)){
            try{
                if($authUser['id']){ //FOR LOGGEDIN CHECK IF NOT ALREADY BOUGHT
                    $getCats = CategoryModel::select('id','category_name','price', \DB::raw('CONCAT("'. url('/public').'/", image) as image'))->with(['getOrderCategory:id,category_id,order_id','getOrderCategory.getOrder'=> function ($q) use($authUser) {
                        $q->where(['user_id'=> $authUser['id'],'status'=>'1']);
                    }])->whereIn('id', $data['cartItems'])->orderBy('id', 'ASC')->get()->toArray();
                    $newData = [];
                    if(!empty($getCats)){
                        foreach($getCats as $k => $cat){
                            if(!count($cat['get_order_category']) || (count($cat['get_order_category']) && !count($cat['get_order_category'][0]['get_order']))){
                                $newData[$k]['id'] = $cat['id'];
                                $newData[$k]['category_name'] = $cat['category_name'];
                                $newData[$k]['price'] = $cat['price'];
                                $newData[$k]['image'] = $cat['image'];
                            }
                        }
                    }
                    $newData = array_values($newData);
                }else{
                    $newData = CategoryModel::select('id','category_name','price', \DB::raw('CONCAT("'. url('/public').'/", image) as image'))->whereIn('id', $data['cartItems'])->orderBy('id', 'ASC')->get()->toArray();
                }
                
                $this->sendResponse($newData, '');
            }catch(\Exception $e){
                $this->sendError(\Config::get('constants.error.cart_empty'), '');                
            }
        }else{
            $this->sendError(\Config::get('constants.error.cart_empty'), '');
        }
    }
    
    
    /**
     * Purchase exam
     */
    public function purchaseExam(Request $request){
        $accessToken = Auth::user()->token();
        $postData = Request::all();
        if(empty($postData['cartItems']) || empty($postData['totalAmount']) || empty($postData['totalPaybleAmount'])){
            $this->sendError(\Config::get('constants.error.invalid_params'), '');
            exit;
        }
        
        if ($accessToken->user_id) {
            $findPrevOrder = ExamOrder::with('orderDetail')->where(['user_id'=>$accessToken->user_id])->get()->toArray();
            if(!empty($findPrevOrder)){
                    $this->buyExam($accessToken->user_id , $postData);
            }else{
                $resp = $this->buyExam($accessToken->user_id , $postData);
            }
        
        } else {
            $this->sendError( \Config::get('constants.error.unauthorized'), '');
        }
    }
    
    public function buyExam($userId, $postData) {
        $findExam = CategoryModel::whereIn('id' , $postData['cartItems'])->where(['status'=>['1', '3']])->get()->toArray();
        if(!empty($findExam) && (count($findExam) == count($postData['cartItems']))){
            
            try{
                $price = 0;
        
                $orderArr = [];
                $orderArr['user_id'] = $userId;
                $orderArr['order_actual_amount'] = $postData['totalAmount'];
                $orderArr['order_amount'] = $postData['totalPaybleAmount'];
                $orderArr['order_num'] = rand(1000, 9999).time().rand(1000, 9999);
                $createOrder = ExamOrder::create($orderArr)->id;
                //create order detail
                $orderDetail = [];
                for($i=0; $i<count($postData['cartItems']); $i++) {
                    $orderDetail[$i]['order_id'] = $createOrder;
                    $orderDetail[$i]['category_id'] = $postData['cartItems'][$i];
                    $orderDetail[$i]['price'] = $postData['examPrice'][$i];
                    $orderDetail[$i]['created_at'] = \Carbon\Carbon::now();
                    $orderDetail[$i]['updated_at'] = \Carbon\Carbon::now();
                    
                }
                ExamOrderDetail::insert($orderDetail);
                $redirectURL        = 'order/purchase?time='.time().'&secret='.$orderArr['order_num'];
                $this->sendResponse($redirectURL, '');
            } catch (\Exception $e){
                $this->sendError(\Config::get('constants.error.invalid_order_data'), $e->getMessage());
            }
            
        }else{
            $this->sendError(\Config::get('constants.error.invalid_order_data'), '');
        }
        

    }
    
    /**
     * get login user's orders
     */
    
    public function myOrders($orderNum=null) {
        try{
            $authUser = Auth::user()->token();
            if($authUser->user_id){
                $requstData = Request::all();
                $page = (isset($requstData['page']) && ($requstData['page']!='0')) ? ($requstData['total']) : 0;
                $params = [];
                $params['page'] = $page;
                $params['limit'] = 20;
                $params['user_id'] = $authUser->user_id;
                $params['order_num'] = $orderNum;
                $orders = ExamOrder::getMyOrders($params);
                $orderData = [];
//                dd($orders);
                if(!empty($orders)){
                   
                    foreach ($orders as $key => $value) {
                        
                        $orderData[$key]['order_id']            = $value['id'];
                        $orderData[$key]['status']              = ($value['status'] == '1') ? 'Success' : (($value['status'] == '0') ? 'Pending' : (($value['status'] == '2') ? 'Canceled' : '')) ;
                        $orderData[$key]['order_amount']        = $value['order_amount'];
                        $orderData[$key]['order_date']          = $value['updated_at'];
                        $orderData[$key]['order_number']        = $value['order_num'];
//                        $orderData[$key]['category_name']       = count($value['order_detail']) ? $value['order_detail'][0]['get_category']['category_name'] : '';
                        $orderData[$key]['image']               = count($value['order_detail']) ? url('/public').'/'.$value['order_detail'][0]['get_category']['image'] : '';
                        $orderData[$key]['transaction_id']      = $value['transaction_id'];
                        $orderData[$key]['payment_mode']        = $value['payment_mode'];
                        $orderData[$key]['bank_name']           = $value['bank_name'];
                        $orderData[$key]['payment_gateway']     = $value['payment_gateway'];
                        $orderData[$key]['order_message']       = $value['order_message'];
                        $orderData[$key]['exams'] = [];
                        
                        if(isset($value['order_detail']) && !empty($value['order_detail'])){
                            foreach($value['order_detail'] as $catKey => $cat){
                                $orderData[$key]['exams'][$catKey]['id'] = $cat['get_category']['id'];
                                $orderData[$key]['exams'][$catKey]['category_name'] = $cat['get_category']['category_name'];
                                $orderData[$key]['exams'][$catKey]['image'] = url('/public').'/'.$cat['get_category']['image'];
                            }
                        }
                    }
                    $this->sendResponse($orderData, \Config::get('constants.success.orders_fetched_successfully'));

                }else{
                    $this->sendError(\Config::get('constants.error.order_not_found'), '');
                }

            }else{
                $this->sendError(\Config::get('constants.error.please_login_to_see_your_orders'), '');
            }
        }catch(\Exception $e){
            $this->sendError(\Config::get('constants.error.orders_not_found'), $e->getLine());
        }
        
        
    }
    
}