<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;

class BaseController extends Controller {

    /**

     * success response method.

     *

     * @return \Illuminate\Http\Response

     */
    public function sendResponse($result, $message) {

        $response = [
            'status' => true,
            'data' => $result,
            'message' => $message,
        ];


        echo json_encode($response);
//        echo json_encode($response);
    }

    /**

     * return error response.

     *

     * @return \Illuminate\Http\Response

     */
    public function sendError($error, $errorMessages = [], $code = 404) {

        $response = [
            'status' => false,
            'message' => $error,
        ];


        if (!empty($errorMessages)) {

            $response['data'] = $errorMessages;
        }


        echo json_encode($response, $code);
		exit();
    }
    
    function createTodo(){
        echo json_encode($response, $code);
    }

}
