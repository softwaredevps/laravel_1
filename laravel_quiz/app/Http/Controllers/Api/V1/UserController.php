<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;
use App\Traits\ApiCommonMethods;
use App\User;
use App\PasswordReset;
use App\Contact;
use Auth;
use Validator;
use DB;
use Hash;
use Crypt;
use Mail;
use Config;
use App\Mail\RegisterEmail;
use App\Mail\ForgotPassword;

//use Parser;
class UserController extends Controller {

    use ApiCommonMethods;

    protected $clientSecret;
    protected $APIURL;

    public function __construct() {
        $this->clientSecret = '';
        $this->clientId = '4';
        $this->APIURL = config("constants.global.apiurl");
    }

    /**

     * Get user profile api

     *

     * @return \Illuminate\Http\Response

     */
    public function getUserProfile(Request $request) {        
        $accessToken = Auth::user()->token();
        
        if ($accessToken->user_id) { 
            $user = User::getUser($accessToken->user_id); 
            
            $sendData = [];
            $sendData['id'] = $user[0]->id;
            $sendData['firstname'] = $user[0]->first_name;
            $sendData['lastname'] = $user[0]->last_name;
            $sendData['email'] = $user[0]->email;
            $sendData['gender'] = $user[0]->gender;
            $sendData['mobile'] = $user[0]->mobile;
//            $sendData['picture'] = ($user[0]->userPicture=='') ? url('images/default-user.png') : ((strpos($user[0]->userPicture,'ttps')>0) ? $user[0]->picture : (\URL::to('public/') .'/'. $user[0]->userPicture));
            $sendData['status'] = $user[0]->status;
            $this->sendResponse($sendData, '');
        } else {
            $this->sendError('', 'Unauthenticated user.');
        }
    }

    /**
     * Update profile api 
     * 
     * @return \Illuminate\Http\Response 
     */
    public function updateProfile(Request $request) {
        $accessToken = Auth::user()->token();
        if ($accessToken->user_id) {
            $credentials = $request->only( 'firstname', 'lastname', 'gender', 'mobile');
            
                $updateUser['first_name'] = $credentials['firstname'];
                $updateUser['last_name'] = $credentials['lastname'];
                $updateUser['gender'] = $credentials['gender'];
                $updateUser['mobile'] = $credentials['mobile'];

                User::where('id', $accessToken->user_id)->update($updateUser);

                $this->getUserProfile($request);
            
        } else{
            $this->sendError('Unauthenticated', '');
        }
    }

    public function updateDP(Request $request) {
        $accessToken = Auth::user()->token();
        
        if ($accessToken->user_id) {

            if ($request->file) {
                $disk = "public_folder";
                $png_url = time().".png";
                $path = public_path().'images/user-images/' . $png_url;    

                if (\Storage::disk($disk)->put($png_url,base64_decode($request->file))) {
                    $updateUser['userPicture'] = 'images/user-images/' . $png_url;
                    User::where('userID', $accessToken->user_id)->update($updateUser);

                    $user = User::getUser($accessToken->user_id); //get current user
                    $sendData = [];
                    $sendData['firstname'] = $user[0]->userFirstName;
                    $sendData['lastname'] = $user[0]->userLastName;
                    $sendData['email'] = $user[0]->userEmail;
                    $sendData['mobileNo'] = $user[0]->userMobileNo;
                    $sendData['gender'] = $user[0]->userGender;
                    $sendData['picture'] = ($user[0]->userPicture=='') ? url('images/default-user.png') : ((strpos($user[0]->userPicture,'ttps') >0) ? $user[0]->userPicture : (\URL::to('public/') .'/'. $user[0]->userPicture));
                    $sendData['status'] = $user[0]->userStatus;
                    $this->sendResponse($sendData, 'Profile picture updated successfully');
                } else {
                    $this->sendError('Something went wrong please try after some time.', '');
                }
            } else {
                $this->sendError('Invalid image', '');
            }
        }else {
            $this->sendError('Unauthenticated', '');
        }
    }

    public function socialLogin(Request $request) {
        $postdata = $request->post();
        //find user
        $user = User::getUser($postdata['email']);

        $credentials = [];
        if (!empty($user)) {

            if ($postdata['provider'] == 'facebook') {
                $updateUser['fb_token'] = $postdata['id'];
            } else {
                $updateUser['google_token'] = $postdata['id'];
            }

            User::where('id', $user[0]->id)->update($updateUser);

            $credentials['username'] = $user[0]->email;
            $credentials['password'] = Crypt::decrypt($user[0]->hashpass);
        } else {
            $name = explode(' ', $postdata['name']);
            $email = explode('@', $postdata['email']);
            $credentials['firstname'] = ($name[0]) ? $name[0] : '';
            $credentials['lastname'] = ($name[1]) ? $name[1] : '';
            $credentials['username'] = $email[0];
            $credentials['email'] = $postdata['email'];
            $credentials['password'] = ($postdata['id']);
            $credentials['picture'] = $postdata['image'];
            $credentials['gender'] = '';
            $credentials['google_token'] = $postdata['id'];
            $credentials['fb_token'] = '';
            $credentials['role'] = 2;
            $credentials['status'] = 1;

            $lastId = User::create($credentials)->id;
        }

        $client = new \GuzzleHttp\Client();

        $response = $client->post(url('/') . '/oauth/token', [
            'form_params' => [
                'username' => $credentials['username'],
                'password' => $credentials['password'],
                'grant_type' => 'password',
                'client_id' => $this->clientId,
                'client_secret' => $this->clientSecret,
            ],
        ]);

        $response = json_decode($response->getBody(), true);
        if ($response) {
//            self::pr($response);die;
            $this->sendResponse($response['access_token'], 'Login successfully');
        } else {
            $this->sendError('Unauthenticated', '');
        }
    }

    /*
     * Contact us
     */

    public function contactUs(Request $request) {
        $post = $request->post();
        if (empty($post['name']) || empty($post['email']) || empty($post['message'])) {
            $this->sendError('Required data is missing or invalid.', '');
        } else {
            $data['name'] = $post['name'];
            $data['email'] = $post['email'];
            $data['message'] = $post['message'];
            //save contact
            Contact::create($data);
            
            //get admin
            $admin = User::getAdmin();
//            self::pr($admin);
            //send email
            Mail::send('mails.contact', ['name' => $post['name'], 'bodyMessage' => $post['message'], 'email'=>$post['email']], function ($message) use ($admin, $post)  {
                $message->from($admin[0]->email, 'Vids App');
                $message->to($admin[0]->email);
                $message->replyTo($post['email']);
            });

            $this->sendResponse('', 'Contact request sent successfully, we will get back to you shortly.');
        }
    }
    
    /**
     * Update password api 
     * 
     * @return \Illuminate\Http\Response 
     */
    public function updatePassword(Request $request) {
        $accessToken = Auth::user()->token();
        if ($accessToken->user_id) {
            $requestData = $request->only( 'password', 'newPassword');
                
            $findUser = User::getUser($accessToken->user_id);
//            dd($findUser);
             $oldPassword = Crypt::decrypt($findUser[0]->password_hash);
             $newPassword = base64_decode($requestData['newPassword']);
            
            if($oldPassword == $newPassword){
                
                $updateUser['password']         = Hash::make($newPassword);
                $updateUser['password_hash']    = Crypt::encrypt($newPassword);
            
                User::where('id', $accessToken->user_id)->update($updateUser);
            }else{
                $this->sendError('Old password not matched', '');
            }
            
            $this->sendResponse([], 'Password changed successfully');

            
        } else{
            $this->sendError('Unauthenticated', '');
        }
    }

}
