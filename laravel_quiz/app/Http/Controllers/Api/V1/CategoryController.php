<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\CategoryModel;

class CategoryController extends Controller
{
    
    /**
     * LIST CATEGORY
     */
    
    public function listCategories() {
        try{
            $cats = \Cache::rememberForever('categories', function()
            {   
                $publicPath = '';
                return CategoryModel::select('id', 'category_name', 'category_slug', \DB::raw('CONCAT("'. url('/public').'/", image) as image'))->where(['status' => 1, 'parent_category_id' => 0])->orderBy('category_slug','ASC')->get();
            });
        }catch(\Exception $e){
            $cats = ['err'=>$e->getMessage()];
        }
        
        if (!empty($cats)) {
            $this->sendResponse($cats, 'Categories fetched successfully!');
        } else {
            $this->sendError('No Categories available!', '');
        }
    }
    
}