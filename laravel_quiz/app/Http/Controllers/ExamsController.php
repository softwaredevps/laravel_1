<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\ExamModel;
use App\CategoryModel;
use Exception;

class ExamsController extends Controller
{
    public function add(Request $request){
         if($request->isMethod('post')){
            $requestData = $request->all();

            $rules = array(
                'title' => 'required|max:200',
                'description' => 'required',
                'type' => 'required',
                'total_time' => 'required',
                'total_marks' => 'required',
                'category_id' => 'required',
            );

            $validator = Validator::make($requestData, $rules);

            if ($validator->fails()) {
                $msg = $validator->messages();
                $msgs = (\GuzzleHttp\json_decode($msg));

                return redirect()->to('/admin/exams/add')->with('validations', $msgs);
            } else {
                unset($requestData['_token']);
//                $requestData['end_date'] = date('Y-m-d', strtotime($requestData['end_date']));
                $res = ExamModel::saveData($requestData);
                if($res){
                   return redirect()->to('/admin/exams/list')->with('info', 'Exam Added Successfully.');
                }
            }
            
        }
        
        $category = CategoryModel::with('childCategory:id,category_name,parent_category_id')->get()->toArray();
//        dd($category);
        return view('exams.add',compact('category'));
    }
    public function listExams(Request $request){
        $res = ExamModel::listData();
//        dd($res);
        return view('exams.list',['exams' => $res]);
    }
    public function update(Request $request, $id){
         $res = ExamModel::listAllData($id);
//         dd($res);
         $category = CategoryModel::with('childCategory:id,category_name,parent_category_id')->get()->toArray();
//         dd($category);
       if($request->isMethod('post')){
           $requestData = $request->all();
            $requestData['update_id'] = $id;
            unset($requestData['_token']);
//            $requestData['end_date'] = date('Y-m-d', strtotime($requestData['end_date']));
            $res = ExamModel::updateData($requestData);
            if($res){
               return redirect()->to('/admin/exams/list')->with('info', 'Exam Updated Successfully.');
            }
        } 
        
          
        return view('exams.add',['res' => $res, 'category'=> $category]);
    }
    public function listdelete($id){
        $res = ExamModel::listDelete($id);
        if($res){
            return back()->with('message', 'Category Deleted Successfully.');
         }
    }
    public function listactive($id,$status){
        $res = ExamModel::listactive($id,$status);
        if($res){
            return back()->with('message', 'Category Changed Successfully.');
         }
    }
    
}