<?php

namespace App\Http\Controllers;

use Maatwebsite\Excel\Concerns\ToModel;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Excel;
use App\imports\UsersImport;

class ImportExcelController extends Controller {

    function index() {
        $data = DB::table('question')->orderBy('id', 'DESC')->get();
        return view('questions.import_excel', compact('data'));
    }

    function import(Request $request) {
        $this->validate($request, [
            'select_file' => 'required|mimes:xls,xlsx'
        ]);
        //READ FILE
        $data = Excel::import(new UsersImport, $request->file('select_file'));
        return back()->with('success', 'Excel Data Imported successfully.');
    }

}