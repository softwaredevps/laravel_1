<?php

namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Auth;
use Illuminate\Http\Request;
use Hash;
use DB;
use Session;
use App\User;

class LoginController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles authenticating users for the application and
      | redirecting them to your home screen. The controller uses a trait
      | to conveniently provide its functionality to your applications.
      |
     */

use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
			
			$this->middleware('guest')->except('logout');
    }
    
    public function index() {
			
			return Redirect::to('login');
    }

    
    function login(Request $request) {
        $rules = array(
            'email' => 'required', // make sure the email is an actual email
            'password' => 'required|alphaNum' // password can only be alphanumeric and has to be greater than 3 characters
        );

// run the validation rules on the inputs from the form
        $validator = Validator::make($request->all(), $rules);
// if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return Redirect::to('login')
                            ->withErrors($validator) // send back all errors to the login form
                            ->withInput($request->except('password')); // send back the input (not the password) so that we can repopulate the form
        } else{

            $userdata = array(
                    'password'      => $request->get('password'),
                    'email'         => $request->get('email'),
                    'userRole'      => ['1', '2']
            );
            if (Auth::attempt($userdata)) {
               return Redirect::to('/admin');
            }   else {
                // validation not successful, send back to form 
                return Redirect::to('login')->with(['error' => 'Please check your credentials!']); // send back all errors to the login form
            }

        }
    }
	
	public function logout () {
		//logout user
		auth()->logout();
		// redirect to homepage
		return redirect('/login');
	}

}
