<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Traits\Paytm;
use App\ExamOrder as Order;
use App\Jobs\SendOrderSuccess;
use App\Jobs\SendOrderFailure;
use App\ExamModel;

class OrderController extends Controller {
    
    use Paytm;
    
    private $merchantKey;
    private $merchantID;
    private $callbackUrl;
    private $txnUrl;
    private $industryType;
    private $channel;
    private $website;
    private $frontUrl;
    private $refundUrl;

    public function __construct() {
        $appEnv = (env('APP_ENV') == 'local') ? 'paytm-wallet-local' : 'paytm-wallet-prod';
        $this->merchantKey = \Config::get("services.$appEnv.merchant_key");
        $this->merchantID = \Config::get("services.$appEnv.merchant_id");
        $this->industryType = \Config::get("services.$appEnv.industry_type");
        $this->channel = \Config::get("services.$appEnv.channel");
        $this->website = \Config::get("services.$appEnv.merchant_website");
        $this->callbackUrl = \Config::get("services.$appEnv.PAYTM_CB_URL");
        $this->txnUrl = \Config::get("services.$appEnv.PAYTM_TXN_URL");
        $this->frontUrl = env('FRONT_URL');
        $this->refundUrl = \Config::get("services.$appEnv.PAYTM_REFUND_URL");
    }

    /**
     * Redirect the user to the Payment Gateway.
     *
     * @return Response
     */
    public function purchaseExam(Request $request) {
        $orderId = $request->get('secret');
        $clickTime = $request->get('time');
        $viewElements = [];
        $viewElements['amount'] = 0;
        
        //CHECK IF NO DATA FOUND THEN THROW ERROR
        if (empty($orderId) || empty($clickTime)) {
            return Redirect::to('/order/payment-error')->with('error', \Config::get('constants.error.order_data_error'));
        } else {
            //CHECK ORDER TO GET TOURNAMENT INFO //
            $order = Order::with('exam','userData:id,email')->select('id', 'user_id', 'order_amount', 'order_actual_amount','order_discount')
                            ->where('order_num', '=', $orderId)->first();
            //CHECK AVAILABLE PLACES
            if(!empty($order)) {
                $order = $order->toArray();
                $viewElements['amount'] = $order['order_amount'];
                $viewElements['orderId'] = $orderId;
                $viewElements['userData'] = $order['user_data'];
                echo $this->examOrderGen($viewElements);
            } else {
                return Redirect::to('/order/payment-error')->with('error', 'invalid_order');
            }
        }
    }

    /**
     * Redirect the user to the Payment Gateway.
     *
     * @return Response
     */
    public function examOrderGen($orderData = []) {

        $checkSum = "";
        $paramList = array();

        $ORDER_ID = $orderData["orderId"];
        $PAYTM_MERCHANT_MID = $this->merchantID;
        $PAYTM_MERCHANT_KEY = $this->merchantKey;
        $INDUSTRY_TYPE_ID = $this->industryType;
        $CHANNEL_ID = $this->channel;
        $TXN_AMOUNT = $orderData["amount"];
        $PAYTM_MERCHANT_WEBSITE = $this->website;
        $PAYTM_CB_URL = $this->callbackUrl;
        $PAYTM_TXN_URL = $this->txnUrl;

        // Create an array having all required parameters for creating checksum.
        $paramList["MID"] = $PAYTM_MERCHANT_MID;
        $paramList["ORDER_ID"] = $orderData['orderId'];
        $paramList["CUST_ID"] = $PAYTM_MERCHANT_MID; 
        $paramList["TXN_AMOUNT"] = $orderData['amount'];
        $paramList["CHANNEL_ID"] = $CHANNEL_ID;
        $paramList["WEBSITE"] = $PAYTM_MERCHANT_WEBSITE;
        $paramList["INDUSTRY_TYPE_ID"] = $INDUSTRY_TYPE_ID;

        $paramList["CALLBACK_URL"] = $PAYTM_CB_URL;
//        $paramList["MSISDN"] = 7777777777; //Mobile number of customer
        $paramList["EMAIL"] = $orderData['userData']['email']; //Email ID of customer
        $paramList["VERIFIED_BY"] = "EMAIL"; //
        $paramList["IS_USER_VERIFIED"] = "YES"; //
        //Here checksum string will return by getChecksumFromArray() function.
        $checkSum = Paytm::getChecksumFromArray($paramList, $PAYTM_MERCHANT_KEY);
//        echo $checkSum;die;
        return view('order/event', compact('paramList', 'checkSum', 'PAYTM_TXN_URL'));
    }

    /**
     * Obtain the payment information.
     *
     * @return Object
     */
    public function paymentCallback(Request $request) {
        $paramList = $request->all();
        if (!empty($paramList)) {

            try {
                $orderTxnData   = [];
                $mailData    = [];
                $getOrderArry   = [];
                
                $paytmChecksum = isset($paramList["CHECKSUMHASH"]) ? $paramList["CHECKSUMHASH"] : ""; //Sent by Paytm pg
                $PAYTM_MERCHANT_KEY = $this->merchantID;
                //Verify all parameters received from Paytm pg to your application. Like MID received from paytm pg is same as your application’s MID, TXN_AMOUNT and ORDER_ID are same as what was sent by you to Paytm PG for initiating transaction etc.
                $isValidChecksum = Paytm::verifychecksum_e($paramList, $PAYTM_MERCHANT_KEY, $paytmChecksum); //will return TRUE or FALSE string.

                $getOrder = Order::with('userData')->where('order_num', '=', $paramList['ORDERID'])->first();
                if (isset($getOrder) && !empty($getOrder)) {
                    $getOrderArry = $getOrder->toArray();
                }
                
                //CHECK AVAILABLE PLACES

                $orderTxnData['transaction_id'] = isset($paramList['TXNID']) ? $paramList['TXNID'] : 0;
                $orderTxnData['payment_mode'] = isset($paramList['PAYMENTMODE']) ? $paramList['PAYMENTMODE'] : "";
                $orderTxnData['transaction_date'] = isset($paramList['TXNDATE']) ? $paramList['TXNDATE'] : date('Y-m-d h:i:s');
                $orderTxnData['bank_transaction_id'] = isset($paramList['BANKTXNID']) ? $paramList['BANKTXNID'] : 0;
                $orderTxnData['bank_name'] = isset($paramList['BANKNAME']) ? $paramList['BANKNAME'] : "PAYTM";
                $orderTxnData['currency'] = isset($paramList['CURRENCY']) ? $paramList['CURRENCY'] : "INR";
                $orderTxnData['payment_gateway'] = isset($paramList['GATEWAYNAME']) ? $paramList['GATEWAYNAME'] : 'PAYTM';
                $orderTxnData['order_message'] = isset($paramList['RESPMSG']) ? $paramList['RESPMSG'] : "";
                $orderTxnData['order_response_code'] = isset($paramList['RESPCODE']) ? $paramList['RESPCODE'] : "";
                
                $mailData['order_num'] = $paramList['ORDERID'];
                $mailData['transaction_date'] = $orderTxnData['transaction_date'];
                $mailData['transaction_id'] = $orderTxnData['transaction_id'];
                $mailData['order_amount'] = $getOrderArry['order_amount'];
                $mailData['order_message'] = $getOrderArry['order_message'];
                $mailData['url'] = env('FRONT_URL').'order-detail/'.$mailData['order_num'];
                
                if ($isValidChecksum == "TRUE") {

                    if ($paramList["STATUS"] == "TXN_SUCCESS") {

                        //Process your transaction here as success transaction.
                        //Verify amount & order id received from Payment gateway with your application's order id and amount.

                        if (isset($paramList) && count($paramList) > 0) {

                            $orderTxnData['status'] = '1';
                            Order::where('id',$getOrderArry['id'])->update($orderTxnData);
                            //Create order detail
                            //SEND ORDER SUCCESS MAIL
//                            Mail::to($getOrderArry['user_data']['userEmail'])->send(new JoinTournament($getOrderArry));
                            
                            SendOrderSuccess::dispatch($mailData, $getOrderArry['user_data']['email']);
                            
                            return Redirect::to($this->frontUrl . 'order/payment-thankyou?status=200&msg=205&order='.$paramList['ORDERID']);
                        }
                    } else {
                        //TRANSACTION FAILED
                        $orderTxnData['orderStatus'] = '2';
                        Order::where('id',$getOrderArry['id'])->update($orderTxnData);;
                        
                        //SEND TOURNAMENT FAILED MAIL
//                        Mail::to($getOrderArry['user_data']['userEmail'])->send(new OrderFailed($getOrderArry));
                        SendOrderFailure::dispatch($mailData, $getOrderArry['user_data']['email']);
                        return Redirect::to($this->frontUrl . 'order/payment-error?status=205&msg=220&order='.$paramList['ORDERID']);
                    }
                } else {
                    //CHECKSOME MISMATCH
                    if($paramList["STATUS"] == 'TXN_SUCCESS'){
                        $orderTxnData['status'] = '1';
                        
                        Order::where('id',$getOrderArry['id'])->update($orderTxnData);
//                        echo $getOrderArry['user_data']['email'];
                        SendOrderSuccess::dispatch($mailData, $getOrderArry['user_data']['email']);
                        
                        return Redirect::to($this->frontUrl . 'order/payment-thankyou?status=200&msg=205&order='.$paramList['ORDERID']);
                    }else{
                        $orderTxnData['status'] = '2';
                        Order::where('id',$getOrderArry['id'])->update($orderTxnData);;
//                        Mail::to($getOrderArry['user_data']['userEmail'])->send(new OrderFailed($getOrderArry));
                        SendOrderFailure::dispatch($mailData, $getOrderArry['user_data']['email']);
                        return Redirect::to($this->frontUrl . 'order/payment-error?status=205&msg=215&order='.$paramList['ORDERID']);
                    }
                }
            } catch (\Exception $e) {
                //UNKNOWN ERROR OCCURED
                return Redirect::to($this->frontUrl . 'order/payment-error?status=205&msg=225&m='.$e->getMessage());
            }
        } else {
            //URL DIRECT ACCESS
            return Redirect::to($this->frontUrl . 'order/payment-error?status=205&msg=210');
        }
    }
    
    public function refund($id) {
        $data = [];
        $data['mid'] = $this->merchantID;;
        $data['txnType'] = 'REFUND';
        $data['orderId'] = '214115880072757774';
        $data['txnId'] = '20200427111212800110168340301499132';
        $data['refId'] = time().'';
        $data['refundAmount'] = '5';
//        $data['CHECKSUM'] = Paytm::getChecksumFromArray($data, $this->merchantKey);
        $refund = Paytm::initiateTxnRefund($data, $this->merchantKey, $this->refundUrl);
    }
    
    
    public function listOrders($id=null) {
        $cond = [];
        $view = 'order.list';
        if($id){
            $cond = ['id'=>$id];
            $view = 'order.view';
        }
        $getOrders = Order::with('orderDetail','orderDetail.getCategory','userData:id,first_name,last_name')->where($cond)->orderBy('id','DESC')->get()->toArray();
 
        return view($view,compact('getOrders'));

    }


    public function couponAdd(){ 
        if(isset($_POST) && !empty($_POST)){
            $saveArray   = [];
            $saveArray['coupon'] = ($_POST['copuon'])?$_POST['copuon']:'';
            $saveArray['users'] = ($_POST['users_list'])?$_POST['users_list']:'';
            $saveArray['start_date'] = ($_POST['coupon_state_date'])?$_POST['coupon_state_date']:'';
            $saveArray['end_date'] = ($_POST['coupon_end_date'])?$_POST['coupon_end_date']:'';
            $saveArray['category'] = ($_POST['select_category'])?$_POST['select_category']:'';

            $res = ExamModel::saveCoupons($saveArray);
            if($res){
                return redirect('/admin/orders/couponList');
            }else{
                return view('order.couponAdd');
            }
        }else{
            return view('order.couponAdd');

        }
    }

    public function couponList(){
        $res = ExamModel::getCouponList();
        return view('order.couponList',compact('res'));
    }
    public function couponActive($id,$status){
        $res = ExamModel::couponActive($id,$status);
        if($res){
            return back()->with('message', 'Coupon Status Updated.');
         }
    }
    public function CouponEdit($id){ 
        if(isset($_POST) && !empty($_POST)){
            $saveArray   = [];
            $saveArray['coupon'] = ($_POST['copuon'])?$_POST['copuon']:'';
            $saveArray['users'] = ($_POST['users_list'])?$_POST['users_list']:'';
            $saveArray['start_date'] = ($_POST['coupon_state_date'])?$_POST['coupon_state_date']:'';
            $saveArray['end_date'] = ($_POST['coupon_end_date'])?$_POST['coupon_end_date']:'';
            $saveArray['category'] = ($_POST['select_category'])?$_POST['select_category']:'';
          
           ExamModel::updateCoupon($id,$saveArray);
           return redirect()->route('couponList')->with('message', 'Coupon Updated Successfully.');
        }else{
            $res = ExamModel::listAllCoupon($id);
            return view('order.couponAdd',['res' => $res]);
        }
    }

}
