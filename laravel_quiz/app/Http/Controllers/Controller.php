<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController {

    use AuthorizesRequests,
        DispatchesJobs,
        ValidatesRequests;

    public static function pr($data) {
        echo '<pre>';
        print_r($data);
        echo '</pre>';
    }

    /**

     * success response method.

     *

     * @return \Illuminate\Http\Response

     */
    public function sendResponse($result, $message) {

        $response = [
            'success' => true,
            'data' => $result,
            'message' => $message,
        ];


        echo response()->json($response, 200)->getContent();
    }

    /**

     * return error response.

     *

     * @return \Illuminate\Http\Response

     */
    public function sendError($error, $errorMessages = [], $code = 404) {   

        $response = [
            'success' => false,
            'message' => $error,
        ];


        if (!empty($errorMessages)) {

            $response['data'] = $errorMessages;
        }


        echo response()->json($response, $code)->getContent();
    }

}
