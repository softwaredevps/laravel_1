<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contactus;
class ContactController extends Controller
{
    public function listContact(){
        $res = Contactus::listContact();
        return view('contact.listContactus',['cat' => $res]);
    }

    public function listdelete($id){
        $res = Contactus::listDelete($id);
        if($res){
            return redirect()->back()->with('message', 'Deleted Successfully.');
         }
    }
    public function listactive($id,$status){
        $res = Contactus::listactive($id,$status);
        if($res){
            return back()->with('message', 'Changed Successfully.');
         }
    }
}
