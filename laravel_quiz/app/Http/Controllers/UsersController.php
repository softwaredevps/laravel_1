<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Hash;
use Request;
use DB;
use Session;
use App\User;

class UsersController extends Controller {
    
    protected $APIURL;
    public function __construct() {
        $this->APIURL = env('API_URL');
        
    }

    public function index() {
        $users = User::whereNotIn('userRole',  ['1', '2'])->get();

        if (isset($users) and !empty($users)) {
            $users = $users->toArray();
        }
        return view('user.manage', compact('users'));
    }

    /*
     * Add Users
     */

    public function create() {
        
        return view('user.add');
    }
    
    public function store(){
        $requestdata = Request::all();
        if (isset($requestdata) and ! empty($requestdata)) {
            unset($requestdata['_token']);
            $result = $this->user_save($requestdata);

            if ($result['status'] == false) {
                return back()->withInput(Request::except('password'))->withErrors($result['msg']);
            } else {
                return redirect()->to('/admin/users')->with('info', $result['msg']);
            }
        }
    }

    /*
     *     Edit User function
     */

    public function show($user_id = null) {
        if (empty($user_id)) {
            return redirect()->to('/admin/users');
        }
        $user = User::findOrFail($user_id);
        
        return view('user.edit')->with('user', $user);
    }
    
    public function update($user_id = null){
        if (empty($user_id)) {
            return redirect()->to('/admin/users');
        }
        $requestdata = Request::except('_token');

        if (isset($requestdata) and ! empty($requestdata)) {
            unset($requestdata['_token']);
            $requestdata['id'] = $user_id;
            $result = $this->user_save($requestdata);

            if ($result['status'] == false) {
                return back()->withInput(Request::except('password'))->with('warning', $result['msg']);
            } else {
                return redirect()->to('/admin/users')->with('info', $result['msg']);
            }
        }
    }
    
    

    function user_save($requestdata) {

        if (isset($requestdata['id']) and ! empty($requestdata['id'])) { //edit case
            $rules = array(
                'first_name' => 'required|max:20',
                'last_name' => 'required|max:20'
            );
        } else {
            $rules = array(
                'first_name' => 'required|max:20',
                'last_name' => 'required|max:20',
                'gender' => 'required',
                'email' => 'required|max:20|unique:users',
                'password' => 'required|max:20|min:6',
                'confirm_password' => 'required|same:password',
            );
        }


        $validator = Validator::make($requestdata, $rules);
        //sdd($requestdata);
        if ($validator->fails()) {
            $msg = $validator->messages();
            $response['status'] = false;
            $response['msg'] = $validator;
            return $response;
            die;
        } else {
            $msg = 'Please try again!';
            $status = false;
            unset($requestdata['_method']);
            unset($requestdata['_token']);
            unset($requestdata['confirm_password']);
            if (empty($requestdata['gender'])) {
                $requestdata['gender'] = "";
            }
            //email validation
            $emailUser = User::select('email')->where('email', $requestdata['email'])->first();
            if (isset($requestdata['id']) and ! empty($requestdata['id'])) {
                unset($requestdata['password']);
                try {
                    $findUsername = User::select('id')->where('email', $requestdata['email'])->where('id', '!=', $requestdata['id'])->first();
                    if (!empty($findUsername)) {
                        $msg = 'username already taken';
                    } else {

                        User::where('id', $requestdata['id'])->update($requestdata);
                        $msg = 'User info updated successfully';
                        $status = true;
                    }
                } catch (\Exception $e) {
                    $msg = $e->getMessage();
                }
            } elseif (isset($emailUser) && !empty($emailUser) && !isset($requestdata['id'])) {
                $msg = 'Email already registered.';
                $status = false;
            } else {
                $requestdata['password'] = Hash::make($requestdata['password']);
                $requestdata['userRole'] = 3;
                $requestdata['picture'] = '';
                $requestdata['mobile'] = $requestdata['mobile'];
                $requestdata['gender'] = $requestdata['gender'];
                $requestdata['google_token'] = '';
                $requestdata['fb_token'] = '';

                User::create($requestdata);
                $msg = 'User added successfully';
                $status = true;
            }
            $response['status'] = $status;
            $response['msg'] = $msg;
            return $response;
            die;
        }
    }

    public function changeStatus($id, $status) {
        if (empty($id) || $status == null) {
            return redirect()->to('/admin/users')->with('warning', 'Invalid parameters!');
        } else {
            $res = User::where('id', $id)->update(['status' => $status]);
            return redirect()->to('/admin/users')->with('info', 'Status changed successfully');
        }
    }

    public function deleteUser($id) {
        if (empty($id)) {
            return redirect()->to('/admin/users')->with('warning', 'Invalid parameters!');
        } else {
            $res = User::where('id', $id)->update(['status' => '2']);
            return redirect()->to('/admin/users')->with('info', 'User deleted successfully');
        }
    }

    public function changePassword() {
        $request_data = Request::all();

        $newPass = Hash::make($request_data['pass']);
        //update password
        $result = User::where('id', $request_data['userID'])->update(['password' => $newPass]);
        if($result){
            echo json_encode(['status'=>true]);
        }else{
            echo json_encode(['status'=>false]);
        }
        die;
    }
    
    /*
     * ACCOUNT VERIFICATION FUNCTION
     */
    public function accountVerify($token) {
        if (empty($token)) {
            return redirect($this->APIURL . 'login?status=unauthenticated');
        } else {
            $user = User::getUser($token);
            
            if (empty($user)) {
                //user not found
                return redirect($this->APIURL . 'login?status=unauthenticated');
            } else {
                //verify account
                $updateUser['userVerifyToken'] = '';
                $updateUser['status'] = '1';
                User::where('id', $user[0]->id)->update($updateUser);
                return redirect($this->APIURL . 'login?status=verified');
            }
        }
    }
    
    public function clearCache(){
            $exitCode = \Artisan::call('cache:clear');
            return redirect()->back()->with('info', 'Cache cleared successfully');
    }
}
