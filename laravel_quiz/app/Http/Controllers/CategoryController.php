<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\CategoryModel;
use Exception;

class CategoryController extends Controller
{
    
    /**
     * Create category
     * @param Request $request
     * @return type
     */
    public function add(Request $request){
         if($request->isMethod('post')){
             $requestData = $request->all();
             
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension(); // getting image extension
//             self::pr($request->all());die;
            if(trim($request->get('categoryname')) == ''){
                return back()->with('warning', 'Please enter category.');
            }else if(!in_array($extension, ['png', 'jpg','jpeg'])){
                return back()->with('warning', 'Please select png/jpg/jpeg file.');
            }else if(($request->get('isSubCategory')) == 'on' && ($request->get('parent_category_id')) == '' ){
                return back()->with('warning', 'Please select parent category.');  
            }else{
                
                
                $filename =time().'.'.$extension;
                try{
                    \Storage::disk('uploads')->putFileAs(
                        '/',
                        $file,
                        $filename
                      );
                }catch(\Exception $e){
                    
                }catch(\InvalidArgumentException $e){
                }
                
                $requestData['categoryname']        = ucwords($request->get('categoryname'));
                $requestData['categoryslug']        = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '_', $request->get('categoryname'))));
                $requestData['image']               = 'uploads/logos/'.$filename;
                $requestData['parent_category_id']  = ($request->get('isSubCategory') == 'on') ? $request->get('parent_category_id') : 0; 
                $requestData['price']               = $request->get('price'); 
//                dd($requestData);
                $checkExists = CategoryModel::getCategory($request->get('categoryslug'));
                if($checkExists){
                    return back()->with('warning', 'Category already exists.');
                }else{
                    $res = CategoryModel::saveData($requestData);
                    if($res){
                       return redirect()->to('/admin/category/list')->with('info', 'Category Added Successfully.');
                    }
                }
                
            }
            
        }
        
        $getParentCategory = CategoryModel::getParentCategry();
        return view('category.addCategory', ['parentCategoryId'=>$getParentCategory, 'type'=>'add']);
    }
    
    /**
     * List Parent categories
     * @param Request $request
     * @return type
     */
    public function listCategory(Request $request){
        $res = CategoryModel::listData('parent');
//        dd($res);
        return view('category.list',['cat' => $res, 'type'=>'parent']);
    }
    
    /**
     * List Child categories
     * @param Request $request
     * @return type
     */
    public function listSubcategory($id){
        $res = CategoryModel::listData($id);
        return view('category.list',['cat' => $res, 'type'=>'child']);
    }
    
    /**
     * Edit Category
     * @param Request $request
     * @param type $id
     * @return type
     */
    public function listedit(Request $request, $id){
         $data = CategoryModel::listAllData($id);  
       if($request->isMethod('post')){
            $requestData['categoryname'] = ucwords($request->get('categoryname'));
            $requestData['categoryslug'] = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '_', $request->get('categoryname'))));
            $file = $request->file('image');
            if(isset($file) && !empty($file)){
                $extension = $file->getClientOriginalExtension(); // getting image extension
                $filename  = time().'.'.$extension;
                try{
                    \Storage::disk('uploads')->putFileAs(
                        '/',
                        $file,
                        $filename
                      );
                    $filename = 'uploads/logos/'.$filename;
                }catch(\Exception $e){
                    
                }
            }else{
                $filename = $data['image'];
            }
            $requestData['image'] = $filename;
            $requestData['update_id'] = $id;
//            if(isset($request->get('parent_category_id'))){
                $requestData['parent_category_id'] = $request->get('parent_category_id') ?? 0;
                $requestData['price'] = $request->get('price') ?? 0;
//            }
            
            
            $checkExists = CategoryModel::getCategory($request->get('categoryslug'));
            if($checkExists){
                return back()->with('warning', 'Category already exists.');
            }else{
                $res = CategoryModel::updateData($requestData);
                if($res){
                   return redirect()->back()->with('info', 'Category Updated Successfully.');
                }
            }
        } 
        
        $getParentCategory = CategoryModel::getParentCategry();
//        dd($data);
        return view('category.addCategory', ['res' => $data, 'parentCategoryId'=>$getParentCategory, 'type'=>'update']);
    }
    
    /**
     * Delete category
     * @param type $id
     * @return type
     */
    public function listdelete($id){
        $res = CategoryModel::listDelete($id);
        if($res){
            return back()->with('message', 'Category Deleted Successfully.');
         }
    }
    
    /**
     * Change category status
     * @param type $id
     * @param type $status
     * @return type
     */
    public function listactive($id,$status){
        $res = CategoryModel::listactive($id,$status);
        if($res){
            return back()->with('message', 'Category Changed Successfully.');
         }
    }
    
}