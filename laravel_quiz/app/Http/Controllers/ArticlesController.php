<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\ArticlesModel;
use Exception;
use DB;

class ArticlesController extends Controller
{
    public function add(Request $request){
        if($request->isMethod('post')){
            $rules = array(
                'articlename' => 'required|max:200',
                'articlename' => 'required|max:200',
            );
            $validator = Validator::make($_POST, $rules);
            
            if ($validator->fails()) {
                $err = $validator->messages()->toArray();
                $errs = '<ul>';
                foreach ($err as $er) {
                    $errs .= '<li>' . $er[0] . '</li>';
                }
                $errs .= '</ul>';
                return back()->withInput($request->all())->with('warning', $errs);
            } else {
                $res = ArticlesModel::saveData($request->all());
                if($res){
                   return redirect()->route('articles')->with('message', 'Article Added Successfully.');
                }
            }
            
        }
        return view('articles.addArticles');
    }
    public function listArticles(){
        $res = ArticlesModel::listData();
        return view('articles.listArticles',['cat' => $res]);
    }

    public function listedit(Request $request){
        if($request->isMethod('post')){
            $res = ArticlesModel::updateData($request->all());
            if($res){
               return redirect()->route('articles')->with('message', 'Article Updated Successfully.');
            }
        }
        $res = ArticlesModel::listAllData($request->get('update_id'));             
        return view('articles.addArticles',['res' => $res]);
    }
    public function listdelete($id){
        $res = ArticlesModel::listDelete($id);
        if($res){
            return redirect()->back()->with('message', 'Article Deleted Successfully.');
         }
    }
    public function listactive($id,$status){
        $res = ArticlesModel::listactive($id,$status);
        if($res){
            return back()->with('message', 'Article Changed Successfully.');
         }
    }

    public function addNews(Request $request){
        if(isset($_POST) && !empty($_POST)){
            $file = $request->file('image');
            $requestData = $request->all();

            $saveArray   = [];
            $saveArray['heading'] = isset($_POST['heading'])?$_POST['heading']:'';
            $saveArray['link'] = isset($_POST['link'])?$_POST['link']:'';
           // $saveArray['image'] = isset($_POST['image'])?$_POST['image']:'0';
            $saveArray['description'] = isset($_POST['description'])?$_POST['description']:'';

            if(isset($_POST['update_id']) && !empty($_POST['update_id'])){
                ArticlesModel::updateNews($saveArray,$_POST['update_id']);
                $res = $_POST['update_id'];
            }else{
                $res = ArticlesModel::saveNews($saveArray);
            }
            //dd($res);
            if($res){

                if($file){
                    $extension = $file->getClientOriginalExtension();
                    $filename = "news$res.".$extension;
                    try{
                        \Storage::disk('news')->putFileAs(
                            '/',
                            $file,
                            $filename
                          );
                    }catch(\Exception $e){

                    }catch(\InvalidArgumentException $e){

                    }

                    DB::table('news')->where('id',$res)->update(['image'=> $filename]);
                }
                
                
                if ($res) {
                    return redirect()->to('/admin/articles/listNews')->with('info', 'News Added Successfully.');
                }

            }else{
                return view('articles.addNews');
            }
        }else{
            return view('articles.addNews');

        }
    }

    public function listNews(){
        $res = ArticlesModel::listNews();             
        return view('articles.listNews',['res' => $res]);
    }

    public function newsEdit($id){
        $res = ArticlesModel::listNewsById($id);             
        return view('articles.addNews',['res' => $res]);
    }

    public function newsActive($id,$status){
        $res = ArticlesModel::listactiveNews($id, $status);
        if ($res) {
            return back()->with('message', 'News Changed Successfully.');
        }
    }

    public function newsdelete($id){
        $res = ArticlesModel::newsdelete($id);
        if ($res) {
            return back()->with('message', 'News Changed Successfully.');
        }
    }
}
