<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Session;
use Hash;
use Config;

use App\User;
use App\CategoryModel;
use App\ExamOrder;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class AdminController extends Controller {

    use AuthenticatesUsers;

    public function __construct(Auth $auth) {
    }
	

    /*
     * Admin main screen
     */

    public function dashboard() { //dd('here');
        $activeUsers = User::where('status','=','1')->whereNotIn('userRole',['1','2'])->count();
        $totalCategories = CategoryModel::where('parent_category_id','=','0')->count();
        $totalOrders = ExamOrder::count();
        $totalEarning = ExamOrder::sum('order_amount');
//        dd($totalEarning);
       
        return view('admins.dashboard', compact('activeUsers','totalCategories','totalOrders','totalEarning'));
    }

   

}
