<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Question;
use App\QuestionExamModel;
use Exception;
use DB;
use App\QuestionReportModel;
//use Request;

class QuestionsController extends Controller {

    public function add(Request $request) {
        if (isset($_POST) && !empty($_POST)) {
            $file = $request->file('image');
            $requestData = $request->all();
//            dd($file);
            $rules = array(
                'questionDesc' => 'required',
                'optionFirst' => 'required|max:200',
                'optionSecond' => 'required|max:200',
                'optionThird' => 'required|max:200',
                'optionFourth' => 'required|max:200',
                'rightAnswer' => 'required'
            );
//            dd($_FILES);
            $validator = Validator::make($requestData, $rules);

            if ($validator->fails()) {
                $err = $validator->messages()->toArray();
                $errs = '<ul>';
                foreach ($err as $er) {
                    $errs .= '<li>' . $er[0] . '</li>';
                }
                $errs .= '</ul>';
                return back()->withInput($requestData)->with('warning', $errs);
            } else {
                
                $res = Question::saveData($requestData);
                
                if($file){
                    $extension = $file->getClientOriginalExtension();
                    $filename = "question$res.".$extension;
                    try{
                        \Storage::disk('questions')->putFileAs(
                            '/',
                            $file,
                            $filename
                          );
                    }catch(\Exception $e){

                    }catch(\InvalidArgumentException $e){

                    }

                    Question::where('id',$res)->update(['image'=> $filename]);
                }
                
                
                if ($res) {
                    return redirect()->to('/admin/questions/list')->with('info', 'Question Added Successfully.');
                }
            }
        }
        $res = [];
        $allExams = DB::table('exams')->get();

        if (isset($allExams) && !empty($allExams)) {
            $allExams = $allExams->toArray();
        }
        return view('questions.addQuestion', compact('allExams','res'));
    }

    public function listQuestions() {
        $examId = isset($_GET['exams']) ? $_GET['exams'] : '';
        if(!empty($examId)){
            $res = QuestionExamModel::listQuestionWithExams($examId);
        }else{
            $res = Question::listData();
        }
//        dd($res);
        return view('questions.listQuestion', ['cat' => $res]);
    }

    public function listedit(Request $request, $id) {
        if (isset($_POST) && !empty($_POST) && $_POST['update']) {
            $res = Question::updateData($_POST);
            if ($res) {
                
                //UPDATE IMAGE IF ANY
                $file = $request->file('image');
//                dd($file);
                if($file){
                    $extension = $file->getClientOriginalExtension();
                    $filename = "question$id.".$extension;
                    try{
                        \Storage::disk('questions')->putFileAs(
                            '/',
                            $file,
                            $filename
                          );
                    }catch(\Exception $e){

                    }catch(\InvalidArgumentException $e){

                    }

                    Question::where('id',$id)->update(['image'=> $filename]);
                }
                $getPrevNextlinks = Question::getLinks($id);
                return redirect()->to('/admin/questions/listedit/'.$id)->with('questionSuccess', 
                        
                        ['msg'=>'Question Updated Successfully.',
                          'listUrl'=>url('/').'/admin/questions/list',
                          'prevUrl'=>(!empty($getPrevNextlinks['prevUrl'])) ? url('/').'/admin/questions/listedit/'.$getPrevNextlinks['prevUrl'][0]->id : 'javascript:;', 
                          'nextUrl'=>(!empty($getPrevNextlinks['nextUrl'])) ? url('/').'/admin/questions/listedit/'.$getPrevNextlinks['nextUrl'][0]->id : 'javascript:;', 
                        ]
                        );
            }
        }
        
        //GET QUESTION DETAIL AND SELECTED CATEGORY LIST
        $res = Question::with('questionExams', 'questionExams.examData')->where('id', $id)->first();
        if (isset($res) && !empty($res)) {
            $res = $res->toArray();
        }
        $allExams = DB::table('exams')->get();

        if (isset($allExams) && !empty($allExams)) {
            $allExams = $allExams->toArray();
        }
        return view('questions.addQuestion', compact('allExams', 'res'));
    }

    public function listdelete($id) {
        $res = Question::listDelete($id);
        if ($res) {
            return back()->with('message', 'Question Deleted Successfully.');
        }
    }

    public function listactive($id, $status) {
        $res = Question::listactive($id, $status);
        if ($res) {
            return back()->with('message', 'Question Changed Successfully.');
        }
    }
    
    public function reports($id=null) {
        $cond = [];
        $view = 'questions.reports';
        if($id){
            $cond = ['id'=>$id];
            $view = 'questions.viewReports';
        }
        $reports = QuestionReportModel::with('userData:id,first_name,last_name','questions')->where($cond)->orderBy('id','DESC')->get()->toArray();
        return view($view, compact('reports'));
    }
    
    public function deleteImage(Request $request) {
        $requestData = $request->all();
//        echo public_path();die;
        if($requestData['questionID']){
            unlink(public_path('/uploads/question-images/question'.$requestData['questionID'].'.jpg'));
            Question::where('id', $requestData['questionID'])->update(['image'=>'']);
            echo json_encode(['status'=>true]);
        }else{
            echo json_encode(['status'=>false]);
        }
    }

}
