<?php

namespace App\Http\Middleware;

// First copy this file into your middleware directoy

use Closure;
use Illuminate\Support\Facades\Redirect;
use App\User;

class CheckRole {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $this->findUserModel = new User;
        // Get the required roles from the route
        $roles = $this->getRequiredRoleForRoute($request->route());
        // Check if a role is required for the route, and
        // if so, ensure that the user has that role.

        if ($this->findUserModel->hasRole($roles) || !$roles) {
            return $next($request);
        }
        return Redirect::to('/');
//		return response([
//			'error' => [
//				'code' => 'INSUFFICIENT_ROLE',
//				'description' => 'You are not authorized to access this resource.'
//			]
//		], 401);
    }

    private function getRequiredRoleForRoute($route) {
        $actions = $route->getAction();
        return isset($actions['roles']) ? $actions['roles'] : null;
    }

}
