<?php

namespace App\Http\Middleware;

use Closure;

class DecodeIdParam
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
         $requestIdEncode = $request->route()->parameters()['id'];
         $decodedChar = config('app.EncodedChar') ?? 15;
//        $decodedChar =  15;
//        echo $requestIdEncode.' '.substr($requestIdEncode, $decodedChar,-$decodedChar).' '.chr(4948);
        try{
            $requestIdDecode   = (substr($requestIdEncode, $decodedChar,-$decodedChar));
        }catch(\Exception $e){
            $requestIdDecode = 0;
        }
//        echo $requestIdDecode;
        $request->route()->setParameter('id', $requestIdDecode);

        return $next($request);
    }
}
