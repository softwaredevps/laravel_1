<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;
use App\ExamCategoryModel;

class ExamOrder extends Model
{
    protected $table = "exam_orders";
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_num', 'category_id', 'user_id', 'order_amount', 'order_actual_amount','order_discount', 'status', 'transaction_id', 'payment_mode', 'transaction_date', 'bank_transaction_id', 'bank_name', 'currency', 'payment_gateway', 'order_message'
    ];
    protected $attributes = [
        'status' => '0',
    ];
    public static function boot(){
        parent::boot();

        static::creating(function ($instance){
            $instance->status = '0';
            $instance->created_at = $instance->updated_at = date('Y-m-d h:i:s');
            
        });
        
    }
    
    public function examCategories()
    {
        return $this->hasMany('App\ExamCategoryModel', 'exam_id');
    }
    
    public function examQuestions()
    {
        return $this->hasMany('App\QuestionExamOrder', 'exam_id');
    }
    
    public function exam(){
        return $this->belongsTo('App\ExamOrder', 'category_id');
    }
    
    public function userData(){
        return $this->belongsTo('App\User', 'user_id');
    }
    
    public function orderDetail(){
        return $this->hasMany('App\ExamOrderDetail', 'order_id');
    }
    
    
    public static function getMyOrders($param){
        $whereCond = ['user_id'=>$param['user_id']];
        if($param['order_num']){
            $whereCond = ['user_id'=>$param['user_id'],'order_num'=>$param['order_num']];
        }
        $orders = self::with('orderDetail','orderDetail.getCategory')->where($whereCond)->skip($param['page'])->take($param['limit'])->orderBy('id', 'DESC')->get()->toArray();
        return $orders;
    }
}
