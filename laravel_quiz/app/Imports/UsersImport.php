<?php

namespace App\Imports;

//use App\QuestionsModel;
use Maatwebsite\Excel\Concerns\ToModel;
use DB;
use App\Question;

class UsersImport implements ToModel {

    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $data) {
        $insert_data = [];
        //IF DATA IS PRESENT
        if (count($data) > 0) {
            if ($data[1] != 'Question') {
                //CHECK CATEGORY IF EXISTS
                $cats = [];
                if (!empty($data[9])) {
                    $cats = explode(',', $data[9]);
                }
                //PREPARE DATA
                $insert_data = array(
                    'questionDesc' => (string) $data[1],
                    'optionFirst' => (string) $data[2],
                    'optionSecond' => (string) $data[3],
                    'optionThird' => (string) $data[4],
                    'optionFourth' => (string) $data[5],
                    'rightAnswer' => (string) $data[6],
                    'status' => (string) $data[7],
                    'type' => ($data[8] == 'English') ? '1' : '2',
                );
                $insert_data['exams'] = $cats;
                //SAVE DATA
                try {
                    $res = Question::saveData($insert_data);
                } catch (\Exception $e) {
                    //Skip error if any
                }
            }
        }
    }

}
