<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class QuestionsModel extends Model
{
    static function saveData($data){
        $data = array(
            'question'=>trim($data['questionDesc']),
            'questionfirst'=>trim($data['questionfirst']),
            'questionsecond'=>trim($data['questionSecond']),
            'questionthrid'=>trim($data['questionThrid']),
            'questionfourth'=>trim($data['questionFourth']),
            'currectanswer'=>trim($data['rightAnswer']),
            'type'=>trim($data['type']),
        );
        return DB::table('question')->insert($data);
    }

    static function listData(){
        return DB::table('question')->get()->toArray();
    }

    static function listAllData($id){
        return DB::table('question')->where('id',$id)->get()->toArray()[0];
    }
    static function updateData($data){
     //   dd($data);
        $res = array(
            'question'=>trim($data['questionDesc']),
            'questionfirst'=>trim($data['questionfirst']),
            'questionsecond'=>trim($data['questionSecond']),
            'questionthrid'=>trim($data['questionThrid']),
            'questionfourth'=>trim($data['questionFourth']),
            'currectanswer'=>trim($data['rightAnswer']),
            'type'=>trim($data['type']),
        );
        return DB::table('question')->where('id',$data['update_id'])->update($res);
    }
    static function listDelete($id){
        return DB::table('question')->where('id', $id)->delete();
    }
    static function listactive($id,$status){
        $res = array('status'=>!$status);
        return DB::table('question')->where('id',$id)->update($res);
    }
}
