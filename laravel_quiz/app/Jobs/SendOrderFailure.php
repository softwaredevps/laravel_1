<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use App\Mail\OrderFailure ;

class SendOrderFailure implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $subject;
    protected $receiver;
    protected $content;
    
    public function __construct($data, $receiver)
    {
        $this->subject = 'Order Failure';
        $this->receiver = $receiver;
        $this->content = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    { 
        $email = new OrderFailure($this->subject,$this->content,$this->receiver);
        
        Mail::to($this->receiver)->send($email);
    }
}
