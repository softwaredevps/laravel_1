<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Request;
use DB; 
use Crypt; 
use Hash; 

class PasswordReset extends Authenticatable
{
    
    public $timestamps = false;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'token','created_at'
    ];

    public function user() {
        return $this->hasOne('App\User', 'email', 'email');
    }
}


