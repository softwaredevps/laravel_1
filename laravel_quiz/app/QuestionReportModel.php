<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;

class QuestionReportModel extends Model
{
    protected $table = "question_reports";
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'question_id', 'user_id', 'comment', 'status'
    ];
    protected $attributes = [
        'status' => '1',
    ];
    public static function boot(){
        parent::boot();

        static::creating(function ($instance){
            $instance->status = '1';
            $instance->created_at = $instance->updated_at = date('Y-m-d h:i:s');
            
        });
        
    }
    
    public function userData(){
        return $this->belongsTo('App\User', 'user_id');
    }
    
    public function questions(){
        return $this->belongsTo('App\Question', 'question_id');
    }
}
