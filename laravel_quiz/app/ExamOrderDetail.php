<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;
use App\ExamCategoryModel;

class ExamOrderDetail extends Model
{
    protected $table = "exam_order_details";
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id', 'category_id'
    ];
    
    
    public function getOrder()
    {
        return $this->hasMany('App\ExamOrder', 'id', 'order_id');
    }
    
    public function getCategory()
    {
        return $this->hasOne('App\CategoryModel', 'id', 'category_id');
    }
    

}
