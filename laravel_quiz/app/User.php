<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Request;
use DB; 
use Crypt; 
use Hash; 

class User extends Authenticatable
{
      use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'password_hash','picture','status','userRole','google_token','fb_token','mobile','gender'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
	
	
	  
    public static function boot()
    { 
        parent::boot();
       static::saving(function ($model) {
            $credentials = Request::all();
		
            if(isset($credentials['provider']) && !empty($credentials['provider'])){
                $password = $credentials['id'];
            }else{
                $password = isset($credentials['password']) ? $credentials['password'] : (($credentials['password']) ? $credentials['password'] : '');
            }
            $model->password_hash   = Crypt::encrypt($password);
            $model->password        = Hash::make($password);
            
        });
    }    
        

			

    public function role() {
        return $this->hasOne('App\Role', 'id');
    }

    public function hasRole($roles) {
//        echo $roles;
        $this->have_role = Request::user()->userRole;

        if ($this->have_role == '1') {
            $this->have_role = 'admin';
        }else if ($this->have_role == '2') {
            $this->have_role = 'editor';
        }else if ($this->have_role == '3') {
            $this->have_role = 'user';
        }else if ($this->have_role == '3') {
            $this->have_role = 'guest';
        } else {
            $this->have_role = 'spam';
        }
        // Check if the user is a root account
//        if (in_array($this->have_role, $roles)) {
//            return true;
//        }
        if (is_array($roles)) {
            foreach ($roles as $need_role) {
                if ($this->checkIfUserHasRole($need_role)) {
                    return true;
                }
            }
        } else {
            return $this->checkIfUserHasRole($roles);
        }
        return false;
    }

    private function getUserRole() {
        return $this->role()->getResults();
    }

    private function checkIfUserHasRole($need_role) {
        return (strtolower($need_role) == strtolower($this->have_role)) ? true : false;
    }
    
    public static function getUser($value=null){

        $user =  DB::table('users')->where(['id'=>$value])->orWhere(['email'=>$value])->get();
        if ($user) {
            return $user->toArray();
        } 
    }
    
    public static function getAdmin(){
        $admin = DB::table('users')->where(['userRole'=>1])->get()->toArray();
        return $admin;
    }
    
    public static function updateProfile($postData){
        echo '<pre>';
        print_r($postData);die;
    }
}


