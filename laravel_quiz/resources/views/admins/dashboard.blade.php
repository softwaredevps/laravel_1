@extends('layouts.app')

@section('content')
   
       
       

			<!-- page content -->
                        <div class="right_col" role="main" >
			  <!-- top tiles -->
			  <div class="row tile_count">
                               @include('layouts.flash')
				<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
				  <span class="count_top"><i class="fa fa-user"></i> Total Active Users</span>
				  <div class="count">{{ $activeUsers ?? 0}}</div>
				</div>
				<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
				  <span class="count_top"><i class="fa fa-clock-o"></i> Total Categories</span>
				  <div class="count">{{ $totalCategories ?? 0}}</div>
				</div>
				<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
				  <span class="count_top"><i class="fa fa-user"></i> Total Order</span>
				  <div class="count green">{{$totalOrders ?? 0}}</div>
				</div>
				<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
				  <span class="count_top"><i class="fa fa-user"></i> Total Earnings</span>
				  <div class="count">&#8377;{{ $totalEarning ?? 0}}</div>
				</div>
<!--				<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
				  <span class="count_top"><i class="fa fa-user"></i> Total Collections</span>
				  <div class="count">2,315</div>
				  <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last Week</span>
				</div>
				<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
				  <span class="count_top"><i class="fa fa-user"></i> Total Connections</span>
				  <div class="count">7,325</div>
				  <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last Week</span>
				</div>-->
			  </div>
			  <!-- /top tiles -->

			 

			


			 
			</div>
			<!-- /page content -->

			

<?php 

$a = [
    [
        'name'=>'Rahul',
        'marks'=>80,
        'subject'=>'Maths',
    ],
    [
        'name'=>'Akhil',
        'marks'=>80,
        'subject'=>'Maths',
    ],
    [
        'name'=>'Rahul',
        'marks'=>70,
        'subject'=>'Physics',
    ],
    [
        'name'=>'Rahul',
        'marks'=>60,
        'subject'=>'Chem',
    ],
    [
        'name'=>'Akhil',
        'marks'=>85,
        'subject'=>'Physics',
    ],
    [
        'name'=>'Gupta',
        'marks'=>90,
        'subject'=>'Maths',
    ],
    [
        'name'=>'Rahul',
        'marks'=>90,
        'subject'=>'Bio',
    ],
    [
        'name'=>'Gupta',
        'marks'=>70,
        'subject'=>'Bio',
    ],
    [
        'name'=>'Gupta',
        'marks'=>60,
        'subject'=>'Physics',
    ]
];

echo '<pre>';
print_r($a);
$b = [];
foreach($a as $k => $v){
    foreach ($a as $k1 => $v1){
        
        if($v['name'] == $v1['name']){
            if(isset($b[$v['name']])){
                $b[$v['name']]['marks'] += $v1['marks'];
                unset($a[$k1]);
            }
            else{
                $b[$v['name']] = $v1;
                 unset($a[$k1]);
            }
        }
        
    }
}

print_r($b);
echo '</pre>';


?>


@endsection