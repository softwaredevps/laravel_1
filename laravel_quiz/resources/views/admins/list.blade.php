
@extends('layouts.app')

@section('content')
<div class="right_col" role="main" style="min-height: 4595px;">
   <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
         <div class="x_panel">
            <div class="x_title">
               <h2>List Category</h2>
               <div class="clearfix"></div>
            </div>
            <div class="x_content">
               <div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                  <div class="row">
                     <div class="col-sm-12">
                        <table id="datatable" class="table table-striped table-bordered dataTable no-footer" style="width: 100%;" role="grid" aria-describedby="datatable_info">
                           <thead>
                              <tr role="row">
                                 <th class="sorting_asc" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 93px;">Id</th>
                                 <th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 153px;">Category Name</th>
                                 <th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width: 67px;">Category Slug</th>
                                 <th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-label="Age: activate to sort column ascending" style="width: 26px;">Status</th> 
                                 <th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-label="Age: activate to sort column ascending" style="width: 26px;">Action</th>   
                              </tr>
                           </thead> 
                           <tbody>                             
                                @foreach ($cat as $key => $res)
                                 <tr role="row" class="odd">
                                       <td class="sorting_1">{{ $key + 1}} </td>
                                       <td> {{ $res->category_name }} </td>
                                       <td> {{ $res->category_slug }} </td>
                                       <td>   
                                          <a class="btn <?php echo (isset($res->status) && $res->status == 1)?'btn-danger':'btn-success' ?>"  href="{{ url('admin/category/listactive/' . $res->id.'/'.$res->status)}}">
                                             <?php echo (isset($res->status) && $res->status == 1)?'InActive':'Active' ?>
                                          </a> 
                                       </td>
                                       <td class=" last">
                                          <a class="fa fa-pencil" href="{{ url('admin/category/listedit/' . $res->id)}}"></a>&nbsp&nbsp
                                          <a class="fa fa-trash" onclick='return confirm("Are you sure you want to delete this Category?")' href="{{ url('admin/category/listdelete/' . $res->id)}}"></a>
                                       </td>
                                 </tr>
                                @endforeach                              
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection