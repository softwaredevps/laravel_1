@extends('layouts.app')

@section('content')
  <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
              {!! Form::open() !!}
               <h1>Admin Login</h1>
       @include('layouts.flash')
       
       <div>
                {!! Form::text('email', null, array('class'=>'form-control', 'placeholder'=>'', 'required'=>true))  !!}
                 @if ($errors->has('email'))
                                    <span class="help-block errors">
                                        <p>{{ $errors->first('email') }}</p>
                                    </span>
                                @endif
              </div>
              <div>
             {{ Form::password('password', array('placeholder'=>'', 'class'=>'form-control', 'required'=>true ) ) }}
       @if ($errors->has('password'))
                                    <span class="help-block  errors">
                                        <p>{{ $errors->first('password') }}</p>
                                    </span>
                                @endif
              </div>
              <div>
                <!--<a class="btn btn-default submit" href="index.html">Log in</a>-->
                 <button type="submit" class="btn btn-default">Login</button>  
              
              </div>

              <div class="clearfix"></div>
              
 <div class="separator">
                

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><i class="fa fa-camera"></i> NTA ADMIN</h1>
                  <p>&copy;{{date('Y')}} All Rights Reserved.</p>
                </div>
              </div>
                {!! Form::close() !!} 
          </section>
        </div>
 </div>
    
@endsection

 <!--<a href="{{ url('/redirect') }}" class="btn btn-primary">Login With Google</a>-->
