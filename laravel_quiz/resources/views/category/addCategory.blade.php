
@extends('layouts.app')

@section('content')
<div class="right_col" role="main">
   <div class="row">
      <div class="col-md-12 col-sm-12 ">
         <div class="x_panel">
            <div class="x_title">
               <h2>Add Catergory</h2>
               <div class="clearfix"></div>
            </div>
            @include('layouts.flash')
            <div class="x_content">
               <br>
               <form id="Category-add"  class="form-horizontal form-label-left" method="post" enctype="multipart/form-data">
               @csrf
                  <div class="item form-group">
                     <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Category Name<span class="required">*</span>
                     </label>
                     <div class="col-md-6 col-sm-6 ">
                        <input placeholder="Enter Catergory Name" type="text" id="categoryname" name="categoryname" required class="form-control" value="<?php echo isset($res)?$res['category_name']:''; ?>" required>
                     </div>
                  </div>
               
                  <div class="item form-group">
                     <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Image<span class="required">*</span>
                     </label>
                     
                      @if(isset($res['image']))
                      <div class="col-md-6 col-sm-6 ">
                        <input  type="file" i name="image" class="form-control" >
                        </div>
                      <div class="col-md-6 col-sm-6 col-md-offset-3">
                        <img src="{{url('/public/').'/'.$res['image'] }}" style="width:100px; height:100px;">
                      </div>
                      @else
                      <div class="col-md-6 col-sm-6 ">
                          <input  type="file" i name="image" class="form-control" required="">
                        </div>
                      @endif
                  </div>
               
               
               
               @if((isset($res) && !empty($res['parent_category'])) || $type == 'add')
                <div class="item form-group">
                     <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Create Sub Category
                     </label>
                     <div class="col-md-6 col-sm-6 ">
                        @if(isset($res) && $res['id'])
                            <input type="checkbox" readonly="readonly" disabled checked id="isSubCategory" name="isSubCategory"  >
                        @else
                            <input type="checkbox" id="isSubCategory" name="isSubCategory"  >
                        @endif
                     </div>
                    
                    <div class="col-md-6 col-sm-6 selectParent" style="display: none;">
                         <span>Select parent category</span>
                        <select  name="parent_category_id" class="form-control" >
                            <option value="">Select Parent Category</option>
                            @foreach($parentCategoryId as $parent)
                                <option value="{{$parent['id']}}" {{ (isset($res) && $res['parent_category'] && $res['parent_category']['id'] == $parent['id']) ? 'selected': '' }} >{{$parent['category_name']}}</option>
                            @endforeach
                        </select>
                        <br>
                        <span>Total Price</span>
                        <input type="number" name="price" class="form-control" value="{{(isset($res) && $res['price'] ) ? $res['price']: '0' }}"> 
                     </div>
                </div>
               @endif
                 
                  <div class="ln_solid"></div>
                  <div class="item form-group">
                     <div class="col-md-6 col-sm-6 offset-md-3">
                        <?php  if(isset($res) && $res['id']){ ?>
                              <input type="submit" class="btn btn-success" name="update" value="Update"/>
                              <input type="hidden" name="update_id" value="<?php echo isset($res)?$res['id']:''; ?>">
                        <?php } ?>
                        <?php  if(!isset($res)){ ?>
                              <input type="submit" class="btn btn-success"/>
                        <?php } ?>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>

<script>
    
    $(document).on('ready', function(){
        var status = $('#isSubCategory').is(':checked');
         if(status){
           $('.selectParent').show();
           $('select[name="parent_category_id"]').attr('required', 'required');
        }
    });
    $(document).on('click', '#isSubCategory', function(){
       var status = $(this).is(':checked');
       if(status){
           $('.selectParent').show();
           $('select[name="parent_category_id"]').attr('required', 'required');
       }else{
           $('.selectParent').hide();
           $('select[name="parent_category_id"]').removeAttr('required');

       }
    });
</script>

@endsection