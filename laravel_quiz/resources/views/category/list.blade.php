
@extends('layouts.app')

@section('content')
<div class="right_col" role="main">
   <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
         <div class="x_panel">
            <div class="x_title">
               <h2>{{ ($type == 'parent') ? 'List Category' : 'List Sub Category' }}</h2>
               <div class="clearfix"></div>
            </div>
             @include('layouts.flash')
            <div class="x_content">
               <div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                  <div class="row">
                     <div class="col-sm-12">
                        <table id="datatable" class="table table-striped table-bordered dataTable no-footer" style="width: 100%;" role="grid" aria-describedby="datatable_info">
                           <thead>
                              <tr role="row">
                                 <th class="sorting_asc" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 93px;">Id</th>
                                 
                                 <th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 153px;">{{ ($type == 'parent') ? 'Category Name' : 'Sub Category Name' }}</th>
                                 @if($type == 'parent')
                                 <th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 153px;">Child category count</th>
                                 @endif
                                 <th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-label="Age: activate to sort column ascending" style="width: 26px;">Status</th> 
                                 <th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-label="Age: activate to sort column ascending" style="width: 26px;">Action</th>   
                              </tr>
                           </thead> 
                           <tbody>                             
                                @foreach ($cat as $key => $res)
                                 <tr role="row" class="odd">
                                       <td class="sorting_1">{{ $key + 1}} </td>
                                       <td> {{ $res['category_name'] }} </td>
                                       @if($type == 'parent')
                                            <td> {{ $res['child_category_count'] }} </td>
                                       @endif
                                       <td class=" " >
                                            <?php if ($res['status'] == '1') { ?> 
                                                <a class="btn btn-success" href="{{ url('admin/category/listactive/'.$res['id'].'/0' ) }}" onclick='return confirm("Are you sure you want to change the status of this category to InActive?")'>Active</a>
                                            <?php } else { ?>
                                                <a class="btn btn-danger" href="{{ url('admin/category/listactive/'.$res['id'].'/1' ) }}" onclick='return confirm("Are you sure you want to change the status of this category to Active?")'>InActive</a> 
                                            <?php } ?></i>
                                        </td>
                                       <td class=" last">
                                          <a class="fa fa-pencil" title='edit category' href="{{ url('admin/category/listedit/' . $res['id'])}}"></a>&nbsp&nbsp
                                          @if($type == 'parent')
                                          <a class="fa fa-eye" title='list sub category' href="{{ url('admin/category/sub-category/' . $res['id'])}}"></a>
                                          @endif
                                       </td>
                                 </tr>
                                @endforeach                              
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection