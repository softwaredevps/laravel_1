
@extends('layouts.app')

@section('content')
<div class="right_col" role="main">
   <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
         <div class="x_panel">
            <div class="x_title">
               <h2>List Exam</h2>
               <div class="clearfix"></div>
            </div>
             @include('layouts.flash')
            <div class="x_content">
               <div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                  <div class="row">
                     <div class="col-sm-12">
                        <table id="datatable" class="table table-striped table-bordered dataTable no-footer" style="width: 100%;" role="grid" aria-describedby="datatable_info">
                           <thead>
                              <tr role="row">
                                 <th class="sorting_asc" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 93px;">Id</th>
                                 
                                 <th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 153px;">Exam Name</th>
                                 <th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 153px;">Questions</th>
                                 
                                 <th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-label="Age: activate to sort column ascending" style="width: 26px;">Status</th> 
                                 <th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-label="Age: activate to sort column ascending" style="width: 26px;">Action</th>   
                              </tr>
                           </thead> 
                           <tbody>
                               @if(isset($exams))
                                @foreach ($exams as $key => $res)
                                 <tr role="row" class="odd">
                                       <td class="sorting_1">{{ $key + 1}} </td>
                                       <td> {{ $res['title'] }} </td>
                                       <td> <a target="_blank" href="{{ url('/').'/admin/questions/list?exams='.$res['id'] }}">{{ count($res['exam_questions']) }}</a> </td>
                                       <td class=" " >
                                            <?php if ($res['status'] == '1') { ?> 
                                                <a class="btn btn-success" href="{{ url('admin/exams/listactive/'.$res['id'].'/0' ) }}" onclick='return confirm("Are you sure you want to change the status of this exam to InActive?")'>Active</a>
                                            <?php } else { ?>
                                                <a class="btn btn-danger" href="{{ url('admin/exams/listactive/'.$res['id'].'/1' ) }}" onclick='return confirm("Are you sure you want to change the status of this exam to Active?")'>InActive</a> 
                                            <?php } ?>
                                        </td>
                                       <td class=" last">
                                          <a class="fa fa-pencil" href="{{ url('admin/exams/edit/' . $res['id'])}}"></a>&nbsp&nbsp
                                       </td>
                                 </tr>
                                @endforeach
                                @endif
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection