
@extends('layouts.app')

@section('content')
<div class="right_col" role="main" >
    <div class="row">
        <div class="col-md-12 col-sm-12 ">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Add Exam</h2>
                    <div class="clearfix"></div>
                </div>
                @include('layouts.flash')
                <div class="x_content">
                    <br>
                    <form id="exams-add"  class="form-horizontal form-label-left" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" >Title<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input placeholder="Enter Title" type="text" id="categoryname" name="title" required class="form-control" value="<?php echo isset($res) ? $res['title'] : ''; ?>">
                            </div>
                        </div>
                        
                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" >Description<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <textarea placeholder="Enter Description" name="description" required class="form-control" ><?php echo isset($res) ? $res['description'] : ''; ?></textarea>
                            </div>
                        </div>

                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" >Category<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <select name="category_id[]" class="form-control" required>
                                    
                                    @foreach($category as $key => $cat)
                                    <optgroup label="{{$cat['category_name']}}">
                                        @if(!empty($cat['child_category']))
                                            @foreach($cat['child_category'] as $child)
                                                <option value="{{$child['id']}}" {{ !empty($res['exam_categories']) ? (($res['exam_categories'][0]['category_id'] == $child['id']) ? 'selected' : ''): '' }}>{{$child['category_name']}}</option>
                                            @endforeach
                                        @endif
                                    </optgroup>
                                    @endforeach
                                    
                                </select>
                            </div>
                        </div>
                        
                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" >Select Type<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <select name="type" class="form-control">
                                    <option value="free" <?php echo (isset($res) && $res['type'] == 'free') ? 'selected="selected"'  : ''; ?>>Free</option>
                                    <option value="paid" <?php echo (isset($res) && $res['type'] == 'paid') ? 'selected="selected"'  : ''; ?>>Paid</option>
                                </select>
                            </div>
                        </div>
                        
<!--                        <div class="item form-group hide" id="examPrice">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" >Price<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input placeholder="Enter Price" type="number" name="price" required class="form-control" value="<?php // echo isset($res) ? $res['price'] : '0'; ?>">
                            </div>
                        </div>
                        
                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" >End Date<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input placeholder="Enter End Date" type="text" name="end_date" required class="form-control" value="<?php // echo isset($res) ? date('d/m/Y', strtotime($res['end_date'])) : ''; ?>">
                            </div>
                        </div>-->
                        
                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" >Total Marks<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input placeholder="Enter Total Marks" type="number" min="1" name="total_marks" required class="form-control" value="<?php echo isset($res) ? $res['total_marks'] : ''; ?>">
                            </div>
                        </div>
                        
                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" >Total Time<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input placeholder="Enter Total Time in Minutes" min="1" max="180" type="number" name="total_time" required class="form-control" value="<?php echo isset($res) ? $res['total_time'] : ''; ?>">
                            </div>
                        </div>


                        <div class="ln_solid"></div>
                        <div class="item form-group">
                            <div class="col-md-6 col-sm-6 offset-md-3">
                                <?php if (isset($res) && $res['id']) { ?>
                                    <input type="submit" class="btn btn-success" name="update" value="Update"/>
                                    <input type="hidden" name="update_id" value="<?php echo isset($res) ? $res['id'] : ''; ?>">
                                <?php } ?>
                                <?php if (!isset($res)) { ?>
                                    <input type="submit" class="btn btn-success"/>
                                <?php } ?>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!--<script>
    $('input[name="end_date"]').datetimepicker({
        format: "DD/MM/YYYY",
         minDate:new Date(),

    });
    $('input[name="end_date"]').val('<?php // echo (isset($res['end_date']))?date('d/m/Y', strtotime($res['end_date'])):"" ?>');
    
    $('select[name="type"]').on('change', function(){
        var selected = $(this).val();
        console.log(selected);
        $('input[name="price"]').val('0');
        if(selected == 'free'){
            $('#examPrice').addClass('hide');
        }else{
            $('#examPrice').removeClass('hide');
        }
    });
</script>-->

@endsection