
@extends('layouts.app')

@section('content')
<div class="right_col" role="main">
   <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
         <div class="x_panel">
            <div class="x_title">
               <h2>Contact Info</h2>
               <div class="clearfix"></div>
            </div>
            <div class="x_content">
               <div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                  <div class="row">
                     <div class="col-sm-12">
                        <table id="datatable" class="table table-striped table-bordered dataTable no-footer" style="width: 100%;" role="grid" aria-describedby="datatable_info">
                           <thead>
                              <tr role="row">
                                 <th class="sorting_asc" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 93px;">Id</th>
                                 <th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 153px;">Name</th>
                                 <th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 153px;">email</th>
                                 <th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 153px;">subject</th>
                                 <th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 153px;">message</th>
                                 <th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width: 67px;">status</th>
                                 <th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-label="Age: activate to sort column ascending" style="width: 26px;">Action</th>   
                              </tr>
                           </thead> 
                           <tbody>                             
                                @foreach ($cat as $key => $res)
                                 <tr role="row" class="odd">
                                    <td class="sorting_1">{{ $key + 1}} </td>
                                    <td class="sorting_1">{{ $res['name'] }} </td>
                                    <td class="sorting_1">{{ $res['email'] }} </td>
                                    <td class="sorting_1">{{ $res['subject'] }} </td>
                                    <td class="sorting_1">{{ $res['message'] }} </td>
                                    <td>   
                                          <a class="btn <?php echo (isset($res['status']) && !$res['status'])?'btn-danger':'btn-success' ?>"  href="{{ url('admin/contact/listactive/' . $res['id'].'/'.$res['status'])}}" onclick='return confirm("Are you sure you want to change the status?")'>
                                             <?php echo (isset($res['status']) && !$res['status'])?'InActive':'Active' ?>
                                          </a> 
                                       </td>
                                    <td class="last">
                                        <a class="fa fa-trash" onclick='return confirm("Are you sure you want to delete?")' href="{{ url('admin/contact/listdelete/' . $res['id'])}}"></a>
                                    </td>
                                 </tr>
                                @endforeach                              
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection