
@extends('layouts.app')

@section('content')
<div class="right_col" role="main" >
   <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
         <div class="x_panel">
            <div class="x_title">
               <h2>List News</h2>
               <div class="clearfix"></div>
            </div>
            <div class="x_content">
               <div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                  <div class="row">
                     <div class="col-sm-12">
                        <table id="datatable" class="table table-striped table-bordered dataTable no-footer" style="width: 100%;" role="grid" aria-describedby="datatable_info">
                           <thead>
                              <tr role="row">
                                 <th class="sorting_asc" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 93px;">Id</th>
                                 <th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 153px;">Image</th>
                                 <th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 153px;">Heading</th>
                                 <th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width: 67px;">Description</th>
                                 <th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width: 67px;">Status</th>

                                 <th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-label="Age: activate to sort column ascending" style="width: 26px;">Action</th>   
                              </tr>
                           </thead> 
                           <tbody>                             
                                @foreach ($res as $key => $res)
                                 <tr role="row" class="odd">
                                    <td class="sorting_1">{{ $key + 1}} </td>
                                    <td class="sorting_1">
                                        <img src="{{$res->image}}"/>
                                    </td>
                                    <td class="sorting_1">{{ $res->heading }} </td>
                                    <td class="sorting_1">{{ $res->description }} </td>
                                    <td>   
                                          <a class="btn <?php echo (isset($res->status) && !$res->status)?'btn-danger':'btn-success' ?>"  href="{{ url('admin/articles/newsActive/' . $res->id.'/'.$res->status)}}" onclick='return confirm("Are you sure you want to change the status of this news?")'>
                                             <?php echo (isset($res->status) && !$res->status)?'InActive':'Active' ?>
                                          </a> 
                                       </td>
                                    <td class="last">
                                        <a class="fa fa-pencil" href="{{ url('admin/articles/newsEdit/' . $res->id)}}"></a>&nbsp&nbsp

                                        <a class="fa fa-trash" onclick='return confirm("Are you sure you want to delete this News?")' href="{{ url('admin/articles/newsdelete/' . $res->id)}}"></a>
                                    </td>
                                 </tr>
                                @endforeach                              
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection