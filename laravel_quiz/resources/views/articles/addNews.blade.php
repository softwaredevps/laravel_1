
@extends('layouts.app')

@section('content')

<div class="right_col" role="main" style="min-height: 3788px;">
   <div class="row">
      <div class="col-md-12 col-sm-12 ">
         <div class="x_panel">
            <div class="x_title">
               <h2>Add News</h2>
               <div class="clearfix"></div>
            </div>
            @include('layouts.flash')
            <div class="x_content">
               <br>
               <form id="coupon-add"  class="form-horizontal form-label-left" method="post" action="/admin/articles/addNews" enctype="multipart/form-data">
               @csrf

                  <div class="item form-group col-md-6 col-sm-6">
                     <label class="col-form-label col-md-6 col-sm-6 label-align" for="copuon">Add Heading<span class="required">*</span>
                     </label>
                     <div class="col-md-12 col-sm-12 ">
                        <input placeholder="Enter Heading" type="text" id="heading" name="heading" required class="form-control" value="<?php echo isset($res->heading)?$res->heading:''; ?>">
                     </div>
                  </div>

                
                  <div class="item form-group col-md-6 col-sm-6">
                     <label class="col-form-label col-md-6 col-sm-6 label-align" for="start_date">Link<span class="required">*</span>
                     </label>
                     <div class="col-md-12 col-sm-12 ">
                        <input placeholder="Enter Link" type="text" id="link" name="link" class="form-control" value="<?php echo isset($res->link)?$res->link:''; ?>">
                     </div>
                  </div>

                  <div class="item form-group col-md-6 col-sm-6">
                     <label class="col-form-label col-md-6 col-sm-6 label-align" for="image">Image<span class="required">*</span>
                     </label>
                     <div class="col-md-12 col-sm-12 ">
                        <input placeholder="Image" type="file" id="image" name="image" class="form-control" value="<?php echo isset($res->image)?$res->image:''; ?>">
                     </div>
                  </div>


                  <div class="item form-group col-md-6 col-sm-6">
                     <label class="col-form-label col-md-6 col-sm-6 label-align" for="description">Description<span class="required">*</span>
                     </label>
                     <div class="col-md-12 col-sm-12 ">
                        <input placeholder="Description" type="text" id="description" name="description" class="form-control" value="<?php echo isset($res->description)?$res->description:''; ?>">
                     </div>
                  </div>


                  <div class="item form-group">
                     <div class="col-md-6 col-sm-6 offset-md-3">
                        <?php  if(isset($res->id)){ ?>
                              <input type="submit" class="btn btn-success" name="update" value="Update"/>
                              <input type="hidden" name="update_id" value="<?php echo isset($res->id)?$res->id:''; ?>">
                        <?php }else{ ?>
                              <input type="submit" class="btn btn-success"/>
                        <?php } ?>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>

@endsection