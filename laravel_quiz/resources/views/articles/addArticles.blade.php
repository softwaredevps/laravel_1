
@extends('layouts.app')

@section('content')
<div class="right_col" role="main">
   <div class="row">
      <div class="col-md-12 col-sm-12 ">
         <div class="x_panel">
            <div class="x_title">
               <h2>Add Latest Articles, Exam Updates</h2>
               <div class="clearfix"></div>
            </div>
            @include('layouts.flash')
            <div class="x_content">
               <br>
               <form id="Article-add"  class="form-horizontal form-label-left" method="post">
               @csrf
                  <div class="item form-group">
                     <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Article Name<span class="required">*</span>
                     </label>
                     <div class="col-md-6 col-sm-6 ">
                        <input placeholder="Enter Article Name" type="text" id="articlename" name="articlename" required class="form-control" value="<?php echo isset($res)?$res->article_name:''; ?>">
                     </div>
                  </div>
              
                  <div class="item form-group">
                     <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Article Desc<span class="required">*</span>
                     </label>
                     <div class="col-md-6 col-sm-6 ">
                        <textarea name="articledesc" required="required" class="form-control" value="<?php echo isset($res)?$res->article_desc:''; ?>"><?php echo isset($res)?$res->article_desc:''; ?></textarea>
                     </div>
                  </div>
                 
                  <div class="ln_solid"></div>
                  <div class="item form-group">
                     <div class="col-md-6 col-sm-6 offset-md-3">
                        <?php  if(isset($res) && $res->id){ ?>
                              <input type="submit" class="btn btn-success" name="update" value="Update"/>
                              <input type="hidden" name="update_id" value="<?php echo isset($res)?$res->id:''; ?>">
                        <?php } ?>
                        <?php  if(!isset($res)){ ?>
                              <input type="submit" class="btn btn-success"/>
                        <?php } ?>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>

@endsection