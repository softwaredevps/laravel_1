<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>ntatest admin </title>

        <link href="{{ asset('/css/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        
        <link href="{{ asset('/css/custom.min.css') }}" rel="stylesheet">
        <link href="{{ asset('/css/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('/css/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">

        <script src="{{ asset('/css/jquery/dist/jquery.min.js') }}"></script>
        <!-- Bootstrap -->
        <script src="{{ asset('/css/bootstrap/dist/js/bootstrap.min.js') }}"></script>
        <!-- bootstrap-daterangepicker -->
        <script src="{{ asset('/css/moment/min/moment.min.js') }}"></script>
        <script src="{{ asset('/css/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
            <script src="{{ asset('/css/datatables.net/js/jquery.dataTables.min.js') }}"></script>

        <!-- Custom Theme Scripts -->
        <script src="{{ asset('/js/custom.js') }}"></script>

    </head>
    @if(Auth::user())
    <body class=" nav-md">
        @else
    <body class="login">
        @endif

        <div class="container body">
            <div class="main_container">
                @if(Auth::user())
                    @include('layouts.leftbar')
                    @include('layouts.adminheader')
                @endif    

                @yield('content')
                
            </div>
        </div>
        <script>
            var dTable = $('#datatable').DataTable();
//            var currentpageDTable = "{{ isset($_GET['questionPage']) ? $_GET['questionPage'] : 1 }}";
//               dTable.page((parseInt(currentpageDTable)-1)).draw(false);

               $('#datatable_filter input').addClass('form-control');
               $('#datatable_length select').addClass('form-control');
          
            
        </script>
    </body>
</html>
