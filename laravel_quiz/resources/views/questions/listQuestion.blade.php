
@extends('layouts.app')

@section('content')
<div class="right_col" role="main">
   <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
         <div class="x_panel">
            <div class="x_title">
               <h2>List Questions</h2>
               <div class="clearfix"></div>
            </div>
             @include('layouts.flash')
            <div class="x_content">
               <div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                  <div class="row">
                     <div class="col-sm-12">
                        <table id="datatable" class="table table-striped table-bordered dataTable no-footer" style="width: 100%;" role="grid" aria-describedby="datatable_info">
                           <thead>
                              <tr role="row">
                                 <th class="sorting_asc" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 93px;">Id</th>
                                 <th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 153px;">Question Description</th>
                                 <th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width: 67px;">Status</th>
                                 <th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-label="Age: activate to sort column ascending" style="width: 26px;">Action</th>   
                              </tr>
                           </thead> 
                           <tbody><?php 
//                            $cat = isset($cat['get_questions']) ? $cat['get_questions'] : $cat;
//                           echo '<pre>'; print_r($cat); die; 
                           ?>
                                @foreach ($cat as $key => $res)
                                <?php 
                                    $res = isset($res['get_questions']) ? $res['get_questions'] : $res;
                                ?>
                                 <tr role="row" class="odd">
                                    <td class="sorting_1">{{ $key + 1}} </td>
                                    <td class="sorting_1">{{ $res['question'] }} </td>
                                       
                                    <td class=" " >
                                            <a class="btn <?php echo (isset($res['status']) && !$res['status'])?'btn-danger':'btn-success' ?>"  href="{{ url('admin/questions/listactive/' . $res['id'].'/'.($res['status']?'0':'1'))}}" onclick='return confirm("Are you sure you want to change the status of this article?")'>
                                             <?php echo (isset($res['status']) && !$res['status'])?'InActive':'Active' ?>
                                          </a>
                                    </td>
                            
                                    <td class="last">
                                        <a class="fa fa-pencil" href="{{ url('admin/questions/listedit/' . $res['id'])}}"></a>&nbsp&nbsp

                                    </td>
                                 </tr>
                                @endforeach                              
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection