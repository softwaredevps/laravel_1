@extends('layouts.app')

@section('content')

<div class="right_col" role="main" >
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Question Report<small></small></h2>

                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <br>
               <form class="form-horizontal form-label-left" role="form">
                    
                    <div class="form-group">
                        <label  class="control-label col-md-3 col-sm-3 col-xs-12">Question: <span class="required"> *</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <p><a href="{{url('/admin/questions/listedit/').'/'.$reports[0]['questions']['id']}}" target="_blank">{{$reports[0]['questions']['question']}}</a></p>
                            <!--<textarea class="form-control" placeholder="" rows="10" readonly="">{</textarea>-->
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="control-label col-md-3 col-sm-3 col-xs-12">Comment: <span class="required"> *</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <p>{{$reports[0]['comment']}}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">User Name: <span class="required"> *</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <a href="{{url('/admin/users/').'/'.$reports[0]['user_data']['id']}}" target='_blank' style="font-size:16px"> {{$reports[0]['user_data']['first_name'].' '.$reports[0]['user_data']['last_name']}}</a> 
                        </div>
                    </div>
                    

                {!! Form::close() !!} 
            </div>
        </div>
    </div>
</div>
</div>



@endsection