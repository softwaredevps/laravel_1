
@extends('layouts.app')

@section('content')
<div class="right_col" role="main" style="min-height: 1200px">
   <div class="row">
      <div class="col-md-12 col-sm-12 ">
         <div class="x_panel">
            <div class="x_title">
               <h2>Add Questions</h2>
               <div class="clearfix"></div>
            </div>
            @include('layouts.flash')
            <div class="x_content">
               <br>
               <form id="Question-add"  class="form-horizontal form-label-left" method="post" enctype="multipart/form-data">
               @csrf

                <div class="item form-group col-md-12 col-sm-12">
                    <label class="col-form-label col-md-6 col-sm-6 label-align" for="first-name"> Question Description<span class="required">*</span>
                     </label>
                    <textarea id="questionDesc" required="required" class="form-control" name="questionDesc" value="<?php echo isset($res['question'])?$res['question']:''; ?>"><?php echo isset($res['question'])?$res['question']:''; ?></textarea>
                </div>

                  <div class="item form-group col-md-6 col-sm-6">
                     <label class="col-form-label col-md-6 col-sm-6 label-align" for="optionFirst">Option 1<span class="required">*</span>
                     </label>
                     <div class="col-md-12 col-sm-12 ">
                        <input placeholder="Enter Option 1" type="text" id="optionFirst" name="optionFirst" required class="form-control" value="<?php echo isset($res['optionFirst'])?$res['optionFirst']:''; ?>">
                     </div>
                  </div>
              
                  <div class="item form-group col-md-6 col-sm-6">
                     <label class="col-form-label col-md-6 col-sm-6 label-align" for="optionSecond">Option 2<span class="required">*</span>
                     </label>
                     <div class="col-md-12 col-sm-12 ">
                        <input  placeholder="Enter Option 2" type="text" id="optionSecond" name="optionSecond" required="required" class="form-control" value="<?php echo isset($res['optionSecond'])?$res['optionSecond']:''; ?>">
                     </div>
                  </div>

                  <div class="item form-group col-md-6 col-sm-6">
                     <label class="col-form-label col-md-6 col-sm-6 label-align" for="optionThird">Option 3<span class="required">*</span>
                     </label>
                     <div class="col-md-12 col-sm-12 ">
                        <input  placeholder="Enter Option 3" type="text" id="optionThird" name="optionThird" required="required" class="form-control" value="<?php echo isset($res['optionThird'])?$res['optionThird']:''; ?>">
                     </div>
                  </div>

                  <div class="item form-group col-md-6 col-sm-6">
                     <label class="col-form-label col-md-6 col-sm-6 label-align" for="optionFourth">Option 4<span class="required">*</span>
                     </label>
                     <div class="col-md-12 col-sm-12 ">
                        <input placeholder="Enter Option 4" type="text" id="optionFourth" name="optionFourth" required="required" class="form-control" value="<?php echo isset($res['optionFourth'])?$res['optionFourth']:''; ?>">
                     </div>
                  </div>

                  <div class="item form-group col-md-12 col-sm-12">
                     <label class="col-form-label col-md-6 col-sm-6 label-align" for="first-name">Right Answer<span class="required">*</span>
                     </label>
                     <div class="col-md-12 col-sm-12 ">
                        <select id="rightAnswer" class="form-control" required name="rightAnswer">
                            <option value="">Choose..</option>
                            <option <?php echo (isset($res['correctAnswer']) && $res['correctAnswer'] == 'optionFirst')?'selected':''; ?> value="optionFirst">Option 1</option>
                            <option <?php echo  (isset($res['correctAnswer']) && $res['correctAnswer'] == 'optionSecond')?'selected':''; ?> value="optionSecond">Option 2</option>
                            <option <?php echo  (isset($res['correctAnswer']) && $res['correctAnswer'] == 'optionThird')?'selected':''; ?> value="optionThird">Option 3</option>
                            <option <?php echo  (isset($res['correctAnswer']) && $res['correctAnswer'] == 'optionFourth')?'selected':''; ?> value="optionFourth">Option 4</option>
                        </select>
                     </div>
                  </div>

                  <div class="item form-group col-md-12 col-sm-12">
                     <label class="col-form-label col-md-6 col-sm-6 label-align" for="first-name">Select Language<span class="required">*</span>
                     </label>
                     <div class="col-md-12 col-sm-12 ">
                        <select id="type" class="form-control" required name="type">
                            <option <?php echo (isset($res['type']) && $res['type'] == 1)?'selected':''; ?> value="1" selected>English</option>
                            <option <?php echo (isset($res['type']) && $res['type'] == 2)?'selected':''; ?> value="2">Hindi</option>
                        </select>
                     </div>
                  </div>
               
               <div class="item form-group col-md-12 col-sm-12">
                     <label class="col-form-label col-md-6 col-sm-6 label-align" for="first-name">Select Exams<span class="required">*</span>
                     </label>
                     <div class="col-md-12 col-sm-12 ">
                        <select id="type" class="form-control" required name="exams[]" multiple>
                            <?php // print_r($res);die; ?>
                            @foreach($allExams as $exam)
                                {{ $slectedArr = '' }}
                                @if(isset($res['question_exams']))
                                    @foreach($res['question_exams'] as $selectedCat)

                                        @if($selectedCat['exam_id'] == $exam->id)
                                            {{ $slectedArr = 'selected'}}
                                        @endif
                                    @endforeach
                                @endif
                                <option  value="{{$exam->id}}"  {{ $slectedArr ?? '' }}  >{{$exam->title}}</option>
                            @endforeach
                        </select>
                     </div>
                  </div> 

                  <div class="item form-group col-md-12 col-sm-12">
                     <label class="col-form-label col-md-6 col-sm-6 label-align" for="first-name">
                        Question Description
                     </label>
                     <div class="col-md-12 col-sm-12 ">
                        <textarea style="height:150px;" name="description" id="description" class="form-control" value="<?php echo isset($res['description'])?$res['description']:''; ?>"><?php echo isset($res['description'])?$res['description']:''; ?></textarea>

                     </div>
                  </div>
               
                    <div class="item form-group col-md-12 col-sm-12">
                        <label class="col-form-label col-md-6 col-sm-6 label-align" for="first-name">
                            Question Image
                        </label>
                        <div class="col-md-12 col-sm-12 ">
                            <input type="file" name="image"><br>
                            <?php if(isset($res['image']) && !empty($res['image'])){
                                echo "<div class='questionImg'>";
                                echo "<p><a href='javascript:;' class='delImage'>Delete Image</a></p>";
                                echo "<img src='".url('/public/uploads/question-images/'.$res['image'].'?time='.time())."' style='width:150px'>";
                                echo "</div>";
                            }  ?>
                        </div>
                    </div>
              
               <div class="item form-group" >
                     <div class="col-md-6 col-sm-6 offset-md-3" style="margin-top:20px">
                        <?php  if(isset($res['id'])){ ?>
                              <input type="submit" class="btn btn-success" name="update" value="Update"/>
                              <input type="hidden" name="update_id" value="<?php echo isset($res['id'])?$res['id']:''; ?>">
                        <?php }else{ ?>
                              <input type="submit" class="btn btn-success"/>
                        <?php } ?>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>


<script>
    $(document).on('click', '.delImage', function () {
        $('.delImage').text('Deleting image, please wait...');
        var questionID = '<?php echo (isset($res['id'])) ? $res['id'] : 0 ?>';
        
        
        if(questionID){
            $.post({
                url: '<?php echo url('/') ?>'+"/admin/questions/deleteImage",
                data:{"_token": "{{ csrf_token() }}", questionID: questionID},
                async: false,
                dataType: 'json',
                success: function(result){ 
                    
                    if(result.status == true){
                        $('.questionImg').hide();
                        alert('Image delete successfully.');
                    }else{
                        alert('Please try again!');
                    }
                },
                    error: function(err) {
                        console.log(err);
                    }
            });
        }else{
            return false;
        }

    });
</script>
@endsection