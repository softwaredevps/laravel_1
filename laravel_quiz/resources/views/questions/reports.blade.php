
@extends('layouts.app')

@section('content')
<div class="right_col" role="main">
   <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
         <div class="x_panel">
            <div class="x_title">
               <h2>List Questions</h2>
               <div class="clearfix"></div>
            </div>
             @include('layouts.flash')
            <div class="x_content">
               <div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                  <div class="row">
                     <div class="col-sm-12">
                        <table id="datatable" class="table table-striped table-bordered dataTable no-footer" style="width: 100%;" role="grid" aria-describedby="datatable_info">
                           <thead>
                              <tr role="row">
                                 <th class="sorting_asc" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 93px;">S.NO.</th>
                                 <th >Question</th>
                                 <th >User Name</th>
                                 <th >Comment</th>   
                                 <th >Action</th>   
                              </tr>
                           </thead> 
                           <tbody>
                                @foreach ($reports as $key => $res)
                                 <tr role="row" class="odd">
                                    <td class="sorting_1">{{ $key + 1}} </td>
                                    <td class="sorting_1">{{ substr($res['questions']['question'],0,100).'...' }} </td>
                                    <td class="sorting_1"><a target="_blank" href="{{url('/admin/users/'.$res['user_data']['id'])}}">{{ $res['user_data']['first_name'].' '.$res['user_data']['last_name'] }}</a></td>
                                    <td class="sorting_1">{{ substr($res['comment'],0,50).'...' }} </td>
                            
                                    <td class="last">
                                        <a class="fa fa-eye" href="{{ url('admin/questions/reports/' . $res['id'])}}"></a>&nbsp&nbsp
                                    </td>
                                 </tr>
                                @endforeach                              
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection