@extends('layouts.app')

@section('content')
<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Order Detail </h3>
              </div>

              
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2></h2>
                    
                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content">

                    <div class="col-md-12 col-sm-12 col-xs-12">

                      <ul class="stats-overview">
                        <li class="hidden-phone">
                          <span class="name"> Order Number</span>
                          <span class="value text-success"> {{$getOrders[0]['order_num'] }} </span>
                        </li>
                        <li class="hidden-phone">
                          <span class="name"> Order Status </span>
                          <span class="value text-success"> {{ ($getOrders[0]['status'] == '1') ? 'Processed' : (($getOrders[0]['status']=='2')?'Canceled':'Pending') }} </span>
                        </li>
                        <li>
                          <span class="name"> Transaction ID </span>
                          <span class="value text-success"> {{$getOrders[0]['transaction_id'] }} </span>
                        </li>
                        
                      </ul>
                        <ul class="stats-overview">
                        
                        <li>
                          <span class="name"> Order Date </span>
                          <span class="value text-success"> {{$getOrders[0]['updated_at']}} </span>
                        </li>
                      
                        <li>
                          <span class="name"> Payment Mode </span>
                          <span class="value text-success"> {{$getOrders[0]['payment_mode']}} </span>
                        </li>
                        
                        <li>
                          <span class="name"> Payment Currency </span>
                          <span class="value text-success"> {{$getOrders[0]['currency']}} </span>
                        </li>
                        <li>
                          <span class="name"> Payment Gateway </span>
                          <span class="value text-success"> {{$getOrders[0]['payment_gateway']}} </span>
                        </li>
                        </ul>
                        <ul class="stats-overview">
                        
                        
                        <li>
                          <span class="name"> Bank Transaction ID Mode </span>
                          <span class="value text-success"> {{$getOrders[0]['bank_transaction_id']}} </span>
                        </li>
                        
                        <li>
                          <span class="name"> Bank Name</span>
                          <span class="value text-success"> {{$getOrders[0]['bank_name']}} </span>
                        </li>
                        
                        <li>
                          <span class="name"> Order Amount </span>
                          <span class="value text-success"> &#8377; {{ $getOrders[0]['order_amount'] }}  </span>
                        </li>
                        <li>
                          <span class="name"> Order Actual Amount </span>
                          <span class="value text-success"> &#8377; {{ $getOrders[0]['order_actual_amount']}} </span>
                        </li>
                        
                      </ul>
                        
                      <br />

                      <h4><strong>Exam Detail</strong></h4><hr>
                        <ul class="messages">
                            @foreach($getOrders[0]['order_detail'] as $exam )
                          <li>
                            <img src="{{url('/public').'/'.$exam['get_category']['image']}}" class="avatar" alt="Avatar">
                            
                            <div class="message_wrapper">
                              <h4 class="heading"> {{ $exam['get_category']['category_name'] }} </h4>
                              <br />
                              <p class="url">
                                Exam Amount: &#8377;{{ $exam['get_category']['price'] }}
                              </p>
                            </div>
                          </li>
                            @endforeach
                        </ul>

                      </div>


                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->


@endsection