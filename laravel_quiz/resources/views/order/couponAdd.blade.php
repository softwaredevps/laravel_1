
@extends('layouts.app')

@section('content')

<div class="right_col" role="main" style="min-height: 3788px;">
   <div class="row">
      <div class="col-md-12 col-sm-12 ">
         <div class="x_panel">
            <div class="x_title">
               <h2>Add Coupon</h2>
               <div class="clearfix"></div>
            </div>
            @include('layouts.flash')
            <div class="x_content">
               <br>
               <form id="coupon-add"  class="form-horizontal form-label-left" method="post">
               @csrf

                  <div class="item form-group col-md-6 col-sm-6">
                     <label class="col-form-label col-md-6 col-sm-6 label-align" for="copuon">Add Coupon<span class="required">*</span>
                     </label>
                     <div class="col-md-12 col-sm-12 ">
                        <input placeholder="Enter Copuon" type="text" id="copuon" name="copuon" required class="form-control" value="<?php echo isset($res->coupon)?$res->coupon:''; ?>">
                     </div>
                  </div>

                  <div class="item form-group col-md-6 col-sm-6">
                     <label class="col-form-label col-md-6 col-sm-6 label-align" for="users_list">Select Users<span class="required">*</span>
                     </label>
                     <div class="col-md-12 col-sm-12 ">
                        <select class="form-control"  name="users_list" >
                            <option value="">Select Users</option>
                            <?php 
                            foreach(usersList() as $key=>$val) {?>
                                <option 
                                    value="<?php echo $val->id; ?>" 
                                    <?php echo (@$res->users == @$val->id)?'selected':''; ?>
                                >
                                    <?php echo $val->first_name.' '.$val->first_name.' ('. $val->email  .') '?>
                                </option>
                            <?php }?>
                        </select>
                     </div>
                  </div>
                
                
                  <div class="item form-group col-md-6 col-sm-6">
                     <label class="col-form-label col-md-6 col-sm-6 label-align" for="start_date">Coupon State Date<span class="required">*</span>
                     </label>
                     <div class="col-md-12 col-sm-12 ">
                        <input placeholder="Copuon Start Date" type="date" id="start_date" name="coupon_state_date" required class="form-control" value="<?php echo isset($res->start_date)?$res->start_date:''; ?>">
                     </div>
                  </div>

                  <div class="item form-group col-md-6 col-sm-6">
                     <label class="col-form-label col-md-6 col-sm-6 label-align" for="end_date">Coupon End Date<span class="required">*</span>
                     </label>
                     <div class="col-md-12 col-sm-12 ">
                        <input placeholder="Copuon End Date" type="date" id="end_date" name="coupon_end_date" required class="form-control" value="<?php echo isset($res->end_date)?$res->end_date:''; ?>">
                     </div>
                  </div>
              

                  <div class="item form-group col-md-12 col-sm-12">
                     <label class="col-form-label col-md-6 col-sm-6 label-align" for="select_category">Select Category
                     </label>
                     <div class="col-md-12 col-sm-12 ">
                        <select class="form-control"  name="select_category" >
                            <option value="">Select Category</option>
                            <?php 
                            foreach(getCategory() as $key=>$val) {?>
                                <option value="<?php echo $val->id; ?>" <?php echo (@$res->category == @$val->id)?'selected':''; ?>><?php echo $val->category_name; ?></option>
                            <?php }?>
                        </select>
                     </div>
                  </div>


                  <div class="item form-group">
                     <div class="col-md-6 col-sm-6 offset-md-3">
                        <?php  if(isset($res->id)){ ?>
                              <input type="submit" class="btn btn-success" name="update" value="Update"/>
                              <input type="hidden" name="update_id" value="<?php echo isset($res->id)?$res->id:''; ?>">
                        <?php }else{ ?>
                              <input type="submit" class="btn btn-success"/>
                        <?php } ?>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>

@endsection