@extends('layouts.app')

@section('content')
<div class="right_col" role="main" >
<div class="row">

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
		
		
            <div class="x_title">
                <h2>List Orders</h2>
                <div class="clearfix"></div>
            </div>
			
            @include('layouts.flash')
            <div id='customMsg'></div>
            <div class="x_content">
                <table id="datatable" class="table table-striped responsive-utilities ">
                    <thead>
                        <tr class="headings">
                            <th>S.No.</th>
                            <th>Order Number</th>
                            <th>Transaction ID</th>
                            <th>Order Amount</th>
                            <th>Order Status</th>
                            <th>Order Date</th>
                            <th>Order User</th>
                            <th class=" no-link last"><span class="nobr">Action</span>
                            </th>
                        </tr>
                    </thead>

                    <tbody>
                        @php 
                        $a=1 
                        @endphp
                        @foreach ($getOrders as $order)                                                  

                        <tr class=" pointer">
                            <td class="a-center ">
                               {{$a}}
                            </td>
                            <td class=" ">{{ $order['order_num']}}</td>
                            <td class=" ">{{ $order['transaction_id']}}</td>
                            <td class=" ">{{ $order['order_amount']}}</td>
                            <td class=" ">{{ ($order['status']=='1')?'Processed':(($order['status']=='2')?'Canceled':'Pending')  }}</td>
                            
                            <td class=" ">{{ date_create($order['updated_at']) -> format('M j, Y')}}</td>
                            <td class=" "><a href="{{url('/admin/users/'.$order['user_data']['id'])}}" target="_blank">{{ ucwords($order['user_data']['first_name'].' '.$order['user_data']['last_name']) }}</a></td>
                            <td class=" last">
                                <a href="{{url('/admin/orders/detail', $order['id'])}}" class="fa fa-eye" data-toggle="tooltip" title="View Order"></a> 
                            </td>
                        </tr>
                       @php  
                        $a++ 
                        @endphp
                        @endforeach                  
                    </tbody>

                </table>
				
				
				
            </div>
			
			
			
        </div>
    </div>

    <br />
    <br />
    <br />

</div>

</div>


@endsection