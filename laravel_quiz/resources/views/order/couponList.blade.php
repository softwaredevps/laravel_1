@extends('layouts.app')

@section('content')
<div class="right_col" role="main" >
<div class="row">

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
		
		
            <div class="x_title">
                <h2>Coupon List</h2>
                <div class="clearfix"></div>
            </div>
			
            @include('layouts.flash')
            <div id='customMsg'></div>
            <div class="x_content">
                <table id="datatable" class="table table-striped responsive-utilities ">
                    <thead>
                        <tr class="headings">
                            <th>S.No.</th>
                            <th>Coupon</th>
                            <th>User</th>
                            <th>Exam Category</th>
                            <th class=" no-link last">
                                <span class="nobr">Status</span>
                            </th>
                            <th>Action</th>

                        </tr>
                    </thead>

                    <tbody>
                        @php 
                        $a=1 
                        @endphp
                        @foreach (getCoupon() as $data)                                                  

                        <tr class=" pointer">
                            <td class="a-center ">
                               {{$a}}
                            </td>
                            <td class=" ">{{ $data->coupon}}</td>
                            <td class=" ">
                                <?php $name =  getUserNameById($data->users); 
                                    echo $name->first_name . ' '.$name->last_name;
                                ?>
                            </td>
                            <td class=" ">
                                <?php $name =  getCategoryNameById($data->category); 
                                    echo $name->category_name;
                                ?>
                            </td>                            
                            <td class=" " >
                                <a class="btn <?php echo (isset($data->status) && !$data->status)?'btn-danger':'btn-success' ?>"  href="{{ url('admin/orders/couponActive/' . $data->id.'/'.($data->status?'1':'0'))}}" onclick='return confirm("Are you sure you want to change the status of this Coupon?")'>
                                    <?php echo (isset($data->status) && !$data->status)?'InActive':'Active' ?>
                                </a>
                            </td>
                    
                            <td class="last">
                                <a class="fa fa-pencil" href="{{ url('admin/orders/CouponEdit/' . $data->id)}}"></a>&nbsp&nbsp
                            </td>
                        </tr>
                       @php  
                        $a++ 
                        @endphp
                        @endforeach                  
                    </tbody>

                </table>
				
				
				
            </div>
			
			
			
        </div>
    </div>

    <br />
    <br />
    <br />

</div>

</div>


@endsection