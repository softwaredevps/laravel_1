@extends('layouts.app')

@section('content')
<div class="right_col" role="main" >
<div class="row">

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
		
		
            <div class="x_title">
                <h2>All Users </h2>
                <div class="clearfix"></div>
            </div>
			
            @include('layouts.flash')
            <div id='customMsg'></div>
            <div class="x_content">
                <table id="datatable" class="table table-striped responsive-utilities ">
                    <thead>
                        <tr class="headings">
                            <th>
                               S.No.
                            </th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th>Gender</th>
                            <th>Role</th>
                            <th>Status</th>  
                            <th>Created</th>
                            <th class=" no-link last"><span class="nobr">Action</span>
                            </th>
                        </tr>
                    </thead>

                    <tbody>
                        @php 
                        $a=1 
                        @endphp
                        @foreach ($users as $user)                                                  

                        <tr class=" pointer">
                            <td class="a-center ">
                               {{$a}}
                            </td>
                            <td class=" ">{{ $user['first_name']}}</td>
                            <td class=" ">{{ $user['last_name']}}</td>
                            <td class=" ">{{ $user['email']}}</td>
                            <td class=" ">{{ $user['gender']}}</td>
                            <td class=" "><?php echo ($user['userRole'] == '2')?'User':'Guest'; ?>
                            </td>
                            <td class=" " >
                                <?php if ($user['status'] == '1') { ?> 
                                    <a class="btn btn-success" href="{{ url('admin/users/changeStatus/'.$user['id'].'/0' ) }}" onclick='return confirm("Are you sure you want to change the status of this user to InActive?")'>Active</a>
                                <?php }elseif ($user['status'] == '2') { ?> 
                                    <a class="btn btn-danger" href="{{ url('admin/users/changeStatus/'.$user['id'].'/1' ) }}" onclick='return confirm("Are you sure you want to change the status of this user to Active?")'>Deleted</a> 
                                <?php }elseif ($user['status'] == '3') { ?> 
                                    <a class="btn btn-danger" href="{{ url('admin/users/changeStatus/'.$user['id'].'/1' ) }}" onclick='return confirm("Are you sure you want to change the status of this user to Active?")'>Spam</a> 
                                <?php } else { ?>
                                    <a class="btn btn-danger" href="{{ url('admin/users/changeStatus/'.$user['id'].'/1' ) }}" onclick='return confirm("Are you sure you want to change the status of this user to Active?")'>InActive</a> 
                                <?php } ?></i>
                            </td>
                            <td class=" ">{{ date_create($user['created_at']) -> format('M j, Y')}}</td>


                            <td class=" last">
                                <a href="{{url('/admin/users', $user['id'])}}" class="fa fa-pencil " data-toggle="tooltip" title="Edit"></a> 
                                <span>|</span>
                                <a href="{{ url('admin/users/delete', $user['id']) }}" onclick='return confirm("Are you sure you want to delete this user?")' class="fa fa-trash" data-toggle="tooltip" title="Delete"></a>
                            </td>
                        </tr>
                       @php  
                        $a++ 
                        @endphp
                        @endforeach                  
                    </tbody>

                </table>
				
				
				
            </div>
			
			
			
        </div>
    </div>

    <br />
    <br />
    <br />

</div>

</div>


@endsection