@extends('layouts.app')

@section('content')

<div class="right_col" role="main" >
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Update User<small></small></h2>

                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <br>
                    
                    {{ Form::model($user, ['class'=>"form-horizontal form-label-left", 'role'=>"form"], array('route' => array('users.update', $user->id), 'files' => true, 'method' => 'PUT')) }}
                    <input name="_method" type="hidden" value="PUT">
                    <div class="form-group">
                        <label for="first-name" class="control-label col-md-3 col-sm-3 col-xs-12">First Name <span class="required"> *</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            {!! Form::text('first_name', null, array('class'=>'form-control', 'placeholder'=>'', 'required'=>true))  !!}
                                @if ($errors->has('first_name'))
                                        <span class="help-block errors">
                                            <p>{{ $errors->first('first_name') }}</p>
                                        </span>
                                @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="last-name" class="control-label col-md-3 col-sm-3 col-xs-12">Last Name <span class="required"> *</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            {!! Form::text('last_name', null, array('class'=>'form-control', 'placeholder'=>'', 'required'=>true))  !!}
                                @if ($errors->has('last_name'))
                                        <span class="help-block errors">
                                            <p>{{ $errors->first('last_name') }}</p>
                                        </span>
                                @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="control-label col-md-3 col-sm-3 col-xs-12">Email <span class="required"> *</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            {!! Form::email('email', null, array('class'=>'form-control', 'placeholder'=>'', 'required'=>true, 'readonly'=>true))  !!}
                                @if ($errors->has('email'))
                                        <span class="help-block errors">
                                            <p>{{ $errors->first('email') }}</p>
                                        </span>
                                @endif
                        </div>
                    </div>
                    
                    
                    <div class="form-group">
                        <label for="gender" class="control-label col-md-3 col-sm-3 col-xs-12">Gender <span class="required"> *</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            {!! Form::select('gender', array('M' => 'Male', 'F' => 'Female', 'O'=> 'Other'),null,array('class'=>'form-control', 'placeholder'=>'', 'required'=>true));  !!}
                                @if ($errors->has('gender'))
                                        <span class="help-block errors">
                                            <p>{{ $errors->first('gender') }}</p>
                                        </span>
                                @endif
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="status" class="control-label col-md-3 col-sm-3 col-xs-12">Status <span class="required"> *</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            {!! Form::select('status', array('0' => 'InActive', '1' => 'Active', '2' => 'Blocked', '3'=> 'Spam'),null,array('class'=>'form-control', 'placeholder'=>'', 'required'=>true));  !!}
                                @if ($errors->has('status'))
                                        <span class="help-block errors">
                                            <p>{{ $errors->first('status') }}</p>
                                        </span>
                                @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Mobile No.<span class="required"> *</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            {!! Form::number('mobile', null, array('class'=>'form-control', 'placeholder'=>'','minLength'=>10, 'maxLength'=>11))  !!}
                                @if ($errors->has('mobile'))
                                        <span class="help-block errors">
                                            <p>{{ $errors->first('mobile') }}</p>
                                        </span>
                                @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >Password<span class="required"> *</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <a href="javascript:;" data-toggle="modal" data-target="#myModal">Change Password</a>									
                        </div>
                    </div>
                    
                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">

                           {!! Form::submit('Update', array('class'=>'btn btn-success') ); !!}
                        </div>
                    </div>


                {!! Form::close() !!} 
            </div>
        </div>
    </div>
</div>
</div>

<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Update Password</h4>
            </div>
            <form method="post" class="form-horizontal form-label-left">
                <div class="modal-body">
                    <input type="hidden" value="{{$user->userID}}" id="userId">
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >Password<span class="required"> *</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">

                            <input type="password" name="password" class="form-control col-md-7 col-xs-12" >
                            <div class="clearfix"></div>

                            <span class="help-block   errors">
                                <p class="passErr"></p>
                            </span>

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Confirm Password<span class="required"> *</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="password"  class="form-control col-md-7 col-xs-12" name="confirm_password">
                            <div class="clearfix"></div>

                            <span class="help-block   errors">
                                <p class="confPassErr"></p>
                            </span>

                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-success savePass" >Save</button>
                </div>
        </div>
        </form>

    </div>
</div>
<script>
    $(document).on('click', '.savePass', function () {
        $(".passErr").html('');
        $(".confPassErr").html('');
        var pass = $("input[name='password']").val();
        var confPass = $("input[name='confirm_password']").val();
        var userID = {{$user->id}};
        var err = 0;
        if (pass == '') {
            $(".passErr").html('This field is required');
            err = 1;
        }
        if (confPass == '') {
            $(".confPassErr").html('This field is required');
            err = 1;
        }

        if ((pass != confPass)) {
            $(".confPassErr").html('Password not matched');
            err = 1;
        }
        
        if(err == 0){
            $.post({
                url: '<?php echo url('/') ?>'+"/admin/users/changePassword",
                data:{"_token": "{{ csrf_token() }}", pass: pass, userID: userID},
                async: false,
                dataType: 'json',
                success: function(result){ 
                    
                if(result.status == true){
                    alert('Password changed successfully.');
                    $('#myModal').modal('hide');
                }else{
                    alert('Please try again!');
                }
            },
                    error: function(err) {
                        console.log(err);
                    }
        });
        }else{
            return false;
        }

    });
</script>

@endsection