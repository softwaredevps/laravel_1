@extends('layouts.app')

@section('content')

<div class="right_col" role="main" >
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Add User<small></small></h2>

                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <br>
               <form class="form-horizontal form-label-left" role="form" method="POST" action="{{ url('/admin/users') }}">
                    {{ csrf_field() }}
                    
                    <div class="form-group">
                        <label for="first-name" class="control-label col-md-3 col-sm-3 col-xs-12">First Name <span class="required"> *</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            {!! Form::text('first_name', null, array('class'=>'form-control', 'placeholder'=>'', 'required'=>true))  !!}
                                @if ($errors->has('first_name'))
                                        <span class="help-block errors">
                                            <p>{{ $errors->first('first_name') }}</p>
                                        </span>
                                @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="last-name" class="control-label col-md-3 col-sm-3 col-xs-12">Last Name <span class="required"> *</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            {!! Form::text('last_name', null, array('class'=>'form-control', 'placeholder'=>'', 'required'=>true))  !!}
                                @if ($errors->has('last_name'))
                                        <span class="help-block errors">
                                            <p>{{ $errors->first('last_name') }}</p>
                                        </span>
                                @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="control-label col-md-3 col-sm-3 col-xs-12">Email <span class="required"> *</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            {!! Form::email('email', null, array('class'=>'form-control', 'placeholder'=>'', 'required'=>true))  !!}
                                @if ($errors->has('email'))
                                        <span class="help-block errors">
                                            <p>{{ $errors->first('email') }}</p>
                                        </span>
                                @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password" class="control-label col-md-3 col-sm-3 col-xs-12">Password <span class="required"> *</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            {!! Form::password('password',  array('class'=>'form-control', 'placeholder'=>'', 'required'=>true)) !!}
                                @if ($errors->has('password'))
                                        <span class="help-block errors">
                                            <p>{{ $errors->first('password') }}</p>
                                        </span>
                                @endif
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="confirm_password" class="control-label col-md-3 col-sm-3 col-xs-12">Confirm Password <span class="required"> *</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            {!! Form::password('confirm_password', array('class'=>'form-control', 'placeholder'=>'', 'required'=>true))  !!}
                                @if ($errors->has('confirm_password'))
                                        <span class="help-block errors">
                                            <p>{{ $errors->first('confirm_password') }}</p>
                                        </span>
                                @endif
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="gender" class="control-label col-md-3 col-sm-3 col-xs-12">Gender <span class="required"> *</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            {!! Form::select('gender', array('M' => 'Male', 'F' => 'Female', 'O'=> 'Other'),null,array('class'=>'form-control', 'placeholder'=>'', 'required'=>true));  !!}
                                @if ($errors->has('gender'))
                                        <span class="help-block errors">
                                            <p>{{ $errors->first('gender') }}</p>
                                        </span>
                                @endif
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="status" class="control-label col-md-3 col-sm-3 col-xs-12">Status <span class="required"> *</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            {!! Form::select('status', array('0' => 'InActive', '1' => 'Active', '2' => 'Blocked', '3'=> 'Spam'),null,array('class'=>'form-control', 'placeholder'=>'', 'required'=>true));  !!}
                                @if ($errors->has('status'))
                                        <span class="help-block errors">
                                            <p>{{ $errors->first('status') }}</p>
                                        </span>
                                @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Mobile No.<span class="required"> *</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            {!! Form::number('mobile', null, array('class'=>'form-control', 'placeholder'=>'','minLength'=>10, 'maxLength'=>11))  !!}
                                @if ($errors->has('mobile'))
                                        <span class="help-block errors">
                                            <p>{{ $errors->first('mobile') }}</p>
                                        </span>
                                @endif
                        </div>
                    </div>


                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">

                           {!! Form::submit('Add', array('class'=>'btn btn-success') ); !!}
                        </div>
                    </div>


                {!! Form::close() !!} 
            </div>
        </div>
    </div>
</div>
</div>



@endsection